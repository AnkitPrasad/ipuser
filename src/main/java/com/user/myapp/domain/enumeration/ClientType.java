package com.user.myapp.domain.enumeration;

/**
 * The ClientType enumeration.
 */
public enum ClientType {
    Hiring,
    Talent,
    All,
}
