package com.user.myapp.domain.enumeration;

/**
 * The Partnertype enumeration.
 */
public enum Partnertype {
    ActivePartner,
    SecretPartner,
    MinorPartner,
    NominalPartner,
}
