package com.user.myapp.domain.enumeration;

/**
 * The Permission enumeration.
 */
public enum Permission {
    SuperAdmin,
    SubAdmin,
    Client,
    SubClient,
    Candidate,
    Partner,
}
