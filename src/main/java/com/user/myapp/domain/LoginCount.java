package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * A LoginCount.
 */
@Entity
@Table(name = "login_count")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class LoginCount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "logintime")
    private Instant logintime;

    @Column(name = "login_id")
    private Long loginId;

    @ManyToOne
    @JsonIgnoreProperties(value = { "userEmails", "loginCounts", "userRolePermissions", "roleMasters" }, allowSetters = true)
    private InPreferenceUser inPreferenceUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public LoginCount id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getLogintime() {
        return this.logintime;
    }

    public LoginCount logintime(Instant logintime) {
        this.setLogintime(logintime);
        return this;
    }

    public void setLogintime(Instant logintime) {
        this.logintime = logintime;
    }

    public Long getLoginId() {
        return this.loginId;
    }

    public LoginCount loginId(Long loginId) {
        this.setLoginId(loginId);
        return this;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public InPreferenceUser getInPreferenceUser() {
        return this.inPreferenceUser;
    }

    public void setInPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.inPreferenceUser = inPreferenceUser;
    }

    public LoginCount inPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.setInPreferenceUser(inPreferenceUser);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoginCount)) {
            return false;
        }
        return id != null && id.equals(((LoginCount) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LoginCount{" +
            "id=" + getId() +
            ", logintime='" + getLogintime() + "'" +
            ", loginId=" + getLoginId() +
            "}";
    }
}
