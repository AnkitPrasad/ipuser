package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Type;

/**
 * A UserPermissionOnAttribute.
 */
@Entity
@Table(name = "user_permission_on_attribute")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserPermissionOnAttribute implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "attribute_value")
    private String attributeValue;

    @ManyToOne
    @JsonIgnoreProperties(value = { "userPermissionOnAttributes", "userRolePermissions", "inPreferenceUser" }, allowSetters = true)
    private RoleMaster roleMaster;

    @ManyToOne
    @JsonIgnoreProperties(value = { "userPermissionOnAttributes" }, allowSetters = true)
    private PermissionsOnAttributeMaster permissionsOnAttributeMaster;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public UserPermissionOnAttribute id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttributeValue() {
        return this.attributeValue;
    }

    public UserPermissionOnAttribute attributeValue(String attributeValue) {
        this.setAttributeValue(attributeValue);
        return this;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public RoleMaster getRoleMaster() {
        return this.roleMaster;
    }

    public void setRoleMaster(RoleMaster roleMaster) {
        this.roleMaster = roleMaster;
    }

    public UserPermissionOnAttribute roleMaster(RoleMaster roleMaster) {
        this.setRoleMaster(roleMaster);
        return this;
    }

    public PermissionsOnAttributeMaster getPermissionsOnAttributeMaster() {
        return this.permissionsOnAttributeMaster;
    }

    public void setPermissionsOnAttributeMaster(PermissionsOnAttributeMaster permissionsOnAttributeMaster) {
        this.permissionsOnAttributeMaster = permissionsOnAttributeMaster;
    }

    public UserPermissionOnAttribute permissionsOnAttributeMaster(PermissionsOnAttributeMaster permissionsOnAttributeMaster) {
        this.setPermissionsOnAttributeMaster(permissionsOnAttributeMaster);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserPermissionOnAttribute)) {
            return false;
        }
        return id != null && id.equals(((UserPermissionOnAttribute) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserPermissionOnAttribute{" +
            "id=" + getId() +
            ", attributeValue='" + getAttributeValue() + "'" +
            "}";
    }
}
