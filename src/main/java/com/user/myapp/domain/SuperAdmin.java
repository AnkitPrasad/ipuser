package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A SuperAdmin.
 */
@Entity
@Table(name = "super_admin")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class SuperAdmin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @JsonIgnoreProperties(value = { "userEmails", "loginCounts", "userRolePermissions", "roleMasters" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private InPreferenceUser inPreferenceUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public SuperAdmin id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public SuperAdmin name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public SuperAdmin email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public InPreferenceUser getInPreferenceUser() {
        return this.inPreferenceUser;
    }

    public void setInPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.inPreferenceUser = inPreferenceUser;
    }

    public SuperAdmin inPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.setInPreferenceUser(inPreferenceUser);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SuperAdmin)) {
            return false;
        }
        return id != null && id.equals(((SuperAdmin) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SuperAdmin{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", email='" + getEmail() + "'" +
            "}";
    }
}
