package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.user.myapp.domain.enumeration.ClientType;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "legal_entity")
    private String legalEntity;

    @Column(name = "industry")
    private String industry;

    @Column(name = "bank_details")
    private String bankDetails;

    @Enumerated(EnumType.STRING)
    @Column(name = "client_type")
    private ClientType clientType;

    @Column(name = "tna_no")
    private String tnaNo;

    @Column(name = "gst_no")
    private String gstNo;

    @Column(name = "pan")
    private String pan;

    @Column(name = "upload_client_logo")
    private String uploadClientLogo;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "status")
    private String status;

    @OneToOne
    @JoinColumn(unique = true)
    private ClientSetting clientSetting;

    @OneToMany(mappedBy = "client")
    @JsonIgnoreProperties(value = { "candidateDetails", "candidateProfiles", "client" }, allowSetters = true)
    private Set<Candidate> candidates = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Client id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLegalEntity() {
        return this.legalEntity;
    }

    public Client legalEntity(String legalEntity) {
        this.setLegalEntity(legalEntity);
        return this;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getIndustry() {
        return this.industry;
    }

    public Client industry(String industry) {
        this.setIndustry(industry);
        return this;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getBankDetails() {
        return this.bankDetails;
    }

    public Client bankDetails(String bankDetails) {
        this.setBankDetails(bankDetails);
        return this;
    }

    public void setBankDetails(String bankDetails) {
        this.bankDetails = bankDetails;
    }

    public ClientType getClientType() {
        return this.clientType;
    }

    public Client clientType(ClientType clientType) {
        this.setClientType(clientType);
        return this;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }

    public String getTnaNo() {
        return this.tnaNo;
    }

    public Client tnaNo(String tnaNo) {
        this.setTnaNo(tnaNo);
        return this;
    }

    public void setTnaNo(String tnaNo) {
        this.tnaNo = tnaNo;
    }

    public String getGstNo() {
        return this.gstNo;
    }

    public Client gstNo(String gstNo) {
        this.setGstNo(gstNo);
        return this;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getPan() {
        return this.pan;
    }

    public Client pan(String pan) {
        this.setPan(pan);
        return this;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getUploadClientLogo() {
        return this.uploadClientLogo;
    }

    public Client uploadClientLogo(String uploadClientLogo) {
        this.setUploadClientLogo(uploadClientLogo);
        return this;
    }

    public void setUploadClientLogo(String uploadClientLogo) {
        this.uploadClientLogo = uploadClientLogo;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Client createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public Client createdBy(Long createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public Client updatedDate(Instant updatedDate) {
        this.setUpdatedDate(updatedDate);
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUpdatedBy() {
        return this.updatedBy;
    }

    public Client updatedBy(Long updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getStatus() {
        return this.status;
    }

    public Client status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ClientSetting getClientSetting() {
        return this.clientSetting;
    }

    public void setClientSetting(ClientSetting clientSetting) {
        this.clientSetting = clientSetting;
    }

    public Client clientSetting(ClientSetting clientSetting) {
        this.setClientSetting(clientSetting);
        return this;
    }

    public Set<Candidate> getCandidates() {
        return this.candidates;
    }

    public void setCandidates(Set<Candidate> candidates) {
        if (this.candidates != null) {
            this.candidates.forEach(i -> i.setClient(null));
        }
        if (candidates != null) {
            candidates.forEach(i -> i.setClient(this));
        }
        this.candidates = candidates;
    }

    public Client candidates(Set<Candidate> candidates) {
        this.setCandidates(candidates);
        return this;
    }

    public Client addCandidate(Candidate candidate) {
        this.candidates.add(candidate);
        candidate.setClient(this);
        return this;
    }

    public Client removeCandidate(Candidate candidate) {
        this.candidates.remove(candidate);
        candidate.setClient(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Client{" +
            "id=" + getId() +
            ", legalEntity='" + getLegalEntity() + "'" +
            ", industry='" + getIndustry() + "'" +
            ", bankDetails='" + getBankDetails() + "'" +
            ", clientType='" + getClientType() + "'" +
            ", tnaNo='" + getTnaNo() + "'" +
            ", gstNo='" + getGstNo() + "'" +
            ", pan='" + getPan() + "'" +
            ", uploadClientLogo='" + getUploadClientLogo() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy=" + getUpdatedBy() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
