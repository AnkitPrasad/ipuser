package com.user.myapp.domain;

import com.user.myapp.domain.enumeration.Partnertype;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * A Partner.
 */
@Entity
@Table(name = "partner")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Partner implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "domain")
    private String domain;

    @Column(name = "partner_commission")
    private Double partnerCommission;

    @Enumerated(EnumType.STRING)
    @Column(name = "partner_type")
    private Partnertype partnerType;

    @Column(name = "value_m")
    private Integer valueM;

    @Column(name = "value_c")
    private Integer valueC;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "status")
    private Boolean status;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Partner id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Partner name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return this.domain;
    }

    public Partner domain(String domain) {
        this.setDomain(domain);
        return this;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Double getPartnerCommission() {
        return this.partnerCommission;
    }

    public Partner partnerCommission(Double partnerCommission) {
        this.setPartnerCommission(partnerCommission);
        return this;
    }

    public void setPartnerCommission(Double partnerCommission) {
        this.partnerCommission = partnerCommission;
    }

    public Partnertype getPartnerType() {
        return this.partnerType;
    }

    public Partner partnerType(Partnertype partnerType) {
        this.setPartnerType(partnerType);
        return this;
    }

    public void setPartnerType(Partnertype partnerType) {
        this.partnerType = partnerType;
    }

    public Integer getValueM() {
        return this.valueM;
    }

    public Partner valueM(Integer valueM) {
        this.setValueM(valueM);
        return this;
    }

    public void setValueM(Integer valueM) {
        this.valueM = valueM;
    }

    public Integer getValueC() {
        return this.valueC;
    }

    public Partner valueC(Integer valueC) {
        this.setValueC(valueC);
        return this;
    }

    public void setValueC(Integer valueC) {
        this.valueC = valueC;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Partner createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public Partner createdBy(Long createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public Partner updatedDate(Instant updatedDate) {
        this.setUpdatedDate(updatedDate);
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUpdatedBy() {
        return this.updatedBy;
    }

    public Partner updatedBy(Long updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Boolean getStatus() {
        return this.status;
    }

    public Partner status(Boolean status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Partner)) {
            return false;
        }
        return id != null && id.equals(((Partner) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Partner{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", domain='" + getDomain() + "'" +
            ", partnerCommission=" + getPartnerCommission() +
            ", partnerType='" + getPartnerType() + "'" +
            ", valueM=" + getValueM() +
            ", valueC=" + getValueC() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy=" + getUpdatedBy() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
