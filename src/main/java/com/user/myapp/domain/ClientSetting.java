package com.user.myapp.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * A ClientSetting.
 */
@Entity
@Table(name = "client_setting")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ClientSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "candidate_self_assessment")
    private Integer candidateSelfAssessment;

    @Column(name = "auto_reninder_assessment")
    private Integer autoReninderAssessment;

    @Column(name = "allow_candidate_paid_version")
    private Boolean allowCandidatePaidVersion;

    @Column(name = "allow_employess_paid_version")
    private Boolean allowEmployessPaidVersion;

    @Column(name = "auto_archieve_requisitions")
    private Integer autoArchieveRequisitions;

    @Column(name = "company_theme_dark_colour")
    private String companyThemeDarkColour;

    @Column(name = "company_theme_light_colour")
    private String companyThemeLightColour;

    @Column(name = "company_url")
    private String companyUrl;

    @Column(name = "company_menu")
    private String companyMenu;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ClientSetting id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCandidateSelfAssessment() {
        return this.candidateSelfAssessment;
    }

    public ClientSetting candidateSelfAssessment(Integer candidateSelfAssessment) {
        this.setCandidateSelfAssessment(candidateSelfAssessment);
        return this;
    }

    public void setCandidateSelfAssessment(Integer candidateSelfAssessment) {
        this.candidateSelfAssessment = candidateSelfAssessment;
    }

    public Integer getAutoReninderAssessment() {
        return this.autoReninderAssessment;
    }

    public ClientSetting autoReninderAssessment(Integer autoReninderAssessment) {
        this.setAutoReninderAssessment(autoReninderAssessment);
        return this;
    }

    public void setAutoReninderAssessment(Integer autoReninderAssessment) {
        this.autoReninderAssessment = autoReninderAssessment;
    }

    public Boolean getAllowCandidatePaidVersion() {
        return this.allowCandidatePaidVersion;
    }

    public ClientSetting allowCandidatePaidVersion(Boolean allowCandidatePaidVersion) {
        this.setAllowCandidatePaidVersion(allowCandidatePaidVersion);
        return this;
    }

    public void setAllowCandidatePaidVersion(Boolean allowCandidatePaidVersion) {
        this.allowCandidatePaidVersion = allowCandidatePaidVersion;
    }

    public Boolean getAllowEmployessPaidVersion() {
        return this.allowEmployessPaidVersion;
    }

    public ClientSetting allowEmployessPaidVersion(Boolean allowEmployessPaidVersion) {
        this.setAllowEmployessPaidVersion(allowEmployessPaidVersion);
        return this;
    }

    public void setAllowEmployessPaidVersion(Boolean allowEmployessPaidVersion) {
        this.allowEmployessPaidVersion = allowEmployessPaidVersion;
    }

    public Integer getAutoArchieveRequisitions() {
        return this.autoArchieveRequisitions;
    }

    public ClientSetting autoArchieveRequisitions(Integer autoArchieveRequisitions) {
        this.setAutoArchieveRequisitions(autoArchieveRequisitions);
        return this;
    }

    public void setAutoArchieveRequisitions(Integer autoArchieveRequisitions) {
        this.autoArchieveRequisitions = autoArchieveRequisitions;
    }

    public String getCompanyThemeDarkColour() {
        return this.companyThemeDarkColour;
    }

    public ClientSetting companyThemeDarkColour(String companyThemeDarkColour) {
        this.setCompanyThemeDarkColour(companyThemeDarkColour);
        return this;
    }

    public void setCompanyThemeDarkColour(String companyThemeDarkColour) {
        this.companyThemeDarkColour = companyThemeDarkColour;
    }

    public String getCompanyThemeLightColour() {
        return this.companyThemeLightColour;
    }

    public ClientSetting companyThemeLightColour(String companyThemeLightColour) {
        this.setCompanyThemeLightColour(companyThemeLightColour);
        return this;
    }

    public void setCompanyThemeLightColour(String companyThemeLightColour) {
        this.companyThemeLightColour = companyThemeLightColour;
    }

    public String getCompanyUrl() {
        return this.companyUrl;
    }

    public ClientSetting companyUrl(String companyUrl) {
        this.setCompanyUrl(companyUrl);
        return this;
    }

    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    public String getCompanyMenu() {
        return this.companyMenu;
    }

    public ClientSetting companyMenu(String companyMenu) {
        this.setCompanyMenu(companyMenu);
        return this;
    }

    public void setCompanyMenu(String companyMenu) {
        this.companyMenu = companyMenu;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientSetting)) {
            return false;
        }
        return id != null && id.equals(((ClientSetting) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClientSetting{" +
            "id=" + getId() +
            ", candidateSelfAssessment=" + getCandidateSelfAssessment() +
            ", autoReninderAssessment=" + getAutoReninderAssessment() +
            ", allowCandidatePaidVersion='" + getAllowCandidatePaidVersion() + "'" +
            ", allowEmployessPaidVersion='" + getAllowEmployessPaidVersion() + "'" +
            ", autoArchieveRequisitions=" + getAutoArchieveRequisitions() +
            ", companyThemeDarkColour='" + getCompanyThemeDarkColour() + "'" +
            ", companyThemeLightColour='" + getCompanyThemeLightColour() + "'" +
            ", companyUrl='" + getCompanyUrl() + "'" +
            ", companyMenu='" + getCompanyMenu() + "'" +
            "}";
    }
}
