package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A UserRolePermission.
 */
@Entity
@Table(name = "user_role_permission")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserRolePermission implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties(value = { "userEmails", "loginCounts", "userRolePermissions", "roleMasters" }, allowSetters = true)
    private InPreferenceUser inPreferenceUser;

    @ManyToOne
    @JsonIgnoreProperties(value = { "userPermissionOnAttributes", "userRolePermissions", "inPreferenceUser" }, allowSetters = true)
    private RoleMaster roleMaster;

    @ManyToOne
    @JsonIgnoreProperties(value = { "userRolePermissions" }, allowSetters = true)
    private PermissionMaster permissionMaster;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public UserRolePermission id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InPreferenceUser getInPreferenceUser() {
        return this.inPreferenceUser;
    }

    public void setInPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.inPreferenceUser = inPreferenceUser;
    }

    public UserRolePermission inPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.setInPreferenceUser(inPreferenceUser);
        return this;
    }

    public RoleMaster getRoleMaster() {
        return this.roleMaster;
    }

    public void setRoleMaster(RoleMaster roleMaster) {
        this.roleMaster = roleMaster;
    }

    public UserRolePermission roleMaster(RoleMaster roleMaster) {
        this.setRoleMaster(roleMaster);
        return this;
    }

    public PermissionMaster getPermissionMaster() {
        return this.permissionMaster;
    }

    public void setPermissionMaster(PermissionMaster permissionMaster) {
        this.permissionMaster = permissionMaster;
    }

    public UserRolePermission permissionMaster(PermissionMaster permissionMaster) {
        this.setPermissionMaster(permissionMaster);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserRolePermission)) {
            return false;
        }
        return id != null && id.equals(((UserRolePermission) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserRolePermission{" +
            "id=" + getId() +
            "}";
    }
}
