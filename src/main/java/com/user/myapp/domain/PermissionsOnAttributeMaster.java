package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A PermissionsOnAttributeMaster.
 */
@Entity
@Table(name = "permissions_on_attribute_master")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PermissionsOnAttributeMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "attribute_key")
    private String attributeKey;

    @Column(name = "attribute_title")
    private String attributeTitle;

    @OneToMany(mappedBy = "permissionsOnAttributeMaster")
    @JsonIgnoreProperties(value = { "roleMaster", "permissionsOnAttributeMaster" }, allowSetters = true)
    private Set<UserPermissionOnAttribute> userPermissionOnAttributes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PermissionsOnAttributeMaster id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttributeKey() {
        return this.attributeKey;
    }

    public PermissionsOnAttributeMaster attributeKey(String attributeKey) {
        this.setAttributeKey(attributeKey);
        return this;
    }

    public void setAttributeKey(String attributeKey) {
        this.attributeKey = attributeKey;
    }

    public String getAttributeTitle() {
        return this.attributeTitle;
    }

    public PermissionsOnAttributeMaster attributeTitle(String attributeTitle) {
        this.setAttributeTitle(attributeTitle);
        return this;
    }

    public void setAttributeTitle(String attributeTitle) {
        this.attributeTitle = attributeTitle;
    }

    public Set<UserPermissionOnAttribute> getUserPermissionOnAttributes() {
        return this.userPermissionOnAttributes;
    }

    public void setUserPermissionOnAttributes(Set<UserPermissionOnAttribute> userPermissionOnAttributes) {
        if (this.userPermissionOnAttributes != null) {
            this.userPermissionOnAttributes.forEach(i -> i.setPermissionsOnAttributeMaster(null));
        }
        if (userPermissionOnAttributes != null) {
            userPermissionOnAttributes.forEach(i -> i.setPermissionsOnAttributeMaster(this));
        }
        this.userPermissionOnAttributes = userPermissionOnAttributes;
    }

    public PermissionsOnAttributeMaster userPermissionOnAttributes(Set<UserPermissionOnAttribute> userPermissionOnAttributes) {
        this.setUserPermissionOnAttributes(userPermissionOnAttributes);
        return this;
    }

    public PermissionsOnAttributeMaster addUserPermissionOnAttribute(UserPermissionOnAttribute userPermissionOnAttribute) {
        this.userPermissionOnAttributes.add(userPermissionOnAttribute);
        userPermissionOnAttribute.setPermissionsOnAttributeMaster(this);
        return this;
    }

    public PermissionsOnAttributeMaster removeUserPermissionOnAttribute(UserPermissionOnAttribute userPermissionOnAttribute) {
        this.userPermissionOnAttributes.remove(userPermissionOnAttribute);
        userPermissionOnAttribute.setPermissionsOnAttributeMaster(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PermissionsOnAttributeMaster)) {
            return false;
        }
        return id != null && id.equals(((PermissionsOnAttributeMaster) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PermissionsOnAttributeMaster{" +
            "id=" + getId() +
            ", attributeKey='" + getAttributeKey() + "'" +
            ", attributeTitle='" + getAttributeTitle() + "'" +
            "}";
    }
}
