package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A RoleMaster.
 */
@Entity
@Table(name = "role_master")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RoleMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "role_name")
    private String roleName;

    @Column(name = "description")
    private String description;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "status")
    private Boolean status;

    @OneToMany(mappedBy = "roleMaster")
    @JsonIgnoreProperties(value = { "roleMaster", "permissionsOnAttributeMaster" }, allowSetters = true)
    private Set<UserPermissionOnAttribute> userPermissionOnAttributes = new HashSet<>();

    @OneToMany(mappedBy = "roleMaster")
    @JsonIgnoreProperties(value = { "inPreferenceUser", "roleMaster", "permissionMaster" }, allowSetters = true)
    private Set<UserRolePermission> userRolePermissions = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "userEmails", "loginCounts", "userRolePermissions", "roleMasters" }, allowSetters = true)
    private InPreferenceUser inPreferenceUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RoleMaster id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return this.roleName;
    }

    public RoleMaster roleName(String roleName) {
        this.setRoleName(roleName);
        return this;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return this.description;
    }

    public RoleMaster description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public RoleMaster createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public RoleMaster createdBy(Long createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public RoleMaster updatedDate(Instant updatedDate) {
        this.setUpdatedDate(updatedDate);
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUpdatedBy() {
        return this.updatedBy;
    }

    public RoleMaster updatedBy(Long updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Boolean getStatus() {
        return this.status;
    }

    public RoleMaster status(Boolean status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Set<UserPermissionOnAttribute> getUserPermissionOnAttributes() {
        return this.userPermissionOnAttributes;
    }

    public void setUserPermissionOnAttributes(Set<UserPermissionOnAttribute> userPermissionOnAttributes) {
        if (this.userPermissionOnAttributes != null) {
            this.userPermissionOnAttributes.forEach(i -> i.setRoleMaster(null));
        }
        if (userPermissionOnAttributes != null) {
            userPermissionOnAttributes.forEach(i -> i.setRoleMaster(this));
        }
        this.userPermissionOnAttributes = userPermissionOnAttributes;
    }

    public RoleMaster userPermissionOnAttributes(Set<UserPermissionOnAttribute> userPermissionOnAttributes) {
        this.setUserPermissionOnAttributes(userPermissionOnAttributes);
        return this;
    }

    public RoleMaster addUserPermissionOnAttribute(UserPermissionOnAttribute userPermissionOnAttribute) {
        this.userPermissionOnAttributes.add(userPermissionOnAttribute);
        userPermissionOnAttribute.setRoleMaster(this);
        return this;
    }

    public RoleMaster removeUserPermissionOnAttribute(UserPermissionOnAttribute userPermissionOnAttribute) {
        this.userPermissionOnAttributes.remove(userPermissionOnAttribute);
        userPermissionOnAttribute.setRoleMaster(null);
        return this;
    }

    public Set<UserRolePermission> getUserRolePermissions() {
        return this.userRolePermissions;
    }

    public void setUserRolePermissions(Set<UserRolePermission> userRolePermissions) {
        if (this.userRolePermissions != null) {
            this.userRolePermissions.forEach(i -> i.setRoleMaster(null));
        }
        if (userRolePermissions != null) {
            userRolePermissions.forEach(i -> i.setRoleMaster(this));
        }
        this.userRolePermissions = userRolePermissions;
    }

    public RoleMaster userRolePermissions(Set<UserRolePermission> userRolePermissions) {
        this.setUserRolePermissions(userRolePermissions);
        return this;
    }

    public RoleMaster addUserRolePermission(UserRolePermission userRolePermission) {
        this.userRolePermissions.add(userRolePermission);
        userRolePermission.setRoleMaster(this);
        return this;
    }

    public RoleMaster removeUserRolePermission(UserRolePermission userRolePermission) {
        this.userRolePermissions.remove(userRolePermission);
        userRolePermission.setRoleMaster(null);
        return this;
    }

    public InPreferenceUser getInPreferenceUser() {
        return this.inPreferenceUser;
    }

    public void setInPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.inPreferenceUser = inPreferenceUser;
    }

    public RoleMaster inPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.setInPreferenceUser(inPreferenceUser);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RoleMaster)) {
            return false;
        }
        return id != null && id.equals(((RoleMaster) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RoleMaster{" +
            "id=" + getId() +
            ", roleName='" + getRoleName() + "'" +
            ", description='" + getDescription() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy=" + getUpdatedBy() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
