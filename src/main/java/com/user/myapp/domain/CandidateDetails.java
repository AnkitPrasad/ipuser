package com.user.myapp.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * A CandidateDetails.
 */
@Entity
@Table(name = "candidate_details")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CandidateDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "candidate_country")
    private String candidateCountry;

    @Column(name = "business_units")
    private String businessUnits;

    @Column(name = "legal_entity")
    private String legalEntity;

    @Column(name = "candidate_sub_function")
    private String candidateSubFunction;

    @Column(name = "designation")
    private String designation;

    @Column(name = "job_title")
    private String jobTitle;

    @Column(name = "location")
    private String location;

    @Column(name = "sub_business_units")
    private String subBusinessUnits;

    @Column(name = "candidate_function")
    private String candidateFunction;

    @Column(name = "work_level")
    private String workLevel;

    @Column(name = "candidate_grade")
    private String candidateGrade;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CandidateDetails id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCandidateCountry() {
        return this.candidateCountry;
    }

    public CandidateDetails candidateCountry(String candidateCountry) {
        this.setCandidateCountry(candidateCountry);
        return this;
    }

    public void setCandidateCountry(String candidateCountry) {
        this.candidateCountry = candidateCountry;
    }

    public String getBusinessUnits() {
        return this.businessUnits;
    }

    public CandidateDetails businessUnits(String businessUnits) {
        this.setBusinessUnits(businessUnits);
        return this;
    }

    public void setBusinessUnits(String businessUnits) {
        this.businessUnits = businessUnits;
    }

    public String getLegalEntity() {
        return this.legalEntity;
    }

    public CandidateDetails legalEntity(String legalEntity) {
        this.setLegalEntity(legalEntity);
        return this;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getCandidateSubFunction() {
        return this.candidateSubFunction;
    }

    public CandidateDetails candidateSubFunction(String candidateSubFunction) {
        this.setCandidateSubFunction(candidateSubFunction);
        return this;
    }

    public void setCandidateSubFunction(String candidateSubFunction) {
        this.candidateSubFunction = candidateSubFunction;
    }

    public String getDesignation() {
        return this.designation;
    }

    public CandidateDetails designation(String designation) {
        this.setDesignation(designation);
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getJobTitle() {
        return this.jobTitle;
    }

    public CandidateDetails jobTitle(String jobTitle) {
        this.setJobTitle(jobTitle);
        return this;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getLocation() {
        return this.location;
    }

    public CandidateDetails location(String location) {
        this.setLocation(location);
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubBusinessUnits() {
        return this.subBusinessUnits;
    }

    public CandidateDetails subBusinessUnits(String subBusinessUnits) {
        this.setSubBusinessUnits(subBusinessUnits);
        return this;
    }

    public void setSubBusinessUnits(String subBusinessUnits) {
        this.subBusinessUnits = subBusinessUnits;
    }

    public String getCandidateFunction() {
        return this.candidateFunction;
    }

    public CandidateDetails candidateFunction(String candidateFunction) {
        this.setCandidateFunction(candidateFunction);
        return this;
    }

    public void setCandidateFunction(String candidateFunction) {
        this.candidateFunction = candidateFunction;
    }

    public String getWorkLevel() {
        return this.workLevel;
    }

    public CandidateDetails workLevel(String workLevel) {
        this.setWorkLevel(workLevel);
        return this;
    }

    public void setWorkLevel(String workLevel) {
        this.workLevel = workLevel;
    }

    public String getCandidateGrade() {
        return this.candidateGrade;
    }

    public CandidateDetails candidateGrade(String candidateGrade) {
        this.setCandidateGrade(candidateGrade);
        return this;
    }

    public void setCandidateGrade(String candidateGrade) {
        this.candidateGrade = candidateGrade;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CandidateDetails)) {
            return false;
        }
        return id != null && id.equals(((CandidateDetails) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CandidateDetails{" +
            "id=" + getId() +
            ", candidateCountry='" + getCandidateCountry() + "'" +
            ", businessUnits='" + getBusinessUnits() + "'" +
            ", legalEntity='" + getLegalEntity() + "'" +
            ", candidateSubFunction='" + getCandidateSubFunction() + "'" +
            ", designation='" + getDesignation() + "'" +
            ", jobTitle='" + getJobTitle() + "'" +
            ", location='" + getLocation() + "'" +
            ", subBusinessUnits='" + getSubBusinessUnits() + "'" +
            ", candidateFunction='" + getCandidateFunction() + "'" +
            ", workLevel='" + getWorkLevel() + "'" +
            ", candidateGrade='" + getCandidateGrade() + "'" +
            "}";
    }
}
