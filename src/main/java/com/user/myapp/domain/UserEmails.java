package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A UserEmails.
 */
@Entity
@Table(name = "user_emails")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserEmails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "login_id")
    private Long loginId;

    @ManyToOne
    @JsonIgnoreProperties(value = { "userEmails", "loginCounts", "userRolePermissions", "roleMasters" }, allowSetters = true)
    private InPreferenceUser inPreferenceUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public UserEmails id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public UserEmails email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getLoginId() {
        return this.loginId;
    }

    public UserEmails loginId(Long loginId) {
        this.setLoginId(loginId);
        return this;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public InPreferenceUser getInPreferenceUser() {
        return this.inPreferenceUser;
    }

    public void setInPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.inPreferenceUser = inPreferenceUser;
    }

    public UserEmails inPreferenceUser(InPreferenceUser inPreferenceUser) {
        this.setInPreferenceUser(inPreferenceUser);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserEmails)) {
            return false;
        }
        return id != null && id.equals(((UserEmails) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserEmails{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", loginId=" + getLoginId() +
            "}";
    }
}
