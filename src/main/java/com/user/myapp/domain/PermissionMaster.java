package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A PermissionMaster.
 */
@Entity
@Table(name = "permission_master")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PermissionMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "page_name")
    private String pageName;

    @Column(name = "page_url")
    private String pageUrl;

    @OneToMany(mappedBy = "permissionMaster")
    @JsonIgnoreProperties(value = { "inPreferenceUser", "roleMaster", "permissionMaster" }, allowSetters = true)
    private Set<UserRolePermission> userRolePermissions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PermissionMaster id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPageName() {
        return this.pageName;
    }

    public PermissionMaster pageName(String pageName) {
        this.setPageName(pageName);
        return this;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageUrl() {
        return this.pageUrl;
    }

    public PermissionMaster pageUrl(String pageUrl) {
        this.setPageUrl(pageUrl);
        return this;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public Set<UserRolePermission> getUserRolePermissions() {
        return this.userRolePermissions;
    }

    public void setUserRolePermissions(Set<UserRolePermission> userRolePermissions) {
        if (this.userRolePermissions != null) {
            this.userRolePermissions.forEach(i -> i.setPermissionMaster(null));
        }
        if (userRolePermissions != null) {
            userRolePermissions.forEach(i -> i.setPermissionMaster(this));
        }
        this.userRolePermissions = userRolePermissions;
    }

    public PermissionMaster userRolePermissions(Set<UserRolePermission> userRolePermissions) {
        this.setUserRolePermissions(userRolePermissions);
        return this;
    }

    public PermissionMaster addUserRolePermission(UserRolePermission userRolePermission) {
        this.userRolePermissions.add(userRolePermission);
        userRolePermission.setPermissionMaster(this);
        return this;
    }

    public PermissionMaster removeUserRolePermission(UserRolePermission userRolePermission) {
        this.userRolePermissions.remove(userRolePermission);
        userRolePermission.setPermissionMaster(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PermissionMaster)) {
            return false;
        }
        return id != null && id.equals(((PermissionMaster) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PermissionMaster{" +
            "id=" + getId() +
            ", pageName='" + getPageName() + "'" +
            ", pageUrl='" + getPageUrl() + "'" +
            "}";
    }
}
