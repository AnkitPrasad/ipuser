package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.user.myapp.domain.enumeration.Permission;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A InPreferenceUser.
 */
@Entity
@Table(name = "in_preference_user")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class InPreferenceUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "mobile_no")
    private String mobileNo;

    @Column(name = "profile_url")
    private String profileUrl;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_type")
    private Permission userType;

    @NotNull
    @Column(name = "email_id", nullable = false)
    private String emailId;

    @NotNull
    @Column(name = "login_id", nullable = false)
    private Long loginId;

    @Column(name = "secret")
    private String secret;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "status")
    private Boolean status;

    @OneToMany(mappedBy = "inPreferenceUser")
    @JsonIgnoreProperties(value = { "inPreferenceUser" }, allowSetters = true)
    private Set<UserEmails> userEmails = new HashSet<>();

    @OneToMany(mappedBy = "inPreferenceUser")
    @JsonIgnoreProperties(value = { "inPreferenceUser" }, allowSetters = true)
    private Set<LoginCount> loginCounts = new HashSet<>();

    @OneToMany(mappedBy = "inPreferenceUser")
    @JsonIgnoreProperties(value = { "inPreferenceUser", "roleMaster", "permissionMaster" }, allowSetters = true)
    private Set<UserRolePermission> userRolePermissions = new HashSet<>();

    @OneToMany(mappedBy = "inPreferenceUser")
    @JsonIgnoreProperties(value = { "userPermissionOnAttributes", "userRolePermissions", "inPreferenceUser" }, allowSetters = true)
    private Set<RoleMaster> roleMasters = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public InPreferenceUser id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public InPreferenceUser firstName(String firstName) {
        this.setFirstName(firstName);
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public InPreferenceUser lastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public InPreferenceUser mobileNo(String mobileNo) {
        this.setMobileNo(mobileNo);
        return this;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getProfileUrl() {
        return this.profileUrl;
    }

    public InPreferenceUser profileUrl(String profileUrl) {
        this.setProfileUrl(profileUrl);
        return this;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public Permission getUserType() {
        return this.userType;
    }

    public InPreferenceUser userType(Permission userType) {
        this.setUserType(userType);
        return this;
    }

    public void setUserType(Permission userType) {
        this.userType = userType;
    }

    public String getEmailId() {
        return this.emailId;
    }

    public InPreferenceUser emailId(String emailId) {
        this.setEmailId(emailId);
        return this;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Long getLoginId() {
        return this.loginId;
    }

    public InPreferenceUser loginId(Long loginId) {
        this.setLoginId(loginId);
        return this;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public String getSecret() {
        return this.secret;
    }

    public InPreferenceUser secret(String secret) {
        this.setSecret(secret);
        return this;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public InPreferenceUser createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public InPreferenceUser createdBy(Long createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public InPreferenceUser updatedDate(Instant updatedDate) {
        this.setUpdatedDate(updatedDate);
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUpdatedBy() {
        return this.updatedBy;
    }

    public InPreferenceUser updatedBy(Long updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Boolean getStatus() {
        return this.status;
    }

    public InPreferenceUser status(Boolean status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Set<UserEmails> getUserEmails() {
        return this.userEmails;
    }

    public void setUserEmails(Set<UserEmails> userEmails) {
        if (this.userEmails != null) {
            this.userEmails.forEach(i -> i.setInPreferenceUser(null));
        }
        if (userEmails != null) {
            userEmails.forEach(i -> i.setInPreferenceUser(this));
        }
        this.userEmails = userEmails;
    }

    public InPreferenceUser userEmails(Set<UserEmails> userEmails) {
        this.setUserEmails(userEmails);
        return this;
    }

    public InPreferenceUser addUserEmails(UserEmails userEmails) {
        this.userEmails.add(userEmails);
        userEmails.setInPreferenceUser(this);
        return this;
    }

    public InPreferenceUser removeUserEmails(UserEmails userEmails) {
        this.userEmails.remove(userEmails);
        userEmails.setInPreferenceUser(null);
        return this;
    }

    public Set<LoginCount> getLoginCounts() {
        return this.loginCounts;
    }

    public void setLoginCounts(Set<LoginCount> loginCounts) {
        if (this.loginCounts != null) {
            this.loginCounts.forEach(i -> i.setInPreferenceUser(null));
        }
        if (loginCounts != null) {
            loginCounts.forEach(i -> i.setInPreferenceUser(this));
        }
        this.loginCounts = loginCounts;
    }

    public InPreferenceUser loginCounts(Set<LoginCount> loginCounts) {
        this.setLoginCounts(loginCounts);
        return this;
    }

    public InPreferenceUser addLoginCount(LoginCount loginCount) {
        this.loginCounts.add(loginCount);
        loginCount.setInPreferenceUser(this);
        return this;
    }

    public InPreferenceUser removeLoginCount(LoginCount loginCount) {
        this.loginCounts.remove(loginCount);
        loginCount.setInPreferenceUser(null);
        return this;
    }

    public Set<UserRolePermission> getUserRolePermissions() {
        return this.userRolePermissions;
    }

    public void setUserRolePermissions(Set<UserRolePermission> userRolePermissions) {
        if (this.userRolePermissions != null) {
            this.userRolePermissions.forEach(i -> i.setInPreferenceUser(null));
        }
        if (userRolePermissions != null) {
            userRolePermissions.forEach(i -> i.setInPreferenceUser(this));
        }
        this.userRolePermissions = userRolePermissions;
    }

    public InPreferenceUser userRolePermissions(Set<UserRolePermission> userRolePermissions) {
        this.setUserRolePermissions(userRolePermissions);
        return this;
    }

    public InPreferenceUser addUserRolePermission(UserRolePermission userRolePermission) {
        this.userRolePermissions.add(userRolePermission);
        userRolePermission.setInPreferenceUser(this);
        return this;
    }

    public InPreferenceUser removeUserRolePermission(UserRolePermission userRolePermission) {
        this.userRolePermissions.remove(userRolePermission);
        userRolePermission.setInPreferenceUser(null);
        return this;
    }

    public Set<RoleMaster> getRoleMasters() {
        return this.roleMasters;
    }

    public void setRoleMasters(Set<RoleMaster> roleMasters) {
        if (this.roleMasters != null) {
            this.roleMasters.forEach(i -> i.setInPreferenceUser(null));
        }
        if (roleMasters != null) {
            roleMasters.forEach(i -> i.setInPreferenceUser(this));
        }
        this.roleMasters = roleMasters;
    }

    public InPreferenceUser roleMasters(Set<RoleMaster> roleMasters) {
        this.setRoleMasters(roleMasters);
        return this;
    }

    public InPreferenceUser addRoleMaster(RoleMaster roleMaster) {
        this.roleMasters.add(roleMaster);
        roleMaster.setInPreferenceUser(this);
        return this;
    }

    public InPreferenceUser removeRoleMaster(RoleMaster roleMaster) {
        this.roleMasters.remove(roleMaster);
        roleMaster.setInPreferenceUser(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InPreferenceUser)) {
            return false;
        }
        return id != null && id.equals(((InPreferenceUser) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InPreferenceUser{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", mobileNo='" + getMobileNo() + "'" +
            ", profileUrl='" + getProfileUrl() + "'" +
            ", userType='" + getUserType() + "'" +
            ", emailId='" + getEmailId() + "'" +
            ", loginId=" + getLoginId() +
            ", secret='" + getSecret() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy=" + getUpdatedBy() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
