package com.user.myapp.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * A RoleAndPermissions.
 */
@Entity
@Table(name = "role_and_permissions")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RoleAndPermissions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "read_access")
    private Boolean readAccess;

    @Column(name = "create_access")
    private Boolean createAccess;

    @Column(name = "edit_access")
    private Boolean editAccess;

    @Column(name = "delete_access")
    private Boolean deleteAccess;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RoleAndPermissions id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getReadAccess() {
        return this.readAccess;
    }

    public RoleAndPermissions readAccess(Boolean readAccess) {
        this.setReadAccess(readAccess);
        return this;
    }

    public void setReadAccess(Boolean readAccess) {
        this.readAccess = readAccess;
    }

    public Boolean getCreateAccess() {
        return this.createAccess;
    }

    public RoleAndPermissions createAccess(Boolean createAccess) {
        this.setCreateAccess(createAccess);
        return this;
    }

    public void setCreateAccess(Boolean createAccess) {
        this.createAccess = createAccess;
    }

    public Boolean getEditAccess() {
        return this.editAccess;
    }

    public RoleAndPermissions editAccess(Boolean editAccess) {
        this.setEditAccess(editAccess);
        return this;
    }

    public void setEditAccess(Boolean editAccess) {
        this.editAccess = editAccess;
    }

    public Boolean getDeleteAccess() {
        return this.deleteAccess;
    }

    public RoleAndPermissions deleteAccess(Boolean deleteAccess) {
        this.setDeleteAccess(deleteAccess);
        return this;
    }

    public void setDeleteAccess(Boolean deleteAccess) {
        this.deleteAccess = deleteAccess;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RoleAndPermissions)) {
            return false;
        }
        return id != null && id.equals(((RoleAndPermissions) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RoleAndPermissions{" +
            "id=" + getId() +
            ", readAccess='" + getReadAccess() + "'" +
            ", createAccess='" + getCreateAccess() + "'" +
            ", editAccess='" + getEditAccess() + "'" +
            ", deleteAccess='" + getDeleteAccess() + "'" +
            "}";
    }
}
