package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Candidate.
 */
@Entity
@Table(name = "candidate")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Candidate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "is_internal_candidate")
    private Boolean isInternalCandidate;

    @Column(name = "company")
    private String company;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "assessment_status")
    private String assessmentStatus;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "archive_status")
    private Boolean archiveStatus;

    @OneToOne
    @JoinColumn(unique = true)
    private CandidateDetails candidateDetails;

    @OneToMany(mappedBy = "candidate")
    @JsonIgnoreProperties(value = { "candidate" }, allowSetters = true)
    private Set<CandidateProfile> candidateProfiles = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "clientSetting", "candidates" }, allowSetters = true)
    private Client client;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Candidate id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsInternalCandidate() {
        return this.isInternalCandidate;
    }

    public Candidate isInternalCandidate(Boolean isInternalCandidate) {
        this.setIsInternalCandidate(isInternalCandidate);
        return this;
    }

    public void setIsInternalCandidate(Boolean isInternalCandidate) {
        this.isInternalCandidate = isInternalCandidate;
    }

    public String getCompany() {
        return this.company;
    }

    public Candidate company(String company) {
        this.setCompany(company);
        return this;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Candidate createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public Candidate createdBy(Long createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public Candidate updatedDate(Instant updatedDate) {
        this.setUpdatedDate(updatedDate);
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUpdatedBy() {
        return this.updatedBy;
    }

    public Candidate updatedBy(Long updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getAssessmentStatus() {
        return this.assessmentStatus;
    }

    public Candidate assessmentStatus(String assessmentStatus) {
        this.setAssessmentStatus(assessmentStatus);
        return this;
    }

    public void setAssessmentStatus(String assessmentStatus) {
        this.assessmentStatus = assessmentStatus;
    }

    public Boolean getStatus() {
        return this.status;
    }

    public Candidate status(Boolean status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getArchiveStatus() {
        return this.archiveStatus;
    }

    public Candidate archiveStatus(Boolean archiveStatus) {
        this.setArchiveStatus(archiveStatus);
        return this;
    }

    public void setArchiveStatus(Boolean archiveStatus) {
        this.archiveStatus = archiveStatus;
    }

    public CandidateDetails getCandidateDetails() {
        return this.candidateDetails;
    }

    public void setCandidateDetails(CandidateDetails candidateDetails) {
        this.candidateDetails = candidateDetails;
    }

    public Candidate candidateDetails(CandidateDetails candidateDetails) {
        this.setCandidateDetails(candidateDetails);
        return this;
    }

    public Set<CandidateProfile> getCandidateProfiles() {
        return this.candidateProfiles;
    }

    public void setCandidateProfiles(Set<CandidateProfile> candidateProfiles) {
        if (this.candidateProfiles != null) {
            this.candidateProfiles.forEach(i -> i.setCandidate(null));
        }
        if (candidateProfiles != null) {
            candidateProfiles.forEach(i -> i.setCandidate(this));
        }
        this.candidateProfiles = candidateProfiles;
    }

    public Candidate candidateProfiles(Set<CandidateProfile> candidateProfiles) {
        this.setCandidateProfiles(candidateProfiles);
        return this;
    }

    public Candidate addCandidateProfile(CandidateProfile candidateProfile) {
        this.candidateProfiles.add(candidateProfile);
        candidateProfile.setCandidate(this);
        return this;
    }

    public Candidate removeCandidateProfile(CandidateProfile candidateProfile) {
        this.candidateProfiles.remove(candidateProfile);
        candidateProfile.setCandidate(null);
        return this;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Candidate client(Client client) {
        this.setClient(client);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Candidate)) {
            return false;
        }
        return id != null && id.equals(((Candidate) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Candidate{" +
            "id=" + getId() +
            ", isInternalCandidate='" + getIsInternalCandidate() + "'" +
            ", company='" + getCompany() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy=" + getUpdatedBy() +
            ", assessmentStatus='" + getAssessmentStatus() + "'" +
            ", status='" + getStatus() + "'" +
            ", archiveStatus='" + getArchiveStatus() + "'" +
            "}";
    }
}
