package com.user.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A CandidateProfile.
 */
@Entity
@Table(name = "candidate_profile")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CandidateProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "about_me")
    private String aboutMe;

    @Column(name = "career_summary")
    private String careerSummary;

    @Column(name = "education")
    private String education;

    @Column(name = "dob")
    private String dob;

    @Column(name = "current_employment")
    private String currentEmployment;

    @Column(name = "certifications")
    private String certifications;

    @Column(name = "experience")
    private String experience;

    @ManyToOne
    @JsonIgnoreProperties(value = { "candidateDetails", "candidateProfiles", "client" }, allowSetters = true)
    private Candidate candidate;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CandidateProfile id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAboutMe() {
        return this.aboutMe;
    }

    public CandidateProfile aboutMe(String aboutMe) {
        this.setAboutMe(aboutMe);
        return this;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getCareerSummary() {
        return this.careerSummary;
    }

    public CandidateProfile careerSummary(String careerSummary) {
        this.setCareerSummary(careerSummary);
        return this;
    }

    public void setCareerSummary(String careerSummary) {
        this.careerSummary = careerSummary;
    }

    public String getEducation() {
        return this.education;
    }

    public CandidateProfile education(String education) {
        this.setEducation(education);
        return this;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getDob() {
        return this.dob;
    }

    public CandidateProfile dob(String dob) {
        this.setDob(dob);
        return this;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCurrentEmployment() {
        return this.currentEmployment;
    }

    public CandidateProfile currentEmployment(String currentEmployment) {
        this.setCurrentEmployment(currentEmployment);
        return this;
    }

    public void setCurrentEmployment(String currentEmployment) {
        this.currentEmployment = currentEmployment;
    }

    public String getCertifications() {
        return this.certifications;
    }

    public CandidateProfile certifications(String certifications) {
        this.setCertifications(certifications);
        return this;
    }

    public void setCertifications(String certifications) {
        this.certifications = certifications;
    }

    public String getExperience() {
        return this.experience;
    }

    public CandidateProfile experience(String experience) {
        this.setExperience(experience);
        return this;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public Candidate getCandidate() {
        return this.candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public CandidateProfile candidate(Candidate candidate) {
        this.setCandidate(candidate);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CandidateProfile)) {
            return false;
        }
        return id != null && id.equals(((CandidateProfile) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CandidateProfile{" +
            "id=" + getId() +
            ", aboutMe='" + getAboutMe() + "'" +
            ", careerSummary='" + getCareerSummary() + "'" +
            ", education='" + getEducation() + "'" +
            ", dob='" + getDob() + "'" +
            ", currentEmployment='" + getCurrentEmployment() + "'" +
            ", certifications='" + getCertifications() + "'" +
            ", experience='" + getExperience() + "'" +
            "}";
    }
}
