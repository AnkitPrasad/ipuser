package com.user.myapp.web.rest;

import com.user.myapp.repository.RoleAndPermissionsRepository;
import com.user.myapp.service.RoleAndPermissionsQueryService;
import com.user.myapp.service.RoleAndPermissionsService;
import com.user.myapp.service.criteria.RoleAndPermissionsCriteria;
import com.user.myapp.service.dto.RoleAndPermissionsDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.RoleAndPermissions}.
 */
@RestController
@RequestMapping("/api")
public class RoleAndPermissionsResource {

    private final Logger log = LoggerFactory.getLogger(RoleAndPermissionsResource.class);

    private static final String ENTITY_NAME = "ipuserRoleAndPermissions";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RoleAndPermissionsService roleAndPermissionsService;

    private final RoleAndPermissionsRepository roleAndPermissionsRepository;

    private final RoleAndPermissionsQueryService roleAndPermissionsQueryService;

    public RoleAndPermissionsResource(
        RoleAndPermissionsService roleAndPermissionsService,
        RoleAndPermissionsRepository roleAndPermissionsRepository,
        RoleAndPermissionsQueryService roleAndPermissionsQueryService
    ) {
        this.roleAndPermissionsService = roleAndPermissionsService;
        this.roleAndPermissionsRepository = roleAndPermissionsRepository;
        this.roleAndPermissionsQueryService = roleAndPermissionsQueryService;
    }

    /**
     * {@code POST  /role-and-permissions} : Create a new roleAndPermissions.
     *
     * @param roleAndPermissionsDTO the roleAndPermissionsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new roleAndPermissionsDTO, or with status {@code 400 (Bad Request)} if the roleAndPermissions has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/role-and-permissions")
    public ResponseEntity<RoleAndPermissionsDTO> createRoleAndPermissions(@RequestBody RoleAndPermissionsDTO roleAndPermissionsDTO)
        throws URISyntaxException {
        log.debug("REST request to save RoleAndPermissions : {}", roleAndPermissionsDTO);
        if (roleAndPermissionsDTO.getId() != null) {
            throw new BadRequestAlertException("A new roleAndPermissions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RoleAndPermissionsDTO result = roleAndPermissionsService.save(roleAndPermissionsDTO);
        return ResponseEntity
            .created(new URI("/api/role-and-permissions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /role-and-permissions/:id} : Updates an existing roleAndPermissions.
     *
     * @param id the id of the roleAndPermissionsDTO to save.
     * @param roleAndPermissionsDTO the roleAndPermissionsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated roleAndPermissionsDTO,
     * or with status {@code 400 (Bad Request)} if the roleAndPermissionsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the roleAndPermissionsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/role-and-permissions/{id}")
    public ResponseEntity<RoleAndPermissionsDTO> updateRoleAndPermissions(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RoleAndPermissionsDTO roleAndPermissionsDTO
    ) throws URISyntaxException {
        log.debug("REST request to update RoleAndPermissions : {}, {}", id, roleAndPermissionsDTO);
        if (roleAndPermissionsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, roleAndPermissionsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!roleAndPermissionsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RoleAndPermissionsDTO result = roleAndPermissionsService.update(roleAndPermissionsDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, roleAndPermissionsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /role-and-permissions/:id} : Partial updates given fields of an existing roleAndPermissions, field will ignore if it is null
     *
     * @param id the id of the roleAndPermissionsDTO to save.
     * @param roleAndPermissionsDTO the roleAndPermissionsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated roleAndPermissionsDTO,
     * or with status {@code 400 (Bad Request)} if the roleAndPermissionsDTO is not valid,
     * or with status {@code 404 (Not Found)} if the roleAndPermissionsDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the roleAndPermissionsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/role-and-permissions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RoleAndPermissionsDTO> partialUpdateRoleAndPermissions(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RoleAndPermissionsDTO roleAndPermissionsDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update RoleAndPermissions partially : {}, {}", id, roleAndPermissionsDTO);
        if (roleAndPermissionsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, roleAndPermissionsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!roleAndPermissionsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RoleAndPermissionsDTO> result = roleAndPermissionsService.partialUpdate(roleAndPermissionsDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, roleAndPermissionsDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /role-and-permissions} : get all the roleAndPermissions.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of roleAndPermissions in body.
     */
    @GetMapping("/role-and-permissions")
    public ResponseEntity<List<RoleAndPermissionsDTO>> getAllRoleAndPermissions(
        RoleAndPermissionsCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get RoleAndPermissions by criteria: {}", criteria);
        Page<RoleAndPermissionsDTO> page = roleAndPermissionsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /role-and-permissions/count} : count all the roleAndPermissions.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/role-and-permissions/count")
    public ResponseEntity<Long> countRoleAndPermissions(RoleAndPermissionsCriteria criteria) {
        log.debug("REST request to count RoleAndPermissions by criteria: {}", criteria);
        return ResponseEntity.ok().body(roleAndPermissionsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /role-and-permissions/:id} : get the "id" roleAndPermissions.
     *
     * @param id the id of the roleAndPermissionsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the roleAndPermissionsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/role-and-permissions/{id}")
    public ResponseEntity<RoleAndPermissionsDTO> getRoleAndPermissions(@PathVariable Long id) {
        log.debug("REST request to get RoleAndPermissions : {}", id);
        Optional<RoleAndPermissionsDTO> roleAndPermissionsDTO = roleAndPermissionsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(roleAndPermissionsDTO);
    }

    /**
     * {@code DELETE  /role-and-permissions/:id} : delete the "id" roleAndPermissions.
     *
     * @param id the id of the roleAndPermissionsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/role-and-permissions/{id}")
    public ResponseEntity<Void> deleteRoleAndPermissions(@PathVariable Long id) {
        log.debug("REST request to delete RoleAndPermissions : {}", id);
        roleAndPermissionsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
