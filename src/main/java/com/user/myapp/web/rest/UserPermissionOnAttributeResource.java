package com.user.myapp.web.rest;

import com.user.myapp.repository.UserPermissionOnAttributeRepository;
import com.user.myapp.service.UserPermissionOnAttributeQueryService;
import com.user.myapp.service.UserPermissionOnAttributeService;
import com.user.myapp.service.criteria.UserPermissionOnAttributeCriteria;
import com.user.myapp.service.dto.UserPermissionOnAttributeDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.UserPermissionOnAttribute}.
 */
@RestController
@RequestMapping("/api")
public class UserPermissionOnAttributeResource {

    private final Logger log = LoggerFactory.getLogger(UserPermissionOnAttributeResource.class);

    private static final String ENTITY_NAME = "ipuserUserPermissionOnAttribute";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserPermissionOnAttributeService userPermissionOnAttributeService;

    private final UserPermissionOnAttributeRepository userPermissionOnAttributeRepository;

    private final UserPermissionOnAttributeQueryService userPermissionOnAttributeQueryService;

    public UserPermissionOnAttributeResource(
        UserPermissionOnAttributeService userPermissionOnAttributeService,
        UserPermissionOnAttributeRepository userPermissionOnAttributeRepository,
        UserPermissionOnAttributeQueryService userPermissionOnAttributeQueryService
    ) {
        this.userPermissionOnAttributeService = userPermissionOnAttributeService;
        this.userPermissionOnAttributeRepository = userPermissionOnAttributeRepository;
        this.userPermissionOnAttributeQueryService = userPermissionOnAttributeQueryService;
    }

    /**
     * {@code POST  /user-permission-on-attributes} : Create a new userPermissionOnAttribute.
     *
     * @param userPermissionOnAttributeDTO the userPermissionOnAttributeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userPermissionOnAttributeDTO, or with status {@code 400 (Bad Request)} if the userPermissionOnAttribute has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-permission-on-attributes")
    public ResponseEntity<UserPermissionOnAttributeDTO> createUserPermissionOnAttribute(
        @RequestBody UserPermissionOnAttributeDTO userPermissionOnAttributeDTO
    ) throws URISyntaxException {
        log.debug("REST request to save UserPermissionOnAttribute : {}", userPermissionOnAttributeDTO);
        if (userPermissionOnAttributeDTO.getId() != null) {
            throw new BadRequestAlertException("A new userPermissionOnAttribute cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserPermissionOnAttributeDTO result = userPermissionOnAttributeService.save(userPermissionOnAttributeDTO);
        return ResponseEntity
            .created(new URI("/api/user-permission-on-attributes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-permission-on-attributes/:id} : Updates an existing userPermissionOnAttribute.
     *
     * @param id the id of the userPermissionOnAttributeDTO to save.
     * @param userPermissionOnAttributeDTO the userPermissionOnAttributeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userPermissionOnAttributeDTO,
     * or with status {@code 400 (Bad Request)} if the userPermissionOnAttributeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userPermissionOnAttributeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-permission-on-attributes/{id}")
    public ResponseEntity<UserPermissionOnAttributeDTO> updateUserPermissionOnAttribute(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserPermissionOnAttributeDTO userPermissionOnAttributeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update UserPermissionOnAttribute : {}, {}", id, userPermissionOnAttributeDTO);
        if (userPermissionOnAttributeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userPermissionOnAttributeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userPermissionOnAttributeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserPermissionOnAttributeDTO result = userPermissionOnAttributeService.update(userPermissionOnAttributeDTO);
        return ResponseEntity
            .ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userPermissionOnAttributeDTO.getId().toString())
            )
            .body(result);
    }

    /**
     * {@code PATCH  /user-permission-on-attributes/:id} : Partial updates given fields of an existing userPermissionOnAttribute, field will ignore if it is null
     *
     * @param id the id of the userPermissionOnAttributeDTO to save.
     * @param userPermissionOnAttributeDTO the userPermissionOnAttributeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userPermissionOnAttributeDTO,
     * or with status {@code 400 (Bad Request)} if the userPermissionOnAttributeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the userPermissionOnAttributeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the userPermissionOnAttributeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/user-permission-on-attributes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<UserPermissionOnAttributeDTO> partialUpdateUserPermissionOnAttribute(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserPermissionOnAttributeDTO userPermissionOnAttributeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserPermissionOnAttribute partially : {}, {}", id, userPermissionOnAttributeDTO);
        if (userPermissionOnAttributeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userPermissionOnAttributeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userPermissionOnAttributeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserPermissionOnAttributeDTO> result = userPermissionOnAttributeService.partialUpdate(userPermissionOnAttributeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userPermissionOnAttributeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /user-permission-on-attributes} : get all the userPermissionOnAttributes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userPermissionOnAttributes in body.
     */
    @GetMapping("/user-permission-on-attributes")
    public ResponseEntity<List<UserPermissionOnAttributeDTO>> getAllUserPermissionOnAttributes(
        UserPermissionOnAttributeCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get UserPermissionOnAttributes by criteria: {}", criteria);
        Page<UserPermissionOnAttributeDTO> page = userPermissionOnAttributeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-permission-on-attributes/count} : count all the userPermissionOnAttributes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/user-permission-on-attributes/count")
    public ResponseEntity<Long> countUserPermissionOnAttributes(UserPermissionOnAttributeCriteria criteria) {
        log.debug("REST request to count UserPermissionOnAttributes by criteria: {}", criteria);
        return ResponseEntity.ok().body(userPermissionOnAttributeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-permission-on-attributes/:id} : get the "id" userPermissionOnAttribute.
     *
     * @param id the id of the userPermissionOnAttributeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userPermissionOnAttributeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-permission-on-attributes/{id}")
    public ResponseEntity<UserPermissionOnAttributeDTO> getUserPermissionOnAttribute(@PathVariable Long id) {
        log.debug("REST request to get UserPermissionOnAttribute : {}", id);
        Optional<UserPermissionOnAttributeDTO> userPermissionOnAttributeDTO = userPermissionOnAttributeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userPermissionOnAttributeDTO);
    }

    /**
     * {@code DELETE  /user-permission-on-attributes/:id} : delete the "id" userPermissionOnAttribute.
     *
     * @param id the id of the userPermissionOnAttributeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-permission-on-attributes/{id}")
    public ResponseEntity<Void> deleteUserPermissionOnAttribute(@PathVariable Long id) {
        log.debug("REST request to delete UserPermissionOnAttribute : {}", id);
        userPermissionOnAttributeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
