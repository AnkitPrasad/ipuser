package com.user.myapp.web.rest;

import com.user.myapp.repository.ClientSettingRepository;
import com.user.myapp.service.ClientSettingQueryService;
import com.user.myapp.service.ClientSettingService;
import com.user.myapp.service.criteria.ClientSettingCriteria;
import com.user.myapp.service.dto.ClientSettingDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.ClientSetting}.
 */
@RestController
@RequestMapping("/api")
public class ClientSettingResource {

    private final Logger log = LoggerFactory.getLogger(ClientSettingResource.class);

    private static final String ENTITY_NAME = "ipuserClientSetting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClientSettingService clientSettingService;

    private final ClientSettingRepository clientSettingRepository;

    private final ClientSettingQueryService clientSettingQueryService;

    public ClientSettingResource(
        ClientSettingService clientSettingService,
        ClientSettingRepository clientSettingRepository,
        ClientSettingQueryService clientSettingQueryService
    ) {
        this.clientSettingService = clientSettingService;
        this.clientSettingRepository = clientSettingRepository;
        this.clientSettingQueryService = clientSettingQueryService;
    }

    /**
     * {@code POST  /client-settings} : Create a new clientSetting.
     *
     * @param clientSettingDTO the clientSettingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientSettingDTO, or with status {@code 400 (Bad Request)} if the clientSetting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-settings")
    public ResponseEntity<ClientSettingDTO> createClientSetting(@RequestBody ClientSettingDTO clientSettingDTO) throws URISyntaxException {
        log.debug("REST request to save ClientSetting : {}", clientSettingDTO);
        if (clientSettingDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientSetting cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientSettingDTO result = clientSettingService.save(clientSettingDTO);
        return ResponseEntity
            .created(new URI("/api/client-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /client-settings/:id} : Updates an existing clientSetting.
     *
     * @param id the id of the clientSettingDTO to save.
     * @param clientSettingDTO the clientSettingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clientSettingDTO,
     * or with status {@code 400 (Bad Request)} if the clientSettingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clientSettingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/client-settings/{id}")
    public ResponseEntity<ClientSettingDTO> updateClientSetting(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ClientSettingDTO clientSettingDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ClientSetting : {}, {}", id, clientSettingDTO);
        if (clientSettingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, clientSettingDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!clientSettingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ClientSettingDTO result = clientSettingService.update(clientSettingDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, clientSettingDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /client-settings/:id} : Partial updates given fields of an existing clientSetting, field will ignore if it is null
     *
     * @param id the id of the clientSettingDTO to save.
     * @param clientSettingDTO the clientSettingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clientSettingDTO,
     * or with status {@code 400 (Bad Request)} if the clientSettingDTO is not valid,
     * or with status {@code 404 (Not Found)} if the clientSettingDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the clientSettingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/client-settings/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ClientSettingDTO> partialUpdateClientSetting(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ClientSettingDTO clientSettingDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ClientSetting partially : {}, {}", id, clientSettingDTO);
        if (clientSettingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, clientSettingDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!clientSettingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ClientSettingDTO> result = clientSettingService.partialUpdate(clientSettingDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, clientSettingDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /client-settings} : get all the clientSettings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clientSettings in body.
     */
    @GetMapping("/client-settings")
    public ResponseEntity<List<ClientSettingDTO>> getAllClientSettings(
        ClientSettingCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get ClientSettings by criteria: {}", criteria);
        Page<ClientSettingDTO> page = clientSettingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /client-settings/count} : count all the clientSettings.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/client-settings/count")
    public ResponseEntity<Long> countClientSettings(ClientSettingCriteria criteria) {
        log.debug("REST request to count ClientSettings by criteria: {}", criteria);
        return ResponseEntity.ok().body(clientSettingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /client-settings/:id} : get the "id" clientSetting.
     *
     * @param id the id of the clientSettingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientSettingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-settings/{id}")
    public ResponseEntity<ClientSettingDTO> getClientSetting(@PathVariable Long id) {
        log.debug("REST request to get ClientSetting : {}", id);
        Optional<ClientSettingDTO> clientSettingDTO = clientSettingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clientSettingDTO);
    }

    /**
     * {@code DELETE  /client-settings/:id} : delete the "id" clientSetting.
     *
     * @param id the id of the clientSettingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/client-settings/{id}")
    public ResponseEntity<Void> deleteClientSetting(@PathVariable Long id) {
        log.debug("REST request to delete ClientSetting : {}", id);
        clientSettingService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
