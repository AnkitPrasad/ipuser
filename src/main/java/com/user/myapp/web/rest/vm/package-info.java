/**
 * View Models used by Spring MVC REST controllers.
 */
package com.user.myapp.web.rest.vm;
