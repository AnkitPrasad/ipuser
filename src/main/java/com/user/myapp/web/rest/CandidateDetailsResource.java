package com.user.myapp.web.rest;

import com.user.myapp.repository.CandidateDetailsRepository;
import com.user.myapp.service.CandidateDetailsQueryService;
import com.user.myapp.service.CandidateDetailsService;
import com.user.myapp.service.criteria.CandidateDetailsCriteria;
import com.user.myapp.service.dto.CandidateDetailsDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.CandidateDetails}.
 */
@RestController
@RequestMapping("/api")
public class CandidateDetailsResource {

    private final Logger log = LoggerFactory.getLogger(CandidateDetailsResource.class);

    private static final String ENTITY_NAME = "ipuserCandidateDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CandidateDetailsService candidateDetailsService;

    private final CandidateDetailsRepository candidateDetailsRepository;

    private final CandidateDetailsQueryService candidateDetailsQueryService;

    public CandidateDetailsResource(
        CandidateDetailsService candidateDetailsService,
        CandidateDetailsRepository candidateDetailsRepository,
        CandidateDetailsQueryService candidateDetailsQueryService
    ) {
        this.candidateDetailsService = candidateDetailsService;
        this.candidateDetailsRepository = candidateDetailsRepository;
        this.candidateDetailsQueryService = candidateDetailsQueryService;
    }

    /**
     * {@code POST  /candidate-details} : Create a new candidateDetails.
     *
     * @param candidateDetailsDTO the candidateDetailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new candidateDetailsDTO, or with status {@code 400 (Bad Request)} if the candidateDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/candidate-details")
    public ResponseEntity<CandidateDetailsDTO> createCandidateDetails(@RequestBody CandidateDetailsDTO candidateDetailsDTO)
        throws URISyntaxException {
        log.debug("REST request to save CandidateDetails : {}", candidateDetailsDTO);
        if (candidateDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new candidateDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CandidateDetailsDTO result = candidateDetailsService.save(candidateDetailsDTO);
        return ResponseEntity
            .created(new URI("/api/candidate-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /candidate-details/:id} : Updates an existing candidateDetails.
     *
     * @param id the id of the candidateDetailsDTO to save.
     * @param candidateDetailsDTO the candidateDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated candidateDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the candidateDetailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the candidateDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/candidate-details/{id}")
    public ResponseEntity<CandidateDetailsDTO> updateCandidateDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CandidateDetailsDTO candidateDetailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CandidateDetails : {}, {}", id, candidateDetailsDTO);
        if (candidateDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, candidateDetailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!candidateDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CandidateDetailsDTO result = candidateDetailsService.update(candidateDetailsDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, candidateDetailsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /candidate-details/:id} : Partial updates given fields of an existing candidateDetails, field will ignore if it is null
     *
     * @param id the id of the candidateDetailsDTO to save.
     * @param candidateDetailsDTO the candidateDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated candidateDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the candidateDetailsDTO is not valid,
     * or with status {@code 404 (Not Found)} if the candidateDetailsDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the candidateDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/candidate-details/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CandidateDetailsDTO> partialUpdateCandidateDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CandidateDetailsDTO candidateDetailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CandidateDetails partially : {}, {}", id, candidateDetailsDTO);
        if (candidateDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, candidateDetailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!candidateDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CandidateDetailsDTO> result = candidateDetailsService.partialUpdate(candidateDetailsDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, candidateDetailsDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /candidate-details} : get all the candidateDetails.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of candidateDetails in body.
     */
    @GetMapping("/candidate-details")
    public ResponseEntity<List<CandidateDetailsDTO>> getAllCandidateDetails(
        CandidateDetailsCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get CandidateDetails by criteria: {}", criteria);
        Page<CandidateDetailsDTO> page = candidateDetailsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /candidate-details/count} : count all the candidateDetails.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/candidate-details/count")
    public ResponseEntity<Long> countCandidateDetails(CandidateDetailsCriteria criteria) {
        log.debug("REST request to count CandidateDetails by criteria: {}", criteria);
        return ResponseEntity.ok().body(candidateDetailsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /candidate-details/:id} : get the "id" candidateDetails.
     *
     * @param id the id of the candidateDetailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the candidateDetailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/candidate-details/{id}")
    public ResponseEntity<CandidateDetailsDTO> getCandidateDetails(@PathVariable Long id) {
        log.debug("REST request to get CandidateDetails : {}", id);
        Optional<CandidateDetailsDTO> candidateDetailsDTO = candidateDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(candidateDetailsDTO);
    }

    /**
     * {@code DELETE  /candidate-details/:id} : delete the "id" candidateDetails.
     *
     * @param id the id of the candidateDetailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/candidate-details/{id}")
    public ResponseEntity<Void> deleteCandidateDetails(@PathVariable Long id) {
        log.debug("REST request to delete CandidateDetails : {}", id);
        candidateDetailsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
