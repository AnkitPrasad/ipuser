package com.user.myapp.web.rest;

import com.user.myapp.repository.CandidateProfileRepository;
import com.user.myapp.service.CandidateProfileQueryService;
import com.user.myapp.service.CandidateProfileService;
import com.user.myapp.service.criteria.CandidateProfileCriteria;
import com.user.myapp.service.dto.CandidateProfileDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.CandidateProfile}.
 */
@RestController
@RequestMapping("/api")
public class CandidateProfileResource {

    private final Logger log = LoggerFactory.getLogger(CandidateProfileResource.class);

    private static final String ENTITY_NAME = "ipuserCandidateProfile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CandidateProfileService candidateProfileService;

    private final CandidateProfileRepository candidateProfileRepository;

    private final CandidateProfileQueryService candidateProfileQueryService;

    public CandidateProfileResource(
        CandidateProfileService candidateProfileService,
        CandidateProfileRepository candidateProfileRepository,
        CandidateProfileQueryService candidateProfileQueryService
    ) {
        this.candidateProfileService = candidateProfileService;
        this.candidateProfileRepository = candidateProfileRepository;
        this.candidateProfileQueryService = candidateProfileQueryService;
    }

    /**
     * {@code POST  /candidate-profiles} : Create a new candidateProfile.
     *
     * @param candidateProfileDTO the candidateProfileDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new candidateProfileDTO, or with status {@code 400 (Bad Request)} if the candidateProfile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/candidate-profiles")
    public ResponseEntity<CandidateProfileDTO> createCandidateProfile(@RequestBody CandidateProfileDTO candidateProfileDTO)
        throws URISyntaxException {
        log.debug("REST request to save CandidateProfile : {}", candidateProfileDTO);
        if (candidateProfileDTO.getId() != null) {
            throw new BadRequestAlertException("A new candidateProfile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CandidateProfileDTO result = candidateProfileService.save(candidateProfileDTO);
        return ResponseEntity
            .created(new URI("/api/candidate-profiles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /candidate-profiles/:id} : Updates an existing candidateProfile.
     *
     * @param id the id of the candidateProfileDTO to save.
     * @param candidateProfileDTO the candidateProfileDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated candidateProfileDTO,
     * or with status {@code 400 (Bad Request)} if the candidateProfileDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the candidateProfileDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/candidate-profiles/{id}")
    public ResponseEntity<CandidateProfileDTO> updateCandidateProfile(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CandidateProfileDTO candidateProfileDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CandidateProfile : {}, {}", id, candidateProfileDTO);
        if (candidateProfileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, candidateProfileDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!candidateProfileRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CandidateProfileDTO result = candidateProfileService.update(candidateProfileDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, candidateProfileDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /candidate-profiles/:id} : Partial updates given fields of an existing candidateProfile, field will ignore if it is null
     *
     * @param id the id of the candidateProfileDTO to save.
     * @param candidateProfileDTO the candidateProfileDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated candidateProfileDTO,
     * or with status {@code 400 (Bad Request)} if the candidateProfileDTO is not valid,
     * or with status {@code 404 (Not Found)} if the candidateProfileDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the candidateProfileDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/candidate-profiles/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CandidateProfileDTO> partialUpdateCandidateProfile(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CandidateProfileDTO candidateProfileDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CandidateProfile partially : {}, {}", id, candidateProfileDTO);
        if (candidateProfileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, candidateProfileDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!candidateProfileRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CandidateProfileDTO> result = candidateProfileService.partialUpdate(candidateProfileDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, candidateProfileDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /candidate-profiles} : get all the candidateProfiles.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of candidateProfiles in body.
     */
    @GetMapping("/candidate-profiles")
    public ResponseEntity<List<CandidateProfileDTO>> getAllCandidateProfiles(
        CandidateProfileCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get CandidateProfiles by criteria: {}", criteria);
        Page<CandidateProfileDTO> page = candidateProfileQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /candidate-profiles/count} : count all the candidateProfiles.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/candidate-profiles/count")
    public ResponseEntity<Long> countCandidateProfiles(CandidateProfileCriteria criteria) {
        log.debug("REST request to count CandidateProfiles by criteria: {}", criteria);
        return ResponseEntity.ok().body(candidateProfileQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /candidate-profiles/:id} : get the "id" candidateProfile.
     *
     * @param id the id of the candidateProfileDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the candidateProfileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/candidate-profiles/{id}")
    public ResponseEntity<CandidateProfileDTO> getCandidateProfile(@PathVariable Long id) {
        log.debug("REST request to get CandidateProfile : {}", id);
        Optional<CandidateProfileDTO> candidateProfileDTO = candidateProfileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(candidateProfileDTO);
    }

    /**
     * {@code DELETE  /candidate-profiles/:id} : delete the "id" candidateProfile.
     *
     * @param id the id of the candidateProfileDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/candidate-profiles/{id}")
    public ResponseEntity<Void> deleteCandidateProfile(@PathVariable Long id) {
        log.debug("REST request to delete CandidateProfile : {}", id);
        candidateProfileService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
