package com.user.myapp.web.rest;

import com.user.myapp.repository.LoginCountRepository;
import com.user.myapp.service.LoginCountQueryService;
import com.user.myapp.service.LoginCountService;
import com.user.myapp.service.criteria.LoginCountCriteria;
import com.user.myapp.service.dto.LoginCountDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.LoginCount}.
 */
@RestController
@RequestMapping("/api")
public class LoginCountResource {

    private final Logger log = LoggerFactory.getLogger(LoginCountResource.class);

    private static final String ENTITY_NAME = "ipuserLoginCount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoginCountService loginCountService;

    private final LoginCountRepository loginCountRepository;

    private final LoginCountQueryService loginCountQueryService;

    public LoginCountResource(
        LoginCountService loginCountService,
        LoginCountRepository loginCountRepository,
        LoginCountQueryService loginCountQueryService
    ) {
        this.loginCountService = loginCountService;
        this.loginCountRepository = loginCountRepository;
        this.loginCountQueryService = loginCountQueryService;
    }

    /**
     * {@code POST  /login-counts} : Create a new loginCount.
     *
     * @param loginCountDTO the loginCountDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loginCountDTO, or with status {@code 400 (Bad Request)} if the loginCount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/login-counts")
    public ResponseEntity<LoginCountDTO> createLoginCount(@RequestBody LoginCountDTO loginCountDTO) throws URISyntaxException {
        log.debug("REST request to save LoginCount : {}", loginCountDTO);
        if (loginCountDTO.getId() != null) {
            throw new BadRequestAlertException("A new loginCount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoginCountDTO result = loginCountService.save(loginCountDTO);
        return ResponseEntity
            .created(new URI("/api/login-counts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /login-counts/:id} : Updates an existing loginCount.
     *
     * @param id the id of the loginCountDTO to save.
     * @param loginCountDTO the loginCountDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loginCountDTO,
     * or with status {@code 400 (Bad Request)} if the loginCountDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loginCountDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/login-counts/{id}")
    public ResponseEntity<LoginCountDTO> updateLoginCount(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LoginCountDTO loginCountDTO
    ) throws URISyntaxException {
        log.debug("REST request to update LoginCount : {}, {}", id, loginCountDTO);
        if (loginCountDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loginCountDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loginCountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        LoginCountDTO result = loginCountService.update(loginCountDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, loginCountDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /login-counts/:id} : Partial updates given fields of an existing loginCount, field will ignore if it is null
     *
     * @param id the id of the loginCountDTO to save.
     * @param loginCountDTO the loginCountDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loginCountDTO,
     * or with status {@code 400 (Bad Request)} if the loginCountDTO is not valid,
     * or with status {@code 404 (Not Found)} if the loginCountDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the loginCountDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/login-counts/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<LoginCountDTO> partialUpdateLoginCount(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LoginCountDTO loginCountDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update LoginCount partially : {}, {}", id, loginCountDTO);
        if (loginCountDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loginCountDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loginCountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<LoginCountDTO> result = loginCountService.partialUpdate(loginCountDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, loginCountDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /login-counts} : get all the loginCounts.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loginCounts in body.
     */
    @GetMapping("/login-counts")
    public ResponseEntity<List<LoginCountDTO>> getAllLoginCounts(
        LoginCountCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get LoginCounts by criteria: {}", criteria);
        Page<LoginCountDTO> page = loginCountQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /login-counts/count} : count all the loginCounts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/login-counts/count")
    public ResponseEntity<Long> countLoginCounts(LoginCountCriteria criteria) {
        log.debug("REST request to count LoginCounts by criteria: {}", criteria);
        return ResponseEntity.ok().body(loginCountQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /login-counts/:id} : get the "id" loginCount.
     *
     * @param id the id of the loginCountDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loginCountDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/login-counts/{id}")
    public ResponseEntity<LoginCountDTO> getLoginCount(@PathVariable Long id) {
        log.debug("REST request to get LoginCount : {}", id);
        Optional<LoginCountDTO> loginCountDTO = loginCountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loginCountDTO);
    }

    /**
     * {@code DELETE  /login-counts/:id} : delete the "id" loginCount.
     *
     * @param id the id of the loginCountDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/login-counts/{id}")
    public ResponseEntity<Void> deleteLoginCount(@PathVariable Long id) {
        log.debug("REST request to delete LoginCount : {}", id);
        loginCountService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
