package com.user.myapp.web.rest;

import com.user.myapp.repository.InPreferenceUserRepository;
import com.user.myapp.service.InPreferenceUserQueryService;
import com.user.myapp.service.InPreferenceUserService;
import com.user.myapp.service.criteria.InPreferenceUserCriteria;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.InPreferenceUser}.
 */
@RestController
@RequestMapping("/api")
public class InPreferenceUserResource {

    private final Logger log = LoggerFactory.getLogger(InPreferenceUserResource.class);

    private static final String ENTITY_NAME = "ipuserInPreferenceUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InPreferenceUserService inPreferenceUserService;

    private final InPreferenceUserRepository inPreferenceUserRepository;

    private final InPreferenceUserQueryService inPreferenceUserQueryService;

    public InPreferenceUserResource(
        InPreferenceUserService inPreferenceUserService,
        InPreferenceUserRepository inPreferenceUserRepository,
        InPreferenceUserQueryService inPreferenceUserQueryService
    ) {
        this.inPreferenceUserService = inPreferenceUserService;
        this.inPreferenceUserRepository = inPreferenceUserRepository;
        this.inPreferenceUserQueryService = inPreferenceUserQueryService;
    }

    /**
     * {@code POST  /in-preference-users} : Create a new inPreferenceUser.
     *
     * @param inPreferenceUserDTO the inPreferenceUserDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new inPreferenceUserDTO, or with status {@code 400 (Bad Request)} if the inPreferenceUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/in-preference-users")
    public ResponseEntity<InPreferenceUserDTO> createInPreferenceUser(@Valid @RequestBody InPreferenceUserDTO inPreferenceUserDTO)
        throws URISyntaxException {
        log.debug("REST request to save InPreferenceUser : {}", inPreferenceUserDTO);
        if (inPreferenceUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new inPreferenceUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InPreferenceUserDTO result = inPreferenceUserService.save(inPreferenceUserDTO);
        return ResponseEntity
            .created(new URI("/api/in-preference-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /in-preference-users/:id} : Updates an existing inPreferenceUser.
     *
     * @param id the id of the inPreferenceUserDTO to save.
     * @param inPreferenceUserDTO the inPreferenceUserDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated inPreferenceUserDTO,
     * or with status {@code 400 (Bad Request)} if the inPreferenceUserDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the inPreferenceUserDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/in-preference-users/{id}")
    public ResponseEntity<InPreferenceUserDTO> updateInPreferenceUser(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody InPreferenceUserDTO inPreferenceUserDTO
    ) throws URISyntaxException {
        log.debug("REST request to update InPreferenceUser : {}, {}", id, inPreferenceUserDTO);
        if (inPreferenceUserDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, inPreferenceUserDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!inPreferenceUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        InPreferenceUserDTO result = inPreferenceUserService.update(inPreferenceUserDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, inPreferenceUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /in-preference-users/:id} : Partial updates given fields of an existing inPreferenceUser, field will ignore if it is null
     *
     * @param id the id of the inPreferenceUserDTO to save.
     * @param inPreferenceUserDTO the inPreferenceUserDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated inPreferenceUserDTO,
     * or with status {@code 400 (Bad Request)} if the inPreferenceUserDTO is not valid,
     * or with status {@code 404 (Not Found)} if the inPreferenceUserDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the inPreferenceUserDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/in-preference-users/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<InPreferenceUserDTO> partialUpdateInPreferenceUser(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody InPreferenceUserDTO inPreferenceUserDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update InPreferenceUser partially : {}, {}", id, inPreferenceUserDTO);
        if (inPreferenceUserDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, inPreferenceUserDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!inPreferenceUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<InPreferenceUserDTO> result = inPreferenceUserService.partialUpdate(inPreferenceUserDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, inPreferenceUserDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /in-preference-users} : get all the inPreferenceUsers.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of inPreferenceUsers in body.
     */
    @GetMapping("/in-preference-users")
    public ResponseEntity<List<InPreferenceUserDTO>> getAllInPreferenceUsers(
        InPreferenceUserCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get InPreferenceUsers by criteria: {}", criteria);
        Page<InPreferenceUserDTO> page = inPreferenceUserQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /in-preference-users/count} : count all the inPreferenceUsers.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/in-preference-users/count")
    public ResponseEntity<Long> countInPreferenceUsers(InPreferenceUserCriteria criteria) {
        log.debug("REST request to count InPreferenceUsers by criteria: {}", criteria);
        return ResponseEntity.ok().body(inPreferenceUserQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /in-preference-users/:id} : get the "id" inPreferenceUser.
     *
     * @param id the id of the inPreferenceUserDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the inPreferenceUserDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/in-preference-users/{id}")
    public ResponseEntity<InPreferenceUserDTO> getInPreferenceUser(@PathVariable Long id) {
        log.debug("REST request to get InPreferenceUser : {}", id);
        Optional<InPreferenceUserDTO> inPreferenceUserDTO = inPreferenceUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(inPreferenceUserDTO);
    }

    /**
     * {@code DELETE  /in-preference-users/:id} : delete the "id" inPreferenceUser.
     *
     * @param id the id of the inPreferenceUserDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/in-preference-users/{id}")
    public ResponseEntity<Void> deleteInPreferenceUser(@PathVariable Long id) {
        log.debug("REST request to delete InPreferenceUser : {}", id);
        inPreferenceUserService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
