package com.user.myapp.web.rest;

import com.user.myapp.repository.UserRolePermissionRepository;
import com.user.myapp.service.UserRolePermissionQueryService;
import com.user.myapp.service.UserRolePermissionService;
import com.user.myapp.service.criteria.UserRolePermissionCriteria;
import com.user.myapp.service.dto.UserRolePermissionDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.UserRolePermission}.
 */
@RestController
@RequestMapping("/api")
public class UserRolePermissionResource {

    private final Logger log = LoggerFactory.getLogger(UserRolePermissionResource.class);

    private static final String ENTITY_NAME = "ipuserUserRolePermission";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserRolePermissionService userRolePermissionService;

    private final UserRolePermissionRepository userRolePermissionRepository;

    private final UserRolePermissionQueryService userRolePermissionQueryService;

    public UserRolePermissionResource(
        UserRolePermissionService userRolePermissionService,
        UserRolePermissionRepository userRolePermissionRepository,
        UserRolePermissionQueryService userRolePermissionQueryService
    ) {
        this.userRolePermissionService = userRolePermissionService;
        this.userRolePermissionRepository = userRolePermissionRepository;
        this.userRolePermissionQueryService = userRolePermissionQueryService;
    }

    /**
     * {@code POST  /user-role-permissions} : Create a new userRolePermission.
     *
     * @param userRolePermissionDTO the userRolePermissionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userRolePermissionDTO, or with status {@code 400 (Bad Request)} if the userRolePermission has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-role-permissions")
    public ResponseEntity<UserRolePermissionDTO> createUserRolePermission(@RequestBody UserRolePermissionDTO userRolePermissionDTO)
        throws URISyntaxException {
        log.debug("REST request to save UserRolePermission : {}", userRolePermissionDTO);
        if (userRolePermissionDTO.getId() != null) {
            throw new BadRequestAlertException("A new userRolePermission cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserRolePermissionDTO result = userRolePermissionService.save(userRolePermissionDTO);
        return ResponseEntity
            .created(new URI("/api/user-role-permissions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-role-permissions/:id} : Updates an existing userRolePermission.
     *
     * @param id the id of the userRolePermissionDTO to save.
     * @param userRolePermissionDTO the userRolePermissionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userRolePermissionDTO,
     * or with status {@code 400 (Bad Request)} if the userRolePermissionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userRolePermissionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-role-permissions/{id}")
    public ResponseEntity<UserRolePermissionDTO> updateUserRolePermission(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserRolePermissionDTO userRolePermissionDTO
    ) throws URISyntaxException {
        log.debug("REST request to update UserRolePermission : {}, {}", id, userRolePermissionDTO);
        if (userRolePermissionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userRolePermissionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userRolePermissionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserRolePermissionDTO result = userRolePermissionService.update(userRolePermissionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userRolePermissionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /user-role-permissions/:id} : Partial updates given fields of an existing userRolePermission, field will ignore if it is null
     *
     * @param id the id of the userRolePermissionDTO to save.
     * @param userRolePermissionDTO the userRolePermissionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userRolePermissionDTO,
     * or with status {@code 400 (Bad Request)} if the userRolePermissionDTO is not valid,
     * or with status {@code 404 (Not Found)} if the userRolePermissionDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the userRolePermissionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/user-role-permissions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<UserRolePermissionDTO> partialUpdateUserRolePermission(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserRolePermissionDTO userRolePermissionDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserRolePermission partially : {}, {}", id, userRolePermissionDTO);
        if (userRolePermissionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userRolePermissionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userRolePermissionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserRolePermissionDTO> result = userRolePermissionService.partialUpdate(userRolePermissionDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userRolePermissionDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /user-role-permissions} : get all the userRolePermissions.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userRolePermissions in body.
     */
    @GetMapping("/user-role-permissions")
    public ResponseEntity<List<UserRolePermissionDTO>> getAllUserRolePermissions(
        UserRolePermissionCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get UserRolePermissions by criteria: {}", criteria);
        Page<UserRolePermissionDTO> page = userRolePermissionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-role-permissions/count} : count all the userRolePermissions.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/user-role-permissions/count")
    public ResponseEntity<Long> countUserRolePermissions(UserRolePermissionCriteria criteria) {
        log.debug("REST request to count UserRolePermissions by criteria: {}", criteria);
        return ResponseEntity.ok().body(userRolePermissionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-role-permissions/:id} : get the "id" userRolePermission.
     *
     * @param id the id of the userRolePermissionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userRolePermissionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-role-permissions/{id}")
    public ResponseEntity<UserRolePermissionDTO> getUserRolePermission(@PathVariable Long id) {
        log.debug("REST request to get UserRolePermission : {}", id);
        Optional<UserRolePermissionDTO> userRolePermissionDTO = userRolePermissionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userRolePermissionDTO);
    }

    /**
     * {@code DELETE  /user-role-permissions/:id} : delete the "id" userRolePermission.
     *
     * @param id the id of the userRolePermissionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-role-permissions/{id}")
    public ResponseEntity<Void> deleteUserRolePermission(@PathVariable Long id) {
        log.debug("REST request to delete UserRolePermission : {}", id);
        userRolePermissionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
