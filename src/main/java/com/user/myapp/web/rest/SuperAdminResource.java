package com.user.myapp.web.rest;

import com.user.myapp.repository.SuperAdminRepository;
import com.user.myapp.service.SuperAdminQueryService;
import com.user.myapp.service.SuperAdminService;
import com.user.myapp.service.criteria.SuperAdminCriteria;
import com.user.myapp.service.dto.SuperAdminDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.SuperAdmin}.
 */
@RestController
@RequestMapping("/api")
public class SuperAdminResource {

    private final Logger log = LoggerFactory.getLogger(SuperAdminResource.class);

    private static final String ENTITY_NAME = "ipuserSuperAdmin";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SuperAdminService superAdminService;

    private final SuperAdminRepository superAdminRepository;

    private final SuperAdminQueryService superAdminQueryService;

    public SuperAdminResource(
        SuperAdminService superAdminService,
        SuperAdminRepository superAdminRepository,
        SuperAdminQueryService superAdminQueryService
    ) {
        this.superAdminService = superAdminService;
        this.superAdminRepository = superAdminRepository;
        this.superAdminQueryService = superAdminQueryService;
    }

    /**
     * {@code POST  /super-admins} : Create a new superAdmin.
     *
     * @param superAdminDTO the superAdminDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new superAdminDTO, or with status {@code 400 (Bad Request)} if the superAdmin has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/super-admins")
    public ResponseEntity<SuperAdminDTO> createSuperAdmin(@RequestBody SuperAdminDTO superAdminDTO) throws URISyntaxException {
        log.debug("REST request to save SuperAdmin : {}", superAdminDTO);
        if (superAdminDTO.getId() != null) {
            throw new BadRequestAlertException("A new superAdmin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SuperAdminDTO result = superAdminService.save(superAdminDTO);
        return ResponseEntity
            .created(new URI("/api/super-admins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /super-admins/:id} : Updates an existing superAdmin.
     *
     * @param id the id of the superAdminDTO to save.
     * @param superAdminDTO the superAdminDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated superAdminDTO,
     * or with status {@code 400 (Bad Request)} if the superAdminDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the superAdminDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/super-admins/{id}")
    public ResponseEntity<SuperAdminDTO> updateSuperAdmin(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SuperAdminDTO superAdminDTO
    ) throws URISyntaxException {
        log.debug("REST request to update SuperAdmin : {}, {}", id, superAdminDTO);
        if (superAdminDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, superAdminDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!superAdminRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SuperAdminDTO result = superAdminService.update(superAdminDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, superAdminDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /super-admins/:id} : Partial updates given fields of an existing superAdmin, field will ignore if it is null
     *
     * @param id the id of the superAdminDTO to save.
     * @param superAdminDTO the superAdminDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated superAdminDTO,
     * or with status {@code 400 (Bad Request)} if the superAdminDTO is not valid,
     * or with status {@code 404 (Not Found)} if the superAdminDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the superAdminDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/super-admins/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SuperAdminDTO> partialUpdateSuperAdmin(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SuperAdminDTO superAdminDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update SuperAdmin partially : {}, {}", id, superAdminDTO);
        if (superAdminDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, superAdminDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!superAdminRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SuperAdminDTO> result = superAdminService.partialUpdate(superAdminDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, superAdminDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /super-admins} : get all the superAdmins.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of superAdmins in body.
     */
    @GetMapping("/super-admins")
    public ResponseEntity<List<SuperAdminDTO>> getAllSuperAdmins(
        SuperAdminCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get SuperAdmins by criteria: {}", criteria);
        Page<SuperAdminDTO> page = superAdminQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /super-admins/count} : count all the superAdmins.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/super-admins/count")
    public ResponseEntity<Long> countSuperAdmins(SuperAdminCriteria criteria) {
        log.debug("REST request to count SuperAdmins by criteria: {}", criteria);
        return ResponseEntity.ok().body(superAdminQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /super-admins/:id} : get the "id" superAdmin.
     *
     * @param id the id of the superAdminDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the superAdminDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/super-admins/{id}")
    public ResponseEntity<SuperAdminDTO> getSuperAdmin(@PathVariable Long id) {
        log.debug("REST request to get SuperAdmin : {}", id);
        Optional<SuperAdminDTO> superAdminDTO = superAdminService.findOne(id);
        return ResponseUtil.wrapOrNotFound(superAdminDTO);
    }

    /**
     * {@code DELETE  /super-admins/:id} : delete the "id" superAdmin.
     *
     * @param id the id of the superAdminDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/super-admins/{id}")
    public ResponseEntity<Void> deleteSuperAdmin(@PathVariable Long id) {
        log.debug("REST request to delete SuperAdmin : {}", id);
        superAdminService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
