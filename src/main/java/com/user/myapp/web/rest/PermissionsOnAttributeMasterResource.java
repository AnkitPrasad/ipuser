package com.user.myapp.web.rest;

import com.user.myapp.repository.PermissionsOnAttributeMasterRepository;
import com.user.myapp.service.PermissionsOnAttributeMasterQueryService;
import com.user.myapp.service.PermissionsOnAttributeMasterService;
import com.user.myapp.service.criteria.PermissionsOnAttributeMasterCriteria;
import com.user.myapp.service.dto.PermissionsOnAttributeMasterDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.PermissionsOnAttributeMaster}.
 */
@RestController
@RequestMapping("/api")
public class PermissionsOnAttributeMasterResource {

    private final Logger log = LoggerFactory.getLogger(PermissionsOnAttributeMasterResource.class);

    private static final String ENTITY_NAME = "ipuserPermissionsOnAttributeMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PermissionsOnAttributeMasterService permissionsOnAttributeMasterService;

    private final PermissionsOnAttributeMasterRepository permissionsOnAttributeMasterRepository;

    private final PermissionsOnAttributeMasterQueryService permissionsOnAttributeMasterQueryService;

    public PermissionsOnAttributeMasterResource(
        PermissionsOnAttributeMasterService permissionsOnAttributeMasterService,
        PermissionsOnAttributeMasterRepository permissionsOnAttributeMasterRepository,
        PermissionsOnAttributeMasterQueryService permissionsOnAttributeMasterQueryService
    ) {
        this.permissionsOnAttributeMasterService = permissionsOnAttributeMasterService;
        this.permissionsOnAttributeMasterRepository = permissionsOnAttributeMasterRepository;
        this.permissionsOnAttributeMasterQueryService = permissionsOnAttributeMasterQueryService;
    }

    /**
     * {@code POST  /permissions-on-attribute-masters} : Create a new permissionsOnAttributeMaster.
     *
     * @param permissionsOnAttributeMasterDTO the permissionsOnAttributeMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new permissionsOnAttributeMasterDTO, or with status {@code 400 (Bad Request)} if the permissionsOnAttributeMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/permissions-on-attribute-masters")
    public ResponseEntity<PermissionsOnAttributeMasterDTO> createPermissionsOnAttributeMaster(
        @RequestBody PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO
    ) throws URISyntaxException {
        log.debug("REST request to save PermissionsOnAttributeMaster : {}", permissionsOnAttributeMasterDTO);
        if (permissionsOnAttributeMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new permissionsOnAttributeMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PermissionsOnAttributeMasterDTO result = permissionsOnAttributeMasterService.save(permissionsOnAttributeMasterDTO);
        return ResponseEntity
            .created(new URI("/api/permissions-on-attribute-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /permissions-on-attribute-masters/:id} : Updates an existing permissionsOnAttributeMaster.
     *
     * @param id the id of the permissionsOnAttributeMasterDTO to save.
     * @param permissionsOnAttributeMasterDTO the permissionsOnAttributeMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated permissionsOnAttributeMasterDTO,
     * or with status {@code 400 (Bad Request)} if the permissionsOnAttributeMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the permissionsOnAttributeMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/permissions-on-attribute-masters/{id}")
    public ResponseEntity<PermissionsOnAttributeMasterDTO> updatePermissionsOnAttributeMaster(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PermissionsOnAttributeMaster : {}, {}", id, permissionsOnAttributeMasterDTO);
        if (permissionsOnAttributeMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, permissionsOnAttributeMasterDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!permissionsOnAttributeMasterRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PermissionsOnAttributeMasterDTO result = permissionsOnAttributeMasterService.update(permissionsOnAttributeMasterDTO);
        return ResponseEntity
            .ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, permissionsOnAttributeMasterDTO.getId().toString())
            )
            .body(result);
    }

    /**
     * {@code PATCH  /permissions-on-attribute-masters/:id} : Partial updates given fields of an existing permissionsOnAttributeMaster, field will ignore if it is null
     *
     * @param id the id of the permissionsOnAttributeMasterDTO to save.
     * @param permissionsOnAttributeMasterDTO the permissionsOnAttributeMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated permissionsOnAttributeMasterDTO,
     * or with status {@code 400 (Bad Request)} if the permissionsOnAttributeMasterDTO is not valid,
     * or with status {@code 404 (Not Found)} if the permissionsOnAttributeMasterDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the permissionsOnAttributeMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/permissions-on-attribute-masters/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PermissionsOnAttributeMasterDTO> partialUpdatePermissionsOnAttributeMaster(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PermissionsOnAttributeMaster partially : {}, {}", id, permissionsOnAttributeMasterDTO);
        if (permissionsOnAttributeMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, permissionsOnAttributeMasterDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!permissionsOnAttributeMasterRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PermissionsOnAttributeMasterDTO> result = permissionsOnAttributeMasterService.partialUpdate(
            permissionsOnAttributeMasterDTO
        );

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, permissionsOnAttributeMasterDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /permissions-on-attribute-masters} : get all the permissionsOnAttributeMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of permissionsOnAttributeMasters in body.
     */
    @GetMapping("/permissions-on-attribute-masters")
    public ResponseEntity<List<PermissionsOnAttributeMasterDTO>> getAllPermissionsOnAttributeMasters(
        PermissionsOnAttributeMasterCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get PermissionsOnAttributeMasters by criteria: {}", criteria);
        Page<PermissionsOnAttributeMasterDTO> page = permissionsOnAttributeMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /permissions-on-attribute-masters/count} : count all the permissionsOnAttributeMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/permissions-on-attribute-masters/count")
    public ResponseEntity<Long> countPermissionsOnAttributeMasters(PermissionsOnAttributeMasterCriteria criteria) {
        log.debug("REST request to count PermissionsOnAttributeMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(permissionsOnAttributeMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /permissions-on-attribute-masters/:id} : get the "id" permissionsOnAttributeMaster.
     *
     * @param id the id of the permissionsOnAttributeMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the permissionsOnAttributeMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/permissions-on-attribute-masters/{id}")
    public ResponseEntity<PermissionsOnAttributeMasterDTO> getPermissionsOnAttributeMaster(@PathVariable Long id) {
        log.debug("REST request to get PermissionsOnAttributeMaster : {}", id);
        Optional<PermissionsOnAttributeMasterDTO> permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(permissionsOnAttributeMasterDTO);
    }

    /**
     * {@code DELETE  /permissions-on-attribute-masters/:id} : delete the "id" permissionsOnAttributeMaster.
     *
     * @param id the id of the permissionsOnAttributeMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/permissions-on-attribute-masters/{id}")
    public ResponseEntity<Void> deletePermissionsOnAttributeMaster(@PathVariable Long id) {
        log.debug("REST request to delete PermissionsOnAttributeMaster : {}", id);
        permissionsOnAttributeMasterService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
