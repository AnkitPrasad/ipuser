package com.user.myapp.web.rest;

import com.user.myapp.repository.UserEmailsRepository;
import com.user.myapp.service.UserEmailsQueryService;
import com.user.myapp.service.UserEmailsService;
import com.user.myapp.service.criteria.UserEmailsCriteria;
import com.user.myapp.service.dto.UserEmailsDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.UserEmails}.
 */
@RestController
@RequestMapping("/api")
public class UserEmailsResource {

    private final Logger log = LoggerFactory.getLogger(UserEmailsResource.class);

    private static final String ENTITY_NAME = "ipuserUserEmails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserEmailsService userEmailsService;

    private final UserEmailsRepository userEmailsRepository;

    private final UserEmailsQueryService userEmailsQueryService;

    public UserEmailsResource(
        UserEmailsService userEmailsService,
        UserEmailsRepository userEmailsRepository,
        UserEmailsQueryService userEmailsQueryService
    ) {
        this.userEmailsService = userEmailsService;
        this.userEmailsRepository = userEmailsRepository;
        this.userEmailsQueryService = userEmailsQueryService;
    }

    /**
     * {@code POST  /user-emails} : Create a new userEmails.
     *
     * @param userEmailsDTO the userEmailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userEmailsDTO, or with status {@code 400 (Bad Request)} if the userEmails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-emails")
    public ResponseEntity<UserEmailsDTO> createUserEmails(@RequestBody UserEmailsDTO userEmailsDTO) throws URISyntaxException {
        log.debug("REST request to save UserEmails : {}", userEmailsDTO);
        if (userEmailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new userEmails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserEmailsDTO result = userEmailsService.save(userEmailsDTO);
        return ResponseEntity
            .created(new URI("/api/user-emails/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-emails/:id} : Updates an existing userEmails.
     *
     * @param id the id of the userEmailsDTO to save.
     * @param userEmailsDTO the userEmailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userEmailsDTO,
     * or with status {@code 400 (Bad Request)} if the userEmailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userEmailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-emails/{id}")
    public ResponseEntity<UserEmailsDTO> updateUserEmails(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserEmailsDTO userEmailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to update UserEmails : {}, {}", id, userEmailsDTO);
        if (userEmailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userEmailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userEmailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserEmailsDTO result = userEmailsService.update(userEmailsDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userEmailsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /user-emails/:id} : Partial updates given fields of an existing userEmails, field will ignore if it is null
     *
     * @param id the id of the userEmailsDTO to save.
     * @param userEmailsDTO the userEmailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userEmailsDTO,
     * or with status {@code 400 (Bad Request)} if the userEmailsDTO is not valid,
     * or with status {@code 404 (Not Found)} if the userEmailsDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the userEmailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/user-emails/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<UserEmailsDTO> partialUpdateUserEmails(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserEmailsDTO userEmailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserEmails partially : {}, {}", id, userEmailsDTO);
        if (userEmailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userEmailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userEmailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserEmailsDTO> result = userEmailsService.partialUpdate(userEmailsDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userEmailsDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /user-emails} : get all the userEmails.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userEmails in body.
     */
    @GetMapping("/user-emails")
    public ResponseEntity<List<UserEmailsDTO>> getAllUserEmails(
        UserEmailsCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get UserEmails by criteria: {}", criteria);
        Page<UserEmailsDTO> page = userEmailsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-emails/count} : count all the userEmails.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/user-emails/count")
    public ResponseEntity<Long> countUserEmails(UserEmailsCriteria criteria) {
        log.debug("REST request to count UserEmails by criteria: {}", criteria);
        return ResponseEntity.ok().body(userEmailsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-emails/:id} : get the "id" userEmails.
     *
     * @param id the id of the userEmailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userEmailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-emails/{id}")
    public ResponseEntity<UserEmailsDTO> getUserEmails(@PathVariable Long id) {
        log.debug("REST request to get UserEmails : {}", id);
        Optional<UserEmailsDTO> userEmailsDTO = userEmailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userEmailsDTO);
    }

    /**
     * {@code DELETE  /user-emails/:id} : delete the "id" userEmails.
     *
     * @param id the id of the userEmailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-emails/{id}")
    public ResponseEntity<Void> deleteUserEmails(@PathVariable Long id) {
        log.debug("REST request to delete UserEmails : {}", id);
        userEmailsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
