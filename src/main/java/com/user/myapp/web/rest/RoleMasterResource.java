package com.user.myapp.web.rest;

import com.user.myapp.repository.RoleMasterRepository;
import com.user.myapp.service.RoleMasterQueryService;
import com.user.myapp.service.RoleMasterService;
import com.user.myapp.service.criteria.RoleMasterCriteria;
import com.user.myapp.service.dto.RoleMasterDTO;
import com.user.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.user.myapp.domain.RoleMaster}.
 */
@RestController
@RequestMapping("/api")
public class RoleMasterResource {

    private final Logger log = LoggerFactory.getLogger(RoleMasterResource.class);

    private static final String ENTITY_NAME = "ipuserRoleMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RoleMasterService roleMasterService;

    private final RoleMasterRepository roleMasterRepository;

    private final RoleMasterQueryService roleMasterQueryService;

    public RoleMasterResource(
        RoleMasterService roleMasterService,
        RoleMasterRepository roleMasterRepository,
        RoleMasterQueryService roleMasterQueryService
    ) {
        this.roleMasterService = roleMasterService;
        this.roleMasterRepository = roleMasterRepository;
        this.roleMasterQueryService = roleMasterQueryService;
    }

    /**
     * {@code POST  /role-masters} : Create a new roleMaster.
     *
     * @param roleMasterDTO the roleMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new roleMasterDTO, or with status {@code 400 (Bad Request)} if the roleMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/role-masters")
    public ResponseEntity<RoleMasterDTO> createRoleMaster(@RequestBody RoleMasterDTO roleMasterDTO) throws URISyntaxException {
        log.debug("REST request to save RoleMaster : {}", roleMasterDTO);
        if (roleMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new roleMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RoleMasterDTO result = roleMasterService.save(roleMasterDTO);
        return ResponseEntity
            .created(new URI("/api/role-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /role-masters/:id} : Updates an existing roleMaster.
     *
     * @param id the id of the roleMasterDTO to save.
     * @param roleMasterDTO the roleMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated roleMasterDTO,
     * or with status {@code 400 (Bad Request)} if the roleMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the roleMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/role-masters/{id}")
    public ResponseEntity<RoleMasterDTO> updateRoleMaster(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RoleMasterDTO roleMasterDTO
    ) throws URISyntaxException {
        log.debug("REST request to update RoleMaster : {}, {}", id, roleMasterDTO);
        if (roleMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, roleMasterDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!roleMasterRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RoleMasterDTO result = roleMasterService.update(roleMasterDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, roleMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /role-masters/:id} : Partial updates given fields of an existing roleMaster, field will ignore if it is null
     *
     * @param id the id of the roleMasterDTO to save.
     * @param roleMasterDTO the roleMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated roleMasterDTO,
     * or with status {@code 400 (Bad Request)} if the roleMasterDTO is not valid,
     * or with status {@code 404 (Not Found)} if the roleMasterDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the roleMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/role-masters/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RoleMasterDTO> partialUpdateRoleMaster(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RoleMasterDTO roleMasterDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update RoleMaster partially : {}, {}", id, roleMasterDTO);
        if (roleMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, roleMasterDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!roleMasterRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RoleMasterDTO> result = roleMasterService.partialUpdate(roleMasterDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, roleMasterDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /role-masters} : get all the roleMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of roleMasters in body.
     */
    @GetMapping("/role-masters")
    public ResponseEntity<List<RoleMasterDTO>> getAllRoleMasters(
        RoleMasterCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get RoleMasters by criteria: {}", criteria);
        Page<RoleMasterDTO> page = roleMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /role-masters/count} : count all the roleMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/role-masters/count")
    public ResponseEntity<Long> countRoleMasters(RoleMasterCriteria criteria) {
        log.debug("REST request to count RoleMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(roleMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /role-masters/:id} : get the "id" roleMaster.
     *
     * @param id the id of the roleMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the roleMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/role-masters/{id}")
    public ResponseEntity<RoleMasterDTO> getRoleMaster(@PathVariable Long id) {
        log.debug("REST request to get RoleMaster : {}", id);
        Optional<RoleMasterDTO> roleMasterDTO = roleMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(roleMasterDTO);
    }

    /**
     * {@code DELETE  /role-masters/:id} : delete the "id" roleMaster.
     *
     * @param id the id of the roleMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/role-masters/{id}")
    public ResponseEntity<Void> deleteRoleMaster(@PathVariable Long id) {
        log.debug("REST request to delete RoleMaster : {}", id);
        roleMasterService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
