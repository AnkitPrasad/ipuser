package com.user.myapp.repository;

import com.user.myapp.domain.PermissionMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the PermissionMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PermissionMasterRepository extends JpaRepository<PermissionMaster, Long>, JpaSpecificationExecutor<PermissionMaster> {}
