package com.user.myapp.repository;

import com.user.myapp.domain.RoleMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the RoleMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoleMasterRepository extends JpaRepository<RoleMaster, Long>, JpaSpecificationExecutor<RoleMaster> {}
