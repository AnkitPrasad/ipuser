package com.user.myapp.repository;

import com.user.myapp.domain.LoginCount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the LoginCount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoginCountRepository extends JpaRepository<LoginCount, Long>, JpaSpecificationExecutor<LoginCount> {}
