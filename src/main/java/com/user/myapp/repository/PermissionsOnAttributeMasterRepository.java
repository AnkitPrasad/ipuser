package com.user.myapp.repository;

import com.user.myapp.domain.PermissionsOnAttributeMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the PermissionsOnAttributeMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PermissionsOnAttributeMasterRepository
    extends JpaRepository<PermissionsOnAttributeMaster, Long>, JpaSpecificationExecutor<PermissionsOnAttributeMaster> {}
