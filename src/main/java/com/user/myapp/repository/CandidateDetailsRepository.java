package com.user.myapp.repository;

import com.user.myapp.domain.CandidateDetails;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the CandidateDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CandidateDetailsRepository extends JpaRepository<CandidateDetails, Long>, JpaSpecificationExecutor<CandidateDetails> {}
