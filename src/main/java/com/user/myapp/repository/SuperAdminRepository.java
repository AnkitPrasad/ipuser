package com.user.myapp.repository;

import com.user.myapp.domain.SuperAdmin;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the SuperAdmin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SuperAdminRepository extends JpaRepository<SuperAdmin, Long>, JpaSpecificationExecutor<SuperAdmin> {}
