package com.user.myapp.repository;

import com.user.myapp.domain.UserPermissionOnAttribute;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the UserPermissionOnAttribute entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserPermissionOnAttributeRepository
    extends JpaRepository<UserPermissionOnAttribute, Long>, JpaSpecificationExecutor<UserPermissionOnAttribute> {}
