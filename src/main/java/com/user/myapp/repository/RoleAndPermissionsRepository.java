package com.user.myapp.repository;

import com.user.myapp.domain.RoleAndPermissions;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the RoleAndPermissions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoleAndPermissionsRepository
    extends JpaRepository<RoleAndPermissions, Long>, JpaSpecificationExecutor<RoleAndPermissions> {}
