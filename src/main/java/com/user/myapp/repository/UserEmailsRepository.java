package com.user.myapp.repository;

import com.user.myapp.domain.UserEmails;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the UserEmails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserEmailsRepository extends JpaRepository<UserEmails, Long>, JpaSpecificationExecutor<UserEmails> {}
