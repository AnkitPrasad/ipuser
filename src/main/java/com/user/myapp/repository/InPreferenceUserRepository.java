package com.user.myapp.repository;

import com.user.myapp.domain.InPreferenceUser;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the InPreferenceUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InPreferenceUserRepository extends JpaRepository<InPreferenceUser, Long>, JpaSpecificationExecutor<InPreferenceUser> {}
