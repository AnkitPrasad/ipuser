package com.user.myapp.repository;

import com.user.myapp.domain.UserRolePermission;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the UserRolePermission entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserRolePermissionRepository
    extends JpaRepository<UserRolePermission, Long>, JpaSpecificationExecutor<UserRolePermission> {}
