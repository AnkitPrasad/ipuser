package com.user.myapp.repository;

import com.user.myapp.domain.CandidateProfile;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the CandidateProfile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CandidateProfileRepository extends JpaRepository<CandidateProfile, Long>, JpaSpecificationExecutor<CandidateProfile> {}
