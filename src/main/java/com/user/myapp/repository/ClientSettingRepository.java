package com.user.myapp.repository;

import com.user.myapp.domain.ClientSetting;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ClientSetting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientSettingRepository extends JpaRepository<ClientSetting, Long>, JpaSpecificationExecutor<ClientSetting> {}
