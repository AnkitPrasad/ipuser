package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.UserPermissionOnAttribute;
import com.user.myapp.repository.UserPermissionOnAttributeRepository;
import com.user.myapp.service.criteria.UserPermissionOnAttributeCriteria;
import com.user.myapp.service.dto.UserPermissionOnAttributeDTO;
import com.user.myapp.service.mapper.UserPermissionOnAttributeMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link UserPermissionOnAttribute} entities in the database.
 * The main input is a {@link UserPermissionOnAttributeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserPermissionOnAttributeDTO} or a {@link Page} of {@link UserPermissionOnAttributeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserPermissionOnAttributeQueryService extends QueryService<UserPermissionOnAttribute> {

    private final Logger log = LoggerFactory.getLogger(UserPermissionOnAttributeQueryService.class);

    private final UserPermissionOnAttributeRepository userPermissionOnAttributeRepository;

    private final UserPermissionOnAttributeMapper userPermissionOnAttributeMapper;

    public UserPermissionOnAttributeQueryService(
        UserPermissionOnAttributeRepository userPermissionOnAttributeRepository,
        UserPermissionOnAttributeMapper userPermissionOnAttributeMapper
    ) {
        this.userPermissionOnAttributeRepository = userPermissionOnAttributeRepository;
        this.userPermissionOnAttributeMapper = userPermissionOnAttributeMapper;
    }

    /**
     * Return a {@link List} of {@link UserPermissionOnAttributeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserPermissionOnAttributeDTO> findByCriteria(UserPermissionOnAttributeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserPermissionOnAttribute> specification = createSpecification(criteria);
        return userPermissionOnAttributeMapper.toDto(userPermissionOnAttributeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserPermissionOnAttributeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserPermissionOnAttributeDTO> findByCriteria(UserPermissionOnAttributeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserPermissionOnAttribute> specification = createSpecification(criteria);
        return userPermissionOnAttributeRepository.findAll(specification, page).map(userPermissionOnAttributeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserPermissionOnAttributeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserPermissionOnAttribute> specification = createSpecification(criteria);
        return userPermissionOnAttributeRepository.count(specification);
    }

    /**
     * Function to convert {@link UserPermissionOnAttributeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UserPermissionOnAttribute> createSpecification(UserPermissionOnAttributeCriteria criteria) {
        Specification<UserPermissionOnAttribute> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UserPermissionOnAttribute_.id));
            }
            if (criteria.getRoleMasterId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRoleMasterId(),
                            root -> root.join(UserPermissionOnAttribute_.roleMaster, JoinType.LEFT).get(RoleMaster_.id)
                        )
                    );
            }
            if (criteria.getPermissionsOnAttributeMasterId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPermissionsOnAttributeMasterId(),
                            root ->
                                root
                                    .join(UserPermissionOnAttribute_.permissionsOnAttributeMaster, JoinType.LEFT)
                                    .get(PermissionsOnAttributeMaster_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
