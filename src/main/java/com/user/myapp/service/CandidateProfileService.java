package com.user.myapp.service;

import com.user.myapp.service.dto.CandidateProfileDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.CandidateProfile}.
 */
public interface CandidateProfileService {
    /**
     * Save a candidateProfile.
     *
     * @param candidateProfileDTO the entity to save.
     * @return the persisted entity.
     */
    CandidateProfileDTO save(CandidateProfileDTO candidateProfileDTO);

    /**
     * Updates a candidateProfile.
     *
     * @param candidateProfileDTO the entity to update.
     * @return the persisted entity.
     */
    CandidateProfileDTO update(CandidateProfileDTO candidateProfileDTO);

    /**
     * Partially updates a candidateProfile.
     *
     * @param candidateProfileDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CandidateProfileDTO> partialUpdate(CandidateProfileDTO candidateProfileDTO);

    /**
     * Get all the candidateProfiles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CandidateProfileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" candidateProfile.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CandidateProfileDTO> findOne(Long id);

    /**
     * Delete the "id" candidateProfile.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
