package com.user.myapp.service;

import com.user.myapp.service.dto.LoginCountDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.LoginCount}.
 */
public interface LoginCountService {
    /**
     * Save a loginCount.
     *
     * @param loginCountDTO the entity to save.
     * @return the persisted entity.
     */
    LoginCountDTO save(LoginCountDTO loginCountDTO);

    /**
     * Updates a loginCount.
     *
     * @param loginCountDTO the entity to update.
     * @return the persisted entity.
     */
    LoginCountDTO update(LoginCountDTO loginCountDTO);

    /**
     * Partially updates a loginCount.
     *
     * @param loginCountDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<LoginCountDTO> partialUpdate(LoginCountDTO loginCountDTO);

    /**
     * Get all the loginCounts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LoginCountDTO> findAll(Pageable pageable);

    /**
     * Get the "id" loginCount.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LoginCountDTO> findOne(Long id);

    /**
     * Delete the "id" loginCount.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
