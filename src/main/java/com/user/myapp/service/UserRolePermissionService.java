package com.user.myapp.service;

import com.user.myapp.service.dto.UserRolePermissionDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.UserRolePermission}.
 */
public interface UserRolePermissionService {
    /**
     * Save a userRolePermission.
     *
     * @param userRolePermissionDTO the entity to save.
     * @return the persisted entity.
     */
    UserRolePermissionDTO save(UserRolePermissionDTO userRolePermissionDTO);

    /**
     * Updates a userRolePermission.
     *
     * @param userRolePermissionDTO the entity to update.
     * @return the persisted entity.
     */
    UserRolePermissionDTO update(UserRolePermissionDTO userRolePermissionDTO);

    /**
     * Partially updates a userRolePermission.
     *
     * @param userRolePermissionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<UserRolePermissionDTO> partialUpdate(UserRolePermissionDTO userRolePermissionDTO);

    /**
     * Get all the userRolePermissions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserRolePermissionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" userRolePermission.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserRolePermissionDTO> findOne(Long id);

    /**
     * Delete the "id" userRolePermission.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
