package com.user.myapp.service.mapper;

import com.user.myapp.domain.Partner;
import com.user.myapp.service.dto.PartnerDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Partner} and its DTO {@link PartnerDTO}.
 */
@Mapper(componentModel = "spring")
public interface PartnerMapper extends EntityMapper<PartnerDTO, Partner> {}
