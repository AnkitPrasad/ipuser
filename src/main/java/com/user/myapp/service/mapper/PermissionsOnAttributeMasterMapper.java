package com.user.myapp.service.mapper;

import com.user.myapp.domain.PermissionsOnAttributeMaster;
import com.user.myapp.service.dto.PermissionsOnAttributeMasterDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PermissionsOnAttributeMaster} and its DTO {@link PermissionsOnAttributeMasterDTO}.
 */
@Mapper(componentModel = "spring")
public interface PermissionsOnAttributeMasterMapper extends EntityMapper<PermissionsOnAttributeMasterDTO, PermissionsOnAttributeMaster> {}
