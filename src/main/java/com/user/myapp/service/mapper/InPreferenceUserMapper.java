package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link InPreferenceUser} and its DTO {@link InPreferenceUserDTO}.
 */
@Mapper(componentModel = "spring")
public interface InPreferenceUserMapper extends EntityMapper<InPreferenceUserDTO, InPreferenceUser> {}
