package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.PermissionMaster;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.domain.UserRolePermission;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.PermissionMasterDTO;
import com.user.myapp.service.dto.RoleMasterDTO;
import com.user.myapp.service.dto.UserRolePermissionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserRolePermission} and its DTO {@link UserRolePermissionDTO}.
 */
@Mapper(componentModel = "spring")
public interface UserRolePermissionMapper extends EntityMapper<UserRolePermissionDTO, UserRolePermission> {
    @Mapping(target = "inPreferenceUser", source = "inPreferenceUser", qualifiedByName = "inPreferenceUserId")
    @Mapping(target = "roleMaster", source = "roleMaster", qualifiedByName = "roleMasterId")
    @Mapping(target = "permissionMaster", source = "permissionMaster", qualifiedByName = "permissionMasterId")
    UserRolePermissionDTO toDto(UserRolePermission s);

    @Named("inPreferenceUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser);

    @Named("roleMasterId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    RoleMasterDTO toDtoRoleMasterId(RoleMaster roleMaster);

    @Named("permissionMasterId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PermissionMasterDTO toDtoPermissionMasterId(PermissionMaster permissionMaster);
}
