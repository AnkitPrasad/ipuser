package com.user.myapp.service.mapper;

import com.user.myapp.domain.PermissionsOnAttributeMaster;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.domain.UserPermissionOnAttribute;
import com.user.myapp.service.dto.PermissionsOnAttributeMasterDTO;
import com.user.myapp.service.dto.RoleMasterDTO;
import com.user.myapp.service.dto.UserPermissionOnAttributeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserPermissionOnAttribute} and its DTO {@link UserPermissionOnAttributeDTO}.
 */
@Mapper(componentModel = "spring")
public interface UserPermissionOnAttributeMapper extends EntityMapper<UserPermissionOnAttributeDTO, UserPermissionOnAttribute> {
    @Mapping(target = "roleMaster", source = "roleMaster", qualifiedByName = "roleMasterId")
    @Mapping(
        target = "permissionsOnAttributeMaster",
        source = "permissionsOnAttributeMaster",
        qualifiedByName = "permissionsOnAttributeMasterId"
    )
    UserPermissionOnAttributeDTO toDto(UserPermissionOnAttribute s);

    @Named("roleMasterId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    RoleMasterDTO toDtoRoleMasterId(RoleMaster roleMaster);

    @Named("permissionsOnAttributeMasterId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PermissionsOnAttributeMasterDTO toDtoPermissionsOnAttributeMasterId(PermissionsOnAttributeMaster permissionsOnAttributeMaster);
}
