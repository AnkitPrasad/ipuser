package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.UserEmails;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.UserEmailsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserEmails} and its DTO {@link UserEmailsDTO}.
 */
@Mapper(componentModel = "spring")
public interface UserEmailsMapper extends EntityMapper<UserEmailsDTO, UserEmails> {
    @Mapping(target = "inPreferenceUser", source = "inPreferenceUser", qualifiedByName = "inPreferenceUserId")
    UserEmailsDTO toDto(UserEmails s);

    @Named("inPreferenceUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser);
}
