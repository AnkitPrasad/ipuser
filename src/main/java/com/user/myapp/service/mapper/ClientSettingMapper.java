package com.user.myapp.service.mapper;

import com.user.myapp.domain.ClientSetting;
import com.user.myapp.service.dto.ClientSettingDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ClientSetting} and its DTO {@link ClientSettingDTO}.
 */
@Mapper(componentModel = "spring")
public interface ClientSettingMapper extends EntityMapper<ClientSettingDTO, ClientSetting> {}
