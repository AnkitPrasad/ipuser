package com.user.myapp.service.mapper;

import com.user.myapp.domain.PermissionMaster;
import com.user.myapp.service.dto.PermissionMasterDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PermissionMaster} and its DTO {@link PermissionMasterDTO}.
 */
@Mapper(componentModel = "spring")
public interface PermissionMasterMapper extends EntityMapper<PermissionMasterDTO, PermissionMaster> {}
