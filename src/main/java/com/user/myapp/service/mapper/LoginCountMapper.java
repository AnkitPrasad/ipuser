package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.LoginCount;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.LoginCountDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link LoginCount} and its DTO {@link LoginCountDTO}.
 */
@Mapper(componentModel = "spring")
public interface LoginCountMapper extends EntityMapper<LoginCountDTO, LoginCount> {
    @Mapping(target = "inPreferenceUser", source = "inPreferenceUser", qualifiedByName = "inPreferenceUserId")
    LoginCountDTO toDto(LoginCount s);

    @Named("inPreferenceUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser);
}
