package com.user.myapp.service.mapper;

import com.user.myapp.domain.Client;
import com.user.myapp.domain.ClientSetting;
import com.user.myapp.service.dto.ClientDTO;
import com.user.myapp.service.dto.ClientSettingDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Client} and its DTO {@link ClientDTO}.
 */
@Mapper(componentModel = "spring")
public interface ClientMapper extends EntityMapper<ClientDTO, Client> {
    @Mapping(target = "clientSetting", source = "clientSetting", qualifiedByName = "clientSettingId")
    ClientDTO toDto(Client s);

    @Named("clientSettingId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientSettingDTO toDtoClientSettingId(ClientSetting clientSetting);
}
