package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.RoleMasterDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link RoleMaster} and its DTO {@link RoleMasterDTO}.
 */
@Mapper(componentModel = "spring")
public interface RoleMasterMapper extends EntityMapper<RoleMasterDTO, RoleMaster> {
    @Mapping(target = "inPreferenceUser", source = "inPreferenceUser", qualifiedByName = "inPreferenceUserId")
    RoleMasterDTO toDto(RoleMaster s);

    @Named("inPreferenceUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser);
}
