package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.SuperAdmin;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.SuperAdminDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link SuperAdmin} and its DTO {@link SuperAdminDTO}.
 */
@Mapper(componentModel = "spring")
public interface SuperAdminMapper extends EntityMapper<SuperAdminDTO, SuperAdmin> {
    @Mapping(target = "inPreferenceUser", source = "inPreferenceUser", qualifiedByName = "inPreferenceUserId")
    SuperAdminDTO toDto(SuperAdmin s);

    @Named("inPreferenceUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser);
}
