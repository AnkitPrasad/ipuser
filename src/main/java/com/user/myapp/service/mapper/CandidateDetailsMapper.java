package com.user.myapp.service.mapper;

import com.user.myapp.domain.CandidateDetails;
import com.user.myapp.service.dto.CandidateDetailsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CandidateDetails} and its DTO {@link CandidateDetailsDTO}.
 */
@Mapper(componentModel = "spring")
public interface CandidateDetailsMapper extends EntityMapper<CandidateDetailsDTO, CandidateDetails> {}
