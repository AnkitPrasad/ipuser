package com.user.myapp.service.mapper;

import com.user.myapp.domain.RoleAndPermissions;
import com.user.myapp.service.dto.RoleAndPermissionsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link RoleAndPermissions} and its DTO {@link RoleAndPermissionsDTO}.
 */
@Mapper(componentModel = "spring")
public interface RoleAndPermissionsMapper extends EntityMapper<RoleAndPermissionsDTO, RoleAndPermissions> {}
