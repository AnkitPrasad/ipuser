package com.user.myapp.service.mapper;

import com.user.myapp.domain.Candidate;
import com.user.myapp.domain.CandidateProfile;
import com.user.myapp.service.dto.CandidateDTO;
import com.user.myapp.service.dto.CandidateProfileDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CandidateProfile} and its DTO {@link CandidateProfileDTO}.
 */
@Mapper(componentModel = "spring")
public interface CandidateProfileMapper extends EntityMapper<CandidateProfileDTO, CandidateProfile> {
    @Mapping(target = "candidate", source = "candidate", qualifiedByName = "candidateId")
    CandidateProfileDTO toDto(CandidateProfile s);

    @Named("candidateId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CandidateDTO toDtoCandidateId(Candidate candidate);
}
