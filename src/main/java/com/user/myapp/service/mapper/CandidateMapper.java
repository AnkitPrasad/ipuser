package com.user.myapp.service.mapper;

import com.user.myapp.domain.Candidate;
import com.user.myapp.domain.CandidateDetails;
import com.user.myapp.domain.Client;
import com.user.myapp.service.dto.CandidateDTO;
import com.user.myapp.service.dto.CandidateDetailsDTO;
import com.user.myapp.service.dto.ClientDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Candidate} and its DTO {@link CandidateDTO}.
 */
@Mapper(componentModel = "spring")
public interface CandidateMapper extends EntityMapper<CandidateDTO, Candidate> {
    @Mapping(target = "candidateDetails", source = "candidateDetails", qualifiedByName = "candidateDetailsId")
    @Mapping(target = "client", source = "client", qualifiedByName = "clientId")
    CandidateDTO toDto(Candidate s);

    @Named("candidateDetailsId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CandidateDetailsDTO toDtoCandidateDetailsId(CandidateDetails candidateDetails);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);
}
