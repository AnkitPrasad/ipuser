package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.repository.RoleMasterRepository;
import com.user.myapp.service.criteria.RoleMasterCriteria;
import com.user.myapp.service.dto.RoleMasterDTO;
import com.user.myapp.service.mapper.RoleMasterMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link RoleMaster} entities in the database.
 * The main input is a {@link RoleMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RoleMasterDTO} or a {@link Page} of {@link RoleMasterDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RoleMasterQueryService extends QueryService<RoleMaster> {

    private final Logger log = LoggerFactory.getLogger(RoleMasterQueryService.class);

    private final RoleMasterRepository roleMasterRepository;

    private final RoleMasterMapper roleMasterMapper;

    public RoleMasterQueryService(RoleMasterRepository roleMasterRepository, RoleMasterMapper roleMasterMapper) {
        this.roleMasterRepository = roleMasterRepository;
        this.roleMasterMapper = roleMasterMapper;
    }

    /**
     * Return a {@link List} of {@link RoleMasterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RoleMasterDTO> findByCriteria(RoleMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RoleMaster> specification = createSpecification(criteria);
        return roleMasterMapper.toDto(roleMasterRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RoleMasterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RoleMasterDTO> findByCriteria(RoleMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RoleMaster> specification = createSpecification(criteria);
        return roleMasterRepository.findAll(specification, page).map(roleMasterMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RoleMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RoleMaster> specification = createSpecification(criteria);
        return roleMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link RoleMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RoleMaster> createSpecification(RoleMasterCriteria criteria) {
        Specification<RoleMaster> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), RoleMaster_.id));
            }
            if (criteria.getRoleName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRoleName(), RoleMaster_.roleName));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), RoleMaster_.description));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), RoleMaster_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedBy(), RoleMaster_.createdBy));
            }
            if (criteria.getUpdatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedDate(), RoleMaster_.updatedDate));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedBy(), RoleMaster_.updatedBy));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), RoleMaster_.status));
            }
            if (criteria.getUserPermissionOnAttributeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getUserPermissionOnAttributeId(),
                            root -> root.join(RoleMaster_.userPermissionOnAttributes, JoinType.LEFT).get(UserPermissionOnAttribute_.id)
                        )
                    );
            }
            if (criteria.getUserRolePermissionId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getUserRolePermissionId(),
                            root -> root.join(RoleMaster_.userRolePermissions, JoinType.LEFT).get(UserRolePermission_.id)
                        )
                    );
            }
            if (criteria.getInPreferenceUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getInPreferenceUserId(),
                            root -> root.join(RoleMaster_.inPreferenceUser, JoinType.LEFT).get(InPreferenceUser_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
