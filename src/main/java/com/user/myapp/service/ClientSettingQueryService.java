package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.ClientSetting;
import com.user.myapp.repository.ClientSettingRepository;
import com.user.myapp.service.criteria.ClientSettingCriteria;
import com.user.myapp.service.dto.ClientSettingDTO;
import com.user.myapp.service.mapper.ClientSettingMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ClientSetting} entities in the database.
 * The main input is a {@link ClientSettingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientSettingDTO} or a {@link Page} of {@link ClientSettingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientSettingQueryService extends QueryService<ClientSetting> {

    private final Logger log = LoggerFactory.getLogger(ClientSettingQueryService.class);

    private final ClientSettingRepository clientSettingRepository;

    private final ClientSettingMapper clientSettingMapper;

    public ClientSettingQueryService(ClientSettingRepository clientSettingRepository, ClientSettingMapper clientSettingMapper) {
        this.clientSettingRepository = clientSettingRepository;
        this.clientSettingMapper = clientSettingMapper;
    }

    /**
     * Return a {@link List} of {@link ClientSettingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientSettingDTO> findByCriteria(ClientSettingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ClientSetting> specification = createSpecification(criteria);
        return clientSettingMapper.toDto(clientSettingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientSettingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientSettingDTO> findByCriteria(ClientSettingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ClientSetting> specification = createSpecification(criteria);
        return clientSettingRepository.findAll(specification, page).map(clientSettingMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ClientSettingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ClientSetting> specification = createSpecification(criteria);
        return clientSettingRepository.count(specification);
    }

    /**
     * Function to convert {@link ClientSettingCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ClientSetting> createSpecification(ClientSettingCriteria criteria) {
        Specification<ClientSetting> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ClientSetting_.id));
            }
            if (criteria.getCandidateSelfAssessment() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getCandidateSelfAssessment(), ClientSetting_.candidateSelfAssessment)
                    );
            }
            if (criteria.getAutoReninderAssessment() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getAutoReninderAssessment(), ClientSetting_.autoReninderAssessment));
            }
            if (criteria.getAllowCandidatePaidVersion() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getAllowCandidatePaidVersion(), ClientSetting_.allowCandidatePaidVersion)
                    );
            }
            if (criteria.getAllowEmployessPaidVersion() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getAllowEmployessPaidVersion(), ClientSetting_.allowEmployessPaidVersion)
                    );
            }
            if (criteria.getAutoArchieveRequisitions() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getAutoArchieveRequisitions(), ClientSetting_.autoArchieveRequisitions)
                    );
            }
            if (criteria.getCompanyThemeDarkColour() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getCompanyThemeDarkColour(), ClientSetting_.companyThemeDarkColour)
                    );
            }
            if (criteria.getCompanyThemeLightColour() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getCompanyThemeLightColour(), ClientSetting_.companyThemeLightColour)
                    );
            }
            if (criteria.getCompanyUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyUrl(), ClientSetting_.companyUrl));
            }
            if (criteria.getCompanyMenu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyMenu(), ClientSetting_.companyMenu));
            }
        }
        return specification;
    }
}
