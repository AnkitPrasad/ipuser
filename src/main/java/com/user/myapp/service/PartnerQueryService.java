package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.Partner;
import com.user.myapp.repository.PartnerRepository;
import com.user.myapp.service.criteria.PartnerCriteria;
import com.user.myapp.service.dto.PartnerDTO;
import com.user.myapp.service.mapper.PartnerMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Partner} entities in the database.
 * The main input is a {@link PartnerCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PartnerDTO} or a {@link Page} of {@link PartnerDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PartnerQueryService extends QueryService<Partner> {

    private final Logger log = LoggerFactory.getLogger(PartnerQueryService.class);

    private final PartnerRepository partnerRepository;

    private final PartnerMapper partnerMapper;

    public PartnerQueryService(PartnerRepository partnerRepository, PartnerMapper partnerMapper) {
        this.partnerRepository = partnerRepository;
        this.partnerMapper = partnerMapper;
    }

    /**
     * Return a {@link List} of {@link PartnerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PartnerDTO> findByCriteria(PartnerCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Partner> specification = createSpecification(criteria);
        return partnerMapper.toDto(partnerRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PartnerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PartnerDTO> findByCriteria(PartnerCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Partner> specification = createSpecification(criteria);
        return partnerRepository.findAll(specification, page).map(partnerMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PartnerCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Partner> specification = createSpecification(criteria);
        return partnerRepository.count(specification);
    }

    /**
     * Function to convert {@link PartnerCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Partner> createSpecification(PartnerCriteria criteria) {
        Specification<Partner> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Partner_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Partner_.name));
            }
            if (criteria.getDomain() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDomain(), Partner_.domain));
            }
            if (criteria.getPartnerCommission() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPartnerCommission(), Partner_.partnerCommission));
            }
            if (criteria.getPartnerType() != null) {
                specification = specification.and(buildSpecification(criteria.getPartnerType(), Partner_.partnerType));
            }
            if (criteria.getValueM() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValueM(), Partner_.valueM));
            }
            if (criteria.getValueC() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValueC(), Partner_.valueC));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), Partner_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedBy(), Partner_.createdBy));
            }
            if (criteria.getUpdatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedDate(), Partner_.updatedDate));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedBy(), Partner_.updatedBy));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), Partner_.status));
            }
        }
        return specification;
    }
}
