package com.user.myapp.service;

import com.user.myapp.service.dto.InPreferenceUserDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.InPreferenceUser}.
 */
public interface InPreferenceUserService {
    /**
     * Save a inPreferenceUser.
     *
     * @param inPreferenceUserDTO the entity to save.
     * @return the persisted entity.
     */
    InPreferenceUserDTO save(InPreferenceUserDTO inPreferenceUserDTO);

    /**
     * Updates a inPreferenceUser.
     *
     * @param inPreferenceUserDTO the entity to update.
     * @return the persisted entity.
     */
    InPreferenceUserDTO update(InPreferenceUserDTO inPreferenceUserDTO);

    /**
     * Partially updates a inPreferenceUser.
     *
     * @param inPreferenceUserDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<InPreferenceUserDTO> partialUpdate(InPreferenceUserDTO inPreferenceUserDTO);

    /**
     * Get all the inPreferenceUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<InPreferenceUserDTO> findAll(Pageable pageable);

    /**
     * Get the "id" inPreferenceUser.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<InPreferenceUserDTO> findOne(Long id);

    /**
     * Delete the "id" inPreferenceUser.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
