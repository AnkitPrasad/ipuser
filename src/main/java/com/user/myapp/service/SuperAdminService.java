package com.user.myapp.service;

import com.user.myapp.service.dto.SuperAdminDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.SuperAdmin}.
 */
public interface SuperAdminService {
    /**
     * Save a superAdmin.
     *
     * @param superAdminDTO the entity to save.
     * @return the persisted entity.
     */
    SuperAdminDTO save(SuperAdminDTO superAdminDTO);

    /**
     * Updates a superAdmin.
     *
     * @param superAdminDTO the entity to update.
     * @return the persisted entity.
     */
    SuperAdminDTO update(SuperAdminDTO superAdminDTO);

    /**
     * Partially updates a superAdmin.
     *
     * @param superAdminDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<SuperAdminDTO> partialUpdate(SuperAdminDTO superAdminDTO);

    /**
     * Get all the superAdmins.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SuperAdminDTO> findAll(Pageable pageable);

    /**
     * Get the "id" superAdmin.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SuperAdminDTO> findOne(Long id);

    /**
     * Delete the "id" superAdmin.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
