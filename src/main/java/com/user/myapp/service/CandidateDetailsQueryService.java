package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.CandidateDetails;
import com.user.myapp.repository.CandidateDetailsRepository;
import com.user.myapp.service.criteria.CandidateDetailsCriteria;
import com.user.myapp.service.dto.CandidateDetailsDTO;
import com.user.myapp.service.mapper.CandidateDetailsMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CandidateDetails} entities in the database.
 * The main input is a {@link CandidateDetailsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CandidateDetailsDTO} or a {@link Page} of {@link CandidateDetailsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CandidateDetailsQueryService extends QueryService<CandidateDetails> {

    private final Logger log = LoggerFactory.getLogger(CandidateDetailsQueryService.class);

    private final CandidateDetailsRepository candidateDetailsRepository;

    private final CandidateDetailsMapper candidateDetailsMapper;

    public CandidateDetailsQueryService(
        CandidateDetailsRepository candidateDetailsRepository,
        CandidateDetailsMapper candidateDetailsMapper
    ) {
        this.candidateDetailsRepository = candidateDetailsRepository;
        this.candidateDetailsMapper = candidateDetailsMapper;
    }

    /**
     * Return a {@link List} of {@link CandidateDetailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CandidateDetailsDTO> findByCriteria(CandidateDetailsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CandidateDetails> specification = createSpecification(criteria);
        return candidateDetailsMapper.toDto(candidateDetailsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CandidateDetailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CandidateDetailsDTO> findByCriteria(CandidateDetailsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CandidateDetails> specification = createSpecification(criteria);
        return candidateDetailsRepository.findAll(specification, page).map(candidateDetailsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CandidateDetailsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CandidateDetails> specification = createSpecification(criteria);
        return candidateDetailsRepository.count(specification);
    }

    /**
     * Function to convert {@link CandidateDetailsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CandidateDetails> createSpecification(CandidateDetailsCriteria criteria) {
        Specification<CandidateDetails> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CandidateDetails_.id));
            }
            if (criteria.getCandidateCountry() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getCandidateCountry(), CandidateDetails_.candidateCountry));
            }
            if (criteria.getBusinessUnits() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBusinessUnits(), CandidateDetails_.businessUnits));
            }
            if (criteria.getLegalEntity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLegalEntity(), CandidateDetails_.legalEntity));
            }
            if (criteria.getCandidateSubFunction() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getCandidateSubFunction(), CandidateDetails_.candidateSubFunction));
            }
            if (criteria.getDesignation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesignation(), CandidateDetails_.designation));
            }
            if (criteria.getJobTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJobTitle(), CandidateDetails_.jobTitle));
            }
            if (criteria.getLocation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLocation(), CandidateDetails_.location));
            }
            if (criteria.getSubBusinessUnits() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getSubBusinessUnits(), CandidateDetails_.subBusinessUnits));
            }
            if (criteria.getCandidateFunction() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getCandidateFunction(), CandidateDetails_.candidateFunction));
            }
            if (criteria.getWorkLevel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getWorkLevel(), CandidateDetails_.workLevel));
            }
            if (criteria.getCandidateGrade() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCandidateGrade(), CandidateDetails_.candidateGrade));
            }
        }
        return specification;
    }
}
