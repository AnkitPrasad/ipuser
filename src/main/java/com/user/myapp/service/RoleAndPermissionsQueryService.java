package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.RoleAndPermissions;
import com.user.myapp.repository.RoleAndPermissionsRepository;
import com.user.myapp.service.criteria.RoleAndPermissionsCriteria;
import com.user.myapp.service.dto.RoleAndPermissionsDTO;
import com.user.myapp.service.mapper.RoleAndPermissionsMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link RoleAndPermissions} entities in the database.
 * The main input is a {@link RoleAndPermissionsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RoleAndPermissionsDTO} or a {@link Page} of {@link RoleAndPermissionsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RoleAndPermissionsQueryService extends QueryService<RoleAndPermissions> {

    private final Logger log = LoggerFactory.getLogger(RoleAndPermissionsQueryService.class);

    private final RoleAndPermissionsRepository roleAndPermissionsRepository;

    private final RoleAndPermissionsMapper roleAndPermissionsMapper;

    public RoleAndPermissionsQueryService(
        RoleAndPermissionsRepository roleAndPermissionsRepository,
        RoleAndPermissionsMapper roleAndPermissionsMapper
    ) {
        this.roleAndPermissionsRepository = roleAndPermissionsRepository;
        this.roleAndPermissionsMapper = roleAndPermissionsMapper;
    }

    /**
     * Return a {@link List} of {@link RoleAndPermissionsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RoleAndPermissionsDTO> findByCriteria(RoleAndPermissionsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RoleAndPermissions> specification = createSpecification(criteria);
        return roleAndPermissionsMapper.toDto(roleAndPermissionsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RoleAndPermissionsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RoleAndPermissionsDTO> findByCriteria(RoleAndPermissionsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RoleAndPermissions> specification = createSpecification(criteria);
        return roleAndPermissionsRepository.findAll(specification, page).map(roleAndPermissionsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RoleAndPermissionsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RoleAndPermissions> specification = createSpecification(criteria);
        return roleAndPermissionsRepository.count(specification);
    }

    /**
     * Function to convert {@link RoleAndPermissionsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<RoleAndPermissions> createSpecification(RoleAndPermissionsCriteria criteria) {
        Specification<RoleAndPermissions> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), RoleAndPermissions_.id));
            }
            if (criteria.getReadAccess() != null) {
                specification = specification.and(buildSpecification(criteria.getReadAccess(), RoleAndPermissions_.readAccess));
            }
            if (criteria.getCreateAccess() != null) {
                specification = specification.and(buildSpecification(criteria.getCreateAccess(), RoleAndPermissions_.createAccess));
            }
            if (criteria.getEditAccess() != null) {
                specification = specification.and(buildSpecification(criteria.getEditAccess(), RoleAndPermissions_.editAccess));
            }
            if (criteria.getDeleteAccess() != null) {
                specification = specification.and(buildSpecification(criteria.getDeleteAccess(), RoleAndPermissions_.deleteAccess));
            }
        }
        return specification;
    }
}
