package com.user.myapp.service;

import com.user.myapp.service.dto.CandidateDetailsDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.CandidateDetails}.
 */
public interface CandidateDetailsService {
    /**
     * Save a candidateDetails.
     *
     * @param candidateDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    CandidateDetailsDTO save(CandidateDetailsDTO candidateDetailsDTO);

    /**
     * Updates a candidateDetails.
     *
     * @param candidateDetailsDTO the entity to update.
     * @return the persisted entity.
     */
    CandidateDetailsDTO update(CandidateDetailsDTO candidateDetailsDTO);

    /**
     * Partially updates a candidateDetails.
     *
     * @param candidateDetailsDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CandidateDetailsDTO> partialUpdate(CandidateDetailsDTO candidateDetailsDTO);

    /**
     * Get all the candidateDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CandidateDetailsDTO> findAll(Pageable pageable);

    /**
     * Get the "id" candidateDetails.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CandidateDetailsDTO> findOne(Long id);

    /**
     * Delete the "id" candidateDetails.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
