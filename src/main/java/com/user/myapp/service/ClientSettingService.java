package com.user.myapp.service;

import com.user.myapp.service.dto.ClientSettingDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.ClientSetting}.
 */
public interface ClientSettingService {
    /**
     * Save a clientSetting.
     *
     * @param clientSettingDTO the entity to save.
     * @return the persisted entity.
     */
    ClientSettingDTO save(ClientSettingDTO clientSettingDTO);

    /**
     * Updates a clientSetting.
     *
     * @param clientSettingDTO the entity to update.
     * @return the persisted entity.
     */
    ClientSettingDTO update(ClientSettingDTO clientSettingDTO);

    /**
     * Partially updates a clientSetting.
     *
     * @param clientSettingDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ClientSettingDTO> partialUpdate(ClientSettingDTO clientSettingDTO);

    /**
     * Get all the clientSettings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ClientSettingDTO> findAll(Pageable pageable);

    /**
     * Get the "id" clientSetting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ClientSettingDTO> findOne(Long id);

    /**
     * Delete the "id" clientSetting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
