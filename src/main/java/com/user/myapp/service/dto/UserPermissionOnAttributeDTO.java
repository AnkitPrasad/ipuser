package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.user.myapp.domain.UserPermissionOnAttribute} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserPermissionOnAttributeDTO implements Serializable {

    private Long id;

    @Lob
    private String attributeValue;

    private RoleMasterDTO roleMaster;

    private PermissionsOnAttributeMasterDTO permissionsOnAttributeMaster;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public RoleMasterDTO getRoleMaster() {
        return roleMaster;
    }

    public void setRoleMaster(RoleMasterDTO roleMaster) {
        this.roleMaster = roleMaster;
    }

    public PermissionsOnAttributeMasterDTO getPermissionsOnAttributeMaster() {
        return permissionsOnAttributeMaster;
    }

    public void setPermissionsOnAttributeMaster(PermissionsOnAttributeMasterDTO permissionsOnAttributeMaster) {
        this.permissionsOnAttributeMaster = permissionsOnAttributeMaster;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserPermissionOnAttributeDTO)) {
            return false;
        }

        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = (UserPermissionOnAttributeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, userPermissionOnAttributeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserPermissionOnAttributeDTO{" +
            "id=" + getId() +
            ", attributeValue='" + getAttributeValue() + "'" +
            ", roleMaster=" + getRoleMaster() +
            ", permissionsOnAttributeMaster=" + getPermissionsOnAttributeMaster() +
            "}";
    }
}
