package com.user.myapp.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.LoginCount} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class LoginCountDTO implements Serializable {

    private Long id;

    private Instant logintime;

    private Long loginId;

    private InPreferenceUserDTO inPreferenceUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getLogintime() {
        return logintime;
    }

    public void setLogintime(Instant logintime) {
        this.logintime = logintime;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public InPreferenceUserDTO getInPreferenceUser() {
        return inPreferenceUser;
    }

    public void setInPreferenceUser(InPreferenceUserDTO inPreferenceUser) {
        this.inPreferenceUser = inPreferenceUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoginCountDTO)) {
            return false;
        }

        LoginCountDTO loginCountDTO = (LoginCountDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, loginCountDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LoginCountDTO{" +
            "id=" + getId() +
            ", logintime='" + getLogintime() + "'" +
            ", loginId=" + getLoginId() +
            ", inPreferenceUser=" + getInPreferenceUser() +
            "}";
    }
}
