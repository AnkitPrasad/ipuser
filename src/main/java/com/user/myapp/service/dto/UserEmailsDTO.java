package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.UserEmails} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserEmailsDTO implements Serializable {

    private Long id;

    private String email;

    private Long loginId;

    private InPreferenceUserDTO inPreferenceUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public InPreferenceUserDTO getInPreferenceUser() {
        return inPreferenceUser;
    }

    public void setInPreferenceUser(InPreferenceUserDTO inPreferenceUser) {
        this.inPreferenceUser = inPreferenceUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserEmailsDTO)) {
            return false;
        }

        UserEmailsDTO userEmailsDTO = (UserEmailsDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, userEmailsDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserEmailsDTO{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", loginId=" + getLoginId() +
            ", inPreferenceUser=" + getInPreferenceUser() +
            "}";
    }
}
