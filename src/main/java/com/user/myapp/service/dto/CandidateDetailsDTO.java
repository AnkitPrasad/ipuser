package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.CandidateDetails} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CandidateDetailsDTO implements Serializable {

    private Long id;

    private String candidateCountry;

    private String businessUnits;

    private String legalEntity;

    private String candidateSubFunction;

    private String designation;

    private String jobTitle;

    private String location;

    private String subBusinessUnits;

    private String candidateFunction;

    private String workLevel;

    private String candidateGrade;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCandidateCountry() {
        return candidateCountry;
    }

    public void setCandidateCountry(String candidateCountry) {
        this.candidateCountry = candidateCountry;
    }

    public String getBusinessUnits() {
        return businessUnits;
    }

    public void setBusinessUnits(String businessUnits) {
        this.businessUnits = businessUnits;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getCandidateSubFunction() {
        return candidateSubFunction;
    }

    public void setCandidateSubFunction(String candidateSubFunction) {
        this.candidateSubFunction = candidateSubFunction;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubBusinessUnits() {
        return subBusinessUnits;
    }

    public void setSubBusinessUnits(String subBusinessUnits) {
        this.subBusinessUnits = subBusinessUnits;
    }

    public String getCandidateFunction() {
        return candidateFunction;
    }

    public void setCandidateFunction(String candidateFunction) {
        this.candidateFunction = candidateFunction;
    }

    public String getWorkLevel() {
        return workLevel;
    }

    public void setWorkLevel(String workLevel) {
        this.workLevel = workLevel;
    }

    public String getCandidateGrade() {
        return candidateGrade;
    }

    public void setCandidateGrade(String candidateGrade) {
        this.candidateGrade = candidateGrade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CandidateDetailsDTO)) {
            return false;
        }

        CandidateDetailsDTO candidateDetailsDTO = (CandidateDetailsDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, candidateDetailsDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CandidateDetailsDTO{" +
            "id=" + getId() +
            ", candidateCountry='" + getCandidateCountry() + "'" +
            ", businessUnits='" + getBusinessUnits() + "'" +
            ", legalEntity='" + getLegalEntity() + "'" +
            ", candidateSubFunction='" + getCandidateSubFunction() + "'" +
            ", designation='" + getDesignation() + "'" +
            ", jobTitle='" + getJobTitle() + "'" +
            ", location='" + getLocation() + "'" +
            ", subBusinessUnits='" + getSubBusinessUnits() + "'" +
            ", candidateFunction='" + getCandidateFunction() + "'" +
            ", workLevel='" + getWorkLevel() + "'" +
            ", candidateGrade='" + getCandidateGrade() + "'" +
            "}";
    }
}
