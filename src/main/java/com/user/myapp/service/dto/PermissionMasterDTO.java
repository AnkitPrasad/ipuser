package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.PermissionMaster} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PermissionMasterDTO implements Serializable {

    private Long id;

    private String pageName;

    private String pageUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PermissionMasterDTO)) {
            return false;
        }

        PermissionMasterDTO permissionMasterDTO = (PermissionMasterDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, permissionMasterDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PermissionMasterDTO{" +
            "id=" + getId() +
            ", pageName='" + getPageName() + "'" +
            ", pageUrl='" + getPageUrl() + "'" +
            "}";
    }
}
