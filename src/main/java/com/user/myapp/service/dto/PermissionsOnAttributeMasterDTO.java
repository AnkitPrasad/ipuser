package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.PermissionsOnAttributeMaster} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PermissionsOnAttributeMasterDTO implements Serializable {

    private Long id;

    private String attributeKey;

    private String attributeTitle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttributeKey() {
        return attributeKey;
    }

    public void setAttributeKey(String attributeKey) {
        this.attributeKey = attributeKey;
    }

    public String getAttributeTitle() {
        return attributeTitle;
    }

    public void setAttributeTitle(String attributeTitle) {
        this.attributeTitle = attributeTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PermissionsOnAttributeMasterDTO)) {
            return false;
        }

        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = (PermissionsOnAttributeMasterDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, permissionsOnAttributeMasterDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PermissionsOnAttributeMasterDTO{" +
            "id=" + getId() +
            ", attributeKey='" + getAttributeKey() + "'" +
            ", attributeTitle='" + getAttributeTitle() + "'" +
            "}";
    }
}
