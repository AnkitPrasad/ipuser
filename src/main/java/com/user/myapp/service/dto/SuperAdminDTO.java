package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.SuperAdmin} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class SuperAdminDTO implements Serializable {

    private Long id;

    private String name;

    private String email;

    private InPreferenceUserDTO inPreferenceUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public InPreferenceUserDTO getInPreferenceUser() {
        return inPreferenceUser;
    }

    public void setInPreferenceUser(InPreferenceUserDTO inPreferenceUser) {
        this.inPreferenceUser = inPreferenceUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SuperAdminDTO)) {
            return false;
        }

        SuperAdminDTO superAdminDTO = (SuperAdminDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, superAdminDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SuperAdminDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", email='" + getEmail() + "'" +
            ", inPreferenceUser=" + getInPreferenceUser() +
            "}";
    }
}
