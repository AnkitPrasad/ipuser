package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.CandidateProfile} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CandidateProfileDTO implements Serializable {

    private Long id;

    private String aboutMe;

    private String careerSummary;

    private String education;

    private String dob;

    private String currentEmployment;

    private String certifications;

    private String experience;

    private CandidateDTO candidate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getCareerSummary() {
        return careerSummary;
    }

    public void setCareerSummary(String careerSummary) {
        this.careerSummary = careerSummary;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCurrentEmployment() {
        return currentEmployment;
    }

    public void setCurrentEmployment(String currentEmployment) {
        this.currentEmployment = currentEmployment;
    }

    public String getCertifications() {
        return certifications;
    }

    public void setCertifications(String certifications) {
        this.certifications = certifications;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public CandidateDTO getCandidate() {
        return candidate;
    }

    public void setCandidate(CandidateDTO candidate) {
        this.candidate = candidate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CandidateProfileDTO)) {
            return false;
        }

        CandidateProfileDTO candidateProfileDTO = (CandidateProfileDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, candidateProfileDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CandidateProfileDTO{" +
            "id=" + getId() +
            ", aboutMe='" + getAboutMe() + "'" +
            ", careerSummary='" + getCareerSummary() + "'" +
            ", education='" + getEducation() + "'" +
            ", dob='" + getDob() + "'" +
            ", currentEmployment='" + getCurrentEmployment() + "'" +
            ", certifications='" + getCertifications() + "'" +
            ", experience='" + getExperience() + "'" +
            ", candidate=" + getCandidate() +
            "}";
    }
}
