package com.user.myapp.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.Candidate} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CandidateDTO implements Serializable {

    private Long id;

    private Boolean isInternalCandidate;

    private String company;

    private Instant createdDate;

    private Long createdBy;

    private Instant updatedDate;

    private Long updatedBy;

    private String assessmentStatus;

    private Boolean status;

    private Boolean archiveStatus;

    private CandidateDetailsDTO candidateDetails;

    private ClientDTO client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsInternalCandidate() {
        return isInternalCandidate;
    }

    public void setIsInternalCandidate(Boolean isInternalCandidate) {
        this.isInternalCandidate = isInternalCandidate;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getAssessmentStatus() {
        return assessmentStatus;
    }

    public void setAssessmentStatus(String assessmentStatus) {
        this.assessmentStatus = assessmentStatus;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getArchiveStatus() {
        return archiveStatus;
    }

    public void setArchiveStatus(Boolean archiveStatus) {
        this.archiveStatus = archiveStatus;
    }

    public CandidateDetailsDTO getCandidateDetails() {
        return candidateDetails;
    }

    public void setCandidateDetails(CandidateDetailsDTO candidateDetails) {
        this.candidateDetails = candidateDetails;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CandidateDTO)) {
            return false;
        }

        CandidateDTO candidateDTO = (CandidateDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, candidateDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CandidateDTO{" +
            "id=" + getId() +
            ", isInternalCandidate='" + getIsInternalCandidate() + "'" +
            ", company='" + getCompany() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy=" + getUpdatedBy() +
            ", assessmentStatus='" + getAssessmentStatus() + "'" +
            ", status='" + getStatus() + "'" +
            ", archiveStatus='" + getArchiveStatus() + "'" +
            ", candidateDetails=" + getCandidateDetails() +
            ", client=" + getClient() +
            "}";
    }
}
