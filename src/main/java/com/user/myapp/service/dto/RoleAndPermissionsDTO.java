package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.RoleAndPermissions} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RoleAndPermissionsDTO implements Serializable {

    private Long id;

    private Boolean readAccess;

    private Boolean createAccess;

    private Boolean editAccess;

    private Boolean deleteAccess;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getReadAccess() {
        return readAccess;
    }

    public void setReadAccess(Boolean readAccess) {
        this.readAccess = readAccess;
    }

    public Boolean getCreateAccess() {
        return createAccess;
    }

    public void setCreateAccess(Boolean createAccess) {
        this.createAccess = createAccess;
    }

    public Boolean getEditAccess() {
        return editAccess;
    }

    public void setEditAccess(Boolean editAccess) {
        this.editAccess = editAccess;
    }

    public Boolean getDeleteAccess() {
        return deleteAccess;
    }

    public void setDeleteAccess(Boolean deleteAccess) {
        this.deleteAccess = deleteAccess;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RoleAndPermissionsDTO)) {
            return false;
        }

        RoleAndPermissionsDTO roleAndPermissionsDTO = (RoleAndPermissionsDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, roleAndPermissionsDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RoleAndPermissionsDTO{" +
            "id=" + getId() +
            ", readAccess='" + getReadAccess() + "'" +
            ", createAccess='" + getCreateAccess() + "'" +
            ", editAccess='" + getEditAccess() + "'" +
            ", deleteAccess='" + getDeleteAccess() + "'" +
            "}";
    }
}
