package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.UserRolePermission} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserRolePermissionDTO implements Serializable {

    private Long id;

    private InPreferenceUserDTO inPreferenceUser;

    private RoleMasterDTO roleMaster;

    private PermissionMasterDTO permissionMaster;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InPreferenceUserDTO getInPreferenceUser() {
        return inPreferenceUser;
    }

    public void setInPreferenceUser(InPreferenceUserDTO inPreferenceUser) {
        this.inPreferenceUser = inPreferenceUser;
    }

    public RoleMasterDTO getRoleMaster() {
        return roleMaster;
    }

    public void setRoleMaster(RoleMasterDTO roleMaster) {
        this.roleMaster = roleMaster;
    }

    public PermissionMasterDTO getPermissionMaster() {
        return permissionMaster;
    }

    public void setPermissionMaster(PermissionMasterDTO permissionMaster) {
        this.permissionMaster = permissionMaster;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserRolePermissionDTO)) {
            return false;
        }

        UserRolePermissionDTO userRolePermissionDTO = (UserRolePermissionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, userRolePermissionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserRolePermissionDTO{" +
            "id=" + getId() +
            ", inPreferenceUser=" + getInPreferenceUser() +
            ", roleMaster=" + getRoleMaster() +
            ", permissionMaster=" + getPermissionMaster() +
            "}";
    }
}
