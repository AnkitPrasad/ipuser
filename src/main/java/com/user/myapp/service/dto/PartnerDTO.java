package com.user.myapp.service.dto;

import com.user.myapp.domain.enumeration.Partnertype;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.Partner} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PartnerDTO implements Serializable {

    private Long id;

    private String name;

    private String domain;

    private Double partnerCommission;

    private Partnertype partnerType;

    private Integer valueM;

    private Integer valueC;

    private Instant createdDate;

    private Long createdBy;

    private Instant updatedDate;

    private Long updatedBy;

    private Boolean status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Double getPartnerCommission() {
        return partnerCommission;
    }

    public void setPartnerCommission(Double partnerCommission) {
        this.partnerCommission = partnerCommission;
    }

    public Partnertype getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(Partnertype partnerType) {
        this.partnerType = partnerType;
    }

    public Integer getValueM() {
        return valueM;
    }

    public void setValueM(Integer valueM) {
        this.valueM = valueM;
    }

    public Integer getValueC() {
        return valueC;
    }

    public void setValueC(Integer valueC) {
        this.valueC = valueC;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PartnerDTO)) {
            return false;
        }

        PartnerDTO partnerDTO = (PartnerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, partnerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PartnerDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", domain='" + getDomain() + "'" +
            ", partnerCommission=" + getPartnerCommission() +
            ", partnerType='" + getPartnerType() + "'" +
            ", valueM=" + getValueM() +
            ", valueC=" + getValueC() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy=" + getUpdatedBy() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
