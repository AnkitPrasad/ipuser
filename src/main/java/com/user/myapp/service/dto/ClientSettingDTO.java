package com.user.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.user.myapp.domain.ClientSetting} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ClientSettingDTO implements Serializable {

    private Long id;

    private Integer candidateSelfAssessment;

    private Integer autoReninderAssessment;

    private Boolean allowCandidatePaidVersion;

    private Boolean allowEmployessPaidVersion;

    private Integer autoArchieveRequisitions;

    private String companyThemeDarkColour;

    private String companyThemeLightColour;

    private String companyUrl;

    private String companyMenu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCandidateSelfAssessment() {
        return candidateSelfAssessment;
    }

    public void setCandidateSelfAssessment(Integer candidateSelfAssessment) {
        this.candidateSelfAssessment = candidateSelfAssessment;
    }

    public Integer getAutoReninderAssessment() {
        return autoReninderAssessment;
    }

    public void setAutoReninderAssessment(Integer autoReninderAssessment) {
        this.autoReninderAssessment = autoReninderAssessment;
    }

    public Boolean getAllowCandidatePaidVersion() {
        return allowCandidatePaidVersion;
    }

    public void setAllowCandidatePaidVersion(Boolean allowCandidatePaidVersion) {
        this.allowCandidatePaidVersion = allowCandidatePaidVersion;
    }

    public Boolean getAllowEmployessPaidVersion() {
        return allowEmployessPaidVersion;
    }

    public void setAllowEmployessPaidVersion(Boolean allowEmployessPaidVersion) {
        this.allowEmployessPaidVersion = allowEmployessPaidVersion;
    }

    public Integer getAutoArchieveRequisitions() {
        return autoArchieveRequisitions;
    }

    public void setAutoArchieveRequisitions(Integer autoArchieveRequisitions) {
        this.autoArchieveRequisitions = autoArchieveRequisitions;
    }

    public String getCompanyThemeDarkColour() {
        return companyThemeDarkColour;
    }

    public void setCompanyThemeDarkColour(String companyThemeDarkColour) {
        this.companyThemeDarkColour = companyThemeDarkColour;
    }

    public String getCompanyThemeLightColour() {
        return companyThemeLightColour;
    }

    public void setCompanyThemeLightColour(String companyThemeLightColour) {
        this.companyThemeLightColour = companyThemeLightColour;
    }

    public String getCompanyUrl() {
        return companyUrl;
    }

    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    public String getCompanyMenu() {
        return companyMenu;
    }

    public void setCompanyMenu(String companyMenu) {
        this.companyMenu = companyMenu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientSettingDTO)) {
            return false;
        }

        ClientSettingDTO clientSettingDTO = (ClientSettingDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, clientSettingDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClientSettingDTO{" +
            "id=" + getId() +
            ", candidateSelfAssessment=" + getCandidateSelfAssessment() +
            ", autoReninderAssessment=" + getAutoReninderAssessment() +
            ", allowCandidatePaidVersion='" + getAllowCandidatePaidVersion() + "'" +
            ", allowEmployessPaidVersion='" + getAllowEmployessPaidVersion() + "'" +
            ", autoArchieveRequisitions=" + getAutoArchieveRequisitions() +
            ", companyThemeDarkColour='" + getCompanyThemeDarkColour() + "'" +
            ", companyThemeLightColour='" + getCompanyThemeLightColour() + "'" +
            ", companyUrl='" + getCompanyUrl() + "'" +
            ", companyMenu='" + getCompanyMenu() + "'" +
            "}";
    }
}
