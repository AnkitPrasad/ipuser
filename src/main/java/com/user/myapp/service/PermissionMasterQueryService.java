package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.PermissionMaster;
import com.user.myapp.repository.PermissionMasterRepository;
import com.user.myapp.service.criteria.PermissionMasterCriteria;
import com.user.myapp.service.dto.PermissionMasterDTO;
import com.user.myapp.service.mapper.PermissionMasterMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PermissionMaster} entities in the database.
 * The main input is a {@link PermissionMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PermissionMasterDTO} or a {@link Page} of {@link PermissionMasterDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PermissionMasterQueryService extends QueryService<PermissionMaster> {

    private final Logger log = LoggerFactory.getLogger(PermissionMasterQueryService.class);

    private final PermissionMasterRepository permissionMasterRepository;

    private final PermissionMasterMapper permissionMasterMapper;

    public PermissionMasterQueryService(
        PermissionMasterRepository permissionMasterRepository,
        PermissionMasterMapper permissionMasterMapper
    ) {
        this.permissionMasterRepository = permissionMasterRepository;
        this.permissionMasterMapper = permissionMasterMapper;
    }

    /**
     * Return a {@link List} of {@link PermissionMasterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PermissionMasterDTO> findByCriteria(PermissionMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PermissionMaster> specification = createSpecification(criteria);
        return permissionMasterMapper.toDto(permissionMasterRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PermissionMasterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PermissionMasterDTO> findByCriteria(PermissionMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PermissionMaster> specification = createSpecification(criteria);
        return permissionMasterRepository.findAll(specification, page).map(permissionMasterMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PermissionMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PermissionMaster> specification = createSpecification(criteria);
        return permissionMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link PermissionMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PermissionMaster> createSpecification(PermissionMasterCriteria criteria) {
        Specification<PermissionMaster> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PermissionMaster_.id));
            }
            if (criteria.getPageName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPageName(), PermissionMaster_.pageName));
            }
            if (criteria.getPageUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPageUrl(), PermissionMaster_.pageUrl));
            }
            if (criteria.getUserRolePermissionId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getUserRolePermissionId(),
                            root -> root.join(PermissionMaster_.userRolePermissions, JoinType.LEFT).get(UserRolePermission_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
