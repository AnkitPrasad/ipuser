package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.RoleAndPermissions} entity. This class is used
 * in {@link com.user.myapp.web.rest.RoleAndPermissionsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /role-and-permissions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RoleAndPermissionsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter readAccess;

    private BooleanFilter createAccess;

    private BooleanFilter editAccess;

    private BooleanFilter deleteAccess;

    private Boolean distinct;

    public RoleAndPermissionsCriteria() {}

    public RoleAndPermissionsCriteria(RoleAndPermissionsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.readAccess = other.readAccess == null ? null : other.readAccess.copy();
        this.createAccess = other.createAccess == null ? null : other.createAccess.copy();
        this.editAccess = other.editAccess == null ? null : other.editAccess.copy();
        this.deleteAccess = other.deleteAccess == null ? null : other.deleteAccess.copy();
        this.distinct = other.distinct;
    }

    @Override
    public RoleAndPermissionsCriteria copy() {
        return new RoleAndPermissionsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getReadAccess() {
        return readAccess;
    }

    public BooleanFilter readAccess() {
        if (readAccess == null) {
            readAccess = new BooleanFilter();
        }
        return readAccess;
    }

    public void setReadAccess(BooleanFilter readAccess) {
        this.readAccess = readAccess;
    }

    public BooleanFilter getCreateAccess() {
        return createAccess;
    }

    public BooleanFilter createAccess() {
        if (createAccess == null) {
            createAccess = new BooleanFilter();
        }
        return createAccess;
    }

    public void setCreateAccess(BooleanFilter createAccess) {
        this.createAccess = createAccess;
    }

    public BooleanFilter getEditAccess() {
        return editAccess;
    }

    public BooleanFilter editAccess() {
        if (editAccess == null) {
            editAccess = new BooleanFilter();
        }
        return editAccess;
    }

    public void setEditAccess(BooleanFilter editAccess) {
        this.editAccess = editAccess;
    }

    public BooleanFilter getDeleteAccess() {
        return deleteAccess;
    }

    public BooleanFilter deleteAccess() {
        if (deleteAccess == null) {
            deleteAccess = new BooleanFilter();
        }
        return deleteAccess;
    }

    public void setDeleteAccess(BooleanFilter deleteAccess) {
        this.deleteAccess = deleteAccess;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RoleAndPermissionsCriteria that = (RoleAndPermissionsCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(readAccess, that.readAccess) &&
            Objects.equals(createAccess, that.createAccess) &&
            Objects.equals(editAccess, that.editAccess) &&
            Objects.equals(deleteAccess, that.deleteAccess) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, readAccess, createAccess, editAccess, deleteAccess, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RoleAndPermissionsCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (readAccess != null ? "readAccess=" + readAccess + ", " : "") +
            (createAccess != null ? "createAccess=" + createAccess + ", " : "") +
            (editAccess != null ? "editAccess=" + editAccess + ", " : "") +
            (deleteAccess != null ? "deleteAccess=" + deleteAccess + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
