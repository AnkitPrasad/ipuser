package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.ClientSetting} entity. This class is used
 * in {@link com.user.myapp.web.rest.ClientSettingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /client-settings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ClientSettingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter candidateSelfAssessment;

    private IntegerFilter autoReninderAssessment;

    private BooleanFilter allowCandidatePaidVersion;

    private BooleanFilter allowEmployessPaidVersion;

    private IntegerFilter autoArchieveRequisitions;

    private StringFilter companyThemeDarkColour;

    private StringFilter companyThemeLightColour;

    private StringFilter companyUrl;

    private StringFilter companyMenu;

    private Boolean distinct;

    public ClientSettingCriteria() {}

    public ClientSettingCriteria(ClientSettingCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.candidateSelfAssessment = other.candidateSelfAssessment == null ? null : other.candidateSelfAssessment.copy();
        this.autoReninderAssessment = other.autoReninderAssessment == null ? null : other.autoReninderAssessment.copy();
        this.allowCandidatePaidVersion = other.allowCandidatePaidVersion == null ? null : other.allowCandidatePaidVersion.copy();
        this.allowEmployessPaidVersion = other.allowEmployessPaidVersion == null ? null : other.allowEmployessPaidVersion.copy();
        this.autoArchieveRequisitions = other.autoArchieveRequisitions == null ? null : other.autoArchieveRequisitions.copy();
        this.companyThemeDarkColour = other.companyThemeDarkColour == null ? null : other.companyThemeDarkColour.copy();
        this.companyThemeLightColour = other.companyThemeLightColour == null ? null : other.companyThemeLightColour.copy();
        this.companyUrl = other.companyUrl == null ? null : other.companyUrl.copy();
        this.companyMenu = other.companyMenu == null ? null : other.companyMenu.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ClientSettingCriteria copy() {
        return new ClientSettingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCandidateSelfAssessment() {
        return candidateSelfAssessment;
    }

    public IntegerFilter candidateSelfAssessment() {
        if (candidateSelfAssessment == null) {
            candidateSelfAssessment = new IntegerFilter();
        }
        return candidateSelfAssessment;
    }

    public void setCandidateSelfAssessment(IntegerFilter candidateSelfAssessment) {
        this.candidateSelfAssessment = candidateSelfAssessment;
    }

    public IntegerFilter getAutoReninderAssessment() {
        return autoReninderAssessment;
    }

    public IntegerFilter autoReninderAssessment() {
        if (autoReninderAssessment == null) {
            autoReninderAssessment = new IntegerFilter();
        }
        return autoReninderAssessment;
    }

    public void setAutoReninderAssessment(IntegerFilter autoReninderAssessment) {
        this.autoReninderAssessment = autoReninderAssessment;
    }

    public BooleanFilter getAllowCandidatePaidVersion() {
        return allowCandidatePaidVersion;
    }

    public BooleanFilter allowCandidatePaidVersion() {
        if (allowCandidatePaidVersion == null) {
            allowCandidatePaidVersion = new BooleanFilter();
        }
        return allowCandidatePaidVersion;
    }

    public void setAllowCandidatePaidVersion(BooleanFilter allowCandidatePaidVersion) {
        this.allowCandidatePaidVersion = allowCandidatePaidVersion;
    }

    public BooleanFilter getAllowEmployessPaidVersion() {
        return allowEmployessPaidVersion;
    }

    public BooleanFilter allowEmployessPaidVersion() {
        if (allowEmployessPaidVersion == null) {
            allowEmployessPaidVersion = new BooleanFilter();
        }
        return allowEmployessPaidVersion;
    }

    public void setAllowEmployessPaidVersion(BooleanFilter allowEmployessPaidVersion) {
        this.allowEmployessPaidVersion = allowEmployessPaidVersion;
    }

    public IntegerFilter getAutoArchieveRequisitions() {
        return autoArchieveRequisitions;
    }

    public IntegerFilter autoArchieveRequisitions() {
        if (autoArchieveRequisitions == null) {
            autoArchieveRequisitions = new IntegerFilter();
        }
        return autoArchieveRequisitions;
    }

    public void setAutoArchieveRequisitions(IntegerFilter autoArchieveRequisitions) {
        this.autoArchieveRequisitions = autoArchieveRequisitions;
    }

    public StringFilter getCompanyThemeDarkColour() {
        return companyThemeDarkColour;
    }

    public StringFilter companyThemeDarkColour() {
        if (companyThemeDarkColour == null) {
            companyThemeDarkColour = new StringFilter();
        }
        return companyThemeDarkColour;
    }

    public void setCompanyThemeDarkColour(StringFilter companyThemeDarkColour) {
        this.companyThemeDarkColour = companyThemeDarkColour;
    }

    public StringFilter getCompanyThemeLightColour() {
        return companyThemeLightColour;
    }

    public StringFilter companyThemeLightColour() {
        if (companyThemeLightColour == null) {
            companyThemeLightColour = new StringFilter();
        }
        return companyThemeLightColour;
    }

    public void setCompanyThemeLightColour(StringFilter companyThemeLightColour) {
        this.companyThemeLightColour = companyThemeLightColour;
    }

    public StringFilter getCompanyUrl() {
        return companyUrl;
    }

    public StringFilter companyUrl() {
        if (companyUrl == null) {
            companyUrl = new StringFilter();
        }
        return companyUrl;
    }

    public void setCompanyUrl(StringFilter companyUrl) {
        this.companyUrl = companyUrl;
    }

    public StringFilter getCompanyMenu() {
        return companyMenu;
    }

    public StringFilter companyMenu() {
        if (companyMenu == null) {
            companyMenu = new StringFilter();
        }
        return companyMenu;
    }

    public void setCompanyMenu(StringFilter companyMenu) {
        this.companyMenu = companyMenu;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClientSettingCriteria that = (ClientSettingCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(candidateSelfAssessment, that.candidateSelfAssessment) &&
            Objects.equals(autoReninderAssessment, that.autoReninderAssessment) &&
            Objects.equals(allowCandidatePaidVersion, that.allowCandidatePaidVersion) &&
            Objects.equals(allowEmployessPaidVersion, that.allowEmployessPaidVersion) &&
            Objects.equals(autoArchieveRequisitions, that.autoArchieveRequisitions) &&
            Objects.equals(companyThemeDarkColour, that.companyThemeDarkColour) &&
            Objects.equals(companyThemeLightColour, that.companyThemeLightColour) &&
            Objects.equals(companyUrl, that.companyUrl) &&
            Objects.equals(companyMenu, that.companyMenu) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            candidateSelfAssessment,
            autoReninderAssessment,
            allowCandidatePaidVersion,
            allowEmployessPaidVersion,
            autoArchieveRequisitions,
            companyThemeDarkColour,
            companyThemeLightColour,
            companyUrl,
            companyMenu,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClientSettingCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (candidateSelfAssessment != null ? "candidateSelfAssessment=" + candidateSelfAssessment + ", " : "") +
            (autoReninderAssessment != null ? "autoReninderAssessment=" + autoReninderAssessment + ", " : "") +
            (allowCandidatePaidVersion != null ? "allowCandidatePaidVersion=" + allowCandidatePaidVersion + ", " : "") +
            (allowEmployessPaidVersion != null ? "allowEmployessPaidVersion=" + allowEmployessPaidVersion + ", " : "") +
            (autoArchieveRequisitions != null ? "autoArchieveRequisitions=" + autoArchieveRequisitions + ", " : "") +
            (companyThemeDarkColour != null ? "companyThemeDarkColour=" + companyThemeDarkColour + ", " : "") +
            (companyThemeLightColour != null ? "companyThemeLightColour=" + companyThemeLightColour + ", " : "") +
            (companyUrl != null ? "companyUrl=" + companyUrl + ", " : "") +
            (companyMenu != null ? "companyMenu=" + companyMenu + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
