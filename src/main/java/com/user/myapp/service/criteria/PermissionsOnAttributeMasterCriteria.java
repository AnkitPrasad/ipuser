package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.PermissionsOnAttributeMaster} entity. This class is used
 * in {@link com.user.myapp.web.rest.PermissionsOnAttributeMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /permissions-on-attribute-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PermissionsOnAttributeMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter attributeKey;

    private StringFilter attributeTitle;

    private LongFilter userPermissionOnAttributeId;

    private Boolean distinct;

    public PermissionsOnAttributeMasterCriteria() {}

    public PermissionsOnAttributeMasterCriteria(PermissionsOnAttributeMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.attributeKey = other.attributeKey == null ? null : other.attributeKey.copy();
        this.attributeTitle = other.attributeTitle == null ? null : other.attributeTitle.copy();
        this.userPermissionOnAttributeId = other.userPermissionOnAttributeId == null ? null : other.userPermissionOnAttributeId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PermissionsOnAttributeMasterCriteria copy() {
        return new PermissionsOnAttributeMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAttributeKey() {
        return attributeKey;
    }

    public StringFilter attributeKey() {
        if (attributeKey == null) {
            attributeKey = new StringFilter();
        }
        return attributeKey;
    }

    public void setAttributeKey(StringFilter attributeKey) {
        this.attributeKey = attributeKey;
    }

    public StringFilter getAttributeTitle() {
        return attributeTitle;
    }

    public StringFilter attributeTitle() {
        if (attributeTitle == null) {
            attributeTitle = new StringFilter();
        }
        return attributeTitle;
    }

    public void setAttributeTitle(StringFilter attributeTitle) {
        this.attributeTitle = attributeTitle;
    }

    public LongFilter getUserPermissionOnAttributeId() {
        return userPermissionOnAttributeId;
    }

    public LongFilter userPermissionOnAttributeId() {
        if (userPermissionOnAttributeId == null) {
            userPermissionOnAttributeId = new LongFilter();
        }
        return userPermissionOnAttributeId;
    }

    public void setUserPermissionOnAttributeId(LongFilter userPermissionOnAttributeId) {
        this.userPermissionOnAttributeId = userPermissionOnAttributeId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PermissionsOnAttributeMasterCriteria that = (PermissionsOnAttributeMasterCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(attributeKey, that.attributeKey) &&
            Objects.equals(attributeTitle, that.attributeTitle) &&
            Objects.equals(userPermissionOnAttributeId, that.userPermissionOnAttributeId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, attributeKey, attributeTitle, userPermissionOnAttributeId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PermissionsOnAttributeMasterCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (attributeKey != null ? "attributeKey=" + attributeKey + ", " : "") +
            (attributeTitle != null ? "attributeTitle=" + attributeTitle + ", " : "") +
            (userPermissionOnAttributeId != null ? "userPermissionOnAttributeId=" + userPermissionOnAttributeId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
