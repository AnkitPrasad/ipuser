package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.UserPermissionOnAttribute} entity. This class is used
 * in {@link com.user.myapp.web.rest.UserPermissionOnAttributeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-permission-on-attributes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserPermissionOnAttributeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter roleMasterId;

    private LongFilter permissionsOnAttributeMasterId;

    private Boolean distinct;

    public UserPermissionOnAttributeCriteria() {}

    public UserPermissionOnAttributeCriteria(UserPermissionOnAttributeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.roleMasterId = other.roleMasterId == null ? null : other.roleMasterId.copy();
        this.permissionsOnAttributeMasterId =
            other.permissionsOnAttributeMasterId == null ? null : other.permissionsOnAttributeMasterId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public UserPermissionOnAttributeCriteria copy() {
        return new UserPermissionOnAttributeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getRoleMasterId() {
        return roleMasterId;
    }

    public LongFilter roleMasterId() {
        if (roleMasterId == null) {
            roleMasterId = new LongFilter();
        }
        return roleMasterId;
    }

    public void setRoleMasterId(LongFilter roleMasterId) {
        this.roleMasterId = roleMasterId;
    }

    public LongFilter getPermissionsOnAttributeMasterId() {
        return permissionsOnAttributeMasterId;
    }

    public LongFilter permissionsOnAttributeMasterId() {
        if (permissionsOnAttributeMasterId == null) {
            permissionsOnAttributeMasterId = new LongFilter();
        }
        return permissionsOnAttributeMasterId;
    }

    public void setPermissionsOnAttributeMasterId(LongFilter permissionsOnAttributeMasterId) {
        this.permissionsOnAttributeMasterId = permissionsOnAttributeMasterId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserPermissionOnAttributeCriteria that = (UserPermissionOnAttributeCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(roleMasterId, that.roleMasterId) &&
            Objects.equals(permissionsOnAttributeMasterId, that.permissionsOnAttributeMasterId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roleMasterId, permissionsOnAttributeMasterId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserPermissionOnAttributeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (roleMasterId != null ? "roleMasterId=" + roleMasterId + ", " : "") +
            (permissionsOnAttributeMasterId != null ? "permissionsOnAttributeMasterId=" + permissionsOnAttributeMasterId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
