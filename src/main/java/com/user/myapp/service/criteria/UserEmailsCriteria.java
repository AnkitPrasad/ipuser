package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.UserEmails} entity. This class is used
 * in {@link com.user.myapp.web.rest.UserEmailsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-emails?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserEmailsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter email;

    private LongFilter loginId;

    private LongFilter inPreferenceUserId;

    private Boolean distinct;

    public UserEmailsCriteria() {}

    public UserEmailsCriteria(UserEmailsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.loginId = other.loginId == null ? null : other.loginId.copy();
        this.inPreferenceUserId = other.inPreferenceUserId == null ? null : other.inPreferenceUserId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public UserEmailsCriteria copy() {
        return new UserEmailsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEmail() {
        return email;
    }

    public StringFilter email() {
        if (email == null) {
            email = new StringFilter();
        }
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public LongFilter getLoginId() {
        return loginId;
    }

    public LongFilter loginId() {
        if (loginId == null) {
            loginId = new LongFilter();
        }
        return loginId;
    }

    public void setLoginId(LongFilter loginId) {
        this.loginId = loginId;
    }

    public LongFilter getInPreferenceUserId() {
        return inPreferenceUserId;
    }

    public LongFilter inPreferenceUserId() {
        if (inPreferenceUserId == null) {
            inPreferenceUserId = new LongFilter();
        }
        return inPreferenceUserId;
    }

    public void setInPreferenceUserId(LongFilter inPreferenceUserId) {
        this.inPreferenceUserId = inPreferenceUserId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserEmailsCriteria that = (UserEmailsCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(email, that.email) &&
            Objects.equals(loginId, that.loginId) &&
            Objects.equals(inPreferenceUserId, that.inPreferenceUserId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, loginId, inPreferenceUserId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserEmailsCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (email != null ? "email=" + email + ", " : "") +
            (loginId != null ? "loginId=" + loginId + ", " : "") +
            (inPreferenceUserId != null ? "inPreferenceUserId=" + inPreferenceUserId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
