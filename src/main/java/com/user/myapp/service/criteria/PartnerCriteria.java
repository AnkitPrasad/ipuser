package com.user.myapp.service.criteria;

import com.user.myapp.domain.enumeration.Partnertype;
import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.Partner} entity. This class is used
 * in {@link com.user.myapp.web.rest.PartnerResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /partners?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PartnerCriteria implements Serializable, Criteria {

    /**
     * Class for filtering Partnertype
     */
    public static class PartnertypeFilter extends Filter<Partnertype> {

        public PartnertypeFilter() {}

        public PartnertypeFilter(PartnertypeFilter filter) {
            super(filter);
        }

        @Override
        public PartnertypeFilter copy() {
            return new PartnertypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter domain;

    private DoubleFilter partnerCommission;

    private PartnertypeFilter partnerType;

    private IntegerFilter valueM;

    private IntegerFilter valueC;

    private InstantFilter createdDate;

    private LongFilter createdBy;

    private InstantFilter updatedDate;

    private LongFilter updatedBy;

    private BooleanFilter status;

    private Boolean distinct;

    public PartnerCriteria() {}

    public PartnerCriteria(PartnerCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.domain = other.domain == null ? null : other.domain.copy();
        this.partnerCommission = other.partnerCommission == null ? null : other.partnerCommission.copy();
        this.partnerType = other.partnerType == null ? null : other.partnerType.copy();
        this.valueM = other.valueM == null ? null : other.valueM.copy();
        this.valueC = other.valueC == null ? null : other.valueC.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.updatedDate = other.updatedDate == null ? null : other.updatedDate.copy();
        this.updatedBy = other.updatedBy == null ? null : other.updatedBy.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PartnerCriteria copy() {
        return new PartnerCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDomain() {
        return domain;
    }

    public StringFilter domain() {
        if (domain == null) {
            domain = new StringFilter();
        }
        return domain;
    }

    public void setDomain(StringFilter domain) {
        this.domain = domain;
    }

    public DoubleFilter getPartnerCommission() {
        return partnerCommission;
    }

    public DoubleFilter partnerCommission() {
        if (partnerCommission == null) {
            partnerCommission = new DoubleFilter();
        }
        return partnerCommission;
    }

    public void setPartnerCommission(DoubleFilter partnerCommission) {
        this.partnerCommission = partnerCommission;
    }

    public PartnertypeFilter getPartnerType() {
        return partnerType;
    }

    public PartnertypeFilter partnerType() {
        if (partnerType == null) {
            partnerType = new PartnertypeFilter();
        }
        return partnerType;
    }

    public void setPartnerType(PartnertypeFilter partnerType) {
        this.partnerType = partnerType;
    }

    public IntegerFilter getValueM() {
        return valueM;
    }

    public IntegerFilter valueM() {
        if (valueM == null) {
            valueM = new IntegerFilter();
        }
        return valueM;
    }

    public void setValueM(IntegerFilter valueM) {
        this.valueM = valueM;
    }

    public IntegerFilter getValueC() {
        return valueC;
    }

    public IntegerFilter valueC() {
        if (valueC == null) {
            valueC = new IntegerFilter();
        }
        return valueC;
    }

    public void setValueC(IntegerFilter valueC) {
        this.valueC = valueC;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public LongFilter getCreatedBy() {
        return createdBy;
    }

    public LongFilter createdBy() {
        if (createdBy == null) {
            createdBy = new LongFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(LongFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getUpdatedDate() {
        return updatedDate;
    }

    public InstantFilter updatedDate() {
        if (updatedDate == null) {
            updatedDate = new InstantFilter();
        }
        return updatedDate;
    }

    public void setUpdatedDate(InstantFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    public LongFilter getUpdatedBy() {
        return updatedBy;
    }

    public LongFilter updatedBy() {
        if (updatedBy == null) {
            updatedBy = new LongFilter();
        }
        return updatedBy;
    }

    public void setUpdatedBy(LongFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public BooleanFilter getStatus() {
        return status;
    }

    public BooleanFilter status() {
        if (status == null) {
            status = new BooleanFilter();
        }
        return status;
    }

    public void setStatus(BooleanFilter status) {
        this.status = status;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PartnerCriteria that = (PartnerCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(domain, that.domain) &&
            Objects.equals(partnerCommission, that.partnerCommission) &&
            Objects.equals(partnerType, that.partnerType) &&
            Objects.equals(valueM, that.valueM) &&
            Objects.equals(valueC, that.valueC) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(updatedDate, that.updatedDate) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(status, that.status) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            name,
            domain,
            partnerCommission,
            partnerType,
            valueM,
            valueC,
            createdDate,
            createdBy,
            updatedDate,
            updatedBy,
            status,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PartnerCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (domain != null ? "domain=" + domain + ", " : "") +
            (partnerCommission != null ? "partnerCommission=" + partnerCommission + ", " : "") +
            (partnerType != null ? "partnerType=" + partnerType + ", " : "") +
            (valueM != null ? "valueM=" + valueM + ", " : "") +
            (valueC != null ? "valueC=" + valueC + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
            (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
            (status != null ? "status=" + status + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
