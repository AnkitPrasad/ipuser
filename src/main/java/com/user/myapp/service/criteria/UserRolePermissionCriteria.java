package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.UserRolePermission} entity. This class is used
 * in {@link com.user.myapp.web.rest.UserRolePermissionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-role-permissions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserRolePermissionCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter inPreferenceUserId;

    private LongFilter roleMasterId;

    private LongFilter permissionMasterId;

    private Boolean distinct;

    public UserRolePermissionCriteria() {}

    public UserRolePermissionCriteria(UserRolePermissionCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.inPreferenceUserId = other.inPreferenceUserId == null ? null : other.inPreferenceUserId.copy();
        this.roleMasterId = other.roleMasterId == null ? null : other.roleMasterId.copy();
        this.permissionMasterId = other.permissionMasterId == null ? null : other.permissionMasterId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public UserRolePermissionCriteria copy() {
        return new UserRolePermissionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getInPreferenceUserId() {
        return inPreferenceUserId;
    }

    public LongFilter inPreferenceUserId() {
        if (inPreferenceUserId == null) {
            inPreferenceUserId = new LongFilter();
        }
        return inPreferenceUserId;
    }

    public void setInPreferenceUserId(LongFilter inPreferenceUserId) {
        this.inPreferenceUserId = inPreferenceUserId;
    }

    public LongFilter getRoleMasterId() {
        return roleMasterId;
    }

    public LongFilter roleMasterId() {
        if (roleMasterId == null) {
            roleMasterId = new LongFilter();
        }
        return roleMasterId;
    }

    public void setRoleMasterId(LongFilter roleMasterId) {
        this.roleMasterId = roleMasterId;
    }

    public LongFilter getPermissionMasterId() {
        return permissionMasterId;
    }

    public LongFilter permissionMasterId() {
        if (permissionMasterId == null) {
            permissionMasterId = new LongFilter();
        }
        return permissionMasterId;
    }

    public void setPermissionMasterId(LongFilter permissionMasterId) {
        this.permissionMasterId = permissionMasterId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserRolePermissionCriteria that = (UserRolePermissionCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(inPreferenceUserId, that.inPreferenceUserId) &&
            Objects.equals(roleMasterId, that.roleMasterId) &&
            Objects.equals(permissionMasterId, that.permissionMasterId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, inPreferenceUserId, roleMasterId, permissionMasterId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserRolePermissionCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (inPreferenceUserId != null ? "inPreferenceUserId=" + inPreferenceUserId + ", " : "") +
            (roleMasterId != null ? "roleMasterId=" + roleMasterId + ", " : "") +
            (permissionMasterId != null ? "permissionMasterId=" + permissionMasterId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
