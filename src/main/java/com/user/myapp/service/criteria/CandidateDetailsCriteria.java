package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.CandidateDetails} entity. This class is used
 * in {@link com.user.myapp.web.rest.CandidateDetailsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /candidate-details?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CandidateDetailsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter candidateCountry;

    private StringFilter businessUnits;

    private StringFilter legalEntity;

    private StringFilter candidateSubFunction;

    private StringFilter designation;

    private StringFilter jobTitle;

    private StringFilter location;

    private StringFilter subBusinessUnits;

    private StringFilter candidateFunction;

    private StringFilter workLevel;

    private StringFilter candidateGrade;

    private Boolean distinct;

    public CandidateDetailsCriteria() {}

    public CandidateDetailsCriteria(CandidateDetailsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.candidateCountry = other.candidateCountry == null ? null : other.candidateCountry.copy();
        this.businessUnits = other.businessUnits == null ? null : other.businessUnits.copy();
        this.legalEntity = other.legalEntity == null ? null : other.legalEntity.copy();
        this.candidateSubFunction = other.candidateSubFunction == null ? null : other.candidateSubFunction.copy();
        this.designation = other.designation == null ? null : other.designation.copy();
        this.jobTitle = other.jobTitle == null ? null : other.jobTitle.copy();
        this.location = other.location == null ? null : other.location.copy();
        this.subBusinessUnits = other.subBusinessUnits == null ? null : other.subBusinessUnits.copy();
        this.candidateFunction = other.candidateFunction == null ? null : other.candidateFunction.copy();
        this.workLevel = other.workLevel == null ? null : other.workLevel.copy();
        this.candidateGrade = other.candidateGrade == null ? null : other.candidateGrade.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CandidateDetailsCriteria copy() {
        return new CandidateDetailsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCandidateCountry() {
        return candidateCountry;
    }

    public StringFilter candidateCountry() {
        if (candidateCountry == null) {
            candidateCountry = new StringFilter();
        }
        return candidateCountry;
    }

    public void setCandidateCountry(StringFilter candidateCountry) {
        this.candidateCountry = candidateCountry;
    }

    public StringFilter getBusinessUnits() {
        return businessUnits;
    }

    public StringFilter businessUnits() {
        if (businessUnits == null) {
            businessUnits = new StringFilter();
        }
        return businessUnits;
    }

    public void setBusinessUnits(StringFilter businessUnits) {
        this.businessUnits = businessUnits;
    }

    public StringFilter getLegalEntity() {
        return legalEntity;
    }

    public StringFilter legalEntity() {
        if (legalEntity == null) {
            legalEntity = new StringFilter();
        }
        return legalEntity;
    }

    public void setLegalEntity(StringFilter legalEntity) {
        this.legalEntity = legalEntity;
    }

    public StringFilter getCandidateSubFunction() {
        return candidateSubFunction;
    }

    public StringFilter candidateSubFunction() {
        if (candidateSubFunction == null) {
            candidateSubFunction = new StringFilter();
        }
        return candidateSubFunction;
    }

    public void setCandidateSubFunction(StringFilter candidateSubFunction) {
        this.candidateSubFunction = candidateSubFunction;
    }

    public StringFilter getDesignation() {
        return designation;
    }

    public StringFilter designation() {
        if (designation == null) {
            designation = new StringFilter();
        }
        return designation;
    }

    public void setDesignation(StringFilter designation) {
        this.designation = designation;
    }

    public StringFilter getJobTitle() {
        return jobTitle;
    }

    public StringFilter jobTitle() {
        if (jobTitle == null) {
            jobTitle = new StringFilter();
        }
        return jobTitle;
    }

    public void setJobTitle(StringFilter jobTitle) {
        this.jobTitle = jobTitle;
    }

    public StringFilter getLocation() {
        return location;
    }

    public StringFilter location() {
        if (location == null) {
            location = new StringFilter();
        }
        return location;
    }

    public void setLocation(StringFilter location) {
        this.location = location;
    }

    public StringFilter getSubBusinessUnits() {
        return subBusinessUnits;
    }

    public StringFilter subBusinessUnits() {
        if (subBusinessUnits == null) {
            subBusinessUnits = new StringFilter();
        }
        return subBusinessUnits;
    }

    public void setSubBusinessUnits(StringFilter subBusinessUnits) {
        this.subBusinessUnits = subBusinessUnits;
    }

    public StringFilter getCandidateFunction() {
        return candidateFunction;
    }

    public StringFilter candidateFunction() {
        if (candidateFunction == null) {
            candidateFunction = new StringFilter();
        }
        return candidateFunction;
    }

    public void setCandidateFunction(StringFilter candidateFunction) {
        this.candidateFunction = candidateFunction;
    }

    public StringFilter getWorkLevel() {
        return workLevel;
    }

    public StringFilter workLevel() {
        if (workLevel == null) {
            workLevel = new StringFilter();
        }
        return workLevel;
    }

    public void setWorkLevel(StringFilter workLevel) {
        this.workLevel = workLevel;
    }

    public StringFilter getCandidateGrade() {
        return candidateGrade;
    }

    public StringFilter candidateGrade() {
        if (candidateGrade == null) {
            candidateGrade = new StringFilter();
        }
        return candidateGrade;
    }

    public void setCandidateGrade(StringFilter candidateGrade) {
        this.candidateGrade = candidateGrade;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CandidateDetailsCriteria that = (CandidateDetailsCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(candidateCountry, that.candidateCountry) &&
            Objects.equals(businessUnits, that.businessUnits) &&
            Objects.equals(legalEntity, that.legalEntity) &&
            Objects.equals(candidateSubFunction, that.candidateSubFunction) &&
            Objects.equals(designation, that.designation) &&
            Objects.equals(jobTitle, that.jobTitle) &&
            Objects.equals(location, that.location) &&
            Objects.equals(subBusinessUnits, that.subBusinessUnits) &&
            Objects.equals(candidateFunction, that.candidateFunction) &&
            Objects.equals(workLevel, that.workLevel) &&
            Objects.equals(candidateGrade, that.candidateGrade) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            candidateCountry,
            businessUnits,
            legalEntity,
            candidateSubFunction,
            designation,
            jobTitle,
            location,
            subBusinessUnits,
            candidateFunction,
            workLevel,
            candidateGrade,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CandidateDetailsCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (candidateCountry != null ? "candidateCountry=" + candidateCountry + ", " : "") +
            (businessUnits != null ? "businessUnits=" + businessUnits + ", " : "") +
            (legalEntity != null ? "legalEntity=" + legalEntity + ", " : "") +
            (candidateSubFunction != null ? "candidateSubFunction=" + candidateSubFunction + ", " : "") +
            (designation != null ? "designation=" + designation + ", " : "") +
            (jobTitle != null ? "jobTitle=" + jobTitle + ", " : "") +
            (location != null ? "location=" + location + ", " : "") +
            (subBusinessUnits != null ? "subBusinessUnits=" + subBusinessUnits + ", " : "") +
            (candidateFunction != null ? "candidateFunction=" + candidateFunction + ", " : "") +
            (workLevel != null ? "workLevel=" + workLevel + ", " : "") +
            (candidateGrade != null ? "candidateGrade=" + candidateGrade + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
