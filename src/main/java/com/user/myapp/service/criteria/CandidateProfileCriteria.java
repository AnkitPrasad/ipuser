package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.CandidateProfile} entity. This class is used
 * in {@link com.user.myapp.web.rest.CandidateProfileResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /candidate-profiles?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CandidateProfileCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter aboutMe;

    private StringFilter careerSummary;

    private StringFilter education;

    private StringFilter dob;

    private StringFilter currentEmployment;

    private StringFilter certifications;

    private StringFilter experience;

    private LongFilter candidateId;

    private Boolean distinct;

    public CandidateProfileCriteria() {}

    public CandidateProfileCriteria(CandidateProfileCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.aboutMe = other.aboutMe == null ? null : other.aboutMe.copy();
        this.careerSummary = other.careerSummary == null ? null : other.careerSummary.copy();
        this.education = other.education == null ? null : other.education.copy();
        this.dob = other.dob == null ? null : other.dob.copy();
        this.currentEmployment = other.currentEmployment == null ? null : other.currentEmployment.copy();
        this.certifications = other.certifications == null ? null : other.certifications.copy();
        this.experience = other.experience == null ? null : other.experience.copy();
        this.candidateId = other.candidateId == null ? null : other.candidateId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CandidateProfileCriteria copy() {
        return new CandidateProfileCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAboutMe() {
        return aboutMe;
    }

    public StringFilter aboutMe() {
        if (aboutMe == null) {
            aboutMe = new StringFilter();
        }
        return aboutMe;
    }

    public void setAboutMe(StringFilter aboutMe) {
        this.aboutMe = aboutMe;
    }

    public StringFilter getCareerSummary() {
        return careerSummary;
    }

    public StringFilter careerSummary() {
        if (careerSummary == null) {
            careerSummary = new StringFilter();
        }
        return careerSummary;
    }

    public void setCareerSummary(StringFilter careerSummary) {
        this.careerSummary = careerSummary;
    }

    public StringFilter getEducation() {
        return education;
    }

    public StringFilter education() {
        if (education == null) {
            education = new StringFilter();
        }
        return education;
    }

    public void setEducation(StringFilter education) {
        this.education = education;
    }

    public StringFilter getDob() {
        return dob;
    }

    public StringFilter dob() {
        if (dob == null) {
            dob = new StringFilter();
        }
        return dob;
    }

    public void setDob(StringFilter dob) {
        this.dob = dob;
    }

    public StringFilter getCurrentEmployment() {
        return currentEmployment;
    }

    public StringFilter currentEmployment() {
        if (currentEmployment == null) {
            currentEmployment = new StringFilter();
        }
        return currentEmployment;
    }

    public void setCurrentEmployment(StringFilter currentEmployment) {
        this.currentEmployment = currentEmployment;
    }

    public StringFilter getCertifications() {
        return certifications;
    }

    public StringFilter certifications() {
        if (certifications == null) {
            certifications = new StringFilter();
        }
        return certifications;
    }

    public void setCertifications(StringFilter certifications) {
        this.certifications = certifications;
    }

    public StringFilter getExperience() {
        return experience;
    }

    public StringFilter experience() {
        if (experience == null) {
            experience = new StringFilter();
        }
        return experience;
    }

    public void setExperience(StringFilter experience) {
        this.experience = experience;
    }

    public LongFilter getCandidateId() {
        return candidateId;
    }

    public LongFilter candidateId() {
        if (candidateId == null) {
            candidateId = new LongFilter();
        }
        return candidateId;
    }

    public void setCandidateId(LongFilter candidateId) {
        this.candidateId = candidateId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CandidateProfileCriteria that = (CandidateProfileCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(aboutMe, that.aboutMe) &&
            Objects.equals(careerSummary, that.careerSummary) &&
            Objects.equals(education, that.education) &&
            Objects.equals(dob, that.dob) &&
            Objects.equals(currentEmployment, that.currentEmployment) &&
            Objects.equals(certifications, that.certifications) &&
            Objects.equals(experience, that.experience) &&
            Objects.equals(candidateId, that.candidateId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            aboutMe,
            careerSummary,
            education,
            dob,
            currentEmployment,
            certifications,
            experience,
            candidateId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CandidateProfileCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (aboutMe != null ? "aboutMe=" + aboutMe + ", " : "") +
            (careerSummary != null ? "careerSummary=" + careerSummary + ", " : "") +
            (education != null ? "education=" + education + ", " : "") +
            (dob != null ? "dob=" + dob + ", " : "") +
            (currentEmployment != null ? "currentEmployment=" + currentEmployment + ", " : "") +
            (certifications != null ? "certifications=" + certifications + ", " : "") +
            (experience != null ? "experience=" + experience + ", " : "") +
            (candidateId != null ? "candidateId=" + candidateId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
