package com.user.myapp.service.criteria;

import com.user.myapp.domain.enumeration.ClientType;
import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.Client} entity. This class is used
 * in {@link com.user.myapp.web.rest.ClientResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /clients?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ClientCriteria implements Serializable, Criteria {

    /**
     * Class for filtering ClientType
     */
    public static class ClientTypeFilter extends Filter<ClientType> {

        public ClientTypeFilter() {}

        public ClientTypeFilter(ClientTypeFilter filter) {
            super(filter);
        }

        @Override
        public ClientTypeFilter copy() {
            return new ClientTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter legalEntity;

    private StringFilter industry;

    private StringFilter bankDetails;

    private ClientTypeFilter clientType;

    private StringFilter tnaNo;

    private StringFilter gstNo;

    private StringFilter pan;

    private StringFilter uploadClientLogo;

    private InstantFilter createdDate;

    private LongFilter createdBy;

    private InstantFilter updatedDate;

    private LongFilter updatedBy;

    private StringFilter status;

    private LongFilter clientSettingId;

    private LongFilter candidateId;

    private Boolean distinct;

    public ClientCriteria() {}

    public ClientCriteria(ClientCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.legalEntity = other.legalEntity == null ? null : other.legalEntity.copy();
        this.industry = other.industry == null ? null : other.industry.copy();
        this.bankDetails = other.bankDetails == null ? null : other.bankDetails.copy();
        this.clientType = other.clientType == null ? null : other.clientType.copy();
        this.tnaNo = other.tnaNo == null ? null : other.tnaNo.copy();
        this.gstNo = other.gstNo == null ? null : other.gstNo.copy();
        this.pan = other.pan == null ? null : other.pan.copy();
        this.uploadClientLogo = other.uploadClientLogo == null ? null : other.uploadClientLogo.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.updatedDate = other.updatedDate == null ? null : other.updatedDate.copy();
        this.updatedBy = other.updatedBy == null ? null : other.updatedBy.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.clientSettingId = other.clientSettingId == null ? null : other.clientSettingId.copy();
        this.candidateId = other.candidateId == null ? null : other.candidateId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ClientCriteria copy() {
        return new ClientCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLegalEntity() {
        return legalEntity;
    }

    public StringFilter legalEntity() {
        if (legalEntity == null) {
            legalEntity = new StringFilter();
        }
        return legalEntity;
    }

    public void setLegalEntity(StringFilter legalEntity) {
        this.legalEntity = legalEntity;
    }

    public StringFilter getIndustry() {
        return industry;
    }

    public StringFilter industry() {
        if (industry == null) {
            industry = new StringFilter();
        }
        return industry;
    }

    public void setIndustry(StringFilter industry) {
        this.industry = industry;
    }

    public StringFilter getBankDetails() {
        return bankDetails;
    }

    public StringFilter bankDetails() {
        if (bankDetails == null) {
            bankDetails = new StringFilter();
        }
        return bankDetails;
    }

    public void setBankDetails(StringFilter bankDetails) {
        this.bankDetails = bankDetails;
    }

    public ClientTypeFilter getClientType() {
        return clientType;
    }

    public ClientTypeFilter clientType() {
        if (clientType == null) {
            clientType = new ClientTypeFilter();
        }
        return clientType;
    }

    public void setClientType(ClientTypeFilter clientType) {
        this.clientType = clientType;
    }

    public StringFilter getTnaNo() {
        return tnaNo;
    }

    public StringFilter tnaNo() {
        if (tnaNo == null) {
            tnaNo = new StringFilter();
        }
        return tnaNo;
    }

    public void setTnaNo(StringFilter tnaNo) {
        this.tnaNo = tnaNo;
    }

    public StringFilter getGstNo() {
        return gstNo;
    }

    public StringFilter gstNo() {
        if (gstNo == null) {
            gstNo = new StringFilter();
        }
        return gstNo;
    }

    public void setGstNo(StringFilter gstNo) {
        this.gstNo = gstNo;
    }

    public StringFilter getPan() {
        return pan;
    }

    public StringFilter pan() {
        if (pan == null) {
            pan = new StringFilter();
        }
        return pan;
    }

    public void setPan(StringFilter pan) {
        this.pan = pan;
    }

    public StringFilter getUploadClientLogo() {
        return uploadClientLogo;
    }

    public StringFilter uploadClientLogo() {
        if (uploadClientLogo == null) {
            uploadClientLogo = new StringFilter();
        }
        return uploadClientLogo;
    }

    public void setUploadClientLogo(StringFilter uploadClientLogo) {
        this.uploadClientLogo = uploadClientLogo;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public LongFilter getCreatedBy() {
        return createdBy;
    }

    public LongFilter createdBy() {
        if (createdBy == null) {
            createdBy = new LongFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(LongFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getUpdatedDate() {
        return updatedDate;
    }

    public InstantFilter updatedDate() {
        if (updatedDate == null) {
            updatedDate = new InstantFilter();
        }
        return updatedDate;
    }

    public void setUpdatedDate(InstantFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    public LongFilter getUpdatedBy() {
        return updatedBy;
    }

    public LongFilter updatedBy() {
        if (updatedBy == null) {
            updatedBy = new LongFilter();
        }
        return updatedBy;
    }

    public void setUpdatedBy(LongFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public StringFilter getStatus() {
        return status;
    }

    public StringFilter status() {
        if (status == null) {
            status = new StringFilter();
        }
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public LongFilter getClientSettingId() {
        return clientSettingId;
    }

    public LongFilter clientSettingId() {
        if (clientSettingId == null) {
            clientSettingId = new LongFilter();
        }
        return clientSettingId;
    }

    public void setClientSettingId(LongFilter clientSettingId) {
        this.clientSettingId = clientSettingId;
    }

    public LongFilter getCandidateId() {
        return candidateId;
    }

    public LongFilter candidateId() {
        if (candidateId == null) {
            candidateId = new LongFilter();
        }
        return candidateId;
    }

    public void setCandidateId(LongFilter candidateId) {
        this.candidateId = candidateId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClientCriteria that = (ClientCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(legalEntity, that.legalEntity) &&
            Objects.equals(industry, that.industry) &&
            Objects.equals(bankDetails, that.bankDetails) &&
            Objects.equals(clientType, that.clientType) &&
            Objects.equals(tnaNo, that.tnaNo) &&
            Objects.equals(gstNo, that.gstNo) &&
            Objects.equals(pan, that.pan) &&
            Objects.equals(uploadClientLogo, that.uploadClientLogo) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(updatedDate, that.updatedDate) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(status, that.status) &&
            Objects.equals(clientSettingId, that.clientSettingId) &&
            Objects.equals(candidateId, that.candidateId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            legalEntity,
            industry,
            bankDetails,
            clientType,
            tnaNo,
            gstNo,
            pan,
            uploadClientLogo,
            createdDate,
            createdBy,
            updatedDate,
            updatedBy,
            status,
            clientSettingId,
            candidateId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClientCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (legalEntity != null ? "legalEntity=" + legalEntity + ", " : "") +
            (industry != null ? "industry=" + industry + ", " : "") +
            (bankDetails != null ? "bankDetails=" + bankDetails + ", " : "") +
            (clientType != null ? "clientType=" + clientType + ", " : "") +
            (tnaNo != null ? "tnaNo=" + tnaNo + ", " : "") +
            (gstNo != null ? "gstNo=" + gstNo + ", " : "") +
            (pan != null ? "pan=" + pan + ", " : "") +
            (uploadClientLogo != null ? "uploadClientLogo=" + uploadClientLogo + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
            (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
            (status != null ? "status=" + status + ", " : "") +
            (clientSettingId != null ? "clientSettingId=" + clientSettingId + ", " : "") +
            (candidateId != null ? "candidateId=" + candidateId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
