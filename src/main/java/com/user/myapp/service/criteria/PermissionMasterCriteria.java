package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.PermissionMaster} entity. This class is used
 * in {@link com.user.myapp.web.rest.PermissionMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /permission-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PermissionMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter pageName;

    private StringFilter pageUrl;

    private LongFilter userRolePermissionId;

    private Boolean distinct;

    public PermissionMasterCriteria() {}

    public PermissionMasterCriteria(PermissionMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.pageName = other.pageName == null ? null : other.pageName.copy();
        this.pageUrl = other.pageUrl == null ? null : other.pageUrl.copy();
        this.userRolePermissionId = other.userRolePermissionId == null ? null : other.userRolePermissionId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PermissionMasterCriteria copy() {
        return new PermissionMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPageName() {
        return pageName;
    }

    public StringFilter pageName() {
        if (pageName == null) {
            pageName = new StringFilter();
        }
        return pageName;
    }

    public void setPageName(StringFilter pageName) {
        this.pageName = pageName;
    }

    public StringFilter getPageUrl() {
        return pageUrl;
    }

    public StringFilter pageUrl() {
        if (pageUrl == null) {
            pageUrl = new StringFilter();
        }
        return pageUrl;
    }

    public void setPageUrl(StringFilter pageUrl) {
        this.pageUrl = pageUrl;
    }

    public LongFilter getUserRolePermissionId() {
        return userRolePermissionId;
    }

    public LongFilter userRolePermissionId() {
        if (userRolePermissionId == null) {
            userRolePermissionId = new LongFilter();
        }
        return userRolePermissionId;
    }

    public void setUserRolePermissionId(LongFilter userRolePermissionId) {
        this.userRolePermissionId = userRolePermissionId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PermissionMasterCriteria that = (PermissionMasterCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(pageName, that.pageName) &&
            Objects.equals(pageUrl, that.pageUrl) &&
            Objects.equals(userRolePermissionId, that.userRolePermissionId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, pageName, pageUrl, userRolePermissionId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PermissionMasterCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (pageName != null ? "pageName=" + pageName + ", " : "") +
            (pageUrl != null ? "pageUrl=" + pageUrl + ", " : "") +
            (userRolePermissionId != null ? "userRolePermissionId=" + userRolePermissionId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
