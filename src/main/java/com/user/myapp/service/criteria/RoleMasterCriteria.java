package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.RoleMaster} entity. This class is used
 * in {@link com.user.myapp.web.rest.RoleMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /role-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RoleMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter roleName;

    private StringFilter description;

    private InstantFilter createdDate;

    private LongFilter createdBy;

    private InstantFilter updatedDate;

    private LongFilter updatedBy;

    private BooleanFilter status;

    private LongFilter userPermissionOnAttributeId;

    private LongFilter userRolePermissionId;

    private LongFilter inPreferenceUserId;

    private Boolean distinct;

    public RoleMasterCriteria() {}

    public RoleMasterCriteria(RoleMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.roleName = other.roleName == null ? null : other.roleName.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.updatedDate = other.updatedDate == null ? null : other.updatedDate.copy();
        this.updatedBy = other.updatedBy == null ? null : other.updatedBy.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.userPermissionOnAttributeId = other.userPermissionOnAttributeId == null ? null : other.userPermissionOnAttributeId.copy();
        this.userRolePermissionId = other.userRolePermissionId == null ? null : other.userRolePermissionId.copy();
        this.inPreferenceUserId = other.inPreferenceUserId == null ? null : other.inPreferenceUserId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public RoleMasterCriteria copy() {
        return new RoleMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getRoleName() {
        return roleName;
    }

    public StringFilter roleName() {
        if (roleName == null) {
            roleName = new StringFilter();
        }
        return roleName;
    }

    public void setRoleName(StringFilter roleName) {
        this.roleName = roleName;
    }

    public StringFilter getDescription() {
        return description;
    }

    public StringFilter description() {
        if (description == null) {
            description = new StringFilter();
        }
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public LongFilter getCreatedBy() {
        return createdBy;
    }

    public LongFilter createdBy() {
        if (createdBy == null) {
            createdBy = new LongFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(LongFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getUpdatedDate() {
        return updatedDate;
    }

    public InstantFilter updatedDate() {
        if (updatedDate == null) {
            updatedDate = new InstantFilter();
        }
        return updatedDate;
    }

    public void setUpdatedDate(InstantFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    public LongFilter getUpdatedBy() {
        return updatedBy;
    }

    public LongFilter updatedBy() {
        if (updatedBy == null) {
            updatedBy = new LongFilter();
        }
        return updatedBy;
    }

    public void setUpdatedBy(LongFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public BooleanFilter getStatus() {
        return status;
    }

    public BooleanFilter status() {
        if (status == null) {
            status = new BooleanFilter();
        }
        return status;
    }

    public void setStatus(BooleanFilter status) {
        this.status = status;
    }

    public LongFilter getUserPermissionOnAttributeId() {
        return userPermissionOnAttributeId;
    }

    public LongFilter userPermissionOnAttributeId() {
        if (userPermissionOnAttributeId == null) {
            userPermissionOnAttributeId = new LongFilter();
        }
        return userPermissionOnAttributeId;
    }

    public void setUserPermissionOnAttributeId(LongFilter userPermissionOnAttributeId) {
        this.userPermissionOnAttributeId = userPermissionOnAttributeId;
    }

    public LongFilter getUserRolePermissionId() {
        return userRolePermissionId;
    }

    public LongFilter userRolePermissionId() {
        if (userRolePermissionId == null) {
            userRolePermissionId = new LongFilter();
        }
        return userRolePermissionId;
    }

    public void setUserRolePermissionId(LongFilter userRolePermissionId) {
        this.userRolePermissionId = userRolePermissionId;
    }

    public LongFilter getInPreferenceUserId() {
        return inPreferenceUserId;
    }

    public LongFilter inPreferenceUserId() {
        if (inPreferenceUserId == null) {
            inPreferenceUserId = new LongFilter();
        }
        return inPreferenceUserId;
    }

    public void setInPreferenceUserId(LongFilter inPreferenceUserId) {
        this.inPreferenceUserId = inPreferenceUserId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RoleMasterCriteria that = (RoleMasterCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(roleName, that.roleName) &&
            Objects.equals(description, that.description) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(updatedDate, that.updatedDate) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(status, that.status) &&
            Objects.equals(userPermissionOnAttributeId, that.userPermissionOnAttributeId) &&
            Objects.equals(userRolePermissionId, that.userRolePermissionId) &&
            Objects.equals(inPreferenceUserId, that.inPreferenceUserId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            roleName,
            description,
            createdDate,
            createdBy,
            updatedDate,
            updatedBy,
            status,
            userPermissionOnAttributeId,
            userRolePermissionId,
            inPreferenceUserId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RoleMasterCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (roleName != null ? "roleName=" + roleName + ", " : "") +
            (description != null ? "description=" + description + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
            (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
            (status != null ? "status=" + status + ", " : "") +
            (userPermissionOnAttributeId != null ? "userPermissionOnAttributeId=" + userPermissionOnAttributeId + ", " : "") +
            (userRolePermissionId != null ? "userRolePermissionId=" + userRolePermissionId + ", " : "") +
            (inPreferenceUserId != null ? "inPreferenceUserId=" + inPreferenceUserId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
