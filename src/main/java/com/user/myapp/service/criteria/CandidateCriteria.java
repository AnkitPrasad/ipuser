package com.user.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.Candidate} entity. This class is used
 * in {@link com.user.myapp.web.rest.CandidateResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /candidates?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CandidateCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter isInternalCandidate;

    private StringFilter company;

    private InstantFilter createdDate;

    private LongFilter createdBy;

    private InstantFilter updatedDate;

    private LongFilter updatedBy;

    private StringFilter assessmentStatus;

    private BooleanFilter status;

    private BooleanFilter archiveStatus;

    private LongFilter candidateDetailsId;

    private LongFilter candidateProfileId;

    private LongFilter clientId;

    private Boolean distinct;

    public CandidateCriteria() {}

    public CandidateCriteria(CandidateCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.isInternalCandidate = other.isInternalCandidate == null ? null : other.isInternalCandidate.copy();
        this.company = other.company == null ? null : other.company.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.updatedDate = other.updatedDate == null ? null : other.updatedDate.copy();
        this.updatedBy = other.updatedBy == null ? null : other.updatedBy.copy();
        this.assessmentStatus = other.assessmentStatus == null ? null : other.assessmentStatus.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.archiveStatus = other.archiveStatus == null ? null : other.archiveStatus.copy();
        this.candidateDetailsId = other.candidateDetailsId == null ? null : other.candidateDetailsId.copy();
        this.candidateProfileId = other.candidateProfileId == null ? null : other.candidateProfileId.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CandidateCriteria copy() {
        return new CandidateCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getIsInternalCandidate() {
        return isInternalCandidate;
    }

    public BooleanFilter isInternalCandidate() {
        if (isInternalCandidate == null) {
            isInternalCandidate = new BooleanFilter();
        }
        return isInternalCandidate;
    }

    public void setIsInternalCandidate(BooleanFilter isInternalCandidate) {
        this.isInternalCandidate = isInternalCandidate;
    }

    public StringFilter getCompany() {
        return company;
    }

    public StringFilter company() {
        if (company == null) {
            company = new StringFilter();
        }
        return company;
    }

    public void setCompany(StringFilter company) {
        this.company = company;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public LongFilter getCreatedBy() {
        return createdBy;
    }

    public LongFilter createdBy() {
        if (createdBy == null) {
            createdBy = new LongFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(LongFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getUpdatedDate() {
        return updatedDate;
    }

    public InstantFilter updatedDate() {
        if (updatedDate == null) {
            updatedDate = new InstantFilter();
        }
        return updatedDate;
    }

    public void setUpdatedDate(InstantFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    public LongFilter getUpdatedBy() {
        return updatedBy;
    }

    public LongFilter updatedBy() {
        if (updatedBy == null) {
            updatedBy = new LongFilter();
        }
        return updatedBy;
    }

    public void setUpdatedBy(LongFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public StringFilter getAssessmentStatus() {
        return assessmentStatus;
    }

    public StringFilter assessmentStatus() {
        if (assessmentStatus == null) {
            assessmentStatus = new StringFilter();
        }
        return assessmentStatus;
    }

    public void setAssessmentStatus(StringFilter assessmentStatus) {
        this.assessmentStatus = assessmentStatus;
    }

    public BooleanFilter getStatus() {
        return status;
    }

    public BooleanFilter status() {
        if (status == null) {
            status = new BooleanFilter();
        }
        return status;
    }

    public void setStatus(BooleanFilter status) {
        this.status = status;
    }

    public BooleanFilter getArchiveStatus() {
        return archiveStatus;
    }

    public BooleanFilter archiveStatus() {
        if (archiveStatus == null) {
            archiveStatus = new BooleanFilter();
        }
        return archiveStatus;
    }

    public void setArchiveStatus(BooleanFilter archiveStatus) {
        this.archiveStatus = archiveStatus;
    }

    public LongFilter getCandidateDetailsId() {
        return candidateDetailsId;
    }

    public LongFilter candidateDetailsId() {
        if (candidateDetailsId == null) {
            candidateDetailsId = new LongFilter();
        }
        return candidateDetailsId;
    }

    public void setCandidateDetailsId(LongFilter candidateDetailsId) {
        this.candidateDetailsId = candidateDetailsId;
    }

    public LongFilter getCandidateProfileId() {
        return candidateProfileId;
    }

    public LongFilter candidateProfileId() {
        if (candidateProfileId == null) {
            candidateProfileId = new LongFilter();
        }
        return candidateProfileId;
    }

    public void setCandidateProfileId(LongFilter candidateProfileId) {
        this.candidateProfileId = candidateProfileId;
    }

    public LongFilter getClientId() {
        return clientId;
    }

    public LongFilter clientId() {
        if (clientId == null) {
            clientId = new LongFilter();
        }
        return clientId;
    }

    public void setClientId(LongFilter clientId) {
        this.clientId = clientId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CandidateCriteria that = (CandidateCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(isInternalCandidate, that.isInternalCandidate) &&
            Objects.equals(company, that.company) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(updatedDate, that.updatedDate) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(assessmentStatus, that.assessmentStatus) &&
            Objects.equals(status, that.status) &&
            Objects.equals(archiveStatus, that.archiveStatus) &&
            Objects.equals(candidateDetailsId, that.candidateDetailsId) &&
            Objects.equals(candidateProfileId, that.candidateProfileId) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            isInternalCandidate,
            company,
            createdDate,
            createdBy,
            updatedDate,
            updatedBy,
            assessmentStatus,
            status,
            archiveStatus,
            candidateDetailsId,
            candidateProfileId,
            clientId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CandidateCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (isInternalCandidate != null ? "isInternalCandidate=" + isInternalCandidate + ", " : "") +
            (company != null ? "company=" + company + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
            (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
            (assessmentStatus != null ? "assessmentStatus=" + assessmentStatus + ", " : "") +
            (status != null ? "status=" + status + ", " : "") +
            (archiveStatus != null ? "archiveStatus=" + archiveStatus + ", " : "") +
            (candidateDetailsId != null ? "candidateDetailsId=" + candidateDetailsId + ", " : "") +
            (candidateProfileId != null ? "candidateProfileId=" + candidateProfileId + ", " : "") +
            (clientId != null ? "clientId=" + clientId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
