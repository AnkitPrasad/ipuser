package com.user.myapp.service.criteria;

import com.user.myapp.domain.enumeration.Permission;
import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.user.myapp.domain.InPreferenceUser} entity. This class is used
 * in {@link com.user.myapp.web.rest.InPreferenceUserResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /in-preference-users?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class InPreferenceUserCriteria implements Serializable, Criteria {

    /**
     * Class for filtering Permission
     */
    public static class PermissionFilter extends Filter<Permission> {

        public PermissionFilter() {}

        public PermissionFilter(PermissionFilter filter) {
            super(filter);
        }

        @Override
        public PermissionFilter copy() {
            return new PermissionFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter firstName;

    private StringFilter lastName;

    private StringFilter mobileNo;

    private StringFilter profileUrl;

    private PermissionFilter userType;

    private StringFilter emailId;

    private LongFilter loginId;

    private StringFilter secret;

    private InstantFilter createdDate;

    private LongFilter createdBy;

    private InstantFilter updatedDate;

    private LongFilter updatedBy;

    private BooleanFilter status;

    private LongFilter userEmailsId;

    private LongFilter loginCountId;

    private LongFilter userRolePermissionId;

    private LongFilter roleMasterId;

    private Boolean distinct;

    public InPreferenceUserCriteria() {}

    public InPreferenceUserCriteria(InPreferenceUserCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.firstName = other.firstName == null ? null : other.firstName.copy();
        this.lastName = other.lastName == null ? null : other.lastName.copy();
        this.mobileNo = other.mobileNo == null ? null : other.mobileNo.copy();
        this.profileUrl = other.profileUrl == null ? null : other.profileUrl.copy();
        this.userType = other.userType == null ? null : other.userType.copy();
        this.emailId = other.emailId == null ? null : other.emailId.copy();
        this.loginId = other.loginId == null ? null : other.loginId.copy();
        this.secret = other.secret == null ? null : other.secret.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.updatedDate = other.updatedDate == null ? null : other.updatedDate.copy();
        this.updatedBy = other.updatedBy == null ? null : other.updatedBy.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.userEmailsId = other.userEmailsId == null ? null : other.userEmailsId.copy();
        this.loginCountId = other.loginCountId == null ? null : other.loginCountId.copy();
        this.userRolePermissionId = other.userRolePermissionId == null ? null : other.userRolePermissionId.copy();
        this.roleMasterId = other.roleMasterId == null ? null : other.roleMasterId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public InPreferenceUserCriteria copy() {
        return new InPreferenceUserCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFirstName() {
        return firstName;
    }

    public StringFilter firstName() {
        if (firstName == null) {
            firstName = new StringFilter();
        }
        return firstName;
    }

    public void setFirstName(StringFilter firstName) {
        this.firstName = firstName;
    }

    public StringFilter getLastName() {
        return lastName;
    }

    public StringFilter lastName() {
        if (lastName == null) {
            lastName = new StringFilter();
        }
        return lastName;
    }

    public void setLastName(StringFilter lastName) {
        this.lastName = lastName;
    }

    public StringFilter getMobileNo() {
        return mobileNo;
    }

    public StringFilter mobileNo() {
        if (mobileNo == null) {
            mobileNo = new StringFilter();
        }
        return mobileNo;
    }

    public void setMobileNo(StringFilter mobileNo) {
        this.mobileNo = mobileNo;
    }

    public StringFilter getProfileUrl() {
        return profileUrl;
    }

    public StringFilter profileUrl() {
        if (profileUrl == null) {
            profileUrl = new StringFilter();
        }
        return profileUrl;
    }

    public void setProfileUrl(StringFilter profileUrl) {
        this.profileUrl = profileUrl;
    }

    public PermissionFilter getUserType() {
        return userType;
    }

    public PermissionFilter userType() {
        if (userType == null) {
            userType = new PermissionFilter();
        }
        return userType;
    }

    public void setUserType(PermissionFilter userType) {
        this.userType = userType;
    }

    public StringFilter getEmailId() {
        return emailId;
    }

    public StringFilter emailId() {
        if (emailId == null) {
            emailId = new StringFilter();
        }
        return emailId;
    }

    public void setEmailId(StringFilter emailId) {
        this.emailId = emailId;
    }

    public LongFilter getLoginId() {
        return loginId;
    }

    public LongFilter loginId() {
        if (loginId == null) {
            loginId = new LongFilter();
        }
        return loginId;
    }

    public void setLoginId(LongFilter loginId) {
        this.loginId = loginId;
    }

    public StringFilter getSecret() {
        return secret;
    }

    public StringFilter secret() {
        if (secret == null) {
            secret = new StringFilter();
        }
        return secret;
    }

    public void setSecret(StringFilter secret) {
        this.secret = secret;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public LongFilter getCreatedBy() {
        return createdBy;
    }

    public LongFilter createdBy() {
        if (createdBy == null) {
            createdBy = new LongFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(LongFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getUpdatedDate() {
        return updatedDate;
    }

    public InstantFilter updatedDate() {
        if (updatedDate == null) {
            updatedDate = new InstantFilter();
        }
        return updatedDate;
    }

    public void setUpdatedDate(InstantFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    public LongFilter getUpdatedBy() {
        return updatedBy;
    }

    public LongFilter updatedBy() {
        if (updatedBy == null) {
            updatedBy = new LongFilter();
        }
        return updatedBy;
    }

    public void setUpdatedBy(LongFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public BooleanFilter getStatus() {
        return status;
    }

    public BooleanFilter status() {
        if (status == null) {
            status = new BooleanFilter();
        }
        return status;
    }

    public void setStatus(BooleanFilter status) {
        this.status = status;
    }

    public LongFilter getUserEmailsId() {
        return userEmailsId;
    }

    public LongFilter userEmailsId() {
        if (userEmailsId == null) {
            userEmailsId = new LongFilter();
        }
        return userEmailsId;
    }

    public void setUserEmailsId(LongFilter userEmailsId) {
        this.userEmailsId = userEmailsId;
    }

    public LongFilter getLoginCountId() {
        return loginCountId;
    }

    public LongFilter loginCountId() {
        if (loginCountId == null) {
            loginCountId = new LongFilter();
        }
        return loginCountId;
    }

    public void setLoginCountId(LongFilter loginCountId) {
        this.loginCountId = loginCountId;
    }

    public LongFilter getUserRolePermissionId() {
        return userRolePermissionId;
    }

    public LongFilter userRolePermissionId() {
        if (userRolePermissionId == null) {
            userRolePermissionId = new LongFilter();
        }
        return userRolePermissionId;
    }

    public void setUserRolePermissionId(LongFilter userRolePermissionId) {
        this.userRolePermissionId = userRolePermissionId;
    }

    public LongFilter getRoleMasterId() {
        return roleMasterId;
    }

    public LongFilter roleMasterId() {
        if (roleMasterId == null) {
            roleMasterId = new LongFilter();
        }
        return roleMasterId;
    }

    public void setRoleMasterId(LongFilter roleMasterId) {
        this.roleMasterId = roleMasterId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final InPreferenceUserCriteria that = (InPreferenceUserCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(firstName, that.firstName) &&
            Objects.equals(lastName, that.lastName) &&
            Objects.equals(mobileNo, that.mobileNo) &&
            Objects.equals(profileUrl, that.profileUrl) &&
            Objects.equals(userType, that.userType) &&
            Objects.equals(emailId, that.emailId) &&
            Objects.equals(loginId, that.loginId) &&
            Objects.equals(secret, that.secret) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(updatedDate, that.updatedDate) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(status, that.status) &&
            Objects.equals(userEmailsId, that.userEmailsId) &&
            Objects.equals(loginCountId, that.loginCountId) &&
            Objects.equals(userRolePermissionId, that.userRolePermissionId) &&
            Objects.equals(roleMasterId, that.roleMasterId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            firstName,
            lastName,
            mobileNo,
            profileUrl,
            userType,
            emailId,
            loginId,
            secret,
            createdDate,
            createdBy,
            updatedDate,
            updatedBy,
            status,
            userEmailsId,
            loginCountId,
            userRolePermissionId,
            roleMasterId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InPreferenceUserCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (firstName != null ? "firstName=" + firstName + ", " : "") +
            (lastName != null ? "lastName=" + lastName + ", " : "") +
            (mobileNo != null ? "mobileNo=" + mobileNo + ", " : "") +
            (profileUrl != null ? "profileUrl=" + profileUrl + ", " : "") +
            (userType != null ? "userType=" + userType + ", " : "") +
            (emailId != null ? "emailId=" + emailId + ", " : "") +
            (loginId != null ? "loginId=" + loginId + ", " : "") +
            (secret != null ? "secret=" + secret + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
            (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
            (status != null ? "status=" + status + ", " : "") +
            (userEmailsId != null ? "userEmailsId=" + userEmailsId + ", " : "") +
            (loginCountId != null ? "loginCountId=" + loginCountId + ", " : "") +
            (userRolePermissionId != null ? "userRolePermissionId=" + userRolePermissionId + ", " : "") +
            (roleMasterId != null ? "roleMasterId=" + roleMasterId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
