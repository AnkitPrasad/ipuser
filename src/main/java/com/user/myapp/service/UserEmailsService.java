package com.user.myapp.service;

import com.user.myapp.service.dto.UserEmailsDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.UserEmails}.
 */
public interface UserEmailsService {
    /**
     * Save a userEmails.
     *
     * @param userEmailsDTO the entity to save.
     * @return the persisted entity.
     */
    UserEmailsDTO save(UserEmailsDTO userEmailsDTO);

    /**
     * Updates a userEmails.
     *
     * @param userEmailsDTO the entity to update.
     * @return the persisted entity.
     */
    UserEmailsDTO update(UserEmailsDTO userEmailsDTO);

    /**
     * Partially updates a userEmails.
     *
     * @param userEmailsDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<UserEmailsDTO> partialUpdate(UserEmailsDTO userEmailsDTO);

    /**
     * Get all the userEmails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserEmailsDTO> findAll(Pageable pageable);

    /**
     * Get the "id" userEmails.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserEmailsDTO> findOne(Long id);

    /**
     * Delete the "id" userEmails.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
