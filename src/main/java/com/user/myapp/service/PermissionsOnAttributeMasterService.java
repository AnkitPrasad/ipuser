package com.user.myapp.service;

import com.user.myapp.service.dto.PermissionsOnAttributeMasterDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.PermissionsOnAttributeMaster}.
 */
public interface PermissionsOnAttributeMasterService {
    /**
     * Save a permissionsOnAttributeMaster.
     *
     * @param permissionsOnAttributeMasterDTO the entity to save.
     * @return the persisted entity.
     */
    PermissionsOnAttributeMasterDTO save(PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO);

    /**
     * Updates a permissionsOnAttributeMaster.
     *
     * @param permissionsOnAttributeMasterDTO the entity to update.
     * @return the persisted entity.
     */
    PermissionsOnAttributeMasterDTO update(PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO);

    /**
     * Partially updates a permissionsOnAttributeMaster.
     *
     * @param permissionsOnAttributeMasterDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PermissionsOnAttributeMasterDTO> partialUpdate(PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO);

    /**
     * Get all the permissionsOnAttributeMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PermissionsOnAttributeMasterDTO> findAll(Pageable pageable);

    /**
     * Get the "id" permissionsOnAttributeMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PermissionsOnAttributeMasterDTO> findOne(Long id);

    /**
     * Delete the "id" permissionsOnAttributeMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
