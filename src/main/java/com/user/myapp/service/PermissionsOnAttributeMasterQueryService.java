package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.PermissionsOnAttributeMaster;
import com.user.myapp.repository.PermissionsOnAttributeMasterRepository;
import com.user.myapp.service.criteria.PermissionsOnAttributeMasterCriteria;
import com.user.myapp.service.dto.PermissionsOnAttributeMasterDTO;
import com.user.myapp.service.mapper.PermissionsOnAttributeMasterMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PermissionsOnAttributeMaster} entities in the database.
 * The main input is a {@link PermissionsOnAttributeMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PermissionsOnAttributeMasterDTO} or a {@link Page} of {@link PermissionsOnAttributeMasterDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PermissionsOnAttributeMasterQueryService extends QueryService<PermissionsOnAttributeMaster> {

    private final Logger log = LoggerFactory.getLogger(PermissionsOnAttributeMasterQueryService.class);

    private final PermissionsOnAttributeMasterRepository permissionsOnAttributeMasterRepository;

    private final PermissionsOnAttributeMasterMapper permissionsOnAttributeMasterMapper;

    public PermissionsOnAttributeMasterQueryService(
        PermissionsOnAttributeMasterRepository permissionsOnAttributeMasterRepository,
        PermissionsOnAttributeMasterMapper permissionsOnAttributeMasterMapper
    ) {
        this.permissionsOnAttributeMasterRepository = permissionsOnAttributeMasterRepository;
        this.permissionsOnAttributeMasterMapper = permissionsOnAttributeMasterMapper;
    }

    /**
     * Return a {@link List} of {@link PermissionsOnAttributeMasterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PermissionsOnAttributeMasterDTO> findByCriteria(PermissionsOnAttributeMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PermissionsOnAttributeMaster> specification = createSpecification(criteria);
        return permissionsOnAttributeMasterMapper.toDto(permissionsOnAttributeMasterRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PermissionsOnAttributeMasterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PermissionsOnAttributeMasterDTO> findByCriteria(PermissionsOnAttributeMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PermissionsOnAttributeMaster> specification = createSpecification(criteria);
        return permissionsOnAttributeMasterRepository.findAll(specification, page).map(permissionsOnAttributeMasterMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PermissionsOnAttributeMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PermissionsOnAttributeMaster> specification = createSpecification(criteria);
        return permissionsOnAttributeMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link PermissionsOnAttributeMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PermissionsOnAttributeMaster> createSpecification(PermissionsOnAttributeMasterCriteria criteria) {
        Specification<PermissionsOnAttributeMaster> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PermissionsOnAttributeMaster_.id));
            }
            if (criteria.getAttributeKey() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getAttributeKey(), PermissionsOnAttributeMaster_.attributeKey));
            }
            if (criteria.getAttributeTitle() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getAttributeTitle(), PermissionsOnAttributeMaster_.attributeTitle));
            }
            if (criteria.getUserPermissionOnAttributeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getUserPermissionOnAttributeId(),
                            root ->
                                root
                                    .join(PermissionsOnAttributeMaster_.userPermissionOnAttributes, JoinType.LEFT)
                                    .get(UserPermissionOnAttribute_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
