package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.repository.InPreferenceUserRepository;
import com.user.myapp.service.criteria.InPreferenceUserCriteria;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.mapper.InPreferenceUserMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link InPreferenceUser} entities in the database.
 * The main input is a {@link InPreferenceUserCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link InPreferenceUserDTO} or a {@link Page} of {@link InPreferenceUserDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class InPreferenceUserQueryService extends QueryService<InPreferenceUser> {

    private final Logger log = LoggerFactory.getLogger(InPreferenceUserQueryService.class);

    private final InPreferenceUserRepository inPreferenceUserRepository;

    private final InPreferenceUserMapper inPreferenceUserMapper;

    public InPreferenceUserQueryService(
        InPreferenceUserRepository inPreferenceUserRepository,
        InPreferenceUserMapper inPreferenceUserMapper
    ) {
        this.inPreferenceUserRepository = inPreferenceUserRepository;
        this.inPreferenceUserMapper = inPreferenceUserMapper;
    }

    /**
     * Return a {@link List} of {@link InPreferenceUserDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<InPreferenceUserDTO> findByCriteria(InPreferenceUserCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<InPreferenceUser> specification = createSpecification(criteria);
        return inPreferenceUserMapper.toDto(inPreferenceUserRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link InPreferenceUserDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<InPreferenceUserDTO> findByCriteria(InPreferenceUserCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<InPreferenceUser> specification = createSpecification(criteria);
        return inPreferenceUserRepository.findAll(specification, page).map(inPreferenceUserMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(InPreferenceUserCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<InPreferenceUser> specification = createSpecification(criteria);
        return inPreferenceUserRepository.count(specification);
    }

    /**
     * Function to convert {@link InPreferenceUserCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<InPreferenceUser> createSpecification(InPreferenceUserCriteria criteria) {
        Specification<InPreferenceUser> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), InPreferenceUser_.id));
            }
            if (criteria.getFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFirstName(), InPreferenceUser_.firstName));
            }
            if (criteria.getLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastName(), InPreferenceUser_.lastName));
            }
            if (criteria.getMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNo(), InPreferenceUser_.mobileNo));
            }
            if (criteria.getProfileUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProfileUrl(), InPreferenceUser_.profileUrl));
            }
            if (criteria.getUserType() != null) {
                specification = specification.and(buildSpecification(criteria.getUserType(), InPreferenceUser_.userType));
            }
            if (criteria.getEmailId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmailId(), InPreferenceUser_.emailId));
            }
            if (criteria.getLoginId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLoginId(), InPreferenceUser_.loginId));
            }
            if (criteria.getSecret() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSecret(), InPreferenceUser_.secret));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), InPreferenceUser_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedBy(), InPreferenceUser_.createdBy));
            }
            if (criteria.getUpdatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedDate(), InPreferenceUser_.updatedDate));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedBy(), InPreferenceUser_.updatedBy));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), InPreferenceUser_.status));
            }
            if (criteria.getUserEmailsId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getUserEmailsId(),
                            root -> root.join(InPreferenceUser_.userEmails, JoinType.LEFT).get(UserEmails_.id)
                        )
                    );
            }
            if (criteria.getLoginCountId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getLoginCountId(),
                            root -> root.join(InPreferenceUser_.loginCounts, JoinType.LEFT).get(LoginCount_.id)
                        )
                    );
            }
            if (criteria.getUserRolePermissionId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getUserRolePermissionId(),
                            root -> root.join(InPreferenceUser_.userRolePermissions, JoinType.LEFT).get(UserRolePermission_.id)
                        )
                    );
            }
            if (criteria.getRoleMasterId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRoleMasterId(),
                            root -> root.join(InPreferenceUser_.roleMasters, JoinType.LEFT).get(RoleMaster_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
