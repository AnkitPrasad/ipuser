package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.SuperAdmin;
import com.user.myapp.repository.SuperAdminRepository;
import com.user.myapp.service.criteria.SuperAdminCriteria;
import com.user.myapp.service.dto.SuperAdminDTO;
import com.user.myapp.service.mapper.SuperAdminMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link SuperAdmin} entities in the database.
 * The main input is a {@link SuperAdminCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SuperAdminDTO} or a {@link Page} of {@link SuperAdminDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SuperAdminQueryService extends QueryService<SuperAdmin> {

    private final Logger log = LoggerFactory.getLogger(SuperAdminQueryService.class);

    private final SuperAdminRepository superAdminRepository;

    private final SuperAdminMapper superAdminMapper;

    public SuperAdminQueryService(SuperAdminRepository superAdminRepository, SuperAdminMapper superAdminMapper) {
        this.superAdminRepository = superAdminRepository;
        this.superAdminMapper = superAdminMapper;
    }

    /**
     * Return a {@link List} of {@link SuperAdminDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SuperAdminDTO> findByCriteria(SuperAdminCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SuperAdmin> specification = createSpecification(criteria);
        return superAdminMapper.toDto(superAdminRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SuperAdminDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SuperAdminDTO> findByCriteria(SuperAdminCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SuperAdmin> specification = createSpecification(criteria);
        return superAdminRepository.findAll(specification, page).map(superAdminMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SuperAdminCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SuperAdmin> specification = createSpecification(criteria);
        return superAdminRepository.count(specification);
    }

    /**
     * Function to convert {@link SuperAdminCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<SuperAdmin> createSpecification(SuperAdminCriteria criteria) {
        Specification<SuperAdmin> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), SuperAdmin_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), SuperAdmin_.name));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), SuperAdmin_.email));
            }
            if (criteria.getInPreferenceUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getInPreferenceUserId(),
                            root -> root.join(SuperAdmin_.inPreferenceUser, JoinType.LEFT).get(InPreferenceUser_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
