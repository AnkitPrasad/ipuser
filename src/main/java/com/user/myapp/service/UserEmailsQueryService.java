package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.UserEmails;
import com.user.myapp.repository.UserEmailsRepository;
import com.user.myapp.service.criteria.UserEmailsCriteria;
import com.user.myapp.service.dto.UserEmailsDTO;
import com.user.myapp.service.mapper.UserEmailsMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link UserEmails} entities in the database.
 * The main input is a {@link UserEmailsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserEmailsDTO} or a {@link Page} of {@link UserEmailsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserEmailsQueryService extends QueryService<UserEmails> {

    private final Logger log = LoggerFactory.getLogger(UserEmailsQueryService.class);

    private final UserEmailsRepository userEmailsRepository;

    private final UserEmailsMapper userEmailsMapper;

    public UserEmailsQueryService(UserEmailsRepository userEmailsRepository, UserEmailsMapper userEmailsMapper) {
        this.userEmailsRepository = userEmailsRepository;
        this.userEmailsMapper = userEmailsMapper;
    }

    /**
     * Return a {@link List} of {@link UserEmailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserEmailsDTO> findByCriteria(UserEmailsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserEmails> specification = createSpecification(criteria);
        return userEmailsMapper.toDto(userEmailsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserEmailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserEmailsDTO> findByCriteria(UserEmailsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserEmails> specification = createSpecification(criteria);
        return userEmailsRepository.findAll(specification, page).map(userEmailsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserEmailsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserEmails> specification = createSpecification(criteria);
        return userEmailsRepository.count(specification);
    }

    /**
     * Function to convert {@link UserEmailsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UserEmails> createSpecification(UserEmailsCriteria criteria) {
        Specification<UserEmails> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UserEmails_.id));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), UserEmails_.email));
            }
            if (criteria.getLoginId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLoginId(), UserEmails_.loginId));
            }
            if (criteria.getInPreferenceUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getInPreferenceUserId(),
                            root -> root.join(UserEmails_.inPreferenceUser, JoinType.LEFT).get(InPreferenceUser_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
