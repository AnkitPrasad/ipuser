package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.CandidateProfile;
import com.user.myapp.repository.CandidateProfileRepository;
import com.user.myapp.service.criteria.CandidateProfileCriteria;
import com.user.myapp.service.dto.CandidateProfileDTO;
import com.user.myapp.service.mapper.CandidateProfileMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CandidateProfile} entities in the database.
 * The main input is a {@link CandidateProfileCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CandidateProfileDTO} or a {@link Page} of {@link CandidateProfileDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CandidateProfileQueryService extends QueryService<CandidateProfile> {

    private final Logger log = LoggerFactory.getLogger(CandidateProfileQueryService.class);

    private final CandidateProfileRepository candidateProfileRepository;

    private final CandidateProfileMapper candidateProfileMapper;

    public CandidateProfileQueryService(
        CandidateProfileRepository candidateProfileRepository,
        CandidateProfileMapper candidateProfileMapper
    ) {
        this.candidateProfileRepository = candidateProfileRepository;
        this.candidateProfileMapper = candidateProfileMapper;
    }

    /**
     * Return a {@link List} of {@link CandidateProfileDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CandidateProfileDTO> findByCriteria(CandidateProfileCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CandidateProfile> specification = createSpecification(criteria);
        return candidateProfileMapper.toDto(candidateProfileRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CandidateProfileDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CandidateProfileDTO> findByCriteria(CandidateProfileCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CandidateProfile> specification = createSpecification(criteria);
        return candidateProfileRepository.findAll(specification, page).map(candidateProfileMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CandidateProfileCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CandidateProfile> specification = createSpecification(criteria);
        return candidateProfileRepository.count(specification);
    }

    /**
     * Function to convert {@link CandidateProfileCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CandidateProfile> createSpecification(CandidateProfileCriteria criteria) {
        Specification<CandidateProfile> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CandidateProfile_.id));
            }
            if (criteria.getAboutMe() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAboutMe(), CandidateProfile_.aboutMe));
            }
            if (criteria.getCareerSummary() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCareerSummary(), CandidateProfile_.careerSummary));
            }
            if (criteria.getEducation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEducation(), CandidateProfile_.education));
            }
            if (criteria.getDob() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDob(), CandidateProfile_.dob));
            }
            if (criteria.getCurrentEmployment() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getCurrentEmployment(), CandidateProfile_.currentEmployment));
            }
            if (criteria.getCertifications() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCertifications(), CandidateProfile_.certifications));
            }
            if (criteria.getExperience() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExperience(), CandidateProfile_.experience));
            }
            if (criteria.getCandidateId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCandidateId(),
                            root -> root.join(CandidateProfile_.candidate, JoinType.LEFT).get(Candidate_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
