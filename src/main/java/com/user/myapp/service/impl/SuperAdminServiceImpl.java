package com.user.myapp.service.impl;

import com.user.myapp.domain.SuperAdmin;
import com.user.myapp.repository.SuperAdminRepository;
import com.user.myapp.service.SuperAdminService;
import com.user.myapp.service.dto.SuperAdminDTO;
import com.user.myapp.service.mapper.SuperAdminMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link SuperAdmin}.
 */
@Service
@Transactional
public class SuperAdminServiceImpl implements SuperAdminService {

    private final Logger log = LoggerFactory.getLogger(SuperAdminServiceImpl.class);

    private final SuperAdminRepository superAdminRepository;

    private final SuperAdminMapper superAdminMapper;

    public SuperAdminServiceImpl(SuperAdminRepository superAdminRepository, SuperAdminMapper superAdminMapper) {
        this.superAdminRepository = superAdminRepository;
        this.superAdminMapper = superAdminMapper;
    }

    @Override
    public SuperAdminDTO save(SuperAdminDTO superAdminDTO) {
        log.debug("Request to save SuperAdmin : {}", superAdminDTO);
        SuperAdmin superAdmin = superAdminMapper.toEntity(superAdminDTO);
        superAdmin = superAdminRepository.save(superAdmin);
        return superAdminMapper.toDto(superAdmin);
    }

    @Override
    public SuperAdminDTO update(SuperAdminDTO superAdminDTO) {
        log.debug("Request to update SuperAdmin : {}", superAdminDTO);
        SuperAdmin superAdmin = superAdminMapper.toEntity(superAdminDTO);
        superAdmin = superAdminRepository.save(superAdmin);
        return superAdminMapper.toDto(superAdmin);
    }

    @Override
    public Optional<SuperAdminDTO> partialUpdate(SuperAdminDTO superAdminDTO) {
        log.debug("Request to partially update SuperAdmin : {}", superAdminDTO);

        return superAdminRepository
            .findById(superAdminDTO.getId())
            .map(existingSuperAdmin -> {
                superAdminMapper.partialUpdate(existingSuperAdmin, superAdminDTO);

                return existingSuperAdmin;
            })
            .map(superAdminRepository::save)
            .map(superAdminMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SuperAdminDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SuperAdmins");
        return superAdminRepository.findAll(pageable).map(superAdminMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SuperAdminDTO> findOne(Long id) {
        log.debug("Request to get SuperAdmin : {}", id);
        return superAdminRepository.findById(id).map(superAdminMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete SuperAdmin : {}", id);
        superAdminRepository.deleteById(id);
    }
}
