package com.user.myapp.service.impl;

import com.user.myapp.domain.PermissionsOnAttributeMaster;
import com.user.myapp.repository.PermissionsOnAttributeMasterRepository;
import com.user.myapp.service.PermissionsOnAttributeMasterService;
import com.user.myapp.service.dto.PermissionsOnAttributeMasterDTO;
import com.user.myapp.service.mapper.PermissionsOnAttributeMasterMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PermissionsOnAttributeMaster}.
 */
@Service
@Transactional
public class PermissionsOnAttributeMasterServiceImpl implements PermissionsOnAttributeMasterService {

    private final Logger log = LoggerFactory.getLogger(PermissionsOnAttributeMasterServiceImpl.class);

    private final PermissionsOnAttributeMasterRepository permissionsOnAttributeMasterRepository;

    private final PermissionsOnAttributeMasterMapper permissionsOnAttributeMasterMapper;

    public PermissionsOnAttributeMasterServiceImpl(
        PermissionsOnAttributeMasterRepository permissionsOnAttributeMasterRepository,
        PermissionsOnAttributeMasterMapper permissionsOnAttributeMasterMapper
    ) {
        this.permissionsOnAttributeMasterRepository = permissionsOnAttributeMasterRepository;
        this.permissionsOnAttributeMasterMapper = permissionsOnAttributeMasterMapper;
    }

    @Override
    public PermissionsOnAttributeMasterDTO save(PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO) {
        log.debug("Request to save PermissionsOnAttributeMaster : {}", permissionsOnAttributeMasterDTO);
        PermissionsOnAttributeMaster permissionsOnAttributeMaster = permissionsOnAttributeMasterMapper.toEntity(
            permissionsOnAttributeMasterDTO
        );
        permissionsOnAttributeMaster = permissionsOnAttributeMasterRepository.save(permissionsOnAttributeMaster);
        return permissionsOnAttributeMasterMapper.toDto(permissionsOnAttributeMaster);
    }

    @Override
    public PermissionsOnAttributeMasterDTO update(PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO) {
        log.debug("Request to update PermissionsOnAttributeMaster : {}", permissionsOnAttributeMasterDTO);
        PermissionsOnAttributeMaster permissionsOnAttributeMaster = permissionsOnAttributeMasterMapper.toEntity(
            permissionsOnAttributeMasterDTO
        );
        permissionsOnAttributeMaster = permissionsOnAttributeMasterRepository.save(permissionsOnAttributeMaster);
        return permissionsOnAttributeMasterMapper.toDto(permissionsOnAttributeMaster);
    }

    @Override
    public Optional<PermissionsOnAttributeMasterDTO> partialUpdate(PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO) {
        log.debug("Request to partially update PermissionsOnAttributeMaster : {}", permissionsOnAttributeMasterDTO);

        return permissionsOnAttributeMasterRepository
            .findById(permissionsOnAttributeMasterDTO.getId())
            .map(existingPermissionsOnAttributeMaster -> {
                permissionsOnAttributeMasterMapper.partialUpdate(existingPermissionsOnAttributeMaster, permissionsOnAttributeMasterDTO);

                return existingPermissionsOnAttributeMaster;
            })
            .map(permissionsOnAttributeMasterRepository::save)
            .map(permissionsOnAttributeMasterMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PermissionsOnAttributeMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PermissionsOnAttributeMasters");
        return permissionsOnAttributeMasterRepository.findAll(pageable).map(permissionsOnAttributeMasterMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PermissionsOnAttributeMasterDTO> findOne(Long id) {
        log.debug("Request to get PermissionsOnAttributeMaster : {}", id);
        return permissionsOnAttributeMasterRepository.findById(id).map(permissionsOnAttributeMasterMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PermissionsOnAttributeMaster : {}", id);
        permissionsOnAttributeMasterRepository.deleteById(id);
    }
}
