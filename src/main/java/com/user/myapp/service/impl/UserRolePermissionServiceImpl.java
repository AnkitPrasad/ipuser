package com.user.myapp.service.impl;

import com.user.myapp.domain.UserRolePermission;
import com.user.myapp.repository.UserRolePermissionRepository;
import com.user.myapp.service.UserRolePermissionService;
import com.user.myapp.service.dto.UserRolePermissionDTO;
import com.user.myapp.service.mapper.UserRolePermissionMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UserRolePermission}.
 */
@Service
@Transactional
public class UserRolePermissionServiceImpl implements UserRolePermissionService {

    private final Logger log = LoggerFactory.getLogger(UserRolePermissionServiceImpl.class);

    private final UserRolePermissionRepository userRolePermissionRepository;

    private final UserRolePermissionMapper userRolePermissionMapper;

    public UserRolePermissionServiceImpl(
        UserRolePermissionRepository userRolePermissionRepository,
        UserRolePermissionMapper userRolePermissionMapper
    ) {
        this.userRolePermissionRepository = userRolePermissionRepository;
        this.userRolePermissionMapper = userRolePermissionMapper;
    }

    @Override
    public UserRolePermissionDTO save(UserRolePermissionDTO userRolePermissionDTO) {
        log.debug("Request to save UserRolePermission : {}", userRolePermissionDTO);
        UserRolePermission userRolePermission = userRolePermissionMapper.toEntity(userRolePermissionDTO);
        userRolePermission = userRolePermissionRepository.save(userRolePermission);
        return userRolePermissionMapper.toDto(userRolePermission);
    }

    @Override
    public UserRolePermissionDTO update(UserRolePermissionDTO userRolePermissionDTO) {
        log.debug("Request to update UserRolePermission : {}", userRolePermissionDTO);
        UserRolePermission userRolePermission = userRolePermissionMapper.toEntity(userRolePermissionDTO);
        userRolePermission = userRolePermissionRepository.save(userRolePermission);
        return userRolePermissionMapper.toDto(userRolePermission);
    }

    @Override
    public Optional<UserRolePermissionDTO> partialUpdate(UserRolePermissionDTO userRolePermissionDTO) {
        log.debug("Request to partially update UserRolePermission : {}", userRolePermissionDTO);

        return userRolePermissionRepository
            .findById(userRolePermissionDTO.getId())
            .map(existingUserRolePermission -> {
                userRolePermissionMapper.partialUpdate(existingUserRolePermission, userRolePermissionDTO);

                return existingUserRolePermission;
            })
            .map(userRolePermissionRepository::save)
            .map(userRolePermissionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserRolePermissionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserRolePermissions");
        return userRolePermissionRepository.findAll(pageable).map(userRolePermissionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserRolePermissionDTO> findOne(Long id) {
        log.debug("Request to get UserRolePermission : {}", id);
        return userRolePermissionRepository.findById(id).map(userRolePermissionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserRolePermission : {}", id);
        userRolePermissionRepository.deleteById(id);
    }
}
