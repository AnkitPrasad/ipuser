package com.user.myapp.service.impl;

import com.user.myapp.domain.CandidateDetails;
import com.user.myapp.repository.CandidateDetailsRepository;
import com.user.myapp.service.CandidateDetailsService;
import com.user.myapp.service.dto.CandidateDetailsDTO;
import com.user.myapp.service.mapper.CandidateDetailsMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CandidateDetails}.
 */
@Service
@Transactional
public class CandidateDetailsServiceImpl implements CandidateDetailsService {

    private final Logger log = LoggerFactory.getLogger(CandidateDetailsServiceImpl.class);

    private final CandidateDetailsRepository candidateDetailsRepository;

    private final CandidateDetailsMapper candidateDetailsMapper;

    public CandidateDetailsServiceImpl(
        CandidateDetailsRepository candidateDetailsRepository,
        CandidateDetailsMapper candidateDetailsMapper
    ) {
        this.candidateDetailsRepository = candidateDetailsRepository;
        this.candidateDetailsMapper = candidateDetailsMapper;
    }

    @Override
    public CandidateDetailsDTO save(CandidateDetailsDTO candidateDetailsDTO) {
        log.debug("Request to save CandidateDetails : {}", candidateDetailsDTO);
        CandidateDetails candidateDetails = candidateDetailsMapper.toEntity(candidateDetailsDTO);
        candidateDetails = candidateDetailsRepository.save(candidateDetails);
        return candidateDetailsMapper.toDto(candidateDetails);
    }

    @Override
    public CandidateDetailsDTO update(CandidateDetailsDTO candidateDetailsDTO) {
        log.debug("Request to update CandidateDetails : {}", candidateDetailsDTO);
        CandidateDetails candidateDetails = candidateDetailsMapper.toEntity(candidateDetailsDTO);
        candidateDetails = candidateDetailsRepository.save(candidateDetails);
        return candidateDetailsMapper.toDto(candidateDetails);
    }

    @Override
    public Optional<CandidateDetailsDTO> partialUpdate(CandidateDetailsDTO candidateDetailsDTO) {
        log.debug("Request to partially update CandidateDetails : {}", candidateDetailsDTO);

        return candidateDetailsRepository
            .findById(candidateDetailsDTO.getId())
            .map(existingCandidateDetails -> {
                candidateDetailsMapper.partialUpdate(existingCandidateDetails, candidateDetailsDTO);

                return existingCandidateDetails;
            })
            .map(candidateDetailsRepository::save)
            .map(candidateDetailsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CandidateDetailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CandidateDetails");
        return candidateDetailsRepository.findAll(pageable).map(candidateDetailsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CandidateDetailsDTO> findOne(Long id) {
        log.debug("Request to get CandidateDetails : {}", id);
        return candidateDetailsRepository.findById(id).map(candidateDetailsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CandidateDetails : {}", id);
        candidateDetailsRepository.deleteById(id);
    }
}
