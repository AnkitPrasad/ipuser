package com.user.myapp.service.impl;

import com.user.myapp.domain.UserEmails;
import com.user.myapp.repository.UserEmailsRepository;
import com.user.myapp.service.UserEmailsService;
import com.user.myapp.service.dto.UserEmailsDTO;
import com.user.myapp.service.mapper.UserEmailsMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UserEmails}.
 */
@Service
@Transactional
public class UserEmailsServiceImpl implements UserEmailsService {

    private final Logger log = LoggerFactory.getLogger(UserEmailsServiceImpl.class);

    private final UserEmailsRepository userEmailsRepository;

    private final UserEmailsMapper userEmailsMapper;

    public UserEmailsServiceImpl(UserEmailsRepository userEmailsRepository, UserEmailsMapper userEmailsMapper) {
        this.userEmailsRepository = userEmailsRepository;
        this.userEmailsMapper = userEmailsMapper;
    }

    @Override
    public UserEmailsDTO save(UserEmailsDTO userEmailsDTO) {
        log.debug("Request to save UserEmails : {}", userEmailsDTO);
        UserEmails userEmails = userEmailsMapper.toEntity(userEmailsDTO);
        userEmails = userEmailsRepository.save(userEmails);
        return userEmailsMapper.toDto(userEmails);
    }

    @Override
    public UserEmailsDTO update(UserEmailsDTO userEmailsDTO) {
        log.debug("Request to update UserEmails : {}", userEmailsDTO);
        UserEmails userEmails = userEmailsMapper.toEntity(userEmailsDTO);
        userEmails = userEmailsRepository.save(userEmails);
        return userEmailsMapper.toDto(userEmails);
    }

    @Override
    public Optional<UserEmailsDTO> partialUpdate(UserEmailsDTO userEmailsDTO) {
        log.debug("Request to partially update UserEmails : {}", userEmailsDTO);

        return userEmailsRepository
            .findById(userEmailsDTO.getId())
            .map(existingUserEmails -> {
                userEmailsMapper.partialUpdate(existingUserEmails, userEmailsDTO);

                return existingUserEmails;
            })
            .map(userEmailsRepository::save)
            .map(userEmailsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserEmailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserEmails");
        return userEmailsRepository.findAll(pageable).map(userEmailsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserEmailsDTO> findOne(Long id) {
        log.debug("Request to get UserEmails : {}", id);
        return userEmailsRepository.findById(id).map(userEmailsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserEmails : {}", id);
        userEmailsRepository.deleteById(id);
    }
}
