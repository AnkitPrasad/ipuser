package com.user.myapp.service.impl;

import com.user.myapp.domain.LoginCount;
import com.user.myapp.repository.LoginCountRepository;
import com.user.myapp.service.LoginCountService;
import com.user.myapp.service.dto.LoginCountDTO;
import com.user.myapp.service.mapper.LoginCountMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link LoginCount}.
 */
@Service
@Transactional
public class LoginCountServiceImpl implements LoginCountService {

    private final Logger log = LoggerFactory.getLogger(LoginCountServiceImpl.class);

    private final LoginCountRepository loginCountRepository;

    private final LoginCountMapper loginCountMapper;

    public LoginCountServiceImpl(LoginCountRepository loginCountRepository, LoginCountMapper loginCountMapper) {
        this.loginCountRepository = loginCountRepository;
        this.loginCountMapper = loginCountMapper;
    }

    @Override
    public LoginCountDTO save(LoginCountDTO loginCountDTO) {
        log.debug("Request to save LoginCount : {}", loginCountDTO);
        LoginCount loginCount = loginCountMapper.toEntity(loginCountDTO);
        loginCount = loginCountRepository.save(loginCount);
        return loginCountMapper.toDto(loginCount);
    }

    @Override
    public LoginCountDTO update(LoginCountDTO loginCountDTO) {
        log.debug("Request to update LoginCount : {}", loginCountDTO);
        LoginCount loginCount = loginCountMapper.toEntity(loginCountDTO);
        loginCount = loginCountRepository.save(loginCount);
        return loginCountMapper.toDto(loginCount);
    }

    @Override
    public Optional<LoginCountDTO> partialUpdate(LoginCountDTO loginCountDTO) {
        log.debug("Request to partially update LoginCount : {}", loginCountDTO);

        return loginCountRepository
            .findById(loginCountDTO.getId())
            .map(existingLoginCount -> {
                loginCountMapper.partialUpdate(existingLoginCount, loginCountDTO);

                return existingLoginCount;
            })
            .map(loginCountRepository::save)
            .map(loginCountMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LoginCountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoginCounts");
        return loginCountRepository.findAll(pageable).map(loginCountMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<LoginCountDTO> findOne(Long id) {
        log.debug("Request to get LoginCount : {}", id);
        return loginCountRepository.findById(id).map(loginCountMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoginCount : {}", id);
        loginCountRepository.deleteById(id);
    }
}
