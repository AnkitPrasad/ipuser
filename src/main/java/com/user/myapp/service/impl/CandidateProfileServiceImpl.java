package com.user.myapp.service.impl;

import com.user.myapp.domain.CandidateProfile;
import com.user.myapp.repository.CandidateProfileRepository;
import com.user.myapp.service.CandidateProfileService;
import com.user.myapp.service.dto.CandidateProfileDTO;
import com.user.myapp.service.mapper.CandidateProfileMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CandidateProfile}.
 */
@Service
@Transactional
public class CandidateProfileServiceImpl implements CandidateProfileService {

    private final Logger log = LoggerFactory.getLogger(CandidateProfileServiceImpl.class);

    private final CandidateProfileRepository candidateProfileRepository;

    private final CandidateProfileMapper candidateProfileMapper;

    public CandidateProfileServiceImpl(
        CandidateProfileRepository candidateProfileRepository,
        CandidateProfileMapper candidateProfileMapper
    ) {
        this.candidateProfileRepository = candidateProfileRepository;
        this.candidateProfileMapper = candidateProfileMapper;
    }

    @Override
    public CandidateProfileDTO save(CandidateProfileDTO candidateProfileDTO) {
        log.debug("Request to save CandidateProfile : {}", candidateProfileDTO);
        CandidateProfile candidateProfile = candidateProfileMapper.toEntity(candidateProfileDTO);
        candidateProfile = candidateProfileRepository.save(candidateProfile);
        return candidateProfileMapper.toDto(candidateProfile);
    }

    @Override
    public CandidateProfileDTO update(CandidateProfileDTO candidateProfileDTO) {
        log.debug("Request to update CandidateProfile : {}", candidateProfileDTO);
        CandidateProfile candidateProfile = candidateProfileMapper.toEntity(candidateProfileDTO);
        candidateProfile = candidateProfileRepository.save(candidateProfile);
        return candidateProfileMapper.toDto(candidateProfile);
    }

    @Override
    public Optional<CandidateProfileDTO> partialUpdate(CandidateProfileDTO candidateProfileDTO) {
        log.debug("Request to partially update CandidateProfile : {}", candidateProfileDTO);

        return candidateProfileRepository
            .findById(candidateProfileDTO.getId())
            .map(existingCandidateProfile -> {
                candidateProfileMapper.partialUpdate(existingCandidateProfile, candidateProfileDTO);

                return existingCandidateProfile;
            })
            .map(candidateProfileRepository::save)
            .map(candidateProfileMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CandidateProfileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CandidateProfiles");
        return candidateProfileRepository.findAll(pageable).map(candidateProfileMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CandidateProfileDTO> findOne(Long id) {
        log.debug("Request to get CandidateProfile : {}", id);
        return candidateProfileRepository.findById(id).map(candidateProfileMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CandidateProfile : {}", id);
        candidateProfileRepository.deleteById(id);
    }
}
