package com.user.myapp.service.impl;

import com.user.myapp.domain.RoleAndPermissions;
import com.user.myapp.repository.RoleAndPermissionsRepository;
import com.user.myapp.service.RoleAndPermissionsService;
import com.user.myapp.service.dto.RoleAndPermissionsDTO;
import com.user.myapp.service.mapper.RoleAndPermissionsMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link RoleAndPermissions}.
 */
@Service
@Transactional
public class RoleAndPermissionsServiceImpl implements RoleAndPermissionsService {

    private final Logger log = LoggerFactory.getLogger(RoleAndPermissionsServiceImpl.class);

    private final RoleAndPermissionsRepository roleAndPermissionsRepository;

    private final RoleAndPermissionsMapper roleAndPermissionsMapper;

    public RoleAndPermissionsServiceImpl(
        RoleAndPermissionsRepository roleAndPermissionsRepository,
        RoleAndPermissionsMapper roleAndPermissionsMapper
    ) {
        this.roleAndPermissionsRepository = roleAndPermissionsRepository;
        this.roleAndPermissionsMapper = roleAndPermissionsMapper;
    }

    @Override
    public RoleAndPermissionsDTO save(RoleAndPermissionsDTO roleAndPermissionsDTO) {
        log.debug("Request to save RoleAndPermissions : {}", roleAndPermissionsDTO);
        RoleAndPermissions roleAndPermissions = roleAndPermissionsMapper.toEntity(roleAndPermissionsDTO);
        roleAndPermissions = roleAndPermissionsRepository.save(roleAndPermissions);
        return roleAndPermissionsMapper.toDto(roleAndPermissions);
    }

    @Override
    public RoleAndPermissionsDTO update(RoleAndPermissionsDTO roleAndPermissionsDTO) {
        log.debug("Request to update RoleAndPermissions : {}", roleAndPermissionsDTO);
        RoleAndPermissions roleAndPermissions = roleAndPermissionsMapper.toEntity(roleAndPermissionsDTO);
        roleAndPermissions = roleAndPermissionsRepository.save(roleAndPermissions);
        return roleAndPermissionsMapper.toDto(roleAndPermissions);
    }

    @Override
    public Optional<RoleAndPermissionsDTO> partialUpdate(RoleAndPermissionsDTO roleAndPermissionsDTO) {
        log.debug("Request to partially update RoleAndPermissions : {}", roleAndPermissionsDTO);

        return roleAndPermissionsRepository
            .findById(roleAndPermissionsDTO.getId())
            .map(existingRoleAndPermissions -> {
                roleAndPermissionsMapper.partialUpdate(existingRoleAndPermissions, roleAndPermissionsDTO);

                return existingRoleAndPermissions;
            })
            .map(roleAndPermissionsRepository::save)
            .map(roleAndPermissionsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RoleAndPermissionsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RoleAndPermissions");
        return roleAndPermissionsRepository.findAll(pageable).map(roleAndPermissionsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RoleAndPermissionsDTO> findOne(Long id) {
        log.debug("Request to get RoleAndPermissions : {}", id);
        return roleAndPermissionsRepository.findById(id).map(roleAndPermissionsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RoleAndPermissions : {}", id);
        roleAndPermissionsRepository.deleteById(id);
    }
}
