package com.user.myapp.service.impl;

import com.user.myapp.domain.ClientSetting;
import com.user.myapp.repository.ClientSettingRepository;
import com.user.myapp.service.ClientSettingService;
import com.user.myapp.service.dto.ClientSettingDTO;
import com.user.myapp.service.mapper.ClientSettingMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ClientSetting}.
 */
@Service
@Transactional
public class ClientSettingServiceImpl implements ClientSettingService {

    private final Logger log = LoggerFactory.getLogger(ClientSettingServiceImpl.class);

    private final ClientSettingRepository clientSettingRepository;

    private final ClientSettingMapper clientSettingMapper;

    public ClientSettingServiceImpl(ClientSettingRepository clientSettingRepository, ClientSettingMapper clientSettingMapper) {
        this.clientSettingRepository = clientSettingRepository;
        this.clientSettingMapper = clientSettingMapper;
    }

    @Override
    public ClientSettingDTO save(ClientSettingDTO clientSettingDTO) {
        log.debug("Request to save ClientSetting : {}", clientSettingDTO);
        ClientSetting clientSetting = clientSettingMapper.toEntity(clientSettingDTO);
        clientSetting = clientSettingRepository.save(clientSetting);
        return clientSettingMapper.toDto(clientSetting);
    }

    @Override
    public ClientSettingDTO update(ClientSettingDTO clientSettingDTO) {
        log.debug("Request to update ClientSetting : {}", clientSettingDTO);
        ClientSetting clientSetting = clientSettingMapper.toEntity(clientSettingDTO);
        clientSetting = clientSettingRepository.save(clientSetting);
        return clientSettingMapper.toDto(clientSetting);
    }

    @Override
    public Optional<ClientSettingDTO> partialUpdate(ClientSettingDTO clientSettingDTO) {
        log.debug("Request to partially update ClientSetting : {}", clientSettingDTO);

        return clientSettingRepository
            .findById(clientSettingDTO.getId())
            .map(existingClientSetting -> {
                clientSettingMapper.partialUpdate(existingClientSetting, clientSettingDTO);

                return existingClientSetting;
            })
            .map(clientSettingRepository::save)
            .map(clientSettingMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ClientSettingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientSettings");
        return clientSettingRepository.findAll(pageable).map(clientSettingMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ClientSettingDTO> findOne(Long id) {
        log.debug("Request to get ClientSetting : {}", id);
        return clientSettingRepository.findById(id).map(clientSettingMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClientSetting : {}", id);
        clientSettingRepository.deleteById(id);
    }
}
