package com.user.myapp.service.impl;

import com.user.myapp.domain.UserPermissionOnAttribute;
import com.user.myapp.repository.UserPermissionOnAttributeRepository;
import com.user.myapp.service.UserPermissionOnAttributeService;
import com.user.myapp.service.dto.UserPermissionOnAttributeDTO;
import com.user.myapp.service.mapper.UserPermissionOnAttributeMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UserPermissionOnAttribute}.
 */
@Service
@Transactional
public class UserPermissionOnAttributeServiceImpl implements UserPermissionOnAttributeService {

    private final Logger log = LoggerFactory.getLogger(UserPermissionOnAttributeServiceImpl.class);

    private final UserPermissionOnAttributeRepository userPermissionOnAttributeRepository;

    private final UserPermissionOnAttributeMapper userPermissionOnAttributeMapper;

    public UserPermissionOnAttributeServiceImpl(
        UserPermissionOnAttributeRepository userPermissionOnAttributeRepository,
        UserPermissionOnAttributeMapper userPermissionOnAttributeMapper
    ) {
        this.userPermissionOnAttributeRepository = userPermissionOnAttributeRepository;
        this.userPermissionOnAttributeMapper = userPermissionOnAttributeMapper;
    }

    @Override
    public UserPermissionOnAttributeDTO save(UserPermissionOnAttributeDTO userPermissionOnAttributeDTO) {
        log.debug("Request to save UserPermissionOnAttribute : {}", userPermissionOnAttributeDTO);
        UserPermissionOnAttribute userPermissionOnAttribute = userPermissionOnAttributeMapper.toEntity(userPermissionOnAttributeDTO);
        userPermissionOnAttribute = userPermissionOnAttributeRepository.save(userPermissionOnAttribute);
        return userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);
    }

    @Override
    public UserPermissionOnAttributeDTO update(UserPermissionOnAttributeDTO userPermissionOnAttributeDTO) {
        log.debug("Request to update UserPermissionOnAttribute : {}", userPermissionOnAttributeDTO);
        UserPermissionOnAttribute userPermissionOnAttribute = userPermissionOnAttributeMapper.toEntity(userPermissionOnAttributeDTO);
        userPermissionOnAttribute = userPermissionOnAttributeRepository.save(userPermissionOnAttribute);
        return userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);
    }

    @Override
    public Optional<UserPermissionOnAttributeDTO> partialUpdate(UserPermissionOnAttributeDTO userPermissionOnAttributeDTO) {
        log.debug("Request to partially update UserPermissionOnAttribute : {}", userPermissionOnAttributeDTO);

        return userPermissionOnAttributeRepository
            .findById(userPermissionOnAttributeDTO.getId())
            .map(existingUserPermissionOnAttribute -> {
                userPermissionOnAttributeMapper.partialUpdate(existingUserPermissionOnAttribute, userPermissionOnAttributeDTO);

                return existingUserPermissionOnAttribute;
            })
            .map(userPermissionOnAttributeRepository::save)
            .map(userPermissionOnAttributeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserPermissionOnAttributeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserPermissionOnAttributes");
        return userPermissionOnAttributeRepository.findAll(pageable).map(userPermissionOnAttributeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserPermissionOnAttributeDTO> findOne(Long id) {
        log.debug("Request to get UserPermissionOnAttribute : {}", id);
        return userPermissionOnAttributeRepository.findById(id).map(userPermissionOnAttributeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserPermissionOnAttribute : {}", id);
        userPermissionOnAttributeRepository.deleteById(id);
    }
}
