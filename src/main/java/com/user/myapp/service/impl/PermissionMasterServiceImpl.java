package com.user.myapp.service.impl;

import com.user.myapp.domain.PermissionMaster;
import com.user.myapp.repository.PermissionMasterRepository;
import com.user.myapp.service.PermissionMasterService;
import com.user.myapp.service.dto.PermissionMasterDTO;
import com.user.myapp.service.mapper.PermissionMasterMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PermissionMaster}.
 */
@Service
@Transactional
public class PermissionMasterServiceImpl implements PermissionMasterService {

    private final Logger log = LoggerFactory.getLogger(PermissionMasterServiceImpl.class);

    private final PermissionMasterRepository permissionMasterRepository;

    private final PermissionMasterMapper permissionMasterMapper;

    public PermissionMasterServiceImpl(
        PermissionMasterRepository permissionMasterRepository,
        PermissionMasterMapper permissionMasterMapper
    ) {
        this.permissionMasterRepository = permissionMasterRepository;
        this.permissionMasterMapper = permissionMasterMapper;
    }

    @Override
    public PermissionMasterDTO save(PermissionMasterDTO permissionMasterDTO) {
        log.debug("Request to save PermissionMaster : {}", permissionMasterDTO);
        PermissionMaster permissionMaster = permissionMasterMapper.toEntity(permissionMasterDTO);
        permissionMaster = permissionMasterRepository.save(permissionMaster);
        return permissionMasterMapper.toDto(permissionMaster);
    }

    @Override
    public PermissionMasterDTO update(PermissionMasterDTO permissionMasterDTO) {
        log.debug("Request to update PermissionMaster : {}", permissionMasterDTO);
        PermissionMaster permissionMaster = permissionMasterMapper.toEntity(permissionMasterDTO);
        permissionMaster = permissionMasterRepository.save(permissionMaster);
        return permissionMasterMapper.toDto(permissionMaster);
    }

    @Override
    public Optional<PermissionMasterDTO> partialUpdate(PermissionMasterDTO permissionMasterDTO) {
        log.debug("Request to partially update PermissionMaster : {}", permissionMasterDTO);

        return permissionMasterRepository
            .findById(permissionMasterDTO.getId())
            .map(existingPermissionMaster -> {
                permissionMasterMapper.partialUpdate(existingPermissionMaster, permissionMasterDTO);

                return existingPermissionMaster;
            })
            .map(permissionMasterRepository::save)
            .map(permissionMasterMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PermissionMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PermissionMasters");
        return permissionMasterRepository.findAll(pageable).map(permissionMasterMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PermissionMasterDTO> findOne(Long id) {
        log.debug("Request to get PermissionMaster : {}", id);
        return permissionMasterRepository.findById(id).map(permissionMasterMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PermissionMaster : {}", id);
        permissionMasterRepository.deleteById(id);
    }
}
