package com.user.myapp.service.impl;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.repository.InPreferenceUserRepository;
import com.user.myapp.service.InPreferenceUserService;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.mapper.InPreferenceUserMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link InPreferenceUser}.
 */
@Service
@Transactional
public class InPreferenceUserServiceImpl implements InPreferenceUserService {

    private final Logger log = LoggerFactory.getLogger(InPreferenceUserServiceImpl.class);

    private final InPreferenceUserRepository inPreferenceUserRepository;

    private final InPreferenceUserMapper inPreferenceUserMapper;

    public InPreferenceUserServiceImpl(
        InPreferenceUserRepository inPreferenceUserRepository,
        InPreferenceUserMapper inPreferenceUserMapper
    ) {
        this.inPreferenceUserRepository = inPreferenceUserRepository;
        this.inPreferenceUserMapper = inPreferenceUserMapper;
    }

    @Override
    public InPreferenceUserDTO save(InPreferenceUserDTO inPreferenceUserDTO) {
        log.debug("Request to save InPreferenceUser : {}", inPreferenceUserDTO);
        InPreferenceUser inPreferenceUser = inPreferenceUserMapper.toEntity(inPreferenceUserDTO);
        inPreferenceUser = inPreferenceUserRepository.save(inPreferenceUser);
        return inPreferenceUserMapper.toDto(inPreferenceUser);
    }

    @Override
    public InPreferenceUserDTO update(InPreferenceUserDTO inPreferenceUserDTO) {
        log.debug("Request to update InPreferenceUser : {}", inPreferenceUserDTO);
        InPreferenceUser inPreferenceUser = inPreferenceUserMapper.toEntity(inPreferenceUserDTO);
        inPreferenceUser = inPreferenceUserRepository.save(inPreferenceUser);
        return inPreferenceUserMapper.toDto(inPreferenceUser);
    }

    @Override
    public Optional<InPreferenceUserDTO> partialUpdate(InPreferenceUserDTO inPreferenceUserDTO) {
        log.debug("Request to partially update InPreferenceUser : {}", inPreferenceUserDTO);

        return inPreferenceUserRepository
            .findById(inPreferenceUserDTO.getId())
            .map(existingInPreferenceUser -> {
                inPreferenceUserMapper.partialUpdate(existingInPreferenceUser, inPreferenceUserDTO);

                return existingInPreferenceUser;
            })
            .map(inPreferenceUserRepository::save)
            .map(inPreferenceUserMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<InPreferenceUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InPreferenceUsers");
        return inPreferenceUserRepository.findAll(pageable).map(inPreferenceUserMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<InPreferenceUserDTO> findOne(Long id) {
        log.debug("Request to get InPreferenceUser : {}", id);
        return inPreferenceUserRepository.findById(id).map(inPreferenceUserMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete InPreferenceUser : {}", id);
        inPreferenceUserRepository.deleteById(id);
    }
}
