package com.user.myapp.service;

import com.user.myapp.service.dto.PermissionMasterDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.PermissionMaster}.
 */
public interface PermissionMasterService {
    /**
     * Save a permissionMaster.
     *
     * @param permissionMasterDTO the entity to save.
     * @return the persisted entity.
     */
    PermissionMasterDTO save(PermissionMasterDTO permissionMasterDTO);

    /**
     * Updates a permissionMaster.
     *
     * @param permissionMasterDTO the entity to update.
     * @return the persisted entity.
     */
    PermissionMasterDTO update(PermissionMasterDTO permissionMasterDTO);

    /**
     * Partially updates a permissionMaster.
     *
     * @param permissionMasterDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PermissionMasterDTO> partialUpdate(PermissionMasterDTO permissionMasterDTO);

    /**
     * Get all the permissionMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PermissionMasterDTO> findAll(Pageable pageable);

    /**
     * Get the "id" permissionMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PermissionMasterDTO> findOne(Long id);

    /**
     * Delete the "id" permissionMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
