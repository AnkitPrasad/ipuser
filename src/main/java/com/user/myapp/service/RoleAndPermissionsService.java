package com.user.myapp.service;

import com.user.myapp.service.dto.RoleAndPermissionsDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.RoleAndPermissions}.
 */
public interface RoleAndPermissionsService {
    /**
     * Save a roleAndPermissions.
     *
     * @param roleAndPermissionsDTO the entity to save.
     * @return the persisted entity.
     */
    RoleAndPermissionsDTO save(RoleAndPermissionsDTO roleAndPermissionsDTO);

    /**
     * Updates a roleAndPermissions.
     *
     * @param roleAndPermissionsDTO the entity to update.
     * @return the persisted entity.
     */
    RoleAndPermissionsDTO update(RoleAndPermissionsDTO roleAndPermissionsDTO);

    /**
     * Partially updates a roleAndPermissions.
     *
     * @param roleAndPermissionsDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RoleAndPermissionsDTO> partialUpdate(RoleAndPermissionsDTO roleAndPermissionsDTO);

    /**
     * Get all the roleAndPermissions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RoleAndPermissionsDTO> findAll(Pageable pageable);

    /**
     * Get the "id" roleAndPermissions.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RoleAndPermissionsDTO> findOne(Long id);

    /**
     * Delete the "id" roleAndPermissions.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
