package com.user.myapp.service;

import com.user.myapp.domain.*; // for static metamodels
import com.user.myapp.domain.UserRolePermission;
import com.user.myapp.repository.UserRolePermissionRepository;
import com.user.myapp.service.criteria.UserRolePermissionCriteria;
import com.user.myapp.service.dto.UserRolePermissionDTO;
import com.user.myapp.service.mapper.UserRolePermissionMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link UserRolePermission} entities in the database.
 * The main input is a {@link UserRolePermissionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserRolePermissionDTO} or a {@link Page} of {@link UserRolePermissionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserRolePermissionQueryService extends QueryService<UserRolePermission> {

    private final Logger log = LoggerFactory.getLogger(UserRolePermissionQueryService.class);

    private final UserRolePermissionRepository userRolePermissionRepository;

    private final UserRolePermissionMapper userRolePermissionMapper;

    public UserRolePermissionQueryService(
        UserRolePermissionRepository userRolePermissionRepository,
        UserRolePermissionMapper userRolePermissionMapper
    ) {
        this.userRolePermissionRepository = userRolePermissionRepository;
        this.userRolePermissionMapper = userRolePermissionMapper;
    }

    /**
     * Return a {@link List} of {@link UserRolePermissionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserRolePermissionDTO> findByCriteria(UserRolePermissionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserRolePermission> specification = createSpecification(criteria);
        return userRolePermissionMapper.toDto(userRolePermissionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserRolePermissionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserRolePermissionDTO> findByCriteria(UserRolePermissionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserRolePermission> specification = createSpecification(criteria);
        return userRolePermissionRepository.findAll(specification, page).map(userRolePermissionMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserRolePermissionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserRolePermission> specification = createSpecification(criteria);
        return userRolePermissionRepository.count(specification);
    }

    /**
     * Function to convert {@link UserRolePermissionCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UserRolePermission> createSpecification(UserRolePermissionCriteria criteria) {
        Specification<UserRolePermission> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UserRolePermission_.id));
            }
            if (criteria.getInPreferenceUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getInPreferenceUserId(),
                            root -> root.join(UserRolePermission_.inPreferenceUser, JoinType.LEFT).get(InPreferenceUser_.id)
                        )
                    );
            }
            if (criteria.getRoleMasterId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRoleMasterId(),
                            root -> root.join(UserRolePermission_.roleMaster, JoinType.LEFT).get(RoleMaster_.id)
                        )
                    );
            }
            if (criteria.getPermissionMasterId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPermissionMasterId(),
                            root -> root.join(UserRolePermission_.permissionMaster, JoinType.LEFT).get(PermissionMaster_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
