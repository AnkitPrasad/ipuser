package com.user.myapp.service;

import com.user.myapp.service.dto.UserPermissionOnAttributeDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.user.myapp.domain.UserPermissionOnAttribute}.
 */
public interface UserPermissionOnAttributeService {
    /**
     * Save a userPermissionOnAttribute.
     *
     * @param userPermissionOnAttributeDTO the entity to save.
     * @return the persisted entity.
     */
    UserPermissionOnAttributeDTO save(UserPermissionOnAttributeDTO userPermissionOnAttributeDTO);

    /**
     * Updates a userPermissionOnAttribute.
     *
     * @param userPermissionOnAttributeDTO the entity to update.
     * @return the persisted entity.
     */
    UserPermissionOnAttributeDTO update(UserPermissionOnAttributeDTO userPermissionOnAttributeDTO);

    /**
     * Partially updates a userPermissionOnAttribute.
     *
     * @param userPermissionOnAttributeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<UserPermissionOnAttributeDTO> partialUpdate(UserPermissionOnAttributeDTO userPermissionOnAttributeDTO);

    /**
     * Get all the userPermissionOnAttributes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserPermissionOnAttributeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" userPermissionOnAttribute.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserPermissionOnAttributeDTO> findOne(Long id);

    /**
     * Delete the "id" userPermissionOnAttribute.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
