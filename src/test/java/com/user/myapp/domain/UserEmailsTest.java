package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserEmailsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserEmails.class);
        UserEmails userEmails1 = new UserEmails();
        userEmails1.setId(1L);
        UserEmails userEmails2 = new UserEmails();
        userEmails2.setId(userEmails1.getId());
        assertThat(userEmails1).isEqualTo(userEmails2);
        userEmails2.setId(2L);
        assertThat(userEmails1).isNotEqualTo(userEmails2);
        userEmails1.setId(null);
        assertThat(userEmails1).isNotEqualTo(userEmails2);
    }
}
