package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class InPreferenceUserTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InPreferenceUser.class);
        InPreferenceUser inPreferenceUser1 = new InPreferenceUser();
        inPreferenceUser1.setId(1L);
        InPreferenceUser inPreferenceUser2 = new InPreferenceUser();
        inPreferenceUser2.setId(inPreferenceUser1.getId());
        assertThat(inPreferenceUser1).isEqualTo(inPreferenceUser2);
        inPreferenceUser2.setId(2L);
        assertThat(inPreferenceUser1).isNotEqualTo(inPreferenceUser2);
        inPreferenceUser1.setId(null);
        assertThat(inPreferenceUser1).isNotEqualTo(inPreferenceUser2);
    }
}
