package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ClientSettingTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientSetting.class);
        ClientSetting clientSetting1 = new ClientSetting();
        clientSetting1.setId(1L);
        ClientSetting clientSetting2 = new ClientSetting();
        clientSetting2.setId(clientSetting1.getId());
        assertThat(clientSetting1).isEqualTo(clientSetting2);
        clientSetting2.setId(2L);
        assertThat(clientSetting1).isNotEqualTo(clientSetting2);
        clientSetting1.setId(null);
        assertThat(clientSetting1).isNotEqualTo(clientSetting2);
    }
}
