package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserRolePermissionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserRolePermission.class);
        UserRolePermission userRolePermission1 = new UserRolePermission();
        userRolePermission1.setId(1L);
        UserRolePermission userRolePermission2 = new UserRolePermission();
        userRolePermission2.setId(userRolePermission1.getId());
        assertThat(userRolePermission1).isEqualTo(userRolePermission2);
        userRolePermission2.setId(2L);
        assertThat(userRolePermission1).isNotEqualTo(userRolePermission2);
        userRolePermission1.setId(null);
        assertThat(userRolePermission1).isNotEqualTo(userRolePermission2);
    }
}
