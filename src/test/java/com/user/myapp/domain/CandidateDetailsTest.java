package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CandidateDetailsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CandidateDetails.class);
        CandidateDetails candidateDetails1 = new CandidateDetails();
        candidateDetails1.setId(1L);
        CandidateDetails candidateDetails2 = new CandidateDetails();
        candidateDetails2.setId(candidateDetails1.getId());
        assertThat(candidateDetails1).isEqualTo(candidateDetails2);
        candidateDetails2.setId(2L);
        assertThat(candidateDetails1).isNotEqualTo(candidateDetails2);
        candidateDetails1.setId(null);
        assertThat(candidateDetails1).isNotEqualTo(candidateDetails2);
    }
}
