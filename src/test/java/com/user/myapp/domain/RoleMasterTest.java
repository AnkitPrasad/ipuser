package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RoleMasterTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoleMaster.class);
        RoleMaster roleMaster1 = new RoleMaster();
        roleMaster1.setId(1L);
        RoleMaster roleMaster2 = new RoleMaster();
        roleMaster2.setId(roleMaster1.getId());
        assertThat(roleMaster1).isEqualTo(roleMaster2);
        roleMaster2.setId(2L);
        assertThat(roleMaster1).isNotEqualTo(roleMaster2);
        roleMaster1.setId(null);
        assertThat(roleMaster1).isNotEqualTo(roleMaster2);
    }
}
