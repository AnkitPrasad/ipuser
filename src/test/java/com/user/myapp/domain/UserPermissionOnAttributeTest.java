package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserPermissionOnAttributeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserPermissionOnAttribute.class);
        UserPermissionOnAttribute userPermissionOnAttribute1 = new UserPermissionOnAttribute();
        userPermissionOnAttribute1.setId(1L);
        UserPermissionOnAttribute userPermissionOnAttribute2 = new UserPermissionOnAttribute();
        userPermissionOnAttribute2.setId(userPermissionOnAttribute1.getId());
        assertThat(userPermissionOnAttribute1).isEqualTo(userPermissionOnAttribute2);
        userPermissionOnAttribute2.setId(2L);
        assertThat(userPermissionOnAttribute1).isNotEqualTo(userPermissionOnAttribute2);
        userPermissionOnAttribute1.setId(null);
        assertThat(userPermissionOnAttribute1).isNotEqualTo(userPermissionOnAttribute2);
    }
}
