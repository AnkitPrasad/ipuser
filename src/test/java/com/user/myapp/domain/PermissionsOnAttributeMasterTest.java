package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PermissionsOnAttributeMasterTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PermissionsOnAttributeMaster.class);
        PermissionsOnAttributeMaster permissionsOnAttributeMaster1 = new PermissionsOnAttributeMaster();
        permissionsOnAttributeMaster1.setId(1L);
        PermissionsOnAttributeMaster permissionsOnAttributeMaster2 = new PermissionsOnAttributeMaster();
        permissionsOnAttributeMaster2.setId(permissionsOnAttributeMaster1.getId());
        assertThat(permissionsOnAttributeMaster1).isEqualTo(permissionsOnAttributeMaster2);
        permissionsOnAttributeMaster2.setId(2L);
        assertThat(permissionsOnAttributeMaster1).isNotEqualTo(permissionsOnAttributeMaster2);
        permissionsOnAttributeMaster1.setId(null);
        assertThat(permissionsOnAttributeMaster1).isNotEqualTo(permissionsOnAttributeMaster2);
    }
}
