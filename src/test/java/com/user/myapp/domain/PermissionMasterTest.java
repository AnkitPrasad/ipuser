package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PermissionMasterTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PermissionMaster.class);
        PermissionMaster permissionMaster1 = new PermissionMaster();
        permissionMaster1.setId(1L);
        PermissionMaster permissionMaster2 = new PermissionMaster();
        permissionMaster2.setId(permissionMaster1.getId());
        assertThat(permissionMaster1).isEqualTo(permissionMaster2);
        permissionMaster2.setId(2L);
        assertThat(permissionMaster1).isNotEqualTo(permissionMaster2);
        permissionMaster1.setId(null);
        assertThat(permissionMaster1).isNotEqualTo(permissionMaster2);
    }
}
