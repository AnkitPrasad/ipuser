package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RoleAndPermissionsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoleAndPermissions.class);
        RoleAndPermissions roleAndPermissions1 = new RoleAndPermissions();
        roleAndPermissions1.setId(1L);
        RoleAndPermissions roleAndPermissions2 = new RoleAndPermissions();
        roleAndPermissions2.setId(roleAndPermissions1.getId());
        assertThat(roleAndPermissions1).isEqualTo(roleAndPermissions2);
        roleAndPermissions2.setId(2L);
        assertThat(roleAndPermissions1).isNotEqualTo(roleAndPermissions2);
        roleAndPermissions1.setId(null);
        assertThat(roleAndPermissions1).isNotEqualTo(roleAndPermissions2);
    }
}
