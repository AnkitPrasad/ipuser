package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SuperAdminTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SuperAdmin.class);
        SuperAdmin superAdmin1 = new SuperAdmin();
        superAdmin1.setId(1L);
        SuperAdmin superAdmin2 = new SuperAdmin();
        superAdmin2.setId(superAdmin1.getId());
        assertThat(superAdmin1).isEqualTo(superAdmin2);
        superAdmin2.setId(2L);
        assertThat(superAdmin1).isNotEqualTo(superAdmin2);
        superAdmin1.setId(null);
        assertThat(superAdmin1).isNotEqualTo(superAdmin2);
    }
}
