package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoginCountTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoginCount.class);
        LoginCount loginCount1 = new LoginCount();
        loginCount1.setId(1L);
        LoginCount loginCount2 = new LoginCount();
        loginCount2.setId(loginCount1.getId());
        assertThat(loginCount1).isEqualTo(loginCount2);
        loginCount2.setId(2L);
        assertThat(loginCount1).isNotEqualTo(loginCount2);
        loginCount1.setId(null);
        assertThat(loginCount1).isNotEqualTo(loginCount2);
    }
}
