package com.user.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CandidateProfileTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CandidateProfile.class);
        CandidateProfile candidateProfile1 = new CandidateProfile();
        candidateProfile1.setId(1L);
        CandidateProfile candidateProfile2 = new CandidateProfile();
        candidateProfile2.setId(candidateProfile1.getId());
        assertThat(candidateProfile1).isEqualTo(candidateProfile2);
        candidateProfile2.setId(2L);
        assertThat(candidateProfile1).isNotEqualTo(candidateProfile2);
        candidateProfile1.setId(null);
        assertThat(candidateProfile1).isNotEqualTo(candidateProfile2);
    }
}
