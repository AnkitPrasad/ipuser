package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserPermissionOnAttributeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserPermissionOnAttributeDTO.class);
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO1 = new UserPermissionOnAttributeDTO();
        userPermissionOnAttributeDTO1.setId(1L);
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO2 = new UserPermissionOnAttributeDTO();
        assertThat(userPermissionOnAttributeDTO1).isNotEqualTo(userPermissionOnAttributeDTO2);
        userPermissionOnAttributeDTO2.setId(userPermissionOnAttributeDTO1.getId());
        assertThat(userPermissionOnAttributeDTO1).isEqualTo(userPermissionOnAttributeDTO2);
        userPermissionOnAttributeDTO2.setId(2L);
        assertThat(userPermissionOnAttributeDTO1).isNotEqualTo(userPermissionOnAttributeDTO2);
        userPermissionOnAttributeDTO1.setId(null);
        assertThat(userPermissionOnAttributeDTO1).isNotEqualTo(userPermissionOnAttributeDTO2);
    }
}
