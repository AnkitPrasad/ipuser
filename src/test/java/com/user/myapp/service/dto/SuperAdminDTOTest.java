package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SuperAdminDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SuperAdminDTO.class);
        SuperAdminDTO superAdminDTO1 = new SuperAdminDTO();
        superAdminDTO1.setId(1L);
        SuperAdminDTO superAdminDTO2 = new SuperAdminDTO();
        assertThat(superAdminDTO1).isNotEqualTo(superAdminDTO2);
        superAdminDTO2.setId(superAdminDTO1.getId());
        assertThat(superAdminDTO1).isEqualTo(superAdminDTO2);
        superAdminDTO2.setId(2L);
        assertThat(superAdminDTO1).isNotEqualTo(superAdminDTO2);
        superAdminDTO1.setId(null);
        assertThat(superAdminDTO1).isNotEqualTo(superAdminDTO2);
    }
}
