package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserRolePermissionDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserRolePermissionDTO.class);
        UserRolePermissionDTO userRolePermissionDTO1 = new UserRolePermissionDTO();
        userRolePermissionDTO1.setId(1L);
        UserRolePermissionDTO userRolePermissionDTO2 = new UserRolePermissionDTO();
        assertThat(userRolePermissionDTO1).isNotEqualTo(userRolePermissionDTO2);
        userRolePermissionDTO2.setId(userRolePermissionDTO1.getId());
        assertThat(userRolePermissionDTO1).isEqualTo(userRolePermissionDTO2);
        userRolePermissionDTO2.setId(2L);
        assertThat(userRolePermissionDTO1).isNotEqualTo(userRolePermissionDTO2);
        userRolePermissionDTO1.setId(null);
        assertThat(userRolePermissionDTO1).isNotEqualTo(userRolePermissionDTO2);
    }
}
