package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserEmailsDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserEmailsDTO.class);
        UserEmailsDTO userEmailsDTO1 = new UserEmailsDTO();
        userEmailsDTO1.setId(1L);
        UserEmailsDTO userEmailsDTO2 = new UserEmailsDTO();
        assertThat(userEmailsDTO1).isNotEqualTo(userEmailsDTO2);
        userEmailsDTO2.setId(userEmailsDTO1.getId());
        assertThat(userEmailsDTO1).isEqualTo(userEmailsDTO2);
        userEmailsDTO2.setId(2L);
        assertThat(userEmailsDTO1).isNotEqualTo(userEmailsDTO2);
        userEmailsDTO1.setId(null);
        assertThat(userEmailsDTO1).isNotEqualTo(userEmailsDTO2);
    }
}
