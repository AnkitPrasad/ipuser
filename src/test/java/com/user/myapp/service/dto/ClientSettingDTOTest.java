package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ClientSettingDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientSettingDTO.class);
        ClientSettingDTO clientSettingDTO1 = new ClientSettingDTO();
        clientSettingDTO1.setId(1L);
        ClientSettingDTO clientSettingDTO2 = new ClientSettingDTO();
        assertThat(clientSettingDTO1).isNotEqualTo(clientSettingDTO2);
        clientSettingDTO2.setId(clientSettingDTO1.getId());
        assertThat(clientSettingDTO1).isEqualTo(clientSettingDTO2);
        clientSettingDTO2.setId(2L);
        assertThat(clientSettingDTO1).isNotEqualTo(clientSettingDTO2);
        clientSettingDTO1.setId(null);
        assertThat(clientSettingDTO1).isNotEqualTo(clientSettingDTO2);
    }
}
