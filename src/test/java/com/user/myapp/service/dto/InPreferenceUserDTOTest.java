package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class InPreferenceUserDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InPreferenceUserDTO.class);
        InPreferenceUserDTO inPreferenceUserDTO1 = new InPreferenceUserDTO();
        inPreferenceUserDTO1.setId(1L);
        InPreferenceUserDTO inPreferenceUserDTO2 = new InPreferenceUserDTO();
        assertThat(inPreferenceUserDTO1).isNotEqualTo(inPreferenceUserDTO2);
        inPreferenceUserDTO2.setId(inPreferenceUserDTO1.getId());
        assertThat(inPreferenceUserDTO1).isEqualTo(inPreferenceUserDTO2);
        inPreferenceUserDTO2.setId(2L);
        assertThat(inPreferenceUserDTO1).isNotEqualTo(inPreferenceUserDTO2);
        inPreferenceUserDTO1.setId(null);
        assertThat(inPreferenceUserDTO1).isNotEqualTo(inPreferenceUserDTO2);
    }
}
