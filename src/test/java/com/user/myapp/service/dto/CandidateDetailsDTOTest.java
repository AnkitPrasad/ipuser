package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CandidateDetailsDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CandidateDetailsDTO.class);
        CandidateDetailsDTO candidateDetailsDTO1 = new CandidateDetailsDTO();
        candidateDetailsDTO1.setId(1L);
        CandidateDetailsDTO candidateDetailsDTO2 = new CandidateDetailsDTO();
        assertThat(candidateDetailsDTO1).isNotEqualTo(candidateDetailsDTO2);
        candidateDetailsDTO2.setId(candidateDetailsDTO1.getId());
        assertThat(candidateDetailsDTO1).isEqualTo(candidateDetailsDTO2);
        candidateDetailsDTO2.setId(2L);
        assertThat(candidateDetailsDTO1).isNotEqualTo(candidateDetailsDTO2);
        candidateDetailsDTO1.setId(null);
        assertThat(candidateDetailsDTO1).isNotEqualTo(candidateDetailsDTO2);
    }
}
