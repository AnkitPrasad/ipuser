package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PermissionsOnAttributeMasterDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PermissionsOnAttributeMasterDTO.class);
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO1 = new PermissionsOnAttributeMasterDTO();
        permissionsOnAttributeMasterDTO1.setId(1L);
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO2 = new PermissionsOnAttributeMasterDTO();
        assertThat(permissionsOnAttributeMasterDTO1).isNotEqualTo(permissionsOnAttributeMasterDTO2);
        permissionsOnAttributeMasterDTO2.setId(permissionsOnAttributeMasterDTO1.getId());
        assertThat(permissionsOnAttributeMasterDTO1).isEqualTo(permissionsOnAttributeMasterDTO2);
        permissionsOnAttributeMasterDTO2.setId(2L);
        assertThat(permissionsOnAttributeMasterDTO1).isNotEqualTo(permissionsOnAttributeMasterDTO2);
        permissionsOnAttributeMasterDTO1.setId(null);
        assertThat(permissionsOnAttributeMasterDTO1).isNotEqualTo(permissionsOnAttributeMasterDTO2);
    }
}
