package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RoleMasterDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoleMasterDTO.class);
        RoleMasterDTO roleMasterDTO1 = new RoleMasterDTO();
        roleMasterDTO1.setId(1L);
        RoleMasterDTO roleMasterDTO2 = new RoleMasterDTO();
        assertThat(roleMasterDTO1).isNotEqualTo(roleMasterDTO2);
        roleMasterDTO2.setId(roleMasterDTO1.getId());
        assertThat(roleMasterDTO1).isEqualTo(roleMasterDTO2);
        roleMasterDTO2.setId(2L);
        assertThat(roleMasterDTO1).isNotEqualTo(roleMasterDTO2);
        roleMasterDTO1.setId(null);
        assertThat(roleMasterDTO1).isNotEqualTo(roleMasterDTO2);
    }
}
