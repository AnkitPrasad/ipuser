package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RoleAndPermissionsDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoleAndPermissionsDTO.class);
        RoleAndPermissionsDTO roleAndPermissionsDTO1 = new RoleAndPermissionsDTO();
        roleAndPermissionsDTO1.setId(1L);
        RoleAndPermissionsDTO roleAndPermissionsDTO2 = new RoleAndPermissionsDTO();
        assertThat(roleAndPermissionsDTO1).isNotEqualTo(roleAndPermissionsDTO2);
        roleAndPermissionsDTO2.setId(roleAndPermissionsDTO1.getId());
        assertThat(roleAndPermissionsDTO1).isEqualTo(roleAndPermissionsDTO2);
        roleAndPermissionsDTO2.setId(2L);
        assertThat(roleAndPermissionsDTO1).isNotEqualTo(roleAndPermissionsDTO2);
        roleAndPermissionsDTO1.setId(null);
        assertThat(roleAndPermissionsDTO1).isNotEqualTo(roleAndPermissionsDTO2);
    }
}
