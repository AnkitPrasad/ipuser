package com.user.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.user.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoginCountDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoginCountDTO.class);
        LoginCountDTO loginCountDTO1 = new LoginCountDTO();
        loginCountDTO1.setId(1L);
        LoginCountDTO loginCountDTO2 = new LoginCountDTO();
        assertThat(loginCountDTO1).isNotEqualTo(loginCountDTO2);
        loginCountDTO2.setId(loginCountDTO1.getId());
        assertThat(loginCountDTO1).isEqualTo(loginCountDTO2);
        loginCountDTO2.setId(2L);
        assertThat(loginCountDTO1).isNotEqualTo(loginCountDTO2);
        loginCountDTO1.setId(null);
        assertThat(loginCountDTO1).isNotEqualTo(loginCountDTO2);
    }
}
