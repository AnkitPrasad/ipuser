package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.Candidate;
import com.user.myapp.domain.CandidateProfile;
import com.user.myapp.repository.CandidateProfileRepository;
import com.user.myapp.service.criteria.CandidateProfileCriteria;
import com.user.myapp.service.dto.CandidateProfileDTO;
import com.user.myapp.service.mapper.CandidateProfileMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CandidateProfileResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CandidateProfileResourceIT {

    private static final String DEFAULT_ABOUT_ME = "AAAAAAAAAA";
    private static final String UPDATED_ABOUT_ME = "BBBBBBBBBB";

    private static final String DEFAULT_CAREER_SUMMARY = "AAAAAAAAAA";
    private static final String UPDATED_CAREER_SUMMARY = "BBBBBBBBBB";

    private static final String DEFAULT_EDUCATION = "AAAAAAAAAA";
    private static final String UPDATED_EDUCATION = "BBBBBBBBBB";

    private static final String DEFAULT_DOB = "AAAAAAAAAA";
    private static final String UPDATED_DOB = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_EMPLOYMENT = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_EMPLOYMENT = "BBBBBBBBBB";

    private static final String DEFAULT_CERTIFICATIONS = "AAAAAAAAAA";
    private static final String UPDATED_CERTIFICATIONS = "BBBBBBBBBB";

    private static final String DEFAULT_EXPERIENCE = "AAAAAAAAAA";
    private static final String UPDATED_EXPERIENCE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/candidate-profiles";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CandidateProfileRepository candidateProfileRepository;

    @Autowired
    private CandidateProfileMapper candidateProfileMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCandidateProfileMockMvc;

    private CandidateProfile candidateProfile;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CandidateProfile createEntity(EntityManager em) {
        CandidateProfile candidateProfile = new CandidateProfile()
            .aboutMe(DEFAULT_ABOUT_ME)
            .careerSummary(DEFAULT_CAREER_SUMMARY)
            .education(DEFAULT_EDUCATION)
            .dob(DEFAULT_DOB)
            .currentEmployment(DEFAULT_CURRENT_EMPLOYMENT)
            .certifications(DEFAULT_CERTIFICATIONS)
            .experience(DEFAULT_EXPERIENCE);
        return candidateProfile;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CandidateProfile createUpdatedEntity(EntityManager em) {
        CandidateProfile candidateProfile = new CandidateProfile()
            .aboutMe(UPDATED_ABOUT_ME)
            .careerSummary(UPDATED_CAREER_SUMMARY)
            .education(UPDATED_EDUCATION)
            .dob(UPDATED_DOB)
            .currentEmployment(UPDATED_CURRENT_EMPLOYMENT)
            .certifications(UPDATED_CERTIFICATIONS)
            .experience(UPDATED_EXPERIENCE);
        return candidateProfile;
    }

    @BeforeEach
    public void initTest() {
        candidateProfile = createEntity(em);
    }

    @Test
    @Transactional
    void createCandidateProfile() throws Exception {
        int databaseSizeBeforeCreate = candidateProfileRepository.findAll().size();
        // Create the CandidateProfile
        CandidateProfileDTO candidateProfileDTO = candidateProfileMapper.toDto(candidateProfile);
        restCandidateProfileMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(candidateProfileDTO))
            )
            .andExpect(status().isCreated());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeCreate + 1);
        CandidateProfile testCandidateProfile = candidateProfileList.get(candidateProfileList.size() - 1);
        assertThat(testCandidateProfile.getAboutMe()).isEqualTo(DEFAULT_ABOUT_ME);
        assertThat(testCandidateProfile.getCareerSummary()).isEqualTo(DEFAULT_CAREER_SUMMARY);
        assertThat(testCandidateProfile.getEducation()).isEqualTo(DEFAULT_EDUCATION);
        assertThat(testCandidateProfile.getDob()).isEqualTo(DEFAULT_DOB);
        assertThat(testCandidateProfile.getCurrentEmployment()).isEqualTo(DEFAULT_CURRENT_EMPLOYMENT);
        assertThat(testCandidateProfile.getCertifications()).isEqualTo(DEFAULT_CERTIFICATIONS);
        assertThat(testCandidateProfile.getExperience()).isEqualTo(DEFAULT_EXPERIENCE);
    }

    @Test
    @Transactional
    void createCandidateProfileWithExistingId() throws Exception {
        // Create the CandidateProfile with an existing ID
        candidateProfile.setId(1L);
        CandidateProfileDTO candidateProfileDTO = candidateProfileMapper.toDto(candidateProfile);

        int databaseSizeBeforeCreate = candidateProfileRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidateProfileMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(candidateProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCandidateProfiles() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList
        restCandidateProfileMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidateProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].aboutMe").value(hasItem(DEFAULT_ABOUT_ME)))
            .andExpect(jsonPath("$.[*].careerSummary").value(hasItem(DEFAULT_CAREER_SUMMARY)))
            .andExpect(jsonPath("$.[*].education").value(hasItem(DEFAULT_EDUCATION)))
            .andExpect(jsonPath("$.[*].dob").value(hasItem(DEFAULT_DOB)))
            .andExpect(jsonPath("$.[*].currentEmployment").value(hasItem(DEFAULT_CURRENT_EMPLOYMENT)))
            .andExpect(jsonPath("$.[*].certifications").value(hasItem(DEFAULT_CERTIFICATIONS)))
            .andExpect(jsonPath("$.[*].experience").value(hasItem(DEFAULT_EXPERIENCE)));
    }

    @Test
    @Transactional
    void getCandidateProfile() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get the candidateProfile
        restCandidateProfileMockMvc
            .perform(get(ENTITY_API_URL_ID, candidateProfile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(candidateProfile.getId().intValue()))
            .andExpect(jsonPath("$.aboutMe").value(DEFAULT_ABOUT_ME))
            .andExpect(jsonPath("$.careerSummary").value(DEFAULT_CAREER_SUMMARY))
            .andExpect(jsonPath("$.education").value(DEFAULT_EDUCATION))
            .andExpect(jsonPath("$.dob").value(DEFAULT_DOB))
            .andExpect(jsonPath("$.currentEmployment").value(DEFAULT_CURRENT_EMPLOYMENT))
            .andExpect(jsonPath("$.certifications").value(DEFAULT_CERTIFICATIONS))
            .andExpect(jsonPath("$.experience").value(DEFAULT_EXPERIENCE));
    }

    @Test
    @Transactional
    void getCandidateProfilesByIdFiltering() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        Long id = candidateProfile.getId();

        defaultCandidateProfileShouldBeFound("id.equals=" + id);
        defaultCandidateProfileShouldNotBeFound("id.notEquals=" + id);

        defaultCandidateProfileShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCandidateProfileShouldNotBeFound("id.greaterThan=" + id);

        defaultCandidateProfileShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCandidateProfileShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByAboutMeIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where aboutMe equals to DEFAULT_ABOUT_ME
        defaultCandidateProfileShouldBeFound("aboutMe.equals=" + DEFAULT_ABOUT_ME);

        // Get all the candidateProfileList where aboutMe equals to UPDATED_ABOUT_ME
        defaultCandidateProfileShouldNotBeFound("aboutMe.equals=" + UPDATED_ABOUT_ME);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByAboutMeIsInShouldWork() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where aboutMe in DEFAULT_ABOUT_ME or UPDATED_ABOUT_ME
        defaultCandidateProfileShouldBeFound("aboutMe.in=" + DEFAULT_ABOUT_ME + "," + UPDATED_ABOUT_ME);

        // Get all the candidateProfileList where aboutMe equals to UPDATED_ABOUT_ME
        defaultCandidateProfileShouldNotBeFound("aboutMe.in=" + UPDATED_ABOUT_ME);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByAboutMeIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where aboutMe is not null
        defaultCandidateProfileShouldBeFound("aboutMe.specified=true");

        // Get all the candidateProfileList where aboutMe is null
        defaultCandidateProfileShouldNotBeFound("aboutMe.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByAboutMeContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where aboutMe contains DEFAULT_ABOUT_ME
        defaultCandidateProfileShouldBeFound("aboutMe.contains=" + DEFAULT_ABOUT_ME);

        // Get all the candidateProfileList where aboutMe contains UPDATED_ABOUT_ME
        defaultCandidateProfileShouldNotBeFound("aboutMe.contains=" + UPDATED_ABOUT_ME);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByAboutMeNotContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where aboutMe does not contain DEFAULT_ABOUT_ME
        defaultCandidateProfileShouldNotBeFound("aboutMe.doesNotContain=" + DEFAULT_ABOUT_ME);

        // Get all the candidateProfileList where aboutMe does not contain UPDATED_ABOUT_ME
        defaultCandidateProfileShouldBeFound("aboutMe.doesNotContain=" + UPDATED_ABOUT_ME);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCareerSummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where careerSummary equals to DEFAULT_CAREER_SUMMARY
        defaultCandidateProfileShouldBeFound("careerSummary.equals=" + DEFAULT_CAREER_SUMMARY);

        // Get all the candidateProfileList where careerSummary equals to UPDATED_CAREER_SUMMARY
        defaultCandidateProfileShouldNotBeFound("careerSummary.equals=" + UPDATED_CAREER_SUMMARY);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCareerSummaryIsInShouldWork() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where careerSummary in DEFAULT_CAREER_SUMMARY or UPDATED_CAREER_SUMMARY
        defaultCandidateProfileShouldBeFound("careerSummary.in=" + DEFAULT_CAREER_SUMMARY + "," + UPDATED_CAREER_SUMMARY);

        // Get all the candidateProfileList where careerSummary equals to UPDATED_CAREER_SUMMARY
        defaultCandidateProfileShouldNotBeFound("careerSummary.in=" + UPDATED_CAREER_SUMMARY);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCareerSummaryIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where careerSummary is not null
        defaultCandidateProfileShouldBeFound("careerSummary.specified=true");

        // Get all the candidateProfileList where careerSummary is null
        defaultCandidateProfileShouldNotBeFound("careerSummary.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCareerSummaryContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where careerSummary contains DEFAULT_CAREER_SUMMARY
        defaultCandidateProfileShouldBeFound("careerSummary.contains=" + DEFAULT_CAREER_SUMMARY);

        // Get all the candidateProfileList where careerSummary contains UPDATED_CAREER_SUMMARY
        defaultCandidateProfileShouldNotBeFound("careerSummary.contains=" + UPDATED_CAREER_SUMMARY);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCareerSummaryNotContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where careerSummary does not contain DEFAULT_CAREER_SUMMARY
        defaultCandidateProfileShouldNotBeFound("careerSummary.doesNotContain=" + DEFAULT_CAREER_SUMMARY);

        // Get all the candidateProfileList where careerSummary does not contain UPDATED_CAREER_SUMMARY
        defaultCandidateProfileShouldBeFound("careerSummary.doesNotContain=" + UPDATED_CAREER_SUMMARY);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByEducationIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where education equals to DEFAULT_EDUCATION
        defaultCandidateProfileShouldBeFound("education.equals=" + DEFAULT_EDUCATION);

        // Get all the candidateProfileList where education equals to UPDATED_EDUCATION
        defaultCandidateProfileShouldNotBeFound("education.equals=" + UPDATED_EDUCATION);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByEducationIsInShouldWork() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where education in DEFAULT_EDUCATION or UPDATED_EDUCATION
        defaultCandidateProfileShouldBeFound("education.in=" + DEFAULT_EDUCATION + "," + UPDATED_EDUCATION);

        // Get all the candidateProfileList where education equals to UPDATED_EDUCATION
        defaultCandidateProfileShouldNotBeFound("education.in=" + UPDATED_EDUCATION);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByEducationIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where education is not null
        defaultCandidateProfileShouldBeFound("education.specified=true");

        // Get all the candidateProfileList where education is null
        defaultCandidateProfileShouldNotBeFound("education.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByEducationContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where education contains DEFAULT_EDUCATION
        defaultCandidateProfileShouldBeFound("education.contains=" + DEFAULT_EDUCATION);

        // Get all the candidateProfileList where education contains UPDATED_EDUCATION
        defaultCandidateProfileShouldNotBeFound("education.contains=" + UPDATED_EDUCATION);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByEducationNotContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where education does not contain DEFAULT_EDUCATION
        defaultCandidateProfileShouldNotBeFound("education.doesNotContain=" + DEFAULT_EDUCATION);

        // Get all the candidateProfileList where education does not contain UPDATED_EDUCATION
        defaultCandidateProfileShouldBeFound("education.doesNotContain=" + UPDATED_EDUCATION);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByDobIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where dob equals to DEFAULT_DOB
        defaultCandidateProfileShouldBeFound("dob.equals=" + DEFAULT_DOB);

        // Get all the candidateProfileList where dob equals to UPDATED_DOB
        defaultCandidateProfileShouldNotBeFound("dob.equals=" + UPDATED_DOB);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByDobIsInShouldWork() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where dob in DEFAULT_DOB or UPDATED_DOB
        defaultCandidateProfileShouldBeFound("dob.in=" + DEFAULT_DOB + "," + UPDATED_DOB);

        // Get all the candidateProfileList where dob equals to UPDATED_DOB
        defaultCandidateProfileShouldNotBeFound("dob.in=" + UPDATED_DOB);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByDobIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where dob is not null
        defaultCandidateProfileShouldBeFound("dob.specified=true");

        // Get all the candidateProfileList where dob is null
        defaultCandidateProfileShouldNotBeFound("dob.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByDobContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where dob contains DEFAULT_DOB
        defaultCandidateProfileShouldBeFound("dob.contains=" + DEFAULT_DOB);

        // Get all the candidateProfileList where dob contains UPDATED_DOB
        defaultCandidateProfileShouldNotBeFound("dob.contains=" + UPDATED_DOB);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByDobNotContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where dob does not contain DEFAULT_DOB
        defaultCandidateProfileShouldNotBeFound("dob.doesNotContain=" + DEFAULT_DOB);

        // Get all the candidateProfileList where dob does not contain UPDATED_DOB
        defaultCandidateProfileShouldBeFound("dob.doesNotContain=" + UPDATED_DOB);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCurrentEmploymentIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where currentEmployment equals to DEFAULT_CURRENT_EMPLOYMENT
        defaultCandidateProfileShouldBeFound("currentEmployment.equals=" + DEFAULT_CURRENT_EMPLOYMENT);

        // Get all the candidateProfileList where currentEmployment equals to UPDATED_CURRENT_EMPLOYMENT
        defaultCandidateProfileShouldNotBeFound("currentEmployment.equals=" + UPDATED_CURRENT_EMPLOYMENT);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCurrentEmploymentIsInShouldWork() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where currentEmployment in DEFAULT_CURRENT_EMPLOYMENT or UPDATED_CURRENT_EMPLOYMENT
        defaultCandidateProfileShouldBeFound("currentEmployment.in=" + DEFAULT_CURRENT_EMPLOYMENT + "," + UPDATED_CURRENT_EMPLOYMENT);

        // Get all the candidateProfileList where currentEmployment equals to UPDATED_CURRENT_EMPLOYMENT
        defaultCandidateProfileShouldNotBeFound("currentEmployment.in=" + UPDATED_CURRENT_EMPLOYMENT);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCurrentEmploymentIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where currentEmployment is not null
        defaultCandidateProfileShouldBeFound("currentEmployment.specified=true");

        // Get all the candidateProfileList where currentEmployment is null
        defaultCandidateProfileShouldNotBeFound("currentEmployment.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCurrentEmploymentContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where currentEmployment contains DEFAULT_CURRENT_EMPLOYMENT
        defaultCandidateProfileShouldBeFound("currentEmployment.contains=" + DEFAULT_CURRENT_EMPLOYMENT);

        // Get all the candidateProfileList where currentEmployment contains UPDATED_CURRENT_EMPLOYMENT
        defaultCandidateProfileShouldNotBeFound("currentEmployment.contains=" + UPDATED_CURRENT_EMPLOYMENT);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCurrentEmploymentNotContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where currentEmployment does not contain DEFAULT_CURRENT_EMPLOYMENT
        defaultCandidateProfileShouldNotBeFound("currentEmployment.doesNotContain=" + DEFAULT_CURRENT_EMPLOYMENT);

        // Get all the candidateProfileList where currentEmployment does not contain UPDATED_CURRENT_EMPLOYMENT
        defaultCandidateProfileShouldBeFound("currentEmployment.doesNotContain=" + UPDATED_CURRENT_EMPLOYMENT);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCertificationsIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where certifications equals to DEFAULT_CERTIFICATIONS
        defaultCandidateProfileShouldBeFound("certifications.equals=" + DEFAULT_CERTIFICATIONS);

        // Get all the candidateProfileList where certifications equals to UPDATED_CERTIFICATIONS
        defaultCandidateProfileShouldNotBeFound("certifications.equals=" + UPDATED_CERTIFICATIONS);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCertificationsIsInShouldWork() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where certifications in DEFAULT_CERTIFICATIONS or UPDATED_CERTIFICATIONS
        defaultCandidateProfileShouldBeFound("certifications.in=" + DEFAULT_CERTIFICATIONS + "," + UPDATED_CERTIFICATIONS);

        // Get all the candidateProfileList where certifications equals to UPDATED_CERTIFICATIONS
        defaultCandidateProfileShouldNotBeFound("certifications.in=" + UPDATED_CERTIFICATIONS);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCertificationsIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where certifications is not null
        defaultCandidateProfileShouldBeFound("certifications.specified=true");

        // Get all the candidateProfileList where certifications is null
        defaultCandidateProfileShouldNotBeFound("certifications.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCertificationsContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where certifications contains DEFAULT_CERTIFICATIONS
        defaultCandidateProfileShouldBeFound("certifications.contains=" + DEFAULT_CERTIFICATIONS);

        // Get all the candidateProfileList where certifications contains UPDATED_CERTIFICATIONS
        defaultCandidateProfileShouldNotBeFound("certifications.contains=" + UPDATED_CERTIFICATIONS);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCertificationsNotContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where certifications does not contain DEFAULT_CERTIFICATIONS
        defaultCandidateProfileShouldNotBeFound("certifications.doesNotContain=" + DEFAULT_CERTIFICATIONS);

        // Get all the candidateProfileList where certifications does not contain UPDATED_CERTIFICATIONS
        defaultCandidateProfileShouldBeFound("certifications.doesNotContain=" + UPDATED_CERTIFICATIONS);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByExperienceIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where experience equals to DEFAULT_EXPERIENCE
        defaultCandidateProfileShouldBeFound("experience.equals=" + DEFAULT_EXPERIENCE);

        // Get all the candidateProfileList where experience equals to UPDATED_EXPERIENCE
        defaultCandidateProfileShouldNotBeFound("experience.equals=" + UPDATED_EXPERIENCE);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByExperienceIsInShouldWork() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where experience in DEFAULT_EXPERIENCE or UPDATED_EXPERIENCE
        defaultCandidateProfileShouldBeFound("experience.in=" + DEFAULT_EXPERIENCE + "," + UPDATED_EXPERIENCE);

        // Get all the candidateProfileList where experience equals to UPDATED_EXPERIENCE
        defaultCandidateProfileShouldNotBeFound("experience.in=" + UPDATED_EXPERIENCE);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByExperienceIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where experience is not null
        defaultCandidateProfileShouldBeFound("experience.specified=true");

        // Get all the candidateProfileList where experience is null
        defaultCandidateProfileShouldNotBeFound("experience.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByExperienceContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where experience contains DEFAULT_EXPERIENCE
        defaultCandidateProfileShouldBeFound("experience.contains=" + DEFAULT_EXPERIENCE);

        // Get all the candidateProfileList where experience contains UPDATED_EXPERIENCE
        defaultCandidateProfileShouldNotBeFound("experience.contains=" + UPDATED_EXPERIENCE);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByExperienceNotContainsSomething() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        // Get all the candidateProfileList where experience does not contain DEFAULT_EXPERIENCE
        defaultCandidateProfileShouldNotBeFound("experience.doesNotContain=" + DEFAULT_EXPERIENCE);

        // Get all the candidateProfileList where experience does not contain UPDATED_EXPERIENCE
        defaultCandidateProfileShouldBeFound("experience.doesNotContain=" + UPDATED_EXPERIENCE);
    }

    @Test
    @Transactional
    void getAllCandidateProfilesByCandidateIsEqualToSomething() throws Exception {
        Candidate candidate;
        if (TestUtil.findAll(em, Candidate.class).isEmpty()) {
            candidateProfileRepository.saveAndFlush(candidateProfile);
            candidate = CandidateResourceIT.createEntity(em);
        } else {
            candidate = TestUtil.findAll(em, Candidate.class).get(0);
        }
        em.persist(candidate);
        em.flush();
        candidateProfile.setCandidate(candidate);
        candidateProfileRepository.saveAndFlush(candidateProfile);
        Long candidateId = candidate.getId();

        // Get all the candidateProfileList where candidate equals to candidateId
        defaultCandidateProfileShouldBeFound("candidateId.equals=" + candidateId);

        // Get all the candidateProfileList where candidate equals to (candidateId + 1)
        defaultCandidateProfileShouldNotBeFound("candidateId.equals=" + (candidateId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCandidateProfileShouldBeFound(String filter) throws Exception {
        restCandidateProfileMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidateProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].aboutMe").value(hasItem(DEFAULT_ABOUT_ME)))
            .andExpect(jsonPath("$.[*].careerSummary").value(hasItem(DEFAULT_CAREER_SUMMARY)))
            .andExpect(jsonPath("$.[*].education").value(hasItem(DEFAULT_EDUCATION)))
            .andExpect(jsonPath("$.[*].dob").value(hasItem(DEFAULT_DOB)))
            .andExpect(jsonPath("$.[*].currentEmployment").value(hasItem(DEFAULT_CURRENT_EMPLOYMENT)))
            .andExpect(jsonPath("$.[*].certifications").value(hasItem(DEFAULT_CERTIFICATIONS)))
            .andExpect(jsonPath("$.[*].experience").value(hasItem(DEFAULT_EXPERIENCE)));

        // Check, that the count call also returns 1
        restCandidateProfileMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCandidateProfileShouldNotBeFound(String filter) throws Exception {
        restCandidateProfileMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCandidateProfileMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCandidateProfile() throws Exception {
        // Get the candidateProfile
        restCandidateProfileMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCandidateProfile() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        int databaseSizeBeforeUpdate = candidateProfileRepository.findAll().size();

        // Update the candidateProfile
        CandidateProfile updatedCandidateProfile = candidateProfileRepository.findById(candidateProfile.getId()).get();
        // Disconnect from session so that the updates on updatedCandidateProfile are not directly saved in db
        em.detach(updatedCandidateProfile);
        updatedCandidateProfile
            .aboutMe(UPDATED_ABOUT_ME)
            .careerSummary(UPDATED_CAREER_SUMMARY)
            .education(UPDATED_EDUCATION)
            .dob(UPDATED_DOB)
            .currentEmployment(UPDATED_CURRENT_EMPLOYMENT)
            .certifications(UPDATED_CERTIFICATIONS)
            .experience(UPDATED_EXPERIENCE);
        CandidateProfileDTO candidateProfileDTO = candidateProfileMapper.toDto(updatedCandidateProfile);

        restCandidateProfileMockMvc
            .perform(
                put(ENTITY_API_URL_ID, candidateProfileDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(candidateProfileDTO))
            )
            .andExpect(status().isOk());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeUpdate);
        CandidateProfile testCandidateProfile = candidateProfileList.get(candidateProfileList.size() - 1);
        assertThat(testCandidateProfile.getAboutMe()).isEqualTo(UPDATED_ABOUT_ME);
        assertThat(testCandidateProfile.getCareerSummary()).isEqualTo(UPDATED_CAREER_SUMMARY);
        assertThat(testCandidateProfile.getEducation()).isEqualTo(UPDATED_EDUCATION);
        assertThat(testCandidateProfile.getDob()).isEqualTo(UPDATED_DOB);
        assertThat(testCandidateProfile.getCurrentEmployment()).isEqualTo(UPDATED_CURRENT_EMPLOYMENT);
        assertThat(testCandidateProfile.getCertifications()).isEqualTo(UPDATED_CERTIFICATIONS);
        assertThat(testCandidateProfile.getExperience()).isEqualTo(UPDATED_EXPERIENCE);
    }

    @Test
    @Transactional
    void putNonExistingCandidateProfile() throws Exception {
        int databaseSizeBeforeUpdate = candidateProfileRepository.findAll().size();
        candidateProfile.setId(count.incrementAndGet());

        // Create the CandidateProfile
        CandidateProfileDTO candidateProfileDTO = candidateProfileMapper.toDto(candidateProfile);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateProfileMockMvc
            .perform(
                put(ENTITY_API_URL_ID, candidateProfileDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(candidateProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCandidateProfile() throws Exception {
        int databaseSizeBeforeUpdate = candidateProfileRepository.findAll().size();
        candidateProfile.setId(count.incrementAndGet());

        // Create the CandidateProfile
        CandidateProfileDTO candidateProfileDTO = candidateProfileMapper.toDto(candidateProfile);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateProfileMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(candidateProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCandidateProfile() throws Exception {
        int databaseSizeBeforeUpdate = candidateProfileRepository.findAll().size();
        candidateProfile.setId(count.incrementAndGet());

        // Create the CandidateProfile
        CandidateProfileDTO candidateProfileDTO = candidateProfileMapper.toDto(candidateProfile);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateProfileMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(candidateProfileDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCandidateProfileWithPatch() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        int databaseSizeBeforeUpdate = candidateProfileRepository.findAll().size();

        // Update the candidateProfile using partial update
        CandidateProfile partialUpdatedCandidateProfile = new CandidateProfile();
        partialUpdatedCandidateProfile.setId(candidateProfile.getId());

        partialUpdatedCandidateProfile
            .careerSummary(UPDATED_CAREER_SUMMARY)
            .education(UPDATED_EDUCATION)
            .certifications(UPDATED_CERTIFICATIONS)
            .experience(UPDATED_EXPERIENCE);

        restCandidateProfileMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCandidateProfile.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCandidateProfile))
            )
            .andExpect(status().isOk());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeUpdate);
        CandidateProfile testCandidateProfile = candidateProfileList.get(candidateProfileList.size() - 1);
        assertThat(testCandidateProfile.getAboutMe()).isEqualTo(DEFAULT_ABOUT_ME);
        assertThat(testCandidateProfile.getCareerSummary()).isEqualTo(UPDATED_CAREER_SUMMARY);
        assertThat(testCandidateProfile.getEducation()).isEqualTo(UPDATED_EDUCATION);
        assertThat(testCandidateProfile.getDob()).isEqualTo(DEFAULT_DOB);
        assertThat(testCandidateProfile.getCurrentEmployment()).isEqualTo(DEFAULT_CURRENT_EMPLOYMENT);
        assertThat(testCandidateProfile.getCertifications()).isEqualTo(UPDATED_CERTIFICATIONS);
        assertThat(testCandidateProfile.getExperience()).isEqualTo(UPDATED_EXPERIENCE);
    }

    @Test
    @Transactional
    void fullUpdateCandidateProfileWithPatch() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        int databaseSizeBeforeUpdate = candidateProfileRepository.findAll().size();

        // Update the candidateProfile using partial update
        CandidateProfile partialUpdatedCandidateProfile = new CandidateProfile();
        partialUpdatedCandidateProfile.setId(candidateProfile.getId());

        partialUpdatedCandidateProfile
            .aboutMe(UPDATED_ABOUT_ME)
            .careerSummary(UPDATED_CAREER_SUMMARY)
            .education(UPDATED_EDUCATION)
            .dob(UPDATED_DOB)
            .currentEmployment(UPDATED_CURRENT_EMPLOYMENT)
            .certifications(UPDATED_CERTIFICATIONS)
            .experience(UPDATED_EXPERIENCE);

        restCandidateProfileMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCandidateProfile.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCandidateProfile))
            )
            .andExpect(status().isOk());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeUpdate);
        CandidateProfile testCandidateProfile = candidateProfileList.get(candidateProfileList.size() - 1);
        assertThat(testCandidateProfile.getAboutMe()).isEqualTo(UPDATED_ABOUT_ME);
        assertThat(testCandidateProfile.getCareerSummary()).isEqualTo(UPDATED_CAREER_SUMMARY);
        assertThat(testCandidateProfile.getEducation()).isEqualTo(UPDATED_EDUCATION);
        assertThat(testCandidateProfile.getDob()).isEqualTo(UPDATED_DOB);
        assertThat(testCandidateProfile.getCurrentEmployment()).isEqualTo(UPDATED_CURRENT_EMPLOYMENT);
        assertThat(testCandidateProfile.getCertifications()).isEqualTo(UPDATED_CERTIFICATIONS);
        assertThat(testCandidateProfile.getExperience()).isEqualTo(UPDATED_EXPERIENCE);
    }

    @Test
    @Transactional
    void patchNonExistingCandidateProfile() throws Exception {
        int databaseSizeBeforeUpdate = candidateProfileRepository.findAll().size();
        candidateProfile.setId(count.incrementAndGet());

        // Create the CandidateProfile
        CandidateProfileDTO candidateProfileDTO = candidateProfileMapper.toDto(candidateProfile);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateProfileMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, candidateProfileDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(candidateProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCandidateProfile() throws Exception {
        int databaseSizeBeforeUpdate = candidateProfileRepository.findAll().size();
        candidateProfile.setId(count.incrementAndGet());

        // Create the CandidateProfile
        CandidateProfileDTO candidateProfileDTO = candidateProfileMapper.toDto(candidateProfile);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateProfileMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(candidateProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCandidateProfile() throws Exception {
        int databaseSizeBeforeUpdate = candidateProfileRepository.findAll().size();
        candidateProfile.setId(count.incrementAndGet());

        // Create the CandidateProfile
        CandidateProfileDTO candidateProfileDTO = candidateProfileMapper.toDto(candidateProfile);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateProfileMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(candidateProfileDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CandidateProfile in the database
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCandidateProfile() throws Exception {
        // Initialize the database
        candidateProfileRepository.saveAndFlush(candidateProfile);

        int databaseSizeBeforeDelete = candidateProfileRepository.findAll().size();

        // Delete the candidateProfile
        restCandidateProfileMockMvc
            .perform(delete(ENTITY_API_URL_ID, candidateProfile.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CandidateProfile> candidateProfileList = candidateProfileRepository.findAll();
        assertThat(candidateProfileList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
