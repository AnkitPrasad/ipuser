package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.domain.UserPermissionOnAttribute;
import com.user.myapp.domain.UserRolePermission;
import com.user.myapp.repository.RoleMasterRepository;
import com.user.myapp.service.criteria.RoleMasterCriteria;
import com.user.myapp.service.dto.RoleMasterDTO;
import com.user.myapp.service.mapper.RoleMasterMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RoleMasterResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RoleMasterResourceIT {

    private static final String DEFAULT_ROLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ROLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_CREATED_BY = 1L;
    private static final Long UPDATED_CREATED_BY = 2L;
    private static final Long SMALLER_CREATED_BY = 1L - 1L;

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_UPDATED_BY = 1L;
    private static final Long UPDATED_UPDATED_BY = 2L;
    private static final Long SMALLER_UPDATED_BY = 1L - 1L;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final String ENTITY_API_URL = "/api/role-masters";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RoleMasterRepository roleMasterRepository;

    @Autowired
    private RoleMasterMapper roleMasterMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRoleMasterMockMvc;

    private RoleMaster roleMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RoleMaster createEntity(EntityManager em) {
        RoleMaster roleMaster = new RoleMaster()
            .roleName(DEFAULT_ROLE_NAME)
            .description(DEFAULT_DESCRIPTION)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .status(DEFAULT_STATUS);
        return roleMaster;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RoleMaster createUpdatedEntity(EntityManager em) {
        RoleMaster roleMaster = new RoleMaster()
            .roleName(UPDATED_ROLE_NAME)
            .description(UPDATED_DESCRIPTION)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);
        return roleMaster;
    }

    @BeforeEach
    public void initTest() {
        roleMaster = createEntity(em);
    }

    @Test
    @Transactional
    void createRoleMaster() throws Exception {
        int databaseSizeBeforeCreate = roleMasterRepository.findAll().size();
        // Create the RoleMaster
        RoleMasterDTO roleMasterDTO = roleMasterMapper.toDto(roleMaster);
        restRoleMasterMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(roleMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeCreate + 1);
        RoleMaster testRoleMaster = roleMasterList.get(roleMasterList.size() - 1);
        assertThat(testRoleMaster.getRoleName()).isEqualTo(DEFAULT_ROLE_NAME);
        assertThat(testRoleMaster.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRoleMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testRoleMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testRoleMaster.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testRoleMaster.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testRoleMaster.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createRoleMasterWithExistingId() throws Exception {
        // Create the RoleMaster with an existing ID
        roleMaster.setId(1L);
        RoleMasterDTO roleMasterDTO = roleMasterMapper.toDto(roleMaster);

        int databaseSizeBeforeCreate = roleMasterRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoleMasterMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(roleMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllRoleMasters() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList
        restRoleMasterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roleMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].roleName").value(hasItem(DEFAULT_ROLE_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }

    @Test
    @Transactional
    void getRoleMaster() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get the roleMaster
        restRoleMasterMockMvc
            .perform(get(ENTITY_API_URL_ID, roleMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(roleMaster.getId().intValue()))
            .andExpect(jsonPath("$.roleName").value(DEFAULT_ROLE_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.intValue()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    void getRoleMastersByIdFiltering() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        Long id = roleMaster.getId();

        defaultRoleMasterShouldBeFound("id.equals=" + id);
        defaultRoleMasterShouldNotBeFound("id.notEquals=" + id);

        defaultRoleMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultRoleMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultRoleMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultRoleMasterShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllRoleMastersByRoleNameIsEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where roleName equals to DEFAULT_ROLE_NAME
        defaultRoleMasterShouldBeFound("roleName.equals=" + DEFAULT_ROLE_NAME);

        // Get all the roleMasterList where roleName equals to UPDATED_ROLE_NAME
        defaultRoleMasterShouldNotBeFound("roleName.equals=" + UPDATED_ROLE_NAME);
    }

    @Test
    @Transactional
    void getAllRoleMastersByRoleNameIsInShouldWork() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where roleName in DEFAULT_ROLE_NAME or UPDATED_ROLE_NAME
        defaultRoleMasterShouldBeFound("roleName.in=" + DEFAULT_ROLE_NAME + "," + UPDATED_ROLE_NAME);

        // Get all the roleMasterList where roleName equals to UPDATED_ROLE_NAME
        defaultRoleMasterShouldNotBeFound("roleName.in=" + UPDATED_ROLE_NAME);
    }

    @Test
    @Transactional
    void getAllRoleMastersByRoleNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where roleName is not null
        defaultRoleMasterShouldBeFound("roleName.specified=true");

        // Get all the roleMasterList where roleName is null
        defaultRoleMasterShouldNotBeFound("roleName.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleMastersByRoleNameContainsSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where roleName contains DEFAULT_ROLE_NAME
        defaultRoleMasterShouldBeFound("roleName.contains=" + DEFAULT_ROLE_NAME);

        // Get all the roleMasterList where roleName contains UPDATED_ROLE_NAME
        defaultRoleMasterShouldNotBeFound("roleName.contains=" + UPDATED_ROLE_NAME);
    }

    @Test
    @Transactional
    void getAllRoleMastersByRoleNameNotContainsSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where roleName does not contain DEFAULT_ROLE_NAME
        defaultRoleMasterShouldNotBeFound("roleName.doesNotContain=" + DEFAULT_ROLE_NAME);

        // Get all the roleMasterList where roleName does not contain UPDATED_ROLE_NAME
        defaultRoleMasterShouldBeFound("roleName.doesNotContain=" + UPDATED_ROLE_NAME);
    }

    @Test
    @Transactional
    void getAllRoleMastersByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where description equals to DEFAULT_DESCRIPTION
        defaultRoleMasterShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the roleMasterList where description equals to UPDATED_DESCRIPTION
        defaultRoleMasterShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRoleMastersByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultRoleMasterShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the roleMasterList where description equals to UPDATED_DESCRIPTION
        defaultRoleMasterShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRoleMastersByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where description is not null
        defaultRoleMasterShouldBeFound("description.specified=true");

        // Get all the roleMasterList where description is null
        defaultRoleMasterShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleMastersByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where description contains DEFAULT_DESCRIPTION
        defaultRoleMasterShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the roleMasterList where description contains UPDATED_DESCRIPTION
        defaultRoleMasterShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRoleMastersByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where description does not contain DEFAULT_DESCRIPTION
        defaultRoleMasterShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the roleMasterList where description does not contain UPDATED_DESCRIPTION
        defaultRoleMasterShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultRoleMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the roleMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultRoleMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultRoleMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the roleMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultRoleMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdDate is not null
        defaultRoleMasterShouldBeFound("createdDate.specified=true");

        // Get all the roleMasterList where createdDate is null
        defaultRoleMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultRoleMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the roleMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultRoleMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultRoleMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the roleMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultRoleMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdBy is not null
        defaultRoleMasterShouldBeFound("createdBy.specified=true");

        // Get all the roleMasterList where createdBy is null
        defaultRoleMasterShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdBy is greater than or equal to DEFAULT_CREATED_BY
        defaultRoleMasterShouldBeFound("createdBy.greaterThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the roleMasterList where createdBy is greater than or equal to UPDATED_CREATED_BY
        defaultRoleMasterShouldNotBeFound("createdBy.greaterThanOrEqual=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdBy is less than or equal to DEFAULT_CREATED_BY
        defaultRoleMasterShouldBeFound("createdBy.lessThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the roleMasterList where createdBy is less than or equal to SMALLER_CREATED_BY
        defaultRoleMasterShouldNotBeFound("createdBy.lessThanOrEqual=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdBy is less than DEFAULT_CREATED_BY
        defaultRoleMasterShouldNotBeFound("createdBy.lessThan=" + DEFAULT_CREATED_BY);

        // Get all the roleMasterList where createdBy is less than UPDATED_CREATED_BY
        defaultRoleMasterShouldBeFound("createdBy.lessThan=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByCreatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where createdBy is greater than DEFAULT_CREATED_BY
        defaultRoleMasterShouldNotBeFound("createdBy.greaterThan=" + DEFAULT_CREATED_BY);

        // Get all the roleMasterList where createdBy is greater than SMALLER_CREATED_BY
        defaultRoleMasterShouldBeFound("createdBy.greaterThan=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultRoleMasterShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the roleMasterList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultRoleMasterShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultRoleMasterShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the roleMasterList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultRoleMasterShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedDate is not null
        defaultRoleMasterShouldBeFound("updatedDate.specified=true");

        // Get all the roleMasterList where updatedDate is null
        defaultRoleMasterShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultRoleMasterShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the roleMasterList where updatedBy equals to UPDATED_UPDATED_BY
        defaultRoleMasterShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultRoleMasterShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the roleMasterList where updatedBy equals to UPDATED_UPDATED_BY
        defaultRoleMasterShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedBy is not null
        defaultRoleMasterShouldBeFound("updatedBy.specified=true");

        // Get all the roleMasterList where updatedBy is null
        defaultRoleMasterShouldNotBeFound("updatedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedBy is greater than or equal to DEFAULT_UPDATED_BY
        defaultRoleMasterShouldBeFound("updatedBy.greaterThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the roleMasterList where updatedBy is greater than or equal to UPDATED_UPDATED_BY
        defaultRoleMasterShouldNotBeFound("updatedBy.greaterThanOrEqual=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedBy is less than or equal to DEFAULT_UPDATED_BY
        defaultRoleMasterShouldBeFound("updatedBy.lessThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the roleMasterList where updatedBy is less than or equal to SMALLER_UPDATED_BY
        defaultRoleMasterShouldNotBeFound("updatedBy.lessThanOrEqual=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedBy is less than DEFAULT_UPDATED_BY
        defaultRoleMasterShouldNotBeFound("updatedBy.lessThan=" + DEFAULT_UPDATED_BY);

        // Get all the roleMasterList where updatedBy is less than UPDATED_UPDATED_BY
        defaultRoleMasterShouldBeFound("updatedBy.lessThan=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByUpdatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where updatedBy is greater than DEFAULT_UPDATED_BY
        defaultRoleMasterShouldNotBeFound("updatedBy.greaterThan=" + DEFAULT_UPDATED_BY);

        // Get all the roleMasterList where updatedBy is greater than SMALLER_UPDATED_BY
        defaultRoleMasterShouldBeFound("updatedBy.greaterThan=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllRoleMastersByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where status equals to DEFAULT_STATUS
        defaultRoleMasterShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the roleMasterList where status equals to UPDATED_STATUS
        defaultRoleMasterShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllRoleMastersByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultRoleMasterShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the roleMasterList where status equals to UPDATED_STATUS
        defaultRoleMasterShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllRoleMastersByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        // Get all the roleMasterList where status is not null
        defaultRoleMasterShouldBeFound("status.specified=true");

        // Get all the roleMasterList where status is null
        defaultRoleMasterShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleMastersByUserPermissionOnAttributeIsEqualToSomething() throws Exception {
        UserPermissionOnAttribute userPermissionOnAttribute;
        if (TestUtil.findAll(em, UserPermissionOnAttribute.class).isEmpty()) {
            roleMasterRepository.saveAndFlush(roleMaster);
            userPermissionOnAttribute = UserPermissionOnAttributeResourceIT.createEntity(em);
        } else {
            userPermissionOnAttribute = TestUtil.findAll(em, UserPermissionOnAttribute.class).get(0);
        }
        em.persist(userPermissionOnAttribute);
        em.flush();
        roleMaster.addUserPermissionOnAttribute(userPermissionOnAttribute);
        roleMasterRepository.saveAndFlush(roleMaster);
        Long userPermissionOnAttributeId = userPermissionOnAttribute.getId();

        // Get all the roleMasterList where userPermissionOnAttribute equals to userPermissionOnAttributeId
        defaultRoleMasterShouldBeFound("userPermissionOnAttributeId.equals=" + userPermissionOnAttributeId);

        // Get all the roleMasterList where userPermissionOnAttribute equals to (userPermissionOnAttributeId + 1)
        defaultRoleMasterShouldNotBeFound("userPermissionOnAttributeId.equals=" + (userPermissionOnAttributeId + 1));
    }

    @Test
    @Transactional
    void getAllRoleMastersByUserRolePermissionIsEqualToSomething() throws Exception {
        UserRolePermission userRolePermission;
        if (TestUtil.findAll(em, UserRolePermission.class).isEmpty()) {
            roleMasterRepository.saveAndFlush(roleMaster);
            userRolePermission = UserRolePermissionResourceIT.createEntity(em);
        } else {
            userRolePermission = TestUtil.findAll(em, UserRolePermission.class).get(0);
        }
        em.persist(userRolePermission);
        em.flush();
        roleMaster.addUserRolePermission(userRolePermission);
        roleMasterRepository.saveAndFlush(roleMaster);
        Long userRolePermissionId = userRolePermission.getId();

        // Get all the roleMasterList where userRolePermission equals to userRolePermissionId
        defaultRoleMasterShouldBeFound("userRolePermissionId.equals=" + userRolePermissionId);

        // Get all the roleMasterList where userRolePermission equals to (userRolePermissionId + 1)
        defaultRoleMasterShouldNotBeFound("userRolePermissionId.equals=" + (userRolePermissionId + 1));
    }

    @Test
    @Transactional
    void getAllRoleMastersByInPreferenceUserIsEqualToSomething() throws Exception {
        InPreferenceUser inPreferenceUser;
        if (TestUtil.findAll(em, InPreferenceUser.class).isEmpty()) {
            roleMasterRepository.saveAndFlush(roleMaster);
            inPreferenceUser = InPreferenceUserResourceIT.createEntity(em);
        } else {
            inPreferenceUser = TestUtil.findAll(em, InPreferenceUser.class).get(0);
        }
        em.persist(inPreferenceUser);
        em.flush();
        roleMaster.setInPreferenceUser(inPreferenceUser);
        roleMasterRepository.saveAndFlush(roleMaster);
        Long inPreferenceUserId = inPreferenceUser.getId();

        // Get all the roleMasterList where inPreferenceUser equals to inPreferenceUserId
        defaultRoleMasterShouldBeFound("inPreferenceUserId.equals=" + inPreferenceUserId);

        // Get all the roleMasterList where inPreferenceUser equals to (inPreferenceUserId + 1)
        defaultRoleMasterShouldNotBeFound("inPreferenceUserId.equals=" + (inPreferenceUserId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRoleMasterShouldBeFound(String filter) throws Exception {
        restRoleMasterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roleMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].roleName").value(hasItem(DEFAULT_ROLE_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));

        // Check, that the count call also returns 1
        restRoleMasterMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRoleMasterShouldNotBeFound(String filter) throws Exception {
        restRoleMasterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRoleMasterMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingRoleMaster() throws Exception {
        // Get the roleMaster
        restRoleMasterMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingRoleMaster() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        int databaseSizeBeforeUpdate = roleMasterRepository.findAll().size();

        // Update the roleMaster
        RoleMaster updatedRoleMaster = roleMasterRepository.findById(roleMaster.getId()).get();
        // Disconnect from session so that the updates on updatedRoleMaster are not directly saved in db
        em.detach(updatedRoleMaster);
        updatedRoleMaster
            .roleName(UPDATED_ROLE_NAME)
            .description(UPDATED_DESCRIPTION)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);
        RoleMasterDTO roleMasterDTO = roleMasterMapper.toDto(updatedRoleMaster);

        restRoleMasterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, roleMasterDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(roleMasterDTO))
            )
            .andExpect(status().isOk());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeUpdate);
        RoleMaster testRoleMaster = roleMasterList.get(roleMasterList.size() - 1);
        assertThat(testRoleMaster.getRoleName()).isEqualTo(UPDATED_ROLE_NAME);
        assertThat(testRoleMaster.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRoleMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testRoleMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testRoleMaster.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testRoleMaster.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testRoleMaster.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingRoleMaster() throws Exception {
        int databaseSizeBeforeUpdate = roleMasterRepository.findAll().size();
        roleMaster.setId(count.incrementAndGet());

        // Create the RoleMaster
        RoleMasterDTO roleMasterDTO = roleMasterMapper.toDto(roleMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRoleMasterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, roleMasterDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(roleMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRoleMaster() throws Exception {
        int databaseSizeBeforeUpdate = roleMasterRepository.findAll().size();
        roleMaster.setId(count.incrementAndGet());

        // Create the RoleMaster
        RoleMasterDTO roleMasterDTO = roleMasterMapper.toDto(roleMaster);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRoleMasterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(roleMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRoleMaster() throws Exception {
        int databaseSizeBeforeUpdate = roleMasterRepository.findAll().size();
        roleMaster.setId(count.incrementAndGet());

        // Create the RoleMaster
        RoleMasterDTO roleMasterDTO = roleMasterMapper.toDto(roleMaster);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRoleMasterMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(roleMasterDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRoleMasterWithPatch() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        int databaseSizeBeforeUpdate = roleMasterRepository.findAll().size();

        // Update the roleMaster using partial update
        RoleMaster partialUpdatedRoleMaster = new RoleMaster();
        partialUpdatedRoleMaster.setId(roleMaster.getId());

        partialUpdatedRoleMaster
            .roleName(UPDATED_ROLE_NAME)
            .description(UPDATED_DESCRIPTION)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restRoleMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRoleMaster.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRoleMaster))
            )
            .andExpect(status().isOk());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeUpdate);
        RoleMaster testRoleMaster = roleMasterList.get(roleMasterList.size() - 1);
        assertThat(testRoleMaster.getRoleName()).isEqualTo(UPDATED_ROLE_NAME);
        assertThat(testRoleMaster.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRoleMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testRoleMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testRoleMaster.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testRoleMaster.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testRoleMaster.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateRoleMasterWithPatch() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        int databaseSizeBeforeUpdate = roleMasterRepository.findAll().size();

        // Update the roleMaster using partial update
        RoleMaster partialUpdatedRoleMaster = new RoleMaster();
        partialUpdatedRoleMaster.setId(roleMaster.getId());

        partialUpdatedRoleMaster
            .roleName(UPDATED_ROLE_NAME)
            .description(UPDATED_DESCRIPTION)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);

        restRoleMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRoleMaster.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRoleMaster))
            )
            .andExpect(status().isOk());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeUpdate);
        RoleMaster testRoleMaster = roleMasterList.get(roleMasterList.size() - 1);
        assertThat(testRoleMaster.getRoleName()).isEqualTo(UPDATED_ROLE_NAME);
        assertThat(testRoleMaster.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRoleMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testRoleMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testRoleMaster.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testRoleMaster.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testRoleMaster.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingRoleMaster() throws Exception {
        int databaseSizeBeforeUpdate = roleMasterRepository.findAll().size();
        roleMaster.setId(count.incrementAndGet());

        // Create the RoleMaster
        RoleMasterDTO roleMasterDTO = roleMasterMapper.toDto(roleMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRoleMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, roleMasterDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(roleMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRoleMaster() throws Exception {
        int databaseSizeBeforeUpdate = roleMasterRepository.findAll().size();
        roleMaster.setId(count.incrementAndGet());

        // Create the RoleMaster
        RoleMasterDTO roleMasterDTO = roleMasterMapper.toDto(roleMaster);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRoleMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(roleMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRoleMaster() throws Exception {
        int databaseSizeBeforeUpdate = roleMasterRepository.findAll().size();
        roleMaster.setId(count.incrementAndGet());

        // Create the RoleMaster
        RoleMasterDTO roleMasterDTO = roleMasterMapper.toDto(roleMaster);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRoleMasterMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(roleMasterDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RoleMaster in the database
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRoleMaster() throws Exception {
        // Initialize the database
        roleMasterRepository.saveAndFlush(roleMaster);

        int databaseSizeBeforeDelete = roleMasterRepository.findAll().size();

        // Delete the roleMaster
        restRoleMasterMockMvc
            .perform(delete(ENTITY_API_URL_ID, roleMaster.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RoleMaster> roleMasterList = roleMasterRepository.findAll();
        assertThat(roleMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
