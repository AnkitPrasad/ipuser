package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.PermissionMaster;
import com.user.myapp.domain.UserRolePermission;
import com.user.myapp.repository.PermissionMasterRepository;
import com.user.myapp.service.criteria.PermissionMasterCriteria;
import com.user.myapp.service.dto.PermissionMasterDTO;
import com.user.myapp.service.mapper.PermissionMasterMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PermissionMasterResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PermissionMasterResourceIT {

    private static final String DEFAULT_PAGE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PAGE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PAGE_URL = "AAAAAAAAAA";
    private static final String UPDATED_PAGE_URL = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/permission-masters";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PermissionMasterRepository permissionMasterRepository;

    @Autowired
    private PermissionMasterMapper permissionMasterMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPermissionMasterMockMvc;

    private PermissionMaster permissionMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PermissionMaster createEntity(EntityManager em) {
        PermissionMaster permissionMaster = new PermissionMaster().pageName(DEFAULT_PAGE_NAME).pageUrl(DEFAULT_PAGE_URL);
        return permissionMaster;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PermissionMaster createUpdatedEntity(EntityManager em) {
        PermissionMaster permissionMaster = new PermissionMaster().pageName(UPDATED_PAGE_NAME).pageUrl(UPDATED_PAGE_URL);
        return permissionMaster;
    }

    @BeforeEach
    public void initTest() {
        permissionMaster = createEntity(em);
    }

    @Test
    @Transactional
    void createPermissionMaster() throws Exception {
        int databaseSizeBeforeCreate = permissionMasterRepository.findAll().size();
        // Create the PermissionMaster
        PermissionMasterDTO permissionMasterDTO = permissionMasterMapper.toDto(permissionMaster);
        restPermissionMasterMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(permissionMasterDTO))
            )
            .andExpect(status().isCreated());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeCreate + 1);
        PermissionMaster testPermissionMaster = permissionMasterList.get(permissionMasterList.size() - 1);
        assertThat(testPermissionMaster.getPageName()).isEqualTo(DEFAULT_PAGE_NAME);
        assertThat(testPermissionMaster.getPageUrl()).isEqualTo(DEFAULT_PAGE_URL);
    }

    @Test
    @Transactional
    void createPermissionMasterWithExistingId() throws Exception {
        // Create the PermissionMaster with an existing ID
        permissionMaster.setId(1L);
        PermissionMasterDTO permissionMasterDTO = permissionMasterMapper.toDto(permissionMaster);

        int databaseSizeBeforeCreate = permissionMasterRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPermissionMasterMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(permissionMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPermissionMasters() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList
        restPermissionMasterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(permissionMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].pageName").value(hasItem(DEFAULT_PAGE_NAME)))
            .andExpect(jsonPath("$.[*].pageUrl").value(hasItem(DEFAULT_PAGE_URL)));
    }

    @Test
    @Transactional
    void getPermissionMaster() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get the permissionMaster
        restPermissionMasterMockMvc
            .perform(get(ENTITY_API_URL_ID, permissionMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(permissionMaster.getId().intValue()))
            .andExpect(jsonPath("$.pageName").value(DEFAULT_PAGE_NAME))
            .andExpect(jsonPath("$.pageUrl").value(DEFAULT_PAGE_URL));
    }

    @Test
    @Transactional
    void getPermissionMastersByIdFiltering() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        Long id = permissionMaster.getId();

        defaultPermissionMasterShouldBeFound("id.equals=" + id);
        defaultPermissionMasterShouldNotBeFound("id.notEquals=" + id);

        defaultPermissionMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPermissionMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultPermissionMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPermissionMasterShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageNameIsEqualToSomething() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageName equals to DEFAULT_PAGE_NAME
        defaultPermissionMasterShouldBeFound("pageName.equals=" + DEFAULT_PAGE_NAME);

        // Get all the permissionMasterList where pageName equals to UPDATED_PAGE_NAME
        defaultPermissionMasterShouldNotBeFound("pageName.equals=" + UPDATED_PAGE_NAME);
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageNameIsInShouldWork() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageName in DEFAULT_PAGE_NAME or UPDATED_PAGE_NAME
        defaultPermissionMasterShouldBeFound("pageName.in=" + DEFAULT_PAGE_NAME + "," + UPDATED_PAGE_NAME);

        // Get all the permissionMasterList where pageName equals to UPDATED_PAGE_NAME
        defaultPermissionMasterShouldNotBeFound("pageName.in=" + UPDATED_PAGE_NAME);
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageName is not null
        defaultPermissionMasterShouldBeFound("pageName.specified=true");

        // Get all the permissionMasterList where pageName is null
        defaultPermissionMasterShouldNotBeFound("pageName.specified=false");
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageNameContainsSomething() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageName contains DEFAULT_PAGE_NAME
        defaultPermissionMasterShouldBeFound("pageName.contains=" + DEFAULT_PAGE_NAME);

        // Get all the permissionMasterList where pageName contains UPDATED_PAGE_NAME
        defaultPermissionMasterShouldNotBeFound("pageName.contains=" + UPDATED_PAGE_NAME);
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageNameNotContainsSomething() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageName does not contain DEFAULT_PAGE_NAME
        defaultPermissionMasterShouldNotBeFound("pageName.doesNotContain=" + DEFAULT_PAGE_NAME);

        // Get all the permissionMasterList where pageName does not contain UPDATED_PAGE_NAME
        defaultPermissionMasterShouldBeFound("pageName.doesNotContain=" + UPDATED_PAGE_NAME);
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageUrl equals to DEFAULT_PAGE_URL
        defaultPermissionMasterShouldBeFound("pageUrl.equals=" + DEFAULT_PAGE_URL);

        // Get all the permissionMasterList where pageUrl equals to UPDATED_PAGE_URL
        defaultPermissionMasterShouldNotBeFound("pageUrl.equals=" + UPDATED_PAGE_URL);
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageUrlIsInShouldWork() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageUrl in DEFAULT_PAGE_URL or UPDATED_PAGE_URL
        defaultPermissionMasterShouldBeFound("pageUrl.in=" + DEFAULT_PAGE_URL + "," + UPDATED_PAGE_URL);

        // Get all the permissionMasterList where pageUrl equals to UPDATED_PAGE_URL
        defaultPermissionMasterShouldNotBeFound("pageUrl.in=" + UPDATED_PAGE_URL);
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageUrl is not null
        defaultPermissionMasterShouldBeFound("pageUrl.specified=true");

        // Get all the permissionMasterList where pageUrl is null
        defaultPermissionMasterShouldNotBeFound("pageUrl.specified=false");
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageUrlContainsSomething() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageUrl contains DEFAULT_PAGE_URL
        defaultPermissionMasterShouldBeFound("pageUrl.contains=" + DEFAULT_PAGE_URL);

        // Get all the permissionMasterList where pageUrl contains UPDATED_PAGE_URL
        defaultPermissionMasterShouldNotBeFound("pageUrl.contains=" + UPDATED_PAGE_URL);
    }

    @Test
    @Transactional
    void getAllPermissionMastersByPageUrlNotContainsSomething() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        // Get all the permissionMasterList where pageUrl does not contain DEFAULT_PAGE_URL
        defaultPermissionMasterShouldNotBeFound("pageUrl.doesNotContain=" + DEFAULT_PAGE_URL);

        // Get all the permissionMasterList where pageUrl does not contain UPDATED_PAGE_URL
        defaultPermissionMasterShouldBeFound("pageUrl.doesNotContain=" + UPDATED_PAGE_URL);
    }

    @Test
    @Transactional
    void getAllPermissionMastersByUserRolePermissionIsEqualToSomething() throws Exception {
        UserRolePermission userRolePermission;
        if (TestUtil.findAll(em, UserRolePermission.class).isEmpty()) {
            permissionMasterRepository.saveAndFlush(permissionMaster);
            userRolePermission = UserRolePermissionResourceIT.createEntity(em);
        } else {
            userRolePermission = TestUtil.findAll(em, UserRolePermission.class).get(0);
        }
        em.persist(userRolePermission);
        em.flush();
        permissionMaster.addUserRolePermission(userRolePermission);
        permissionMasterRepository.saveAndFlush(permissionMaster);
        Long userRolePermissionId = userRolePermission.getId();

        // Get all the permissionMasterList where userRolePermission equals to userRolePermissionId
        defaultPermissionMasterShouldBeFound("userRolePermissionId.equals=" + userRolePermissionId);

        // Get all the permissionMasterList where userRolePermission equals to (userRolePermissionId + 1)
        defaultPermissionMasterShouldNotBeFound("userRolePermissionId.equals=" + (userRolePermissionId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPermissionMasterShouldBeFound(String filter) throws Exception {
        restPermissionMasterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(permissionMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].pageName").value(hasItem(DEFAULT_PAGE_NAME)))
            .andExpect(jsonPath("$.[*].pageUrl").value(hasItem(DEFAULT_PAGE_URL)));

        // Check, that the count call also returns 1
        restPermissionMasterMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPermissionMasterShouldNotBeFound(String filter) throws Exception {
        restPermissionMasterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPermissionMasterMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPermissionMaster() throws Exception {
        // Get the permissionMaster
        restPermissionMasterMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPermissionMaster() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        int databaseSizeBeforeUpdate = permissionMasterRepository.findAll().size();

        // Update the permissionMaster
        PermissionMaster updatedPermissionMaster = permissionMasterRepository.findById(permissionMaster.getId()).get();
        // Disconnect from session so that the updates on updatedPermissionMaster are not directly saved in db
        em.detach(updatedPermissionMaster);
        updatedPermissionMaster.pageName(UPDATED_PAGE_NAME).pageUrl(UPDATED_PAGE_URL);
        PermissionMasterDTO permissionMasterDTO = permissionMasterMapper.toDto(updatedPermissionMaster);

        restPermissionMasterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, permissionMasterDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(permissionMasterDTO))
            )
            .andExpect(status().isOk());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeUpdate);
        PermissionMaster testPermissionMaster = permissionMasterList.get(permissionMasterList.size() - 1);
        assertThat(testPermissionMaster.getPageName()).isEqualTo(UPDATED_PAGE_NAME);
        assertThat(testPermissionMaster.getPageUrl()).isEqualTo(UPDATED_PAGE_URL);
    }

    @Test
    @Transactional
    void putNonExistingPermissionMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionMasterRepository.findAll().size();
        permissionMaster.setId(count.incrementAndGet());

        // Create the PermissionMaster
        PermissionMasterDTO permissionMasterDTO = permissionMasterMapper.toDto(permissionMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPermissionMasterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, permissionMasterDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(permissionMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPermissionMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionMasterRepository.findAll().size();
        permissionMaster.setId(count.incrementAndGet());

        // Create the PermissionMaster
        PermissionMasterDTO permissionMasterDTO = permissionMasterMapper.toDto(permissionMaster);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPermissionMasterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(permissionMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPermissionMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionMasterRepository.findAll().size();
        permissionMaster.setId(count.incrementAndGet());

        // Create the PermissionMaster
        PermissionMasterDTO permissionMasterDTO = permissionMasterMapper.toDto(permissionMaster);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPermissionMasterMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(permissionMasterDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePermissionMasterWithPatch() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        int databaseSizeBeforeUpdate = permissionMasterRepository.findAll().size();

        // Update the permissionMaster using partial update
        PermissionMaster partialUpdatedPermissionMaster = new PermissionMaster();
        partialUpdatedPermissionMaster.setId(permissionMaster.getId());

        restPermissionMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPermissionMaster.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPermissionMaster))
            )
            .andExpect(status().isOk());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeUpdate);
        PermissionMaster testPermissionMaster = permissionMasterList.get(permissionMasterList.size() - 1);
        assertThat(testPermissionMaster.getPageName()).isEqualTo(DEFAULT_PAGE_NAME);
        assertThat(testPermissionMaster.getPageUrl()).isEqualTo(DEFAULT_PAGE_URL);
    }

    @Test
    @Transactional
    void fullUpdatePermissionMasterWithPatch() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        int databaseSizeBeforeUpdate = permissionMasterRepository.findAll().size();

        // Update the permissionMaster using partial update
        PermissionMaster partialUpdatedPermissionMaster = new PermissionMaster();
        partialUpdatedPermissionMaster.setId(permissionMaster.getId());

        partialUpdatedPermissionMaster.pageName(UPDATED_PAGE_NAME).pageUrl(UPDATED_PAGE_URL);

        restPermissionMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPermissionMaster.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPermissionMaster))
            )
            .andExpect(status().isOk());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeUpdate);
        PermissionMaster testPermissionMaster = permissionMasterList.get(permissionMasterList.size() - 1);
        assertThat(testPermissionMaster.getPageName()).isEqualTo(UPDATED_PAGE_NAME);
        assertThat(testPermissionMaster.getPageUrl()).isEqualTo(UPDATED_PAGE_URL);
    }

    @Test
    @Transactional
    void patchNonExistingPermissionMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionMasterRepository.findAll().size();
        permissionMaster.setId(count.incrementAndGet());

        // Create the PermissionMaster
        PermissionMasterDTO permissionMasterDTO = permissionMasterMapper.toDto(permissionMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPermissionMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, permissionMasterDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(permissionMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPermissionMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionMasterRepository.findAll().size();
        permissionMaster.setId(count.incrementAndGet());

        // Create the PermissionMaster
        PermissionMasterDTO permissionMasterDTO = permissionMasterMapper.toDto(permissionMaster);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPermissionMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(permissionMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPermissionMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionMasterRepository.findAll().size();
        permissionMaster.setId(count.incrementAndGet());

        // Create the PermissionMaster
        PermissionMasterDTO permissionMasterDTO = permissionMasterMapper.toDto(permissionMaster);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPermissionMasterMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(permissionMasterDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PermissionMaster in the database
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePermissionMaster() throws Exception {
        // Initialize the database
        permissionMasterRepository.saveAndFlush(permissionMaster);

        int databaseSizeBeforeDelete = permissionMasterRepository.findAll().size();

        // Delete the permissionMaster
        restPermissionMasterMockMvc
            .perform(delete(ENTITY_API_URL_ID, permissionMaster.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PermissionMaster> permissionMasterList = permissionMasterRepository.findAll();
        assertThat(permissionMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
