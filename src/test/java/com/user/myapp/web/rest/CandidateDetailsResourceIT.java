package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.CandidateDetails;
import com.user.myapp.repository.CandidateDetailsRepository;
import com.user.myapp.service.criteria.CandidateDetailsCriteria;
import com.user.myapp.service.dto.CandidateDetailsDTO;
import com.user.myapp.service.mapper.CandidateDetailsMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CandidateDetailsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CandidateDetailsResourceIT {

    private static final String DEFAULT_CANDIDATE_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_CANDIDATE_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_BUSINESS_UNITS = "AAAAAAAAAA";
    private static final String UPDATED_BUSINESS_UNITS = "BBBBBBBBBB";

    private static final String DEFAULT_LEGAL_ENTITY = "AAAAAAAAAA";
    private static final String UPDATED_LEGAL_ENTITY = "BBBBBBBBBB";

    private static final String DEFAULT_CANDIDATE_SUB_FUNCTION = "AAAAAAAAAA";
    private static final String UPDATED_CANDIDATE_SUB_FUNCTION = "BBBBBBBBBB";

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final String DEFAULT_JOB_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_JOB_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_BUSINESS_UNITS = "AAAAAAAAAA";
    private static final String UPDATED_SUB_BUSINESS_UNITS = "BBBBBBBBBB";

    private static final String DEFAULT_CANDIDATE_FUNCTION = "AAAAAAAAAA";
    private static final String UPDATED_CANDIDATE_FUNCTION = "BBBBBBBBBB";

    private static final String DEFAULT_WORK_LEVEL = "AAAAAAAAAA";
    private static final String UPDATED_WORK_LEVEL = "BBBBBBBBBB";

    private static final String DEFAULT_CANDIDATE_GRADE = "AAAAAAAAAA";
    private static final String UPDATED_CANDIDATE_GRADE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/candidate-details";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CandidateDetailsRepository candidateDetailsRepository;

    @Autowired
    private CandidateDetailsMapper candidateDetailsMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCandidateDetailsMockMvc;

    private CandidateDetails candidateDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CandidateDetails createEntity(EntityManager em) {
        CandidateDetails candidateDetails = new CandidateDetails()
            .candidateCountry(DEFAULT_CANDIDATE_COUNTRY)
            .businessUnits(DEFAULT_BUSINESS_UNITS)
            .legalEntity(DEFAULT_LEGAL_ENTITY)
            .candidateSubFunction(DEFAULT_CANDIDATE_SUB_FUNCTION)
            .designation(DEFAULT_DESIGNATION)
            .jobTitle(DEFAULT_JOB_TITLE)
            .location(DEFAULT_LOCATION)
            .subBusinessUnits(DEFAULT_SUB_BUSINESS_UNITS)
            .candidateFunction(DEFAULT_CANDIDATE_FUNCTION)
            .workLevel(DEFAULT_WORK_LEVEL)
            .candidateGrade(DEFAULT_CANDIDATE_GRADE);
        return candidateDetails;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CandidateDetails createUpdatedEntity(EntityManager em) {
        CandidateDetails candidateDetails = new CandidateDetails()
            .candidateCountry(UPDATED_CANDIDATE_COUNTRY)
            .businessUnits(UPDATED_BUSINESS_UNITS)
            .legalEntity(UPDATED_LEGAL_ENTITY)
            .candidateSubFunction(UPDATED_CANDIDATE_SUB_FUNCTION)
            .designation(UPDATED_DESIGNATION)
            .jobTitle(UPDATED_JOB_TITLE)
            .location(UPDATED_LOCATION)
            .subBusinessUnits(UPDATED_SUB_BUSINESS_UNITS)
            .candidateFunction(UPDATED_CANDIDATE_FUNCTION)
            .workLevel(UPDATED_WORK_LEVEL)
            .candidateGrade(UPDATED_CANDIDATE_GRADE);
        return candidateDetails;
    }

    @BeforeEach
    public void initTest() {
        candidateDetails = createEntity(em);
    }

    @Test
    @Transactional
    void createCandidateDetails() throws Exception {
        int databaseSizeBeforeCreate = candidateDetailsRepository.findAll().size();
        // Create the CandidateDetails
        CandidateDetailsDTO candidateDetailsDTO = candidateDetailsMapper.toDto(candidateDetails);
        restCandidateDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(candidateDetailsDTO))
            )
            .andExpect(status().isCreated());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        CandidateDetails testCandidateDetails = candidateDetailsList.get(candidateDetailsList.size() - 1);
        assertThat(testCandidateDetails.getCandidateCountry()).isEqualTo(DEFAULT_CANDIDATE_COUNTRY);
        assertThat(testCandidateDetails.getBusinessUnits()).isEqualTo(DEFAULT_BUSINESS_UNITS);
        assertThat(testCandidateDetails.getLegalEntity()).isEqualTo(DEFAULT_LEGAL_ENTITY);
        assertThat(testCandidateDetails.getCandidateSubFunction()).isEqualTo(DEFAULT_CANDIDATE_SUB_FUNCTION);
        assertThat(testCandidateDetails.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testCandidateDetails.getJobTitle()).isEqualTo(DEFAULT_JOB_TITLE);
        assertThat(testCandidateDetails.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testCandidateDetails.getSubBusinessUnits()).isEqualTo(DEFAULT_SUB_BUSINESS_UNITS);
        assertThat(testCandidateDetails.getCandidateFunction()).isEqualTo(DEFAULT_CANDIDATE_FUNCTION);
        assertThat(testCandidateDetails.getWorkLevel()).isEqualTo(DEFAULT_WORK_LEVEL);
        assertThat(testCandidateDetails.getCandidateGrade()).isEqualTo(DEFAULT_CANDIDATE_GRADE);
    }

    @Test
    @Transactional
    void createCandidateDetailsWithExistingId() throws Exception {
        // Create the CandidateDetails with an existing ID
        candidateDetails.setId(1L);
        CandidateDetailsDTO candidateDetailsDTO = candidateDetailsMapper.toDto(candidateDetails);

        int databaseSizeBeforeCreate = candidateDetailsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidateDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(candidateDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCandidateDetails() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList
        restCandidateDetailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidateDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].candidateCountry").value(hasItem(DEFAULT_CANDIDATE_COUNTRY)))
            .andExpect(jsonPath("$.[*].businessUnits").value(hasItem(DEFAULT_BUSINESS_UNITS)))
            .andExpect(jsonPath("$.[*].legalEntity").value(hasItem(DEFAULT_LEGAL_ENTITY)))
            .andExpect(jsonPath("$.[*].candidateSubFunction").value(hasItem(DEFAULT_CANDIDATE_SUB_FUNCTION)))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].jobTitle").value(hasItem(DEFAULT_JOB_TITLE)))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].subBusinessUnits").value(hasItem(DEFAULT_SUB_BUSINESS_UNITS)))
            .andExpect(jsonPath("$.[*].candidateFunction").value(hasItem(DEFAULT_CANDIDATE_FUNCTION)))
            .andExpect(jsonPath("$.[*].workLevel").value(hasItem(DEFAULT_WORK_LEVEL)))
            .andExpect(jsonPath("$.[*].candidateGrade").value(hasItem(DEFAULT_CANDIDATE_GRADE)));
    }

    @Test
    @Transactional
    void getCandidateDetails() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get the candidateDetails
        restCandidateDetailsMockMvc
            .perform(get(ENTITY_API_URL_ID, candidateDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(candidateDetails.getId().intValue()))
            .andExpect(jsonPath("$.candidateCountry").value(DEFAULT_CANDIDATE_COUNTRY))
            .andExpect(jsonPath("$.businessUnits").value(DEFAULT_BUSINESS_UNITS))
            .andExpect(jsonPath("$.legalEntity").value(DEFAULT_LEGAL_ENTITY))
            .andExpect(jsonPath("$.candidateSubFunction").value(DEFAULT_CANDIDATE_SUB_FUNCTION))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION))
            .andExpect(jsonPath("$.jobTitle").value(DEFAULT_JOB_TITLE))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION))
            .andExpect(jsonPath("$.subBusinessUnits").value(DEFAULT_SUB_BUSINESS_UNITS))
            .andExpect(jsonPath("$.candidateFunction").value(DEFAULT_CANDIDATE_FUNCTION))
            .andExpect(jsonPath("$.workLevel").value(DEFAULT_WORK_LEVEL))
            .andExpect(jsonPath("$.candidateGrade").value(DEFAULT_CANDIDATE_GRADE));
    }

    @Test
    @Transactional
    void getCandidateDetailsByIdFiltering() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        Long id = candidateDetails.getId();

        defaultCandidateDetailsShouldBeFound("id.equals=" + id);
        defaultCandidateDetailsShouldNotBeFound("id.notEquals=" + id);

        defaultCandidateDetailsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCandidateDetailsShouldNotBeFound("id.greaterThan=" + id);

        defaultCandidateDetailsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCandidateDetailsShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateCountry equals to DEFAULT_CANDIDATE_COUNTRY
        defaultCandidateDetailsShouldBeFound("candidateCountry.equals=" + DEFAULT_CANDIDATE_COUNTRY);

        // Get all the candidateDetailsList where candidateCountry equals to UPDATED_CANDIDATE_COUNTRY
        defaultCandidateDetailsShouldNotBeFound("candidateCountry.equals=" + UPDATED_CANDIDATE_COUNTRY);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateCountryIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateCountry in DEFAULT_CANDIDATE_COUNTRY or UPDATED_CANDIDATE_COUNTRY
        defaultCandidateDetailsShouldBeFound("candidateCountry.in=" + DEFAULT_CANDIDATE_COUNTRY + "," + UPDATED_CANDIDATE_COUNTRY);

        // Get all the candidateDetailsList where candidateCountry equals to UPDATED_CANDIDATE_COUNTRY
        defaultCandidateDetailsShouldNotBeFound("candidateCountry.in=" + UPDATED_CANDIDATE_COUNTRY);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateCountryIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateCountry is not null
        defaultCandidateDetailsShouldBeFound("candidateCountry.specified=true");

        // Get all the candidateDetailsList where candidateCountry is null
        defaultCandidateDetailsShouldNotBeFound("candidateCountry.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateCountryContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateCountry contains DEFAULT_CANDIDATE_COUNTRY
        defaultCandidateDetailsShouldBeFound("candidateCountry.contains=" + DEFAULT_CANDIDATE_COUNTRY);

        // Get all the candidateDetailsList where candidateCountry contains UPDATED_CANDIDATE_COUNTRY
        defaultCandidateDetailsShouldNotBeFound("candidateCountry.contains=" + UPDATED_CANDIDATE_COUNTRY);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateCountryNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateCountry does not contain DEFAULT_CANDIDATE_COUNTRY
        defaultCandidateDetailsShouldNotBeFound("candidateCountry.doesNotContain=" + DEFAULT_CANDIDATE_COUNTRY);

        // Get all the candidateDetailsList where candidateCountry does not contain UPDATED_CANDIDATE_COUNTRY
        defaultCandidateDetailsShouldBeFound("candidateCountry.doesNotContain=" + UPDATED_CANDIDATE_COUNTRY);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByBusinessUnitsIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where businessUnits equals to DEFAULT_BUSINESS_UNITS
        defaultCandidateDetailsShouldBeFound("businessUnits.equals=" + DEFAULT_BUSINESS_UNITS);

        // Get all the candidateDetailsList where businessUnits equals to UPDATED_BUSINESS_UNITS
        defaultCandidateDetailsShouldNotBeFound("businessUnits.equals=" + UPDATED_BUSINESS_UNITS);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByBusinessUnitsIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where businessUnits in DEFAULT_BUSINESS_UNITS or UPDATED_BUSINESS_UNITS
        defaultCandidateDetailsShouldBeFound("businessUnits.in=" + DEFAULT_BUSINESS_UNITS + "," + UPDATED_BUSINESS_UNITS);

        // Get all the candidateDetailsList where businessUnits equals to UPDATED_BUSINESS_UNITS
        defaultCandidateDetailsShouldNotBeFound("businessUnits.in=" + UPDATED_BUSINESS_UNITS);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByBusinessUnitsIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where businessUnits is not null
        defaultCandidateDetailsShouldBeFound("businessUnits.specified=true");

        // Get all the candidateDetailsList where businessUnits is null
        defaultCandidateDetailsShouldNotBeFound("businessUnits.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByBusinessUnitsContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where businessUnits contains DEFAULT_BUSINESS_UNITS
        defaultCandidateDetailsShouldBeFound("businessUnits.contains=" + DEFAULT_BUSINESS_UNITS);

        // Get all the candidateDetailsList where businessUnits contains UPDATED_BUSINESS_UNITS
        defaultCandidateDetailsShouldNotBeFound("businessUnits.contains=" + UPDATED_BUSINESS_UNITS);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByBusinessUnitsNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where businessUnits does not contain DEFAULT_BUSINESS_UNITS
        defaultCandidateDetailsShouldNotBeFound("businessUnits.doesNotContain=" + DEFAULT_BUSINESS_UNITS);

        // Get all the candidateDetailsList where businessUnits does not contain UPDATED_BUSINESS_UNITS
        defaultCandidateDetailsShouldBeFound("businessUnits.doesNotContain=" + UPDATED_BUSINESS_UNITS);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLegalEntityIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where legalEntity equals to DEFAULT_LEGAL_ENTITY
        defaultCandidateDetailsShouldBeFound("legalEntity.equals=" + DEFAULT_LEGAL_ENTITY);

        // Get all the candidateDetailsList where legalEntity equals to UPDATED_LEGAL_ENTITY
        defaultCandidateDetailsShouldNotBeFound("legalEntity.equals=" + UPDATED_LEGAL_ENTITY);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLegalEntityIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where legalEntity in DEFAULT_LEGAL_ENTITY or UPDATED_LEGAL_ENTITY
        defaultCandidateDetailsShouldBeFound("legalEntity.in=" + DEFAULT_LEGAL_ENTITY + "," + UPDATED_LEGAL_ENTITY);

        // Get all the candidateDetailsList where legalEntity equals to UPDATED_LEGAL_ENTITY
        defaultCandidateDetailsShouldNotBeFound("legalEntity.in=" + UPDATED_LEGAL_ENTITY);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLegalEntityIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where legalEntity is not null
        defaultCandidateDetailsShouldBeFound("legalEntity.specified=true");

        // Get all the candidateDetailsList where legalEntity is null
        defaultCandidateDetailsShouldNotBeFound("legalEntity.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLegalEntityContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where legalEntity contains DEFAULT_LEGAL_ENTITY
        defaultCandidateDetailsShouldBeFound("legalEntity.contains=" + DEFAULT_LEGAL_ENTITY);

        // Get all the candidateDetailsList where legalEntity contains UPDATED_LEGAL_ENTITY
        defaultCandidateDetailsShouldNotBeFound("legalEntity.contains=" + UPDATED_LEGAL_ENTITY);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLegalEntityNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where legalEntity does not contain DEFAULT_LEGAL_ENTITY
        defaultCandidateDetailsShouldNotBeFound("legalEntity.doesNotContain=" + DEFAULT_LEGAL_ENTITY);

        // Get all the candidateDetailsList where legalEntity does not contain UPDATED_LEGAL_ENTITY
        defaultCandidateDetailsShouldBeFound("legalEntity.doesNotContain=" + UPDATED_LEGAL_ENTITY);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateSubFunctionIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateSubFunction equals to DEFAULT_CANDIDATE_SUB_FUNCTION
        defaultCandidateDetailsShouldBeFound("candidateSubFunction.equals=" + DEFAULT_CANDIDATE_SUB_FUNCTION);

        // Get all the candidateDetailsList where candidateSubFunction equals to UPDATED_CANDIDATE_SUB_FUNCTION
        defaultCandidateDetailsShouldNotBeFound("candidateSubFunction.equals=" + UPDATED_CANDIDATE_SUB_FUNCTION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateSubFunctionIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateSubFunction in DEFAULT_CANDIDATE_SUB_FUNCTION or UPDATED_CANDIDATE_SUB_FUNCTION
        defaultCandidateDetailsShouldBeFound(
            "candidateSubFunction.in=" + DEFAULT_CANDIDATE_SUB_FUNCTION + "," + UPDATED_CANDIDATE_SUB_FUNCTION
        );

        // Get all the candidateDetailsList where candidateSubFunction equals to UPDATED_CANDIDATE_SUB_FUNCTION
        defaultCandidateDetailsShouldNotBeFound("candidateSubFunction.in=" + UPDATED_CANDIDATE_SUB_FUNCTION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateSubFunctionIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateSubFunction is not null
        defaultCandidateDetailsShouldBeFound("candidateSubFunction.specified=true");

        // Get all the candidateDetailsList where candidateSubFunction is null
        defaultCandidateDetailsShouldNotBeFound("candidateSubFunction.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateSubFunctionContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateSubFunction contains DEFAULT_CANDIDATE_SUB_FUNCTION
        defaultCandidateDetailsShouldBeFound("candidateSubFunction.contains=" + DEFAULT_CANDIDATE_SUB_FUNCTION);

        // Get all the candidateDetailsList where candidateSubFunction contains UPDATED_CANDIDATE_SUB_FUNCTION
        defaultCandidateDetailsShouldNotBeFound("candidateSubFunction.contains=" + UPDATED_CANDIDATE_SUB_FUNCTION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateSubFunctionNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateSubFunction does not contain DEFAULT_CANDIDATE_SUB_FUNCTION
        defaultCandidateDetailsShouldNotBeFound("candidateSubFunction.doesNotContain=" + DEFAULT_CANDIDATE_SUB_FUNCTION);

        // Get all the candidateDetailsList where candidateSubFunction does not contain UPDATED_CANDIDATE_SUB_FUNCTION
        defaultCandidateDetailsShouldBeFound("candidateSubFunction.doesNotContain=" + UPDATED_CANDIDATE_SUB_FUNCTION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByDesignationIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where designation equals to DEFAULT_DESIGNATION
        defaultCandidateDetailsShouldBeFound("designation.equals=" + DEFAULT_DESIGNATION);

        // Get all the candidateDetailsList where designation equals to UPDATED_DESIGNATION
        defaultCandidateDetailsShouldNotBeFound("designation.equals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByDesignationIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where designation in DEFAULT_DESIGNATION or UPDATED_DESIGNATION
        defaultCandidateDetailsShouldBeFound("designation.in=" + DEFAULT_DESIGNATION + "," + UPDATED_DESIGNATION);

        // Get all the candidateDetailsList where designation equals to UPDATED_DESIGNATION
        defaultCandidateDetailsShouldNotBeFound("designation.in=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByDesignationIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where designation is not null
        defaultCandidateDetailsShouldBeFound("designation.specified=true");

        // Get all the candidateDetailsList where designation is null
        defaultCandidateDetailsShouldNotBeFound("designation.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByDesignationContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where designation contains DEFAULT_DESIGNATION
        defaultCandidateDetailsShouldBeFound("designation.contains=" + DEFAULT_DESIGNATION);

        // Get all the candidateDetailsList where designation contains UPDATED_DESIGNATION
        defaultCandidateDetailsShouldNotBeFound("designation.contains=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByDesignationNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where designation does not contain DEFAULT_DESIGNATION
        defaultCandidateDetailsShouldNotBeFound("designation.doesNotContain=" + DEFAULT_DESIGNATION);

        // Get all the candidateDetailsList where designation does not contain UPDATED_DESIGNATION
        defaultCandidateDetailsShouldBeFound("designation.doesNotContain=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByJobTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where jobTitle equals to DEFAULT_JOB_TITLE
        defaultCandidateDetailsShouldBeFound("jobTitle.equals=" + DEFAULT_JOB_TITLE);

        // Get all the candidateDetailsList where jobTitle equals to UPDATED_JOB_TITLE
        defaultCandidateDetailsShouldNotBeFound("jobTitle.equals=" + UPDATED_JOB_TITLE);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByJobTitleIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where jobTitle in DEFAULT_JOB_TITLE or UPDATED_JOB_TITLE
        defaultCandidateDetailsShouldBeFound("jobTitle.in=" + DEFAULT_JOB_TITLE + "," + UPDATED_JOB_TITLE);

        // Get all the candidateDetailsList where jobTitle equals to UPDATED_JOB_TITLE
        defaultCandidateDetailsShouldNotBeFound("jobTitle.in=" + UPDATED_JOB_TITLE);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByJobTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where jobTitle is not null
        defaultCandidateDetailsShouldBeFound("jobTitle.specified=true");

        // Get all the candidateDetailsList where jobTitle is null
        defaultCandidateDetailsShouldNotBeFound("jobTitle.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByJobTitleContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where jobTitle contains DEFAULT_JOB_TITLE
        defaultCandidateDetailsShouldBeFound("jobTitle.contains=" + DEFAULT_JOB_TITLE);

        // Get all the candidateDetailsList where jobTitle contains UPDATED_JOB_TITLE
        defaultCandidateDetailsShouldNotBeFound("jobTitle.contains=" + UPDATED_JOB_TITLE);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByJobTitleNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where jobTitle does not contain DEFAULT_JOB_TITLE
        defaultCandidateDetailsShouldNotBeFound("jobTitle.doesNotContain=" + DEFAULT_JOB_TITLE);

        // Get all the candidateDetailsList where jobTitle does not contain UPDATED_JOB_TITLE
        defaultCandidateDetailsShouldBeFound("jobTitle.doesNotContain=" + UPDATED_JOB_TITLE);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where location equals to DEFAULT_LOCATION
        defaultCandidateDetailsShouldBeFound("location.equals=" + DEFAULT_LOCATION);

        // Get all the candidateDetailsList where location equals to UPDATED_LOCATION
        defaultCandidateDetailsShouldNotBeFound("location.equals=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLocationIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where location in DEFAULT_LOCATION or UPDATED_LOCATION
        defaultCandidateDetailsShouldBeFound("location.in=" + DEFAULT_LOCATION + "," + UPDATED_LOCATION);

        // Get all the candidateDetailsList where location equals to UPDATED_LOCATION
        defaultCandidateDetailsShouldNotBeFound("location.in=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLocationIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where location is not null
        defaultCandidateDetailsShouldBeFound("location.specified=true");

        // Get all the candidateDetailsList where location is null
        defaultCandidateDetailsShouldNotBeFound("location.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLocationContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where location contains DEFAULT_LOCATION
        defaultCandidateDetailsShouldBeFound("location.contains=" + DEFAULT_LOCATION);

        // Get all the candidateDetailsList where location contains UPDATED_LOCATION
        defaultCandidateDetailsShouldNotBeFound("location.contains=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByLocationNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where location does not contain DEFAULT_LOCATION
        defaultCandidateDetailsShouldNotBeFound("location.doesNotContain=" + DEFAULT_LOCATION);

        // Get all the candidateDetailsList where location does not contain UPDATED_LOCATION
        defaultCandidateDetailsShouldBeFound("location.doesNotContain=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsBySubBusinessUnitsIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where subBusinessUnits equals to DEFAULT_SUB_BUSINESS_UNITS
        defaultCandidateDetailsShouldBeFound("subBusinessUnits.equals=" + DEFAULT_SUB_BUSINESS_UNITS);

        // Get all the candidateDetailsList where subBusinessUnits equals to UPDATED_SUB_BUSINESS_UNITS
        defaultCandidateDetailsShouldNotBeFound("subBusinessUnits.equals=" + UPDATED_SUB_BUSINESS_UNITS);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsBySubBusinessUnitsIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where subBusinessUnits in DEFAULT_SUB_BUSINESS_UNITS or UPDATED_SUB_BUSINESS_UNITS
        defaultCandidateDetailsShouldBeFound("subBusinessUnits.in=" + DEFAULT_SUB_BUSINESS_UNITS + "," + UPDATED_SUB_BUSINESS_UNITS);

        // Get all the candidateDetailsList where subBusinessUnits equals to UPDATED_SUB_BUSINESS_UNITS
        defaultCandidateDetailsShouldNotBeFound("subBusinessUnits.in=" + UPDATED_SUB_BUSINESS_UNITS);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsBySubBusinessUnitsIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where subBusinessUnits is not null
        defaultCandidateDetailsShouldBeFound("subBusinessUnits.specified=true");

        // Get all the candidateDetailsList where subBusinessUnits is null
        defaultCandidateDetailsShouldNotBeFound("subBusinessUnits.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsBySubBusinessUnitsContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where subBusinessUnits contains DEFAULT_SUB_BUSINESS_UNITS
        defaultCandidateDetailsShouldBeFound("subBusinessUnits.contains=" + DEFAULT_SUB_BUSINESS_UNITS);

        // Get all the candidateDetailsList where subBusinessUnits contains UPDATED_SUB_BUSINESS_UNITS
        defaultCandidateDetailsShouldNotBeFound("subBusinessUnits.contains=" + UPDATED_SUB_BUSINESS_UNITS);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsBySubBusinessUnitsNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where subBusinessUnits does not contain DEFAULT_SUB_BUSINESS_UNITS
        defaultCandidateDetailsShouldNotBeFound("subBusinessUnits.doesNotContain=" + DEFAULT_SUB_BUSINESS_UNITS);

        // Get all the candidateDetailsList where subBusinessUnits does not contain UPDATED_SUB_BUSINESS_UNITS
        defaultCandidateDetailsShouldBeFound("subBusinessUnits.doesNotContain=" + UPDATED_SUB_BUSINESS_UNITS);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateFunctionIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateFunction equals to DEFAULT_CANDIDATE_FUNCTION
        defaultCandidateDetailsShouldBeFound("candidateFunction.equals=" + DEFAULT_CANDIDATE_FUNCTION);

        // Get all the candidateDetailsList where candidateFunction equals to UPDATED_CANDIDATE_FUNCTION
        defaultCandidateDetailsShouldNotBeFound("candidateFunction.equals=" + UPDATED_CANDIDATE_FUNCTION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateFunctionIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateFunction in DEFAULT_CANDIDATE_FUNCTION or UPDATED_CANDIDATE_FUNCTION
        defaultCandidateDetailsShouldBeFound("candidateFunction.in=" + DEFAULT_CANDIDATE_FUNCTION + "," + UPDATED_CANDIDATE_FUNCTION);

        // Get all the candidateDetailsList where candidateFunction equals to UPDATED_CANDIDATE_FUNCTION
        defaultCandidateDetailsShouldNotBeFound("candidateFunction.in=" + UPDATED_CANDIDATE_FUNCTION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateFunctionIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateFunction is not null
        defaultCandidateDetailsShouldBeFound("candidateFunction.specified=true");

        // Get all the candidateDetailsList where candidateFunction is null
        defaultCandidateDetailsShouldNotBeFound("candidateFunction.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateFunctionContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateFunction contains DEFAULT_CANDIDATE_FUNCTION
        defaultCandidateDetailsShouldBeFound("candidateFunction.contains=" + DEFAULT_CANDIDATE_FUNCTION);

        // Get all the candidateDetailsList where candidateFunction contains UPDATED_CANDIDATE_FUNCTION
        defaultCandidateDetailsShouldNotBeFound("candidateFunction.contains=" + UPDATED_CANDIDATE_FUNCTION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateFunctionNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateFunction does not contain DEFAULT_CANDIDATE_FUNCTION
        defaultCandidateDetailsShouldNotBeFound("candidateFunction.doesNotContain=" + DEFAULT_CANDIDATE_FUNCTION);

        // Get all the candidateDetailsList where candidateFunction does not contain UPDATED_CANDIDATE_FUNCTION
        defaultCandidateDetailsShouldBeFound("candidateFunction.doesNotContain=" + UPDATED_CANDIDATE_FUNCTION);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByWorkLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where workLevel equals to DEFAULT_WORK_LEVEL
        defaultCandidateDetailsShouldBeFound("workLevel.equals=" + DEFAULT_WORK_LEVEL);

        // Get all the candidateDetailsList where workLevel equals to UPDATED_WORK_LEVEL
        defaultCandidateDetailsShouldNotBeFound("workLevel.equals=" + UPDATED_WORK_LEVEL);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByWorkLevelIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where workLevel in DEFAULT_WORK_LEVEL or UPDATED_WORK_LEVEL
        defaultCandidateDetailsShouldBeFound("workLevel.in=" + DEFAULT_WORK_LEVEL + "," + UPDATED_WORK_LEVEL);

        // Get all the candidateDetailsList where workLevel equals to UPDATED_WORK_LEVEL
        defaultCandidateDetailsShouldNotBeFound("workLevel.in=" + UPDATED_WORK_LEVEL);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByWorkLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where workLevel is not null
        defaultCandidateDetailsShouldBeFound("workLevel.specified=true");

        // Get all the candidateDetailsList where workLevel is null
        defaultCandidateDetailsShouldNotBeFound("workLevel.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByWorkLevelContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where workLevel contains DEFAULT_WORK_LEVEL
        defaultCandidateDetailsShouldBeFound("workLevel.contains=" + DEFAULT_WORK_LEVEL);

        // Get all the candidateDetailsList where workLevel contains UPDATED_WORK_LEVEL
        defaultCandidateDetailsShouldNotBeFound("workLevel.contains=" + UPDATED_WORK_LEVEL);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByWorkLevelNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where workLevel does not contain DEFAULT_WORK_LEVEL
        defaultCandidateDetailsShouldNotBeFound("workLevel.doesNotContain=" + DEFAULT_WORK_LEVEL);

        // Get all the candidateDetailsList where workLevel does not contain UPDATED_WORK_LEVEL
        defaultCandidateDetailsShouldBeFound("workLevel.doesNotContain=" + UPDATED_WORK_LEVEL);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateGradeIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateGrade equals to DEFAULT_CANDIDATE_GRADE
        defaultCandidateDetailsShouldBeFound("candidateGrade.equals=" + DEFAULT_CANDIDATE_GRADE);

        // Get all the candidateDetailsList where candidateGrade equals to UPDATED_CANDIDATE_GRADE
        defaultCandidateDetailsShouldNotBeFound("candidateGrade.equals=" + UPDATED_CANDIDATE_GRADE);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateGradeIsInShouldWork() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateGrade in DEFAULT_CANDIDATE_GRADE or UPDATED_CANDIDATE_GRADE
        defaultCandidateDetailsShouldBeFound("candidateGrade.in=" + DEFAULT_CANDIDATE_GRADE + "," + UPDATED_CANDIDATE_GRADE);

        // Get all the candidateDetailsList where candidateGrade equals to UPDATED_CANDIDATE_GRADE
        defaultCandidateDetailsShouldNotBeFound("candidateGrade.in=" + UPDATED_CANDIDATE_GRADE);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateGradeIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateGrade is not null
        defaultCandidateDetailsShouldBeFound("candidateGrade.specified=true");

        // Get all the candidateDetailsList where candidateGrade is null
        defaultCandidateDetailsShouldNotBeFound("candidateGrade.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateGradeContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateGrade contains DEFAULT_CANDIDATE_GRADE
        defaultCandidateDetailsShouldBeFound("candidateGrade.contains=" + DEFAULT_CANDIDATE_GRADE);

        // Get all the candidateDetailsList where candidateGrade contains UPDATED_CANDIDATE_GRADE
        defaultCandidateDetailsShouldNotBeFound("candidateGrade.contains=" + UPDATED_CANDIDATE_GRADE);
    }

    @Test
    @Transactional
    void getAllCandidateDetailsByCandidateGradeNotContainsSomething() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        // Get all the candidateDetailsList where candidateGrade does not contain DEFAULT_CANDIDATE_GRADE
        defaultCandidateDetailsShouldNotBeFound("candidateGrade.doesNotContain=" + DEFAULT_CANDIDATE_GRADE);

        // Get all the candidateDetailsList where candidateGrade does not contain UPDATED_CANDIDATE_GRADE
        defaultCandidateDetailsShouldBeFound("candidateGrade.doesNotContain=" + UPDATED_CANDIDATE_GRADE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCandidateDetailsShouldBeFound(String filter) throws Exception {
        restCandidateDetailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidateDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].candidateCountry").value(hasItem(DEFAULT_CANDIDATE_COUNTRY)))
            .andExpect(jsonPath("$.[*].businessUnits").value(hasItem(DEFAULT_BUSINESS_UNITS)))
            .andExpect(jsonPath("$.[*].legalEntity").value(hasItem(DEFAULT_LEGAL_ENTITY)))
            .andExpect(jsonPath("$.[*].candidateSubFunction").value(hasItem(DEFAULT_CANDIDATE_SUB_FUNCTION)))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].jobTitle").value(hasItem(DEFAULT_JOB_TITLE)))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].subBusinessUnits").value(hasItem(DEFAULT_SUB_BUSINESS_UNITS)))
            .andExpect(jsonPath("$.[*].candidateFunction").value(hasItem(DEFAULT_CANDIDATE_FUNCTION)))
            .andExpect(jsonPath("$.[*].workLevel").value(hasItem(DEFAULT_WORK_LEVEL)))
            .andExpect(jsonPath("$.[*].candidateGrade").value(hasItem(DEFAULT_CANDIDATE_GRADE)));

        // Check, that the count call also returns 1
        restCandidateDetailsMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCandidateDetailsShouldNotBeFound(String filter) throws Exception {
        restCandidateDetailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCandidateDetailsMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCandidateDetails() throws Exception {
        // Get the candidateDetails
        restCandidateDetailsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCandidateDetails() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        int databaseSizeBeforeUpdate = candidateDetailsRepository.findAll().size();

        // Update the candidateDetails
        CandidateDetails updatedCandidateDetails = candidateDetailsRepository.findById(candidateDetails.getId()).get();
        // Disconnect from session so that the updates on updatedCandidateDetails are not directly saved in db
        em.detach(updatedCandidateDetails);
        updatedCandidateDetails
            .candidateCountry(UPDATED_CANDIDATE_COUNTRY)
            .businessUnits(UPDATED_BUSINESS_UNITS)
            .legalEntity(UPDATED_LEGAL_ENTITY)
            .candidateSubFunction(UPDATED_CANDIDATE_SUB_FUNCTION)
            .designation(UPDATED_DESIGNATION)
            .jobTitle(UPDATED_JOB_TITLE)
            .location(UPDATED_LOCATION)
            .subBusinessUnits(UPDATED_SUB_BUSINESS_UNITS)
            .candidateFunction(UPDATED_CANDIDATE_FUNCTION)
            .workLevel(UPDATED_WORK_LEVEL)
            .candidateGrade(UPDATED_CANDIDATE_GRADE);
        CandidateDetailsDTO candidateDetailsDTO = candidateDetailsMapper.toDto(updatedCandidateDetails);

        restCandidateDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, candidateDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(candidateDetailsDTO))
            )
            .andExpect(status().isOk());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeUpdate);
        CandidateDetails testCandidateDetails = candidateDetailsList.get(candidateDetailsList.size() - 1);
        assertThat(testCandidateDetails.getCandidateCountry()).isEqualTo(UPDATED_CANDIDATE_COUNTRY);
        assertThat(testCandidateDetails.getBusinessUnits()).isEqualTo(UPDATED_BUSINESS_UNITS);
        assertThat(testCandidateDetails.getLegalEntity()).isEqualTo(UPDATED_LEGAL_ENTITY);
        assertThat(testCandidateDetails.getCandidateSubFunction()).isEqualTo(UPDATED_CANDIDATE_SUB_FUNCTION);
        assertThat(testCandidateDetails.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testCandidateDetails.getJobTitle()).isEqualTo(UPDATED_JOB_TITLE);
        assertThat(testCandidateDetails.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testCandidateDetails.getSubBusinessUnits()).isEqualTo(UPDATED_SUB_BUSINESS_UNITS);
        assertThat(testCandidateDetails.getCandidateFunction()).isEqualTo(UPDATED_CANDIDATE_FUNCTION);
        assertThat(testCandidateDetails.getWorkLevel()).isEqualTo(UPDATED_WORK_LEVEL);
        assertThat(testCandidateDetails.getCandidateGrade()).isEqualTo(UPDATED_CANDIDATE_GRADE);
    }

    @Test
    @Transactional
    void putNonExistingCandidateDetails() throws Exception {
        int databaseSizeBeforeUpdate = candidateDetailsRepository.findAll().size();
        candidateDetails.setId(count.incrementAndGet());

        // Create the CandidateDetails
        CandidateDetailsDTO candidateDetailsDTO = candidateDetailsMapper.toDto(candidateDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, candidateDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(candidateDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCandidateDetails() throws Exception {
        int databaseSizeBeforeUpdate = candidateDetailsRepository.findAll().size();
        candidateDetails.setId(count.incrementAndGet());

        // Create the CandidateDetails
        CandidateDetailsDTO candidateDetailsDTO = candidateDetailsMapper.toDto(candidateDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(candidateDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCandidateDetails() throws Exception {
        int databaseSizeBeforeUpdate = candidateDetailsRepository.findAll().size();
        candidateDetails.setId(count.incrementAndGet());

        // Create the CandidateDetails
        CandidateDetailsDTO candidateDetailsDTO = candidateDetailsMapper.toDto(candidateDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateDetailsMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(candidateDetailsDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCandidateDetailsWithPatch() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        int databaseSizeBeforeUpdate = candidateDetailsRepository.findAll().size();

        // Update the candidateDetails using partial update
        CandidateDetails partialUpdatedCandidateDetails = new CandidateDetails();
        partialUpdatedCandidateDetails.setId(candidateDetails.getId());

        partialUpdatedCandidateDetails
            .candidateCountry(UPDATED_CANDIDATE_COUNTRY)
            .businessUnits(UPDATED_BUSINESS_UNITS)
            .candidateSubFunction(UPDATED_CANDIDATE_SUB_FUNCTION)
            .designation(UPDATED_DESIGNATION)
            .jobTitle(UPDATED_JOB_TITLE)
            .location(UPDATED_LOCATION)
            .subBusinessUnits(UPDATED_SUB_BUSINESS_UNITS);

        restCandidateDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCandidateDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCandidateDetails))
            )
            .andExpect(status().isOk());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeUpdate);
        CandidateDetails testCandidateDetails = candidateDetailsList.get(candidateDetailsList.size() - 1);
        assertThat(testCandidateDetails.getCandidateCountry()).isEqualTo(UPDATED_CANDIDATE_COUNTRY);
        assertThat(testCandidateDetails.getBusinessUnits()).isEqualTo(UPDATED_BUSINESS_UNITS);
        assertThat(testCandidateDetails.getLegalEntity()).isEqualTo(DEFAULT_LEGAL_ENTITY);
        assertThat(testCandidateDetails.getCandidateSubFunction()).isEqualTo(UPDATED_CANDIDATE_SUB_FUNCTION);
        assertThat(testCandidateDetails.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testCandidateDetails.getJobTitle()).isEqualTo(UPDATED_JOB_TITLE);
        assertThat(testCandidateDetails.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testCandidateDetails.getSubBusinessUnits()).isEqualTo(UPDATED_SUB_BUSINESS_UNITS);
        assertThat(testCandidateDetails.getCandidateFunction()).isEqualTo(DEFAULT_CANDIDATE_FUNCTION);
        assertThat(testCandidateDetails.getWorkLevel()).isEqualTo(DEFAULT_WORK_LEVEL);
        assertThat(testCandidateDetails.getCandidateGrade()).isEqualTo(DEFAULT_CANDIDATE_GRADE);
    }

    @Test
    @Transactional
    void fullUpdateCandidateDetailsWithPatch() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        int databaseSizeBeforeUpdate = candidateDetailsRepository.findAll().size();

        // Update the candidateDetails using partial update
        CandidateDetails partialUpdatedCandidateDetails = new CandidateDetails();
        partialUpdatedCandidateDetails.setId(candidateDetails.getId());

        partialUpdatedCandidateDetails
            .candidateCountry(UPDATED_CANDIDATE_COUNTRY)
            .businessUnits(UPDATED_BUSINESS_UNITS)
            .legalEntity(UPDATED_LEGAL_ENTITY)
            .candidateSubFunction(UPDATED_CANDIDATE_SUB_FUNCTION)
            .designation(UPDATED_DESIGNATION)
            .jobTitle(UPDATED_JOB_TITLE)
            .location(UPDATED_LOCATION)
            .subBusinessUnits(UPDATED_SUB_BUSINESS_UNITS)
            .candidateFunction(UPDATED_CANDIDATE_FUNCTION)
            .workLevel(UPDATED_WORK_LEVEL)
            .candidateGrade(UPDATED_CANDIDATE_GRADE);

        restCandidateDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCandidateDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCandidateDetails))
            )
            .andExpect(status().isOk());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeUpdate);
        CandidateDetails testCandidateDetails = candidateDetailsList.get(candidateDetailsList.size() - 1);
        assertThat(testCandidateDetails.getCandidateCountry()).isEqualTo(UPDATED_CANDIDATE_COUNTRY);
        assertThat(testCandidateDetails.getBusinessUnits()).isEqualTo(UPDATED_BUSINESS_UNITS);
        assertThat(testCandidateDetails.getLegalEntity()).isEqualTo(UPDATED_LEGAL_ENTITY);
        assertThat(testCandidateDetails.getCandidateSubFunction()).isEqualTo(UPDATED_CANDIDATE_SUB_FUNCTION);
        assertThat(testCandidateDetails.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testCandidateDetails.getJobTitle()).isEqualTo(UPDATED_JOB_TITLE);
        assertThat(testCandidateDetails.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testCandidateDetails.getSubBusinessUnits()).isEqualTo(UPDATED_SUB_BUSINESS_UNITS);
        assertThat(testCandidateDetails.getCandidateFunction()).isEqualTo(UPDATED_CANDIDATE_FUNCTION);
        assertThat(testCandidateDetails.getWorkLevel()).isEqualTo(UPDATED_WORK_LEVEL);
        assertThat(testCandidateDetails.getCandidateGrade()).isEqualTo(UPDATED_CANDIDATE_GRADE);
    }

    @Test
    @Transactional
    void patchNonExistingCandidateDetails() throws Exception {
        int databaseSizeBeforeUpdate = candidateDetailsRepository.findAll().size();
        candidateDetails.setId(count.incrementAndGet());

        // Create the CandidateDetails
        CandidateDetailsDTO candidateDetailsDTO = candidateDetailsMapper.toDto(candidateDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, candidateDetailsDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(candidateDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCandidateDetails() throws Exception {
        int databaseSizeBeforeUpdate = candidateDetailsRepository.findAll().size();
        candidateDetails.setId(count.incrementAndGet());

        // Create the CandidateDetails
        CandidateDetailsDTO candidateDetailsDTO = candidateDetailsMapper.toDto(candidateDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(candidateDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCandidateDetails() throws Exception {
        int databaseSizeBeforeUpdate = candidateDetailsRepository.findAll().size();
        candidateDetails.setId(count.incrementAndGet());

        // Create the CandidateDetails
        CandidateDetailsDTO candidateDetailsDTO = candidateDetailsMapper.toDto(candidateDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(candidateDetailsDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CandidateDetails in the database
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCandidateDetails() throws Exception {
        // Initialize the database
        candidateDetailsRepository.saveAndFlush(candidateDetails);

        int databaseSizeBeforeDelete = candidateDetailsRepository.findAll().size();

        // Delete the candidateDetails
        restCandidateDetailsMockMvc
            .perform(delete(ENTITY_API_URL_ID, candidateDetails.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CandidateDetails> candidateDetailsList = candidateDetailsRepository.findAll();
        assertThat(candidateDetailsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
