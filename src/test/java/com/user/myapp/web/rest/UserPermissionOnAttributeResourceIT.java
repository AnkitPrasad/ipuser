package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.PermissionsOnAttributeMaster;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.domain.UserPermissionOnAttribute;
import com.user.myapp.repository.UserPermissionOnAttributeRepository;
import com.user.myapp.service.criteria.UserPermissionOnAttributeCriteria;
import com.user.myapp.service.dto.UserPermissionOnAttributeDTO;
import com.user.myapp.service.mapper.UserPermissionOnAttributeMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link UserPermissionOnAttributeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UserPermissionOnAttributeResourceIT {

    private static final String DEFAULT_ATTRIBUTE_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_ATTRIBUTE_VALUE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/user-permission-on-attributes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UserPermissionOnAttributeRepository userPermissionOnAttributeRepository;

    @Autowired
    private UserPermissionOnAttributeMapper userPermissionOnAttributeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserPermissionOnAttributeMockMvc;

    private UserPermissionOnAttribute userPermissionOnAttribute;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserPermissionOnAttribute createEntity(EntityManager em) {
        UserPermissionOnAttribute userPermissionOnAttribute = new UserPermissionOnAttribute().attributeValue(DEFAULT_ATTRIBUTE_VALUE);
        return userPermissionOnAttribute;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserPermissionOnAttribute createUpdatedEntity(EntityManager em) {
        UserPermissionOnAttribute userPermissionOnAttribute = new UserPermissionOnAttribute().attributeValue(UPDATED_ATTRIBUTE_VALUE);
        return userPermissionOnAttribute;
    }

    @BeforeEach
    public void initTest() {
        userPermissionOnAttribute = createEntity(em);
    }

    @Test
    @Transactional
    void createUserPermissionOnAttribute() throws Exception {
        int databaseSizeBeforeCreate = userPermissionOnAttributeRepository.findAll().size();
        // Create the UserPermissionOnAttribute
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);
        restUserPermissionOnAttributeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userPermissionOnAttributeDTO))
            )
            .andExpect(status().isCreated());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeCreate + 1);
        UserPermissionOnAttribute testUserPermissionOnAttribute = userPermissionOnAttributeList.get(
            userPermissionOnAttributeList.size() - 1
        );
        assertThat(testUserPermissionOnAttribute.getAttributeValue()).isEqualTo(DEFAULT_ATTRIBUTE_VALUE);
    }

    @Test
    @Transactional
    void createUserPermissionOnAttributeWithExistingId() throws Exception {
        // Create the UserPermissionOnAttribute with an existing ID
        userPermissionOnAttribute.setId(1L);
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);

        int databaseSizeBeforeCreate = userPermissionOnAttributeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserPermissionOnAttributeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userPermissionOnAttributeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllUserPermissionOnAttributes() throws Exception {
        // Initialize the database
        userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);

        // Get all the userPermissionOnAttributeList
        restUserPermissionOnAttributeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userPermissionOnAttribute.getId().intValue())))
            .andExpect(jsonPath("$.[*].attributeValue").value(hasItem(DEFAULT_ATTRIBUTE_VALUE.toString())));
    }

    @Test
    @Transactional
    void getUserPermissionOnAttribute() throws Exception {
        // Initialize the database
        userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);

        // Get the userPermissionOnAttribute
        restUserPermissionOnAttributeMockMvc
            .perform(get(ENTITY_API_URL_ID, userPermissionOnAttribute.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userPermissionOnAttribute.getId().intValue()))
            .andExpect(jsonPath("$.attributeValue").value(DEFAULT_ATTRIBUTE_VALUE.toString()));
    }

    @Test
    @Transactional
    void getUserPermissionOnAttributesByIdFiltering() throws Exception {
        // Initialize the database
        userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);

        Long id = userPermissionOnAttribute.getId();

        defaultUserPermissionOnAttributeShouldBeFound("id.equals=" + id);
        defaultUserPermissionOnAttributeShouldNotBeFound("id.notEquals=" + id);

        defaultUserPermissionOnAttributeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUserPermissionOnAttributeShouldNotBeFound("id.greaterThan=" + id);

        defaultUserPermissionOnAttributeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUserPermissionOnAttributeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllUserPermissionOnAttributesByRoleMasterIsEqualToSomething() throws Exception {
        RoleMaster roleMaster;
        if (TestUtil.findAll(em, RoleMaster.class).isEmpty()) {
            userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);
            roleMaster = RoleMasterResourceIT.createEntity(em);
        } else {
            roleMaster = TestUtil.findAll(em, RoleMaster.class).get(0);
        }
        em.persist(roleMaster);
        em.flush();
        userPermissionOnAttribute.setRoleMaster(roleMaster);
        userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);
        Long roleMasterId = roleMaster.getId();

        // Get all the userPermissionOnAttributeList where roleMaster equals to roleMasterId
        defaultUserPermissionOnAttributeShouldBeFound("roleMasterId.equals=" + roleMasterId);

        // Get all the userPermissionOnAttributeList where roleMaster equals to (roleMasterId + 1)
        defaultUserPermissionOnAttributeShouldNotBeFound("roleMasterId.equals=" + (roleMasterId + 1));
    }

    @Test
    @Transactional
    void getAllUserPermissionOnAttributesByPermissionsOnAttributeMasterIsEqualToSomething() throws Exception {
        PermissionsOnAttributeMaster permissionsOnAttributeMaster;
        if (TestUtil.findAll(em, PermissionsOnAttributeMaster.class).isEmpty()) {
            userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);
            permissionsOnAttributeMaster = PermissionsOnAttributeMasterResourceIT.createEntity(em);
        } else {
            permissionsOnAttributeMaster = TestUtil.findAll(em, PermissionsOnAttributeMaster.class).get(0);
        }
        em.persist(permissionsOnAttributeMaster);
        em.flush();
        userPermissionOnAttribute.setPermissionsOnAttributeMaster(permissionsOnAttributeMaster);
        userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);
        Long permissionsOnAttributeMasterId = permissionsOnAttributeMaster.getId();

        // Get all the userPermissionOnAttributeList where permissionsOnAttributeMaster equals to permissionsOnAttributeMasterId
        defaultUserPermissionOnAttributeShouldBeFound("permissionsOnAttributeMasterId.equals=" + permissionsOnAttributeMasterId);

        // Get all the userPermissionOnAttributeList where permissionsOnAttributeMaster equals to (permissionsOnAttributeMasterId + 1)
        defaultUserPermissionOnAttributeShouldNotBeFound("permissionsOnAttributeMasterId.equals=" + (permissionsOnAttributeMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserPermissionOnAttributeShouldBeFound(String filter) throws Exception {
        restUserPermissionOnAttributeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userPermissionOnAttribute.getId().intValue())))
            .andExpect(jsonPath("$.[*].attributeValue").value(hasItem(DEFAULT_ATTRIBUTE_VALUE.toString())));

        // Check, that the count call also returns 1
        restUserPermissionOnAttributeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserPermissionOnAttributeShouldNotBeFound(String filter) throws Exception {
        restUserPermissionOnAttributeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserPermissionOnAttributeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingUserPermissionOnAttribute() throws Exception {
        // Get the userPermissionOnAttribute
        restUserPermissionOnAttributeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingUserPermissionOnAttribute() throws Exception {
        // Initialize the database
        userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);

        int databaseSizeBeforeUpdate = userPermissionOnAttributeRepository.findAll().size();

        // Update the userPermissionOnAttribute
        UserPermissionOnAttribute updatedUserPermissionOnAttribute = userPermissionOnAttributeRepository
            .findById(userPermissionOnAttribute.getId())
            .get();
        // Disconnect from session so that the updates on updatedUserPermissionOnAttribute are not directly saved in db
        em.detach(updatedUserPermissionOnAttribute);
        updatedUserPermissionOnAttribute.attributeValue(UPDATED_ATTRIBUTE_VALUE);
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = userPermissionOnAttributeMapper.toDto(updatedUserPermissionOnAttribute);

        restUserPermissionOnAttributeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userPermissionOnAttributeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userPermissionOnAttributeDTO))
            )
            .andExpect(status().isOk());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeUpdate);
        UserPermissionOnAttribute testUserPermissionOnAttribute = userPermissionOnAttributeList.get(
            userPermissionOnAttributeList.size() - 1
        );
        assertThat(testUserPermissionOnAttribute.getAttributeValue()).isEqualTo(UPDATED_ATTRIBUTE_VALUE);
    }

    @Test
    @Transactional
    void putNonExistingUserPermissionOnAttribute() throws Exception {
        int databaseSizeBeforeUpdate = userPermissionOnAttributeRepository.findAll().size();
        userPermissionOnAttribute.setId(count.incrementAndGet());

        // Create the UserPermissionOnAttribute
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserPermissionOnAttributeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userPermissionOnAttributeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userPermissionOnAttributeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUserPermissionOnAttribute() throws Exception {
        int databaseSizeBeforeUpdate = userPermissionOnAttributeRepository.findAll().size();
        userPermissionOnAttribute.setId(count.incrementAndGet());

        // Create the UserPermissionOnAttribute
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserPermissionOnAttributeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userPermissionOnAttributeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUserPermissionOnAttribute() throws Exception {
        int databaseSizeBeforeUpdate = userPermissionOnAttributeRepository.findAll().size();
        userPermissionOnAttribute.setId(count.incrementAndGet());

        // Create the UserPermissionOnAttribute
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserPermissionOnAttributeMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userPermissionOnAttributeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUserPermissionOnAttributeWithPatch() throws Exception {
        // Initialize the database
        userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);

        int databaseSizeBeforeUpdate = userPermissionOnAttributeRepository.findAll().size();

        // Update the userPermissionOnAttribute using partial update
        UserPermissionOnAttribute partialUpdatedUserPermissionOnAttribute = new UserPermissionOnAttribute();
        partialUpdatedUserPermissionOnAttribute.setId(userPermissionOnAttribute.getId());

        restUserPermissionOnAttributeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserPermissionOnAttribute.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserPermissionOnAttribute))
            )
            .andExpect(status().isOk());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeUpdate);
        UserPermissionOnAttribute testUserPermissionOnAttribute = userPermissionOnAttributeList.get(
            userPermissionOnAttributeList.size() - 1
        );
        assertThat(testUserPermissionOnAttribute.getAttributeValue()).isEqualTo(DEFAULT_ATTRIBUTE_VALUE);
    }

    @Test
    @Transactional
    void fullUpdateUserPermissionOnAttributeWithPatch() throws Exception {
        // Initialize the database
        userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);

        int databaseSizeBeforeUpdate = userPermissionOnAttributeRepository.findAll().size();

        // Update the userPermissionOnAttribute using partial update
        UserPermissionOnAttribute partialUpdatedUserPermissionOnAttribute = new UserPermissionOnAttribute();
        partialUpdatedUserPermissionOnAttribute.setId(userPermissionOnAttribute.getId());

        partialUpdatedUserPermissionOnAttribute.attributeValue(UPDATED_ATTRIBUTE_VALUE);

        restUserPermissionOnAttributeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserPermissionOnAttribute.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserPermissionOnAttribute))
            )
            .andExpect(status().isOk());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeUpdate);
        UserPermissionOnAttribute testUserPermissionOnAttribute = userPermissionOnAttributeList.get(
            userPermissionOnAttributeList.size() - 1
        );
        assertThat(testUserPermissionOnAttribute.getAttributeValue()).isEqualTo(UPDATED_ATTRIBUTE_VALUE);
    }

    @Test
    @Transactional
    void patchNonExistingUserPermissionOnAttribute() throws Exception {
        int databaseSizeBeforeUpdate = userPermissionOnAttributeRepository.findAll().size();
        userPermissionOnAttribute.setId(count.incrementAndGet());

        // Create the UserPermissionOnAttribute
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserPermissionOnAttributeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, userPermissionOnAttributeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userPermissionOnAttributeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUserPermissionOnAttribute() throws Exception {
        int databaseSizeBeforeUpdate = userPermissionOnAttributeRepository.findAll().size();
        userPermissionOnAttribute.setId(count.incrementAndGet());

        // Create the UserPermissionOnAttribute
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserPermissionOnAttributeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userPermissionOnAttributeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUserPermissionOnAttribute() throws Exception {
        int databaseSizeBeforeUpdate = userPermissionOnAttributeRepository.findAll().size();
        userPermissionOnAttribute.setId(count.incrementAndGet());

        // Create the UserPermissionOnAttribute
        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = userPermissionOnAttributeMapper.toDto(userPermissionOnAttribute);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserPermissionOnAttributeMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userPermissionOnAttributeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserPermissionOnAttribute in the database
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUserPermissionOnAttribute() throws Exception {
        // Initialize the database
        userPermissionOnAttributeRepository.saveAndFlush(userPermissionOnAttribute);

        int databaseSizeBeforeDelete = userPermissionOnAttributeRepository.findAll().size();

        // Delete the userPermissionOnAttribute
        restUserPermissionOnAttributeMockMvc
            .perform(delete(ENTITY_API_URL_ID, userPermissionOnAttribute.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserPermissionOnAttribute> userPermissionOnAttributeList = userPermissionOnAttributeRepository.findAll();
        assertThat(userPermissionOnAttributeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
