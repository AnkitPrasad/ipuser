package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.LoginCount;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.domain.UserEmails;
import com.user.myapp.domain.UserRolePermission;
import com.user.myapp.domain.enumeration.Permission;
import com.user.myapp.repository.InPreferenceUserRepository;
import com.user.myapp.service.criteria.InPreferenceUserCriteria;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.mapper.InPreferenceUserMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link InPreferenceUserResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class InPreferenceUserResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILE_NO = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_PROFILE_URL = "AAAAAAAAAA";
    private static final String UPDATED_PROFILE_URL = "BBBBBBBBBB";

    private static final Permission DEFAULT_USER_TYPE = Permission.SuperAdmin;
    private static final Permission UPDATED_USER_TYPE = Permission.SubAdmin;

    private static final String DEFAULT_EMAIL_ID = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_ID = "BBBBBBBBBB";

    private static final Long DEFAULT_LOGIN_ID = 1L;
    private static final Long UPDATED_LOGIN_ID = 2L;
    private static final Long SMALLER_LOGIN_ID = 1L - 1L;

    private static final String DEFAULT_SECRET = "AAAAAAAAAA";
    private static final String UPDATED_SECRET = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_CREATED_BY = 1L;
    private static final Long UPDATED_CREATED_BY = 2L;
    private static final Long SMALLER_CREATED_BY = 1L - 1L;

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_UPDATED_BY = 1L;
    private static final Long UPDATED_UPDATED_BY = 2L;
    private static final Long SMALLER_UPDATED_BY = 1L - 1L;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final String ENTITY_API_URL = "/api/in-preference-users";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private InPreferenceUserRepository inPreferenceUserRepository;

    @Autowired
    private InPreferenceUserMapper inPreferenceUserMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInPreferenceUserMockMvc;

    private InPreferenceUser inPreferenceUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InPreferenceUser createEntity(EntityManager em) {
        InPreferenceUser inPreferenceUser = new InPreferenceUser()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .mobileNo(DEFAULT_MOBILE_NO)
            .profileUrl(DEFAULT_PROFILE_URL)
            .userType(DEFAULT_USER_TYPE)
            .emailId(DEFAULT_EMAIL_ID)
            .loginId(DEFAULT_LOGIN_ID)
            .secret(DEFAULT_SECRET)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .status(DEFAULT_STATUS);
        return inPreferenceUser;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InPreferenceUser createUpdatedEntity(EntityManager em) {
        InPreferenceUser inPreferenceUser = new InPreferenceUser()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .mobileNo(UPDATED_MOBILE_NO)
            .profileUrl(UPDATED_PROFILE_URL)
            .userType(UPDATED_USER_TYPE)
            .emailId(UPDATED_EMAIL_ID)
            .loginId(UPDATED_LOGIN_ID)
            .secret(UPDATED_SECRET)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);
        return inPreferenceUser;
    }

    @BeforeEach
    public void initTest() {
        inPreferenceUser = createEntity(em);
    }

    @Test
    @Transactional
    void createInPreferenceUser() throws Exception {
        int databaseSizeBeforeCreate = inPreferenceUserRepository.findAll().size();
        // Create the InPreferenceUser
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);
        restInPreferenceUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isCreated());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeCreate + 1);
        InPreferenceUser testInPreferenceUser = inPreferenceUserList.get(inPreferenceUserList.size() - 1);
        assertThat(testInPreferenceUser.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testInPreferenceUser.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testInPreferenceUser.getMobileNo()).isEqualTo(DEFAULT_MOBILE_NO);
        assertThat(testInPreferenceUser.getProfileUrl()).isEqualTo(DEFAULT_PROFILE_URL);
        assertThat(testInPreferenceUser.getUserType()).isEqualTo(DEFAULT_USER_TYPE);
        assertThat(testInPreferenceUser.getEmailId()).isEqualTo(DEFAULT_EMAIL_ID);
        assertThat(testInPreferenceUser.getLoginId()).isEqualTo(DEFAULT_LOGIN_ID);
        assertThat(testInPreferenceUser.getSecret()).isEqualTo(DEFAULT_SECRET);
        assertThat(testInPreferenceUser.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testInPreferenceUser.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testInPreferenceUser.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testInPreferenceUser.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testInPreferenceUser.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createInPreferenceUserWithExistingId() throws Exception {
        // Create the InPreferenceUser with an existing ID
        inPreferenceUser.setId(1L);
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        int databaseSizeBeforeCreate = inPreferenceUserRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restInPreferenceUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = inPreferenceUserRepository.findAll().size();
        // set the field null
        inPreferenceUser.setFirstName(null);

        // Create the InPreferenceUser, which fails.
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        restInPreferenceUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isBadRequest());

        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = inPreferenceUserRepository.findAll().size();
        // set the field null
        inPreferenceUser.setLastName(null);

        // Create the InPreferenceUser, which fails.
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        restInPreferenceUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isBadRequest());

        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEmailIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = inPreferenceUserRepository.findAll().size();
        // set the field null
        inPreferenceUser.setEmailId(null);

        // Create the InPreferenceUser, which fails.
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        restInPreferenceUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isBadRequest());

        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLoginIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = inPreferenceUserRepository.findAll().size();
        // set the field null
        inPreferenceUser.setLoginId(null);

        // Create the InPreferenceUser, which fails.
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        restInPreferenceUserMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isBadRequest());

        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsers() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList
        restInPreferenceUserMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inPreferenceUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].mobileNo").value(hasItem(DEFAULT_MOBILE_NO)))
            .andExpect(jsonPath("$.[*].profileUrl").value(hasItem(DEFAULT_PROFILE_URL)))
            .andExpect(jsonPath("$.[*].userType").value(hasItem(DEFAULT_USER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].emailId").value(hasItem(DEFAULT_EMAIL_ID)))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID.intValue())))
            .andExpect(jsonPath("$.[*].secret").value(hasItem(DEFAULT_SECRET)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }

    @Test
    @Transactional
    void getInPreferenceUser() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get the inPreferenceUser
        restInPreferenceUserMockMvc
            .perform(get(ENTITY_API_URL_ID, inPreferenceUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(inPreferenceUser.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.mobileNo").value(DEFAULT_MOBILE_NO))
            .andExpect(jsonPath("$.profileUrl").value(DEFAULT_PROFILE_URL))
            .andExpect(jsonPath("$.userType").value(DEFAULT_USER_TYPE.toString()))
            .andExpect(jsonPath("$.emailId").value(DEFAULT_EMAIL_ID))
            .andExpect(jsonPath("$.loginId").value(DEFAULT_LOGIN_ID.intValue()))
            .andExpect(jsonPath("$.secret").value(DEFAULT_SECRET))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.intValue()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    void getInPreferenceUsersByIdFiltering() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        Long id = inPreferenceUser.getId();

        defaultInPreferenceUserShouldBeFound("id.equals=" + id);
        defaultInPreferenceUserShouldNotBeFound("id.notEquals=" + id);

        defaultInPreferenceUserShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultInPreferenceUserShouldNotBeFound("id.greaterThan=" + id);

        defaultInPreferenceUserShouldBeFound("id.lessThanOrEqual=" + id);
        defaultInPreferenceUserShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByFirstNameIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where firstName equals to DEFAULT_FIRST_NAME
        defaultInPreferenceUserShouldBeFound("firstName.equals=" + DEFAULT_FIRST_NAME);

        // Get all the inPreferenceUserList where firstName equals to UPDATED_FIRST_NAME
        defaultInPreferenceUserShouldNotBeFound("firstName.equals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByFirstNameIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where firstName in DEFAULT_FIRST_NAME or UPDATED_FIRST_NAME
        defaultInPreferenceUserShouldBeFound("firstName.in=" + DEFAULT_FIRST_NAME + "," + UPDATED_FIRST_NAME);

        // Get all the inPreferenceUserList where firstName equals to UPDATED_FIRST_NAME
        defaultInPreferenceUserShouldNotBeFound("firstName.in=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByFirstNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where firstName is not null
        defaultInPreferenceUserShouldBeFound("firstName.specified=true");

        // Get all the inPreferenceUserList where firstName is null
        defaultInPreferenceUserShouldNotBeFound("firstName.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByFirstNameContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where firstName contains DEFAULT_FIRST_NAME
        defaultInPreferenceUserShouldBeFound("firstName.contains=" + DEFAULT_FIRST_NAME);

        // Get all the inPreferenceUserList where firstName contains UPDATED_FIRST_NAME
        defaultInPreferenceUserShouldNotBeFound("firstName.contains=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByFirstNameNotContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where firstName does not contain DEFAULT_FIRST_NAME
        defaultInPreferenceUserShouldNotBeFound("firstName.doesNotContain=" + DEFAULT_FIRST_NAME);

        // Get all the inPreferenceUserList where firstName does not contain UPDATED_FIRST_NAME
        defaultInPreferenceUserShouldBeFound("firstName.doesNotContain=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLastNameIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where lastName equals to DEFAULT_LAST_NAME
        defaultInPreferenceUserShouldBeFound("lastName.equals=" + DEFAULT_LAST_NAME);

        // Get all the inPreferenceUserList where lastName equals to UPDATED_LAST_NAME
        defaultInPreferenceUserShouldNotBeFound("lastName.equals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLastNameIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where lastName in DEFAULT_LAST_NAME or UPDATED_LAST_NAME
        defaultInPreferenceUserShouldBeFound("lastName.in=" + DEFAULT_LAST_NAME + "," + UPDATED_LAST_NAME);

        // Get all the inPreferenceUserList where lastName equals to UPDATED_LAST_NAME
        defaultInPreferenceUserShouldNotBeFound("lastName.in=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLastNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where lastName is not null
        defaultInPreferenceUserShouldBeFound("lastName.specified=true");

        // Get all the inPreferenceUserList where lastName is null
        defaultInPreferenceUserShouldNotBeFound("lastName.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLastNameContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where lastName contains DEFAULT_LAST_NAME
        defaultInPreferenceUserShouldBeFound("lastName.contains=" + DEFAULT_LAST_NAME);

        // Get all the inPreferenceUserList where lastName contains UPDATED_LAST_NAME
        defaultInPreferenceUserShouldNotBeFound("lastName.contains=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLastNameNotContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where lastName does not contain DEFAULT_LAST_NAME
        defaultInPreferenceUserShouldNotBeFound("lastName.doesNotContain=" + DEFAULT_LAST_NAME);

        // Get all the inPreferenceUserList where lastName does not contain UPDATED_LAST_NAME
        defaultInPreferenceUserShouldBeFound("lastName.doesNotContain=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByMobileNoIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where mobileNo equals to DEFAULT_MOBILE_NO
        defaultInPreferenceUserShouldBeFound("mobileNo.equals=" + DEFAULT_MOBILE_NO);

        // Get all the inPreferenceUserList where mobileNo equals to UPDATED_MOBILE_NO
        defaultInPreferenceUserShouldNotBeFound("mobileNo.equals=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByMobileNoIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where mobileNo in DEFAULT_MOBILE_NO or UPDATED_MOBILE_NO
        defaultInPreferenceUserShouldBeFound("mobileNo.in=" + DEFAULT_MOBILE_NO + "," + UPDATED_MOBILE_NO);

        // Get all the inPreferenceUserList where mobileNo equals to UPDATED_MOBILE_NO
        defaultInPreferenceUserShouldNotBeFound("mobileNo.in=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByMobileNoIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where mobileNo is not null
        defaultInPreferenceUserShouldBeFound("mobileNo.specified=true");

        // Get all the inPreferenceUserList where mobileNo is null
        defaultInPreferenceUserShouldNotBeFound("mobileNo.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByMobileNoContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where mobileNo contains DEFAULT_MOBILE_NO
        defaultInPreferenceUserShouldBeFound("mobileNo.contains=" + DEFAULT_MOBILE_NO);

        // Get all the inPreferenceUserList where mobileNo contains UPDATED_MOBILE_NO
        defaultInPreferenceUserShouldNotBeFound("mobileNo.contains=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByMobileNoNotContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where mobileNo does not contain DEFAULT_MOBILE_NO
        defaultInPreferenceUserShouldNotBeFound("mobileNo.doesNotContain=" + DEFAULT_MOBILE_NO);

        // Get all the inPreferenceUserList where mobileNo does not contain UPDATED_MOBILE_NO
        defaultInPreferenceUserShouldBeFound("mobileNo.doesNotContain=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByProfileUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where profileUrl equals to DEFAULT_PROFILE_URL
        defaultInPreferenceUserShouldBeFound("profileUrl.equals=" + DEFAULT_PROFILE_URL);

        // Get all the inPreferenceUserList where profileUrl equals to UPDATED_PROFILE_URL
        defaultInPreferenceUserShouldNotBeFound("profileUrl.equals=" + UPDATED_PROFILE_URL);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByProfileUrlIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where profileUrl in DEFAULT_PROFILE_URL or UPDATED_PROFILE_URL
        defaultInPreferenceUserShouldBeFound("profileUrl.in=" + DEFAULT_PROFILE_URL + "," + UPDATED_PROFILE_URL);

        // Get all the inPreferenceUserList where profileUrl equals to UPDATED_PROFILE_URL
        defaultInPreferenceUserShouldNotBeFound("profileUrl.in=" + UPDATED_PROFILE_URL);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByProfileUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where profileUrl is not null
        defaultInPreferenceUserShouldBeFound("profileUrl.specified=true");

        // Get all the inPreferenceUserList where profileUrl is null
        defaultInPreferenceUserShouldNotBeFound("profileUrl.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByProfileUrlContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where profileUrl contains DEFAULT_PROFILE_URL
        defaultInPreferenceUserShouldBeFound("profileUrl.contains=" + DEFAULT_PROFILE_URL);

        // Get all the inPreferenceUserList where profileUrl contains UPDATED_PROFILE_URL
        defaultInPreferenceUserShouldNotBeFound("profileUrl.contains=" + UPDATED_PROFILE_URL);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByProfileUrlNotContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where profileUrl does not contain DEFAULT_PROFILE_URL
        defaultInPreferenceUserShouldNotBeFound("profileUrl.doesNotContain=" + DEFAULT_PROFILE_URL);

        // Get all the inPreferenceUserList where profileUrl does not contain UPDATED_PROFILE_URL
        defaultInPreferenceUserShouldBeFound("profileUrl.doesNotContain=" + UPDATED_PROFILE_URL);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUserTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where userType equals to DEFAULT_USER_TYPE
        defaultInPreferenceUserShouldBeFound("userType.equals=" + DEFAULT_USER_TYPE);

        // Get all the inPreferenceUserList where userType equals to UPDATED_USER_TYPE
        defaultInPreferenceUserShouldNotBeFound("userType.equals=" + UPDATED_USER_TYPE);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUserTypeIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where userType in DEFAULT_USER_TYPE or UPDATED_USER_TYPE
        defaultInPreferenceUserShouldBeFound("userType.in=" + DEFAULT_USER_TYPE + "," + UPDATED_USER_TYPE);

        // Get all the inPreferenceUserList where userType equals to UPDATED_USER_TYPE
        defaultInPreferenceUserShouldNotBeFound("userType.in=" + UPDATED_USER_TYPE);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUserTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where userType is not null
        defaultInPreferenceUserShouldBeFound("userType.specified=true");

        // Get all the inPreferenceUserList where userType is null
        defaultInPreferenceUserShouldNotBeFound("userType.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByEmailIdIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where emailId equals to DEFAULT_EMAIL_ID
        defaultInPreferenceUserShouldBeFound("emailId.equals=" + DEFAULT_EMAIL_ID);

        // Get all the inPreferenceUserList where emailId equals to UPDATED_EMAIL_ID
        defaultInPreferenceUserShouldNotBeFound("emailId.equals=" + UPDATED_EMAIL_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByEmailIdIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where emailId in DEFAULT_EMAIL_ID or UPDATED_EMAIL_ID
        defaultInPreferenceUserShouldBeFound("emailId.in=" + DEFAULT_EMAIL_ID + "," + UPDATED_EMAIL_ID);

        // Get all the inPreferenceUserList where emailId equals to UPDATED_EMAIL_ID
        defaultInPreferenceUserShouldNotBeFound("emailId.in=" + UPDATED_EMAIL_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByEmailIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where emailId is not null
        defaultInPreferenceUserShouldBeFound("emailId.specified=true");

        // Get all the inPreferenceUserList where emailId is null
        defaultInPreferenceUserShouldNotBeFound("emailId.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByEmailIdContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where emailId contains DEFAULT_EMAIL_ID
        defaultInPreferenceUserShouldBeFound("emailId.contains=" + DEFAULT_EMAIL_ID);

        // Get all the inPreferenceUserList where emailId contains UPDATED_EMAIL_ID
        defaultInPreferenceUserShouldNotBeFound("emailId.contains=" + UPDATED_EMAIL_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByEmailIdNotContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where emailId does not contain DEFAULT_EMAIL_ID
        defaultInPreferenceUserShouldNotBeFound("emailId.doesNotContain=" + DEFAULT_EMAIL_ID);

        // Get all the inPreferenceUserList where emailId does not contain UPDATED_EMAIL_ID
        defaultInPreferenceUserShouldBeFound("emailId.doesNotContain=" + UPDATED_EMAIL_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLoginIdIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where loginId equals to DEFAULT_LOGIN_ID
        defaultInPreferenceUserShouldBeFound("loginId.equals=" + DEFAULT_LOGIN_ID);

        // Get all the inPreferenceUserList where loginId equals to UPDATED_LOGIN_ID
        defaultInPreferenceUserShouldNotBeFound("loginId.equals=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLoginIdIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where loginId in DEFAULT_LOGIN_ID or UPDATED_LOGIN_ID
        defaultInPreferenceUserShouldBeFound("loginId.in=" + DEFAULT_LOGIN_ID + "," + UPDATED_LOGIN_ID);

        // Get all the inPreferenceUserList where loginId equals to UPDATED_LOGIN_ID
        defaultInPreferenceUserShouldNotBeFound("loginId.in=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLoginIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where loginId is not null
        defaultInPreferenceUserShouldBeFound("loginId.specified=true");

        // Get all the inPreferenceUserList where loginId is null
        defaultInPreferenceUserShouldNotBeFound("loginId.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLoginIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where loginId is greater than or equal to DEFAULT_LOGIN_ID
        defaultInPreferenceUserShouldBeFound("loginId.greaterThanOrEqual=" + DEFAULT_LOGIN_ID);

        // Get all the inPreferenceUserList where loginId is greater than or equal to UPDATED_LOGIN_ID
        defaultInPreferenceUserShouldNotBeFound("loginId.greaterThanOrEqual=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLoginIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where loginId is less than or equal to DEFAULT_LOGIN_ID
        defaultInPreferenceUserShouldBeFound("loginId.lessThanOrEqual=" + DEFAULT_LOGIN_ID);

        // Get all the inPreferenceUserList where loginId is less than or equal to SMALLER_LOGIN_ID
        defaultInPreferenceUserShouldNotBeFound("loginId.lessThanOrEqual=" + SMALLER_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLoginIdIsLessThanSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where loginId is less than DEFAULT_LOGIN_ID
        defaultInPreferenceUserShouldNotBeFound("loginId.lessThan=" + DEFAULT_LOGIN_ID);

        // Get all the inPreferenceUserList where loginId is less than UPDATED_LOGIN_ID
        defaultInPreferenceUserShouldBeFound("loginId.lessThan=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLoginIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where loginId is greater than DEFAULT_LOGIN_ID
        defaultInPreferenceUserShouldNotBeFound("loginId.greaterThan=" + DEFAULT_LOGIN_ID);

        // Get all the inPreferenceUserList where loginId is greater than SMALLER_LOGIN_ID
        defaultInPreferenceUserShouldBeFound("loginId.greaterThan=" + SMALLER_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersBySecretIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where secret equals to DEFAULT_SECRET
        defaultInPreferenceUserShouldBeFound("secret.equals=" + DEFAULT_SECRET);

        // Get all the inPreferenceUserList where secret equals to UPDATED_SECRET
        defaultInPreferenceUserShouldNotBeFound("secret.equals=" + UPDATED_SECRET);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersBySecretIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where secret in DEFAULT_SECRET or UPDATED_SECRET
        defaultInPreferenceUserShouldBeFound("secret.in=" + DEFAULT_SECRET + "," + UPDATED_SECRET);

        // Get all the inPreferenceUserList where secret equals to UPDATED_SECRET
        defaultInPreferenceUserShouldNotBeFound("secret.in=" + UPDATED_SECRET);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersBySecretIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where secret is not null
        defaultInPreferenceUserShouldBeFound("secret.specified=true");

        // Get all the inPreferenceUserList where secret is null
        defaultInPreferenceUserShouldNotBeFound("secret.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersBySecretContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where secret contains DEFAULT_SECRET
        defaultInPreferenceUserShouldBeFound("secret.contains=" + DEFAULT_SECRET);

        // Get all the inPreferenceUserList where secret contains UPDATED_SECRET
        defaultInPreferenceUserShouldNotBeFound("secret.contains=" + UPDATED_SECRET);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersBySecretNotContainsSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where secret does not contain DEFAULT_SECRET
        defaultInPreferenceUserShouldNotBeFound("secret.doesNotContain=" + DEFAULT_SECRET);

        // Get all the inPreferenceUserList where secret does not contain UPDATED_SECRET
        defaultInPreferenceUserShouldBeFound("secret.doesNotContain=" + UPDATED_SECRET);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdDate equals to DEFAULT_CREATED_DATE
        defaultInPreferenceUserShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the inPreferenceUserList where createdDate equals to UPDATED_CREATED_DATE
        defaultInPreferenceUserShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultInPreferenceUserShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the inPreferenceUserList where createdDate equals to UPDATED_CREATED_DATE
        defaultInPreferenceUserShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdDate is not null
        defaultInPreferenceUserShouldBeFound("createdDate.specified=true");

        // Get all the inPreferenceUserList where createdDate is null
        defaultInPreferenceUserShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdBy equals to DEFAULT_CREATED_BY
        defaultInPreferenceUserShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the inPreferenceUserList where createdBy equals to UPDATED_CREATED_BY
        defaultInPreferenceUserShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultInPreferenceUserShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the inPreferenceUserList where createdBy equals to UPDATED_CREATED_BY
        defaultInPreferenceUserShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdBy is not null
        defaultInPreferenceUserShouldBeFound("createdBy.specified=true");

        // Get all the inPreferenceUserList where createdBy is null
        defaultInPreferenceUserShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdBy is greater than or equal to DEFAULT_CREATED_BY
        defaultInPreferenceUserShouldBeFound("createdBy.greaterThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the inPreferenceUserList where createdBy is greater than or equal to UPDATED_CREATED_BY
        defaultInPreferenceUserShouldNotBeFound("createdBy.greaterThanOrEqual=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdBy is less than or equal to DEFAULT_CREATED_BY
        defaultInPreferenceUserShouldBeFound("createdBy.lessThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the inPreferenceUserList where createdBy is less than or equal to SMALLER_CREATED_BY
        defaultInPreferenceUserShouldNotBeFound("createdBy.lessThanOrEqual=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdBy is less than DEFAULT_CREATED_BY
        defaultInPreferenceUserShouldNotBeFound("createdBy.lessThan=" + DEFAULT_CREATED_BY);

        // Get all the inPreferenceUserList where createdBy is less than UPDATED_CREATED_BY
        defaultInPreferenceUserShouldBeFound("createdBy.lessThan=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByCreatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where createdBy is greater than DEFAULT_CREATED_BY
        defaultInPreferenceUserShouldNotBeFound("createdBy.greaterThan=" + DEFAULT_CREATED_BY);

        // Get all the inPreferenceUserList where createdBy is greater than SMALLER_CREATED_BY
        defaultInPreferenceUserShouldBeFound("createdBy.greaterThan=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultInPreferenceUserShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the inPreferenceUserList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultInPreferenceUserShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultInPreferenceUserShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the inPreferenceUserList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultInPreferenceUserShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedDate is not null
        defaultInPreferenceUserShouldBeFound("updatedDate.specified=true");

        // Get all the inPreferenceUserList where updatedDate is null
        defaultInPreferenceUserShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultInPreferenceUserShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the inPreferenceUserList where updatedBy equals to UPDATED_UPDATED_BY
        defaultInPreferenceUserShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultInPreferenceUserShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the inPreferenceUserList where updatedBy equals to UPDATED_UPDATED_BY
        defaultInPreferenceUserShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedBy is not null
        defaultInPreferenceUserShouldBeFound("updatedBy.specified=true");

        // Get all the inPreferenceUserList where updatedBy is null
        defaultInPreferenceUserShouldNotBeFound("updatedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedBy is greater than or equal to DEFAULT_UPDATED_BY
        defaultInPreferenceUserShouldBeFound("updatedBy.greaterThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the inPreferenceUserList where updatedBy is greater than or equal to UPDATED_UPDATED_BY
        defaultInPreferenceUserShouldNotBeFound("updatedBy.greaterThanOrEqual=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedBy is less than or equal to DEFAULT_UPDATED_BY
        defaultInPreferenceUserShouldBeFound("updatedBy.lessThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the inPreferenceUserList where updatedBy is less than or equal to SMALLER_UPDATED_BY
        defaultInPreferenceUserShouldNotBeFound("updatedBy.lessThanOrEqual=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedBy is less than DEFAULT_UPDATED_BY
        defaultInPreferenceUserShouldNotBeFound("updatedBy.lessThan=" + DEFAULT_UPDATED_BY);

        // Get all the inPreferenceUserList where updatedBy is less than UPDATED_UPDATED_BY
        defaultInPreferenceUserShouldBeFound("updatedBy.lessThan=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUpdatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where updatedBy is greater than DEFAULT_UPDATED_BY
        defaultInPreferenceUserShouldNotBeFound("updatedBy.greaterThan=" + DEFAULT_UPDATED_BY);

        // Get all the inPreferenceUserList where updatedBy is greater than SMALLER_UPDATED_BY
        defaultInPreferenceUserShouldBeFound("updatedBy.greaterThan=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where status equals to DEFAULT_STATUS
        defaultInPreferenceUserShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the inPreferenceUserList where status equals to UPDATED_STATUS
        defaultInPreferenceUserShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultInPreferenceUserShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the inPreferenceUserList where status equals to UPDATED_STATUS
        defaultInPreferenceUserShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        // Get all the inPreferenceUserList where status is not null
        defaultInPreferenceUserShouldBeFound("status.specified=true");

        // Get all the inPreferenceUserList where status is null
        defaultInPreferenceUserShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUserEmailsIsEqualToSomething() throws Exception {
        UserEmails userEmails;
        if (TestUtil.findAll(em, UserEmails.class).isEmpty()) {
            inPreferenceUserRepository.saveAndFlush(inPreferenceUser);
            userEmails = UserEmailsResourceIT.createEntity(em);
        } else {
            userEmails = TestUtil.findAll(em, UserEmails.class).get(0);
        }
        em.persist(userEmails);
        em.flush();
        inPreferenceUser.addUserEmails(userEmails);
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);
        Long userEmailsId = userEmails.getId();

        // Get all the inPreferenceUserList where userEmails equals to userEmailsId
        defaultInPreferenceUserShouldBeFound("userEmailsId.equals=" + userEmailsId);

        // Get all the inPreferenceUserList where userEmails equals to (userEmailsId + 1)
        defaultInPreferenceUserShouldNotBeFound("userEmailsId.equals=" + (userEmailsId + 1));
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByLoginCountIsEqualToSomething() throws Exception {
        LoginCount loginCount;
        if (TestUtil.findAll(em, LoginCount.class).isEmpty()) {
            inPreferenceUserRepository.saveAndFlush(inPreferenceUser);
            loginCount = LoginCountResourceIT.createEntity(em);
        } else {
            loginCount = TestUtil.findAll(em, LoginCount.class).get(0);
        }
        em.persist(loginCount);
        em.flush();
        inPreferenceUser.addLoginCount(loginCount);
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);
        Long loginCountId = loginCount.getId();

        // Get all the inPreferenceUserList where loginCount equals to loginCountId
        defaultInPreferenceUserShouldBeFound("loginCountId.equals=" + loginCountId);

        // Get all the inPreferenceUserList where loginCount equals to (loginCountId + 1)
        defaultInPreferenceUserShouldNotBeFound("loginCountId.equals=" + (loginCountId + 1));
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByUserRolePermissionIsEqualToSomething() throws Exception {
        UserRolePermission userRolePermission;
        if (TestUtil.findAll(em, UserRolePermission.class).isEmpty()) {
            inPreferenceUserRepository.saveAndFlush(inPreferenceUser);
            userRolePermission = UserRolePermissionResourceIT.createEntity(em);
        } else {
            userRolePermission = TestUtil.findAll(em, UserRolePermission.class).get(0);
        }
        em.persist(userRolePermission);
        em.flush();
        inPreferenceUser.addUserRolePermission(userRolePermission);
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);
        Long userRolePermissionId = userRolePermission.getId();

        // Get all the inPreferenceUserList where userRolePermission equals to userRolePermissionId
        defaultInPreferenceUserShouldBeFound("userRolePermissionId.equals=" + userRolePermissionId);

        // Get all the inPreferenceUserList where userRolePermission equals to (userRolePermissionId + 1)
        defaultInPreferenceUserShouldNotBeFound("userRolePermissionId.equals=" + (userRolePermissionId + 1));
    }

    @Test
    @Transactional
    void getAllInPreferenceUsersByRoleMasterIsEqualToSomething() throws Exception {
        RoleMaster roleMaster;
        if (TestUtil.findAll(em, RoleMaster.class).isEmpty()) {
            inPreferenceUserRepository.saveAndFlush(inPreferenceUser);
            roleMaster = RoleMasterResourceIT.createEntity(em);
        } else {
            roleMaster = TestUtil.findAll(em, RoleMaster.class).get(0);
        }
        em.persist(roleMaster);
        em.flush();
        inPreferenceUser.addRoleMaster(roleMaster);
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);
        Long roleMasterId = roleMaster.getId();

        // Get all the inPreferenceUserList where roleMaster equals to roleMasterId
        defaultInPreferenceUserShouldBeFound("roleMasterId.equals=" + roleMasterId);

        // Get all the inPreferenceUserList where roleMaster equals to (roleMasterId + 1)
        defaultInPreferenceUserShouldNotBeFound("roleMasterId.equals=" + (roleMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultInPreferenceUserShouldBeFound(String filter) throws Exception {
        restInPreferenceUserMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inPreferenceUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].mobileNo").value(hasItem(DEFAULT_MOBILE_NO)))
            .andExpect(jsonPath("$.[*].profileUrl").value(hasItem(DEFAULT_PROFILE_URL)))
            .andExpect(jsonPath("$.[*].userType").value(hasItem(DEFAULT_USER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].emailId").value(hasItem(DEFAULT_EMAIL_ID)))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID.intValue())))
            .andExpect(jsonPath("$.[*].secret").value(hasItem(DEFAULT_SECRET)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));

        // Check, that the count call also returns 1
        restInPreferenceUserMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultInPreferenceUserShouldNotBeFound(String filter) throws Exception {
        restInPreferenceUserMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restInPreferenceUserMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingInPreferenceUser() throws Exception {
        // Get the inPreferenceUser
        restInPreferenceUserMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingInPreferenceUser() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        int databaseSizeBeforeUpdate = inPreferenceUserRepository.findAll().size();

        // Update the inPreferenceUser
        InPreferenceUser updatedInPreferenceUser = inPreferenceUserRepository.findById(inPreferenceUser.getId()).get();
        // Disconnect from session so that the updates on updatedInPreferenceUser are not directly saved in db
        em.detach(updatedInPreferenceUser);
        updatedInPreferenceUser
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .mobileNo(UPDATED_MOBILE_NO)
            .profileUrl(UPDATED_PROFILE_URL)
            .userType(UPDATED_USER_TYPE)
            .emailId(UPDATED_EMAIL_ID)
            .loginId(UPDATED_LOGIN_ID)
            .secret(UPDATED_SECRET)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(updatedInPreferenceUser);

        restInPreferenceUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, inPreferenceUserDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isOk());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeUpdate);
        InPreferenceUser testInPreferenceUser = inPreferenceUserList.get(inPreferenceUserList.size() - 1);
        assertThat(testInPreferenceUser.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testInPreferenceUser.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testInPreferenceUser.getMobileNo()).isEqualTo(UPDATED_MOBILE_NO);
        assertThat(testInPreferenceUser.getProfileUrl()).isEqualTo(UPDATED_PROFILE_URL);
        assertThat(testInPreferenceUser.getUserType()).isEqualTo(UPDATED_USER_TYPE);
        assertThat(testInPreferenceUser.getEmailId()).isEqualTo(UPDATED_EMAIL_ID);
        assertThat(testInPreferenceUser.getLoginId()).isEqualTo(UPDATED_LOGIN_ID);
        assertThat(testInPreferenceUser.getSecret()).isEqualTo(UPDATED_SECRET);
        assertThat(testInPreferenceUser.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testInPreferenceUser.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testInPreferenceUser.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testInPreferenceUser.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testInPreferenceUser.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingInPreferenceUser() throws Exception {
        int databaseSizeBeforeUpdate = inPreferenceUserRepository.findAll().size();
        inPreferenceUser.setId(count.incrementAndGet());

        // Create the InPreferenceUser
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInPreferenceUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, inPreferenceUserDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchInPreferenceUser() throws Exception {
        int databaseSizeBeforeUpdate = inPreferenceUserRepository.findAll().size();
        inPreferenceUser.setId(count.incrementAndGet());

        // Create the InPreferenceUser
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInPreferenceUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamInPreferenceUser() throws Exception {
        int databaseSizeBeforeUpdate = inPreferenceUserRepository.findAll().size();
        inPreferenceUser.setId(count.incrementAndGet());

        // Create the InPreferenceUser
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInPreferenceUserMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateInPreferenceUserWithPatch() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        int databaseSizeBeforeUpdate = inPreferenceUserRepository.findAll().size();

        // Update the inPreferenceUser using partial update
        InPreferenceUser partialUpdatedInPreferenceUser = new InPreferenceUser();
        partialUpdatedInPreferenceUser.setId(inPreferenceUser.getId());

        partialUpdatedInPreferenceUser
            .lastName(UPDATED_LAST_NAME)
            .mobileNo(UPDATED_MOBILE_NO)
            .userType(UPDATED_USER_TYPE)
            .emailId(UPDATED_EMAIL_ID)
            .secret(UPDATED_SECRET)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);

        restInPreferenceUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInPreferenceUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInPreferenceUser))
            )
            .andExpect(status().isOk());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeUpdate);
        InPreferenceUser testInPreferenceUser = inPreferenceUserList.get(inPreferenceUserList.size() - 1);
        assertThat(testInPreferenceUser.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testInPreferenceUser.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testInPreferenceUser.getMobileNo()).isEqualTo(UPDATED_MOBILE_NO);
        assertThat(testInPreferenceUser.getProfileUrl()).isEqualTo(DEFAULT_PROFILE_URL);
        assertThat(testInPreferenceUser.getUserType()).isEqualTo(UPDATED_USER_TYPE);
        assertThat(testInPreferenceUser.getEmailId()).isEqualTo(UPDATED_EMAIL_ID);
        assertThat(testInPreferenceUser.getLoginId()).isEqualTo(DEFAULT_LOGIN_ID);
        assertThat(testInPreferenceUser.getSecret()).isEqualTo(UPDATED_SECRET);
        assertThat(testInPreferenceUser.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testInPreferenceUser.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testInPreferenceUser.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testInPreferenceUser.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testInPreferenceUser.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateInPreferenceUserWithPatch() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        int databaseSizeBeforeUpdate = inPreferenceUserRepository.findAll().size();

        // Update the inPreferenceUser using partial update
        InPreferenceUser partialUpdatedInPreferenceUser = new InPreferenceUser();
        partialUpdatedInPreferenceUser.setId(inPreferenceUser.getId());

        partialUpdatedInPreferenceUser
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .mobileNo(UPDATED_MOBILE_NO)
            .profileUrl(UPDATED_PROFILE_URL)
            .userType(UPDATED_USER_TYPE)
            .emailId(UPDATED_EMAIL_ID)
            .loginId(UPDATED_LOGIN_ID)
            .secret(UPDATED_SECRET)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);

        restInPreferenceUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInPreferenceUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInPreferenceUser))
            )
            .andExpect(status().isOk());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeUpdate);
        InPreferenceUser testInPreferenceUser = inPreferenceUserList.get(inPreferenceUserList.size() - 1);
        assertThat(testInPreferenceUser.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testInPreferenceUser.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testInPreferenceUser.getMobileNo()).isEqualTo(UPDATED_MOBILE_NO);
        assertThat(testInPreferenceUser.getProfileUrl()).isEqualTo(UPDATED_PROFILE_URL);
        assertThat(testInPreferenceUser.getUserType()).isEqualTo(UPDATED_USER_TYPE);
        assertThat(testInPreferenceUser.getEmailId()).isEqualTo(UPDATED_EMAIL_ID);
        assertThat(testInPreferenceUser.getLoginId()).isEqualTo(UPDATED_LOGIN_ID);
        assertThat(testInPreferenceUser.getSecret()).isEqualTo(UPDATED_SECRET);
        assertThat(testInPreferenceUser.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testInPreferenceUser.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testInPreferenceUser.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testInPreferenceUser.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testInPreferenceUser.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingInPreferenceUser() throws Exception {
        int databaseSizeBeforeUpdate = inPreferenceUserRepository.findAll().size();
        inPreferenceUser.setId(count.incrementAndGet());

        // Create the InPreferenceUser
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInPreferenceUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, inPreferenceUserDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchInPreferenceUser() throws Exception {
        int databaseSizeBeforeUpdate = inPreferenceUserRepository.findAll().size();
        inPreferenceUser.setId(count.incrementAndGet());

        // Create the InPreferenceUser
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInPreferenceUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamInPreferenceUser() throws Exception {
        int databaseSizeBeforeUpdate = inPreferenceUserRepository.findAll().size();
        inPreferenceUser.setId(count.incrementAndGet());

        // Create the InPreferenceUser
        InPreferenceUserDTO inPreferenceUserDTO = inPreferenceUserMapper.toDto(inPreferenceUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInPreferenceUserMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(inPreferenceUserDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the InPreferenceUser in the database
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteInPreferenceUser() throws Exception {
        // Initialize the database
        inPreferenceUserRepository.saveAndFlush(inPreferenceUser);

        int databaseSizeBeforeDelete = inPreferenceUserRepository.findAll().size();

        // Delete the inPreferenceUser
        restInPreferenceUserMockMvc
            .perform(delete(ENTITY_API_URL_ID, inPreferenceUser.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<InPreferenceUser> inPreferenceUserList = inPreferenceUserRepository.findAll();
        assertThat(inPreferenceUserList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
