package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.Partner;
import com.user.myapp.domain.enumeration.Partnertype;
import com.user.myapp.repository.PartnerRepository;
import com.user.myapp.service.criteria.PartnerCriteria;
import com.user.myapp.service.dto.PartnerDTO;
import com.user.myapp.service.mapper.PartnerMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PartnerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PartnerResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DOMAIN = "AAAAAAAAAA";
    private static final String UPDATED_DOMAIN = "BBBBBBBBBB";

    private static final Double DEFAULT_PARTNER_COMMISSION = 1D;
    private static final Double UPDATED_PARTNER_COMMISSION = 2D;
    private static final Double SMALLER_PARTNER_COMMISSION = 1D - 1D;

    private static final Partnertype DEFAULT_PARTNER_TYPE = Partnertype.ActivePartner;
    private static final Partnertype UPDATED_PARTNER_TYPE = Partnertype.SecretPartner;

    private static final Integer DEFAULT_VALUE_M = 1;
    private static final Integer UPDATED_VALUE_M = 2;
    private static final Integer SMALLER_VALUE_M = 1 - 1;

    private static final Integer DEFAULT_VALUE_C = 1;
    private static final Integer UPDATED_VALUE_C = 2;
    private static final Integer SMALLER_VALUE_C = 1 - 1;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_CREATED_BY = 1L;
    private static final Long UPDATED_CREATED_BY = 2L;
    private static final Long SMALLER_CREATED_BY = 1L - 1L;

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_UPDATED_BY = 1L;
    private static final Long UPDATED_UPDATED_BY = 2L;
    private static final Long SMALLER_UPDATED_BY = 1L - 1L;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final String ENTITY_API_URL = "/api/partners";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PartnerRepository partnerRepository;

    @Autowired
    private PartnerMapper partnerMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPartnerMockMvc;

    private Partner partner;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Partner createEntity(EntityManager em) {
        Partner partner = new Partner()
            .name(DEFAULT_NAME)
            .domain(DEFAULT_DOMAIN)
            .partnerCommission(DEFAULT_PARTNER_COMMISSION)
            .partnerType(DEFAULT_PARTNER_TYPE)
            .valueM(DEFAULT_VALUE_M)
            .valueC(DEFAULT_VALUE_C)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .status(DEFAULT_STATUS);
        return partner;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Partner createUpdatedEntity(EntityManager em) {
        Partner partner = new Partner()
            .name(UPDATED_NAME)
            .domain(UPDATED_DOMAIN)
            .partnerCommission(UPDATED_PARTNER_COMMISSION)
            .partnerType(UPDATED_PARTNER_TYPE)
            .valueM(UPDATED_VALUE_M)
            .valueC(UPDATED_VALUE_C)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);
        return partner;
    }

    @BeforeEach
    public void initTest() {
        partner = createEntity(em);
    }

    @Test
    @Transactional
    void createPartner() throws Exception {
        int databaseSizeBeforeCreate = partnerRepository.findAll().size();
        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);
        restPartnerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerDTO)))
            .andExpect(status().isCreated());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeCreate + 1);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPartner.getDomain()).isEqualTo(DEFAULT_DOMAIN);
        assertThat(testPartner.getPartnerCommission()).isEqualTo(DEFAULT_PARTNER_COMMISSION);
        assertThat(testPartner.getPartnerType()).isEqualTo(DEFAULT_PARTNER_TYPE);
        assertThat(testPartner.getValueM()).isEqualTo(DEFAULT_VALUE_M);
        assertThat(testPartner.getValueC()).isEqualTo(DEFAULT_VALUE_C);
        assertThat(testPartner.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPartner.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPartner.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testPartner.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testPartner.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createPartnerWithExistingId() throws Exception {
        // Create the Partner with an existing ID
        partner.setId(1L);
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        int databaseSizeBeforeCreate = partnerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPartnerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPartners() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partner.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].domain").value(hasItem(DEFAULT_DOMAIN)))
            .andExpect(jsonPath("$.[*].partnerCommission").value(hasItem(DEFAULT_PARTNER_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].partnerType").value(hasItem(DEFAULT_PARTNER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].valueM").value(hasItem(DEFAULT_VALUE_M)))
            .andExpect(jsonPath("$.[*].valueC").value(hasItem(DEFAULT_VALUE_C)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }

    @Test
    @Transactional
    void getPartner() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get the partner
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL_ID, partner.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(partner.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.domain").value(DEFAULT_DOMAIN))
            .andExpect(jsonPath("$.partnerCommission").value(DEFAULT_PARTNER_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.partnerType").value(DEFAULT_PARTNER_TYPE.toString()))
            .andExpect(jsonPath("$.valueM").value(DEFAULT_VALUE_M))
            .andExpect(jsonPath("$.valueC").value(DEFAULT_VALUE_C))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.intValue()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    void getPartnersByIdFiltering() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        Long id = partner.getId();

        defaultPartnerShouldBeFound("id.equals=" + id);
        defaultPartnerShouldNotBeFound("id.notEquals=" + id);

        defaultPartnerShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPartnerShouldNotBeFound("id.greaterThan=" + id);

        defaultPartnerShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPartnerShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPartnersByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name equals to DEFAULT_NAME
        defaultPartnerShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the partnerList where name equals to UPDATED_NAME
        defaultPartnerShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnersByNameIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPartnerShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the partnerList where name equals to UPDATED_NAME
        defaultPartnerShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnersByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name is not null
        defaultPartnerShouldBeFound("name.specified=true");

        // Get all the partnerList where name is null
        defaultPartnerShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByNameContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name contains DEFAULT_NAME
        defaultPartnerShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the partnerList where name contains UPDATED_NAME
        defaultPartnerShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnersByNameNotContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name does not contain DEFAULT_NAME
        defaultPartnerShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the partnerList where name does not contain UPDATED_NAME
        defaultPartnerShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnersByDomainIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where domain equals to DEFAULT_DOMAIN
        defaultPartnerShouldBeFound("domain.equals=" + DEFAULT_DOMAIN);

        // Get all the partnerList where domain equals to UPDATED_DOMAIN
        defaultPartnerShouldNotBeFound("domain.equals=" + UPDATED_DOMAIN);
    }

    @Test
    @Transactional
    void getAllPartnersByDomainIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where domain in DEFAULT_DOMAIN or UPDATED_DOMAIN
        defaultPartnerShouldBeFound("domain.in=" + DEFAULT_DOMAIN + "," + UPDATED_DOMAIN);

        // Get all the partnerList where domain equals to UPDATED_DOMAIN
        defaultPartnerShouldNotBeFound("domain.in=" + UPDATED_DOMAIN);
    }

    @Test
    @Transactional
    void getAllPartnersByDomainIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where domain is not null
        defaultPartnerShouldBeFound("domain.specified=true");

        // Get all the partnerList where domain is null
        defaultPartnerShouldNotBeFound("domain.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByDomainContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where domain contains DEFAULT_DOMAIN
        defaultPartnerShouldBeFound("domain.contains=" + DEFAULT_DOMAIN);

        // Get all the partnerList where domain contains UPDATED_DOMAIN
        defaultPartnerShouldNotBeFound("domain.contains=" + UPDATED_DOMAIN);
    }

    @Test
    @Transactional
    void getAllPartnersByDomainNotContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where domain does not contain DEFAULT_DOMAIN
        defaultPartnerShouldNotBeFound("domain.doesNotContain=" + DEFAULT_DOMAIN);

        // Get all the partnerList where domain does not contain UPDATED_DOMAIN
        defaultPartnerShouldBeFound("domain.doesNotContain=" + UPDATED_DOMAIN);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerCommissionIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerCommission equals to DEFAULT_PARTNER_COMMISSION
        defaultPartnerShouldBeFound("partnerCommission.equals=" + DEFAULT_PARTNER_COMMISSION);

        // Get all the partnerList where partnerCommission equals to UPDATED_PARTNER_COMMISSION
        defaultPartnerShouldNotBeFound("partnerCommission.equals=" + UPDATED_PARTNER_COMMISSION);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerCommissionIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerCommission in DEFAULT_PARTNER_COMMISSION or UPDATED_PARTNER_COMMISSION
        defaultPartnerShouldBeFound("partnerCommission.in=" + DEFAULT_PARTNER_COMMISSION + "," + UPDATED_PARTNER_COMMISSION);

        // Get all the partnerList where partnerCommission equals to UPDATED_PARTNER_COMMISSION
        defaultPartnerShouldNotBeFound("partnerCommission.in=" + UPDATED_PARTNER_COMMISSION);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerCommissionIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerCommission is not null
        defaultPartnerShouldBeFound("partnerCommission.specified=true");

        // Get all the partnerList where partnerCommission is null
        defaultPartnerShouldNotBeFound("partnerCommission.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerCommissionIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerCommission is greater than or equal to DEFAULT_PARTNER_COMMISSION
        defaultPartnerShouldBeFound("partnerCommission.greaterThanOrEqual=" + DEFAULT_PARTNER_COMMISSION);

        // Get all the partnerList where partnerCommission is greater than or equal to UPDATED_PARTNER_COMMISSION
        defaultPartnerShouldNotBeFound("partnerCommission.greaterThanOrEqual=" + UPDATED_PARTNER_COMMISSION);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerCommissionIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerCommission is less than or equal to DEFAULT_PARTNER_COMMISSION
        defaultPartnerShouldBeFound("partnerCommission.lessThanOrEqual=" + DEFAULT_PARTNER_COMMISSION);

        // Get all the partnerList where partnerCommission is less than or equal to SMALLER_PARTNER_COMMISSION
        defaultPartnerShouldNotBeFound("partnerCommission.lessThanOrEqual=" + SMALLER_PARTNER_COMMISSION);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerCommissionIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerCommission is less than DEFAULT_PARTNER_COMMISSION
        defaultPartnerShouldNotBeFound("partnerCommission.lessThan=" + DEFAULT_PARTNER_COMMISSION);

        // Get all the partnerList where partnerCommission is less than UPDATED_PARTNER_COMMISSION
        defaultPartnerShouldBeFound("partnerCommission.lessThan=" + UPDATED_PARTNER_COMMISSION);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerCommissionIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerCommission is greater than DEFAULT_PARTNER_COMMISSION
        defaultPartnerShouldNotBeFound("partnerCommission.greaterThan=" + DEFAULT_PARTNER_COMMISSION);

        // Get all the partnerList where partnerCommission is greater than SMALLER_PARTNER_COMMISSION
        defaultPartnerShouldBeFound("partnerCommission.greaterThan=" + SMALLER_PARTNER_COMMISSION);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerType equals to DEFAULT_PARTNER_TYPE
        defaultPartnerShouldBeFound("partnerType.equals=" + DEFAULT_PARTNER_TYPE);

        // Get all the partnerList where partnerType equals to UPDATED_PARTNER_TYPE
        defaultPartnerShouldNotBeFound("partnerType.equals=" + UPDATED_PARTNER_TYPE);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerTypeIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerType in DEFAULT_PARTNER_TYPE or UPDATED_PARTNER_TYPE
        defaultPartnerShouldBeFound("partnerType.in=" + DEFAULT_PARTNER_TYPE + "," + UPDATED_PARTNER_TYPE);

        // Get all the partnerList where partnerType equals to UPDATED_PARTNER_TYPE
        defaultPartnerShouldNotBeFound("partnerType.in=" + UPDATED_PARTNER_TYPE);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerType is not null
        defaultPartnerShouldBeFound("partnerType.specified=true");

        // Get all the partnerList where partnerType is null
        defaultPartnerShouldNotBeFound("partnerType.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByValueMIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueM equals to DEFAULT_VALUE_M
        defaultPartnerShouldBeFound("valueM.equals=" + DEFAULT_VALUE_M);

        // Get all the partnerList where valueM equals to UPDATED_VALUE_M
        defaultPartnerShouldNotBeFound("valueM.equals=" + UPDATED_VALUE_M);
    }

    @Test
    @Transactional
    void getAllPartnersByValueMIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueM in DEFAULT_VALUE_M or UPDATED_VALUE_M
        defaultPartnerShouldBeFound("valueM.in=" + DEFAULT_VALUE_M + "," + UPDATED_VALUE_M);

        // Get all the partnerList where valueM equals to UPDATED_VALUE_M
        defaultPartnerShouldNotBeFound("valueM.in=" + UPDATED_VALUE_M);
    }

    @Test
    @Transactional
    void getAllPartnersByValueMIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueM is not null
        defaultPartnerShouldBeFound("valueM.specified=true");

        // Get all the partnerList where valueM is null
        defaultPartnerShouldNotBeFound("valueM.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByValueMIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueM is greater than or equal to DEFAULT_VALUE_M
        defaultPartnerShouldBeFound("valueM.greaterThanOrEqual=" + DEFAULT_VALUE_M);

        // Get all the partnerList where valueM is greater than or equal to UPDATED_VALUE_M
        defaultPartnerShouldNotBeFound("valueM.greaterThanOrEqual=" + UPDATED_VALUE_M);
    }

    @Test
    @Transactional
    void getAllPartnersByValueMIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueM is less than or equal to DEFAULT_VALUE_M
        defaultPartnerShouldBeFound("valueM.lessThanOrEqual=" + DEFAULT_VALUE_M);

        // Get all the partnerList where valueM is less than or equal to SMALLER_VALUE_M
        defaultPartnerShouldNotBeFound("valueM.lessThanOrEqual=" + SMALLER_VALUE_M);
    }

    @Test
    @Transactional
    void getAllPartnersByValueMIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueM is less than DEFAULT_VALUE_M
        defaultPartnerShouldNotBeFound("valueM.lessThan=" + DEFAULT_VALUE_M);

        // Get all the partnerList where valueM is less than UPDATED_VALUE_M
        defaultPartnerShouldBeFound("valueM.lessThan=" + UPDATED_VALUE_M);
    }

    @Test
    @Transactional
    void getAllPartnersByValueMIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueM is greater than DEFAULT_VALUE_M
        defaultPartnerShouldNotBeFound("valueM.greaterThan=" + DEFAULT_VALUE_M);

        // Get all the partnerList where valueM is greater than SMALLER_VALUE_M
        defaultPartnerShouldBeFound("valueM.greaterThan=" + SMALLER_VALUE_M);
    }

    @Test
    @Transactional
    void getAllPartnersByValueCIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueC equals to DEFAULT_VALUE_C
        defaultPartnerShouldBeFound("valueC.equals=" + DEFAULT_VALUE_C);

        // Get all the partnerList where valueC equals to UPDATED_VALUE_C
        defaultPartnerShouldNotBeFound("valueC.equals=" + UPDATED_VALUE_C);
    }

    @Test
    @Transactional
    void getAllPartnersByValueCIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueC in DEFAULT_VALUE_C or UPDATED_VALUE_C
        defaultPartnerShouldBeFound("valueC.in=" + DEFAULT_VALUE_C + "," + UPDATED_VALUE_C);

        // Get all the partnerList where valueC equals to UPDATED_VALUE_C
        defaultPartnerShouldNotBeFound("valueC.in=" + UPDATED_VALUE_C);
    }

    @Test
    @Transactional
    void getAllPartnersByValueCIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueC is not null
        defaultPartnerShouldBeFound("valueC.specified=true");

        // Get all the partnerList where valueC is null
        defaultPartnerShouldNotBeFound("valueC.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByValueCIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueC is greater than or equal to DEFAULT_VALUE_C
        defaultPartnerShouldBeFound("valueC.greaterThanOrEqual=" + DEFAULT_VALUE_C);

        // Get all the partnerList where valueC is greater than or equal to UPDATED_VALUE_C
        defaultPartnerShouldNotBeFound("valueC.greaterThanOrEqual=" + UPDATED_VALUE_C);
    }

    @Test
    @Transactional
    void getAllPartnersByValueCIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueC is less than or equal to DEFAULT_VALUE_C
        defaultPartnerShouldBeFound("valueC.lessThanOrEqual=" + DEFAULT_VALUE_C);

        // Get all the partnerList where valueC is less than or equal to SMALLER_VALUE_C
        defaultPartnerShouldNotBeFound("valueC.lessThanOrEqual=" + SMALLER_VALUE_C);
    }

    @Test
    @Transactional
    void getAllPartnersByValueCIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueC is less than DEFAULT_VALUE_C
        defaultPartnerShouldNotBeFound("valueC.lessThan=" + DEFAULT_VALUE_C);

        // Get all the partnerList where valueC is less than UPDATED_VALUE_C
        defaultPartnerShouldBeFound("valueC.lessThan=" + UPDATED_VALUE_C);
    }

    @Test
    @Transactional
    void getAllPartnersByValueCIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where valueC is greater than DEFAULT_VALUE_C
        defaultPartnerShouldNotBeFound("valueC.greaterThan=" + DEFAULT_VALUE_C);

        // Get all the partnerList where valueC is greater than SMALLER_VALUE_C
        defaultPartnerShouldBeFound("valueC.greaterThan=" + SMALLER_VALUE_C);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdDate equals to DEFAULT_CREATED_DATE
        defaultPartnerShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the partnerList where createdDate equals to UPDATED_CREATED_DATE
        defaultPartnerShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultPartnerShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the partnerList where createdDate equals to UPDATED_CREATED_DATE
        defaultPartnerShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdDate is not null
        defaultPartnerShouldBeFound("createdDate.specified=true");

        // Get all the partnerList where createdDate is null
        defaultPartnerShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy equals to DEFAULT_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the partnerList where createdBy equals to UPDATED_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the partnerList where createdBy equals to UPDATED_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy is not null
        defaultPartnerShouldBeFound("createdBy.specified=true");

        // Get all the partnerList where createdBy is null
        defaultPartnerShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy is greater than or equal to DEFAULT_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.greaterThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the partnerList where createdBy is greater than or equal to UPDATED_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.greaterThanOrEqual=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy is less than or equal to DEFAULT_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.lessThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the partnerList where createdBy is less than or equal to SMALLER_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.lessThanOrEqual=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy is less than DEFAULT_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.lessThan=" + DEFAULT_CREATED_BY);

        // Get all the partnerList where createdBy is less than UPDATED_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.lessThan=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy is greater than DEFAULT_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.greaterThan=" + DEFAULT_CREATED_BY);

        // Get all the partnerList where createdBy is greater than SMALLER_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.greaterThan=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultPartnerShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the partnerList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultPartnerShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultPartnerShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the partnerList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultPartnerShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedDate is not null
        defaultPartnerShouldBeFound("updatedDate.specified=true");

        // Get all the partnerList where updatedDate is null
        defaultPartnerShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultPartnerShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the partnerList where updatedBy equals to UPDATED_UPDATED_BY
        defaultPartnerShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultPartnerShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the partnerList where updatedBy equals to UPDATED_UPDATED_BY
        defaultPartnerShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedBy is not null
        defaultPartnerShouldBeFound("updatedBy.specified=true");

        // Get all the partnerList where updatedBy is null
        defaultPartnerShouldNotBeFound("updatedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedBy is greater than or equal to DEFAULT_UPDATED_BY
        defaultPartnerShouldBeFound("updatedBy.greaterThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the partnerList where updatedBy is greater than or equal to UPDATED_UPDATED_BY
        defaultPartnerShouldNotBeFound("updatedBy.greaterThanOrEqual=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedBy is less than or equal to DEFAULT_UPDATED_BY
        defaultPartnerShouldBeFound("updatedBy.lessThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the partnerList where updatedBy is less than or equal to SMALLER_UPDATED_BY
        defaultPartnerShouldNotBeFound("updatedBy.lessThanOrEqual=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedBy is less than DEFAULT_UPDATED_BY
        defaultPartnerShouldNotBeFound("updatedBy.lessThan=" + DEFAULT_UPDATED_BY);

        // Get all the partnerList where updatedBy is less than UPDATED_UPDATED_BY
        defaultPartnerShouldBeFound("updatedBy.lessThan=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByUpdatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where updatedBy is greater than DEFAULT_UPDATED_BY
        defaultPartnerShouldNotBeFound("updatedBy.greaterThan=" + DEFAULT_UPDATED_BY);

        // Get all the partnerList where updatedBy is greater than SMALLER_UPDATED_BY
        defaultPartnerShouldBeFound("updatedBy.greaterThan=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where status equals to DEFAULT_STATUS
        defaultPartnerShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the partnerList where status equals to UPDATED_STATUS
        defaultPartnerShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllPartnersByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultPartnerShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the partnerList where status equals to UPDATED_STATUS
        defaultPartnerShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllPartnersByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where status is not null
        defaultPartnerShouldBeFound("status.specified=true");

        // Get all the partnerList where status is null
        defaultPartnerShouldNotBeFound("status.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPartnerShouldBeFound(String filter) throws Exception {
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partner.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].domain").value(hasItem(DEFAULT_DOMAIN)))
            .andExpect(jsonPath("$.[*].partnerCommission").value(hasItem(DEFAULT_PARTNER_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].partnerType").value(hasItem(DEFAULT_PARTNER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].valueM").value(hasItem(DEFAULT_VALUE_M)))
            .andExpect(jsonPath("$.[*].valueC").value(hasItem(DEFAULT_VALUE_C)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));

        // Check, that the count call also returns 1
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPartnerShouldNotBeFound(String filter) throws Exception {
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPartner() throws Exception {
        // Get the partner
        restPartnerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPartner() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();

        // Update the partner
        Partner updatedPartner = partnerRepository.findById(partner.getId()).get();
        // Disconnect from session so that the updates on updatedPartner are not directly saved in db
        em.detach(updatedPartner);
        updatedPartner
            .name(UPDATED_NAME)
            .domain(UPDATED_DOMAIN)
            .partnerCommission(UPDATED_PARTNER_COMMISSION)
            .partnerType(UPDATED_PARTNER_TYPE)
            .valueM(UPDATED_VALUE_M)
            .valueC(UPDATED_VALUE_C)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);
        PartnerDTO partnerDTO = partnerMapper.toDto(updatedPartner);

        restPartnerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, partnerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isOk());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPartner.getDomain()).isEqualTo(UPDATED_DOMAIN);
        assertThat(testPartner.getPartnerCommission()).isEqualTo(UPDATED_PARTNER_COMMISSION);
        assertThat(testPartner.getPartnerType()).isEqualTo(UPDATED_PARTNER_TYPE);
        assertThat(testPartner.getValueM()).isEqualTo(UPDATED_VALUE_M);
        assertThat(testPartner.getValueC()).isEqualTo(UPDATED_VALUE_C);
        assertThat(testPartner.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPartner.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPartner.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testPartner.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testPartner.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, partnerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePartnerWithPatch() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();

        // Update the partner using partial update
        Partner partialUpdatedPartner = new Partner();
        partialUpdatedPartner.setId(partner.getId());

        partialUpdatedPartner
            .domain(UPDATED_DOMAIN)
            .partnerCommission(UPDATED_PARTNER_COMMISSION)
            .partnerType(UPDATED_PARTNER_TYPE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPartner.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPartner))
            )
            .andExpect(status().isOk());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPartner.getDomain()).isEqualTo(UPDATED_DOMAIN);
        assertThat(testPartner.getPartnerCommission()).isEqualTo(UPDATED_PARTNER_COMMISSION);
        assertThat(testPartner.getPartnerType()).isEqualTo(UPDATED_PARTNER_TYPE);
        assertThat(testPartner.getValueM()).isEqualTo(DEFAULT_VALUE_M);
        assertThat(testPartner.getValueC()).isEqualTo(DEFAULT_VALUE_C);
        assertThat(testPartner.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPartner.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPartner.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testPartner.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testPartner.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void fullUpdatePartnerWithPatch() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();

        // Update the partner using partial update
        Partner partialUpdatedPartner = new Partner();
        partialUpdatedPartner.setId(partner.getId());

        partialUpdatedPartner
            .name(UPDATED_NAME)
            .domain(UPDATED_DOMAIN)
            .partnerCommission(UPDATED_PARTNER_COMMISSION)
            .partnerType(UPDATED_PARTNER_TYPE)
            .valueM(UPDATED_VALUE_M)
            .valueC(UPDATED_VALUE_C)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);

        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPartner.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPartner))
            )
            .andExpect(status().isOk());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPartner.getDomain()).isEqualTo(UPDATED_DOMAIN);
        assertThat(testPartner.getPartnerCommission()).isEqualTo(UPDATED_PARTNER_COMMISSION);
        assertThat(testPartner.getPartnerType()).isEqualTo(UPDATED_PARTNER_TYPE);
        assertThat(testPartner.getValueM()).isEqualTo(UPDATED_VALUE_M);
        assertThat(testPartner.getValueC()).isEqualTo(UPDATED_VALUE_C);
        assertThat(testPartner.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPartner.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPartner.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testPartner.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testPartner.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partnerDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePartner() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        int databaseSizeBeforeDelete = partnerRepository.findAll().size();

        // Delete the partner
        restPartnerMockMvc
            .perform(delete(ENTITY_API_URL_ID, partner.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
