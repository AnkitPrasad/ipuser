package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.RoleAndPermissions;
import com.user.myapp.repository.RoleAndPermissionsRepository;
import com.user.myapp.service.criteria.RoleAndPermissionsCriteria;
import com.user.myapp.service.dto.RoleAndPermissionsDTO;
import com.user.myapp.service.mapper.RoleAndPermissionsMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RoleAndPermissionsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RoleAndPermissionsResourceIT {

    private static final Boolean DEFAULT_READ_ACCESS = false;
    private static final Boolean UPDATED_READ_ACCESS = true;

    private static final Boolean DEFAULT_CREATE_ACCESS = false;
    private static final Boolean UPDATED_CREATE_ACCESS = true;

    private static final Boolean DEFAULT_EDIT_ACCESS = false;
    private static final Boolean UPDATED_EDIT_ACCESS = true;

    private static final Boolean DEFAULT_DELETE_ACCESS = false;
    private static final Boolean UPDATED_DELETE_ACCESS = true;

    private static final String ENTITY_API_URL = "/api/role-and-permissions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RoleAndPermissionsRepository roleAndPermissionsRepository;

    @Autowired
    private RoleAndPermissionsMapper roleAndPermissionsMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRoleAndPermissionsMockMvc;

    private RoleAndPermissions roleAndPermissions;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RoleAndPermissions createEntity(EntityManager em) {
        RoleAndPermissions roleAndPermissions = new RoleAndPermissions()
            .readAccess(DEFAULT_READ_ACCESS)
            .createAccess(DEFAULT_CREATE_ACCESS)
            .editAccess(DEFAULT_EDIT_ACCESS)
            .deleteAccess(DEFAULT_DELETE_ACCESS);
        return roleAndPermissions;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RoleAndPermissions createUpdatedEntity(EntityManager em) {
        RoleAndPermissions roleAndPermissions = new RoleAndPermissions()
            .readAccess(UPDATED_READ_ACCESS)
            .createAccess(UPDATED_CREATE_ACCESS)
            .editAccess(UPDATED_EDIT_ACCESS)
            .deleteAccess(UPDATED_DELETE_ACCESS);
        return roleAndPermissions;
    }

    @BeforeEach
    public void initTest() {
        roleAndPermissions = createEntity(em);
    }

    @Test
    @Transactional
    void createRoleAndPermissions() throws Exception {
        int databaseSizeBeforeCreate = roleAndPermissionsRepository.findAll().size();
        // Create the RoleAndPermissions
        RoleAndPermissionsDTO roleAndPermissionsDTO = roleAndPermissionsMapper.toDto(roleAndPermissions);
        restRoleAndPermissionsMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(roleAndPermissionsDTO))
            )
            .andExpect(status().isCreated());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeCreate + 1);
        RoleAndPermissions testRoleAndPermissions = roleAndPermissionsList.get(roleAndPermissionsList.size() - 1);
        assertThat(testRoleAndPermissions.getReadAccess()).isEqualTo(DEFAULT_READ_ACCESS);
        assertThat(testRoleAndPermissions.getCreateAccess()).isEqualTo(DEFAULT_CREATE_ACCESS);
        assertThat(testRoleAndPermissions.getEditAccess()).isEqualTo(DEFAULT_EDIT_ACCESS);
        assertThat(testRoleAndPermissions.getDeleteAccess()).isEqualTo(DEFAULT_DELETE_ACCESS);
    }

    @Test
    @Transactional
    void createRoleAndPermissionsWithExistingId() throws Exception {
        // Create the RoleAndPermissions with an existing ID
        roleAndPermissions.setId(1L);
        RoleAndPermissionsDTO roleAndPermissionsDTO = roleAndPermissionsMapper.toDto(roleAndPermissions);

        int databaseSizeBeforeCreate = roleAndPermissionsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoleAndPermissionsMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(roleAndPermissionsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissions() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList
        restRoleAndPermissionsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roleAndPermissions.getId().intValue())))
            .andExpect(jsonPath("$.[*].readAccess").value(hasItem(DEFAULT_READ_ACCESS.booleanValue())))
            .andExpect(jsonPath("$.[*].createAccess").value(hasItem(DEFAULT_CREATE_ACCESS.booleanValue())))
            .andExpect(jsonPath("$.[*].editAccess").value(hasItem(DEFAULT_EDIT_ACCESS.booleanValue())))
            .andExpect(jsonPath("$.[*].deleteAccess").value(hasItem(DEFAULT_DELETE_ACCESS.booleanValue())));
    }

    @Test
    @Transactional
    void getRoleAndPermissions() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get the roleAndPermissions
        restRoleAndPermissionsMockMvc
            .perform(get(ENTITY_API_URL_ID, roleAndPermissions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(roleAndPermissions.getId().intValue()))
            .andExpect(jsonPath("$.readAccess").value(DEFAULT_READ_ACCESS.booleanValue()))
            .andExpect(jsonPath("$.createAccess").value(DEFAULT_CREATE_ACCESS.booleanValue()))
            .andExpect(jsonPath("$.editAccess").value(DEFAULT_EDIT_ACCESS.booleanValue()))
            .andExpect(jsonPath("$.deleteAccess").value(DEFAULT_DELETE_ACCESS.booleanValue()));
    }

    @Test
    @Transactional
    void getRoleAndPermissionsByIdFiltering() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        Long id = roleAndPermissions.getId();

        defaultRoleAndPermissionsShouldBeFound("id.equals=" + id);
        defaultRoleAndPermissionsShouldNotBeFound("id.notEquals=" + id);

        defaultRoleAndPermissionsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultRoleAndPermissionsShouldNotBeFound("id.greaterThan=" + id);

        defaultRoleAndPermissionsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultRoleAndPermissionsShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByReadAccessIsEqualToSomething() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where readAccess equals to DEFAULT_READ_ACCESS
        defaultRoleAndPermissionsShouldBeFound("readAccess.equals=" + DEFAULT_READ_ACCESS);

        // Get all the roleAndPermissionsList where readAccess equals to UPDATED_READ_ACCESS
        defaultRoleAndPermissionsShouldNotBeFound("readAccess.equals=" + UPDATED_READ_ACCESS);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByReadAccessIsInShouldWork() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where readAccess in DEFAULT_READ_ACCESS or UPDATED_READ_ACCESS
        defaultRoleAndPermissionsShouldBeFound("readAccess.in=" + DEFAULT_READ_ACCESS + "," + UPDATED_READ_ACCESS);

        // Get all the roleAndPermissionsList where readAccess equals to UPDATED_READ_ACCESS
        defaultRoleAndPermissionsShouldNotBeFound("readAccess.in=" + UPDATED_READ_ACCESS);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByReadAccessIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where readAccess is not null
        defaultRoleAndPermissionsShouldBeFound("readAccess.specified=true");

        // Get all the roleAndPermissionsList where readAccess is null
        defaultRoleAndPermissionsShouldNotBeFound("readAccess.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByCreateAccessIsEqualToSomething() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where createAccess equals to DEFAULT_CREATE_ACCESS
        defaultRoleAndPermissionsShouldBeFound("createAccess.equals=" + DEFAULT_CREATE_ACCESS);

        // Get all the roleAndPermissionsList where createAccess equals to UPDATED_CREATE_ACCESS
        defaultRoleAndPermissionsShouldNotBeFound("createAccess.equals=" + UPDATED_CREATE_ACCESS);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByCreateAccessIsInShouldWork() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where createAccess in DEFAULT_CREATE_ACCESS or UPDATED_CREATE_ACCESS
        defaultRoleAndPermissionsShouldBeFound("createAccess.in=" + DEFAULT_CREATE_ACCESS + "," + UPDATED_CREATE_ACCESS);

        // Get all the roleAndPermissionsList where createAccess equals to UPDATED_CREATE_ACCESS
        defaultRoleAndPermissionsShouldNotBeFound("createAccess.in=" + UPDATED_CREATE_ACCESS);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByCreateAccessIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where createAccess is not null
        defaultRoleAndPermissionsShouldBeFound("createAccess.specified=true");

        // Get all the roleAndPermissionsList where createAccess is null
        defaultRoleAndPermissionsShouldNotBeFound("createAccess.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByEditAccessIsEqualToSomething() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where editAccess equals to DEFAULT_EDIT_ACCESS
        defaultRoleAndPermissionsShouldBeFound("editAccess.equals=" + DEFAULT_EDIT_ACCESS);

        // Get all the roleAndPermissionsList where editAccess equals to UPDATED_EDIT_ACCESS
        defaultRoleAndPermissionsShouldNotBeFound("editAccess.equals=" + UPDATED_EDIT_ACCESS);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByEditAccessIsInShouldWork() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where editAccess in DEFAULT_EDIT_ACCESS or UPDATED_EDIT_ACCESS
        defaultRoleAndPermissionsShouldBeFound("editAccess.in=" + DEFAULT_EDIT_ACCESS + "," + UPDATED_EDIT_ACCESS);

        // Get all the roleAndPermissionsList where editAccess equals to UPDATED_EDIT_ACCESS
        defaultRoleAndPermissionsShouldNotBeFound("editAccess.in=" + UPDATED_EDIT_ACCESS);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByEditAccessIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where editAccess is not null
        defaultRoleAndPermissionsShouldBeFound("editAccess.specified=true");

        // Get all the roleAndPermissionsList where editAccess is null
        defaultRoleAndPermissionsShouldNotBeFound("editAccess.specified=false");
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByDeleteAccessIsEqualToSomething() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where deleteAccess equals to DEFAULT_DELETE_ACCESS
        defaultRoleAndPermissionsShouldBeFound("deleteAccess.equals=" + DEFAULT_DELETE_ACCESS);

        // Get all the roleAndPermissionsList where deleteAccess equals to UPDATED_DELETE_ACCESS
        defaultRoleAndPermissionsShouldNotBeFound("deleteAccess.equals=" + UPDATED_DELETE_ACCESS);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByDeleteAccessIsInShouldWork() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where deleteAccess in DEFAULT_DELETE_ACCESS or UPDATED_DELETE_ACCESS
        defaultRoleAndPermissionsShouldBeFound("deleteAccess.in=" + DEFAULT_DELETE_ACCESS + "," + UPDATED_DELETE_ACCESS);

        // Get all the roleAndPermissionsList where deleteAccess equals to UPDATED_DELETE_ACCESS
        defaultRoleAndPermissionsShouldNotBeFound("deleteAccess.in=" + UPDATED_DELETE_ACCESS);
    }

    @Test
    @Transactional
    void getAllRoleAndPermissionsByDeleteAccessIsNullOrNotNull() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        // Get all the roleAndPermissionsList where deleteAccess is not null
        defaultRoleAndPermissionsShouldBeFound("deleteAccess.specified=true");

        // Get all the roleAndPermissionsList where deleteAccess is null
        defaultRoleAndPermissionsShouldNotBeFound("deleteAccess.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRoleAndPermissionsShouldBeFound(String filter) throws Exception {
        restRoleAndPermissionsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roleAndPermissions.getId().intValue())))
            .andExpect(jsonPath("$.[*].readAccess").value(hasItem(DEFAULT_READ_ACCESS.booleanValue())))
            .andExpect(jsonPath("$.[*].createAccess").value(hasItem(DEFAULT_CREATE_ACCESS.booleanValue())))
            .andExpect(jsonPath("$.[*].editAccess").value(hasItem(DEFAULT_EDIT_ACCESS.booleanValue())))
            .andExpect(jsonPath("$.[*].deleteAccess").value(hasItem(DEFAULT_DELETE_ACCESS.booleanValue())));

        // Check, that the count call also returns 1
        restRoleAndPermissionsMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRoleAndPermissionsShouldNotBeFound(String filter) throws Exception {
        restRoleAndPermissionsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRoleAndPermissionsMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingRoleAndPermissions() throws Exception {
        // Get the roleAndPermissions
        restRoleAndPermissionsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingRoleAndPermissions() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        int databaseSizeBeforeUpdate = roleAndPermissionsRepository.findAll().size();

        // Update the roleAndPermissions
        RoleAndPermissions updatedRoleAndPermissions = roleAndPermissionsRepository.findById(roleAndPermissions.getId()).get();
        // Disconnect from session so that the updates on updatedRoleAndPermissions are not directly saved in db
        em.detach(updatedRoleAndPermissions);
        updatedRoleAndPermissions
            .readAccess(UPDATED_READ_ACCESS)
            .createAccess(UPDATED_CREATE_ACCESS)
            .editAccess(UPDATED_EDIT_ACCESS)
            .deleteAccess(UPDATED_DELETE_ACCESS);
        RoleAndPermissionsDTO roleAndPermissionsDTO = roleAndPermissionsMapper.toDto(updatedRoleAndPermissions);

        restRoleAndPermissionsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, roleAndPermissionsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(roleAndPermissionsDTO))
            )
            .andExpect(status().isOk());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeUpdate);
        RoleAndPermissions testRoleAndPermissions = roleAndPermissionsList.get(roleAndPermissionsList.size() - 1);
        assertThat(testRoleAndPermissions.getReadAccess()).isEqualTo(UPDATED_READ_ACCESS);
        assertThat(testRoleAndPermissions.getCreateAccess()).isEqualTo(UPDATED_CREATE_ACCESS);
        assertThat(testRoleAndPermissions.getEditAccess()).isEqualTo(UPDATED_EDIT_ACCESS);
        assertThat(testRoleAndPermissions.getDeleteAccess()).isEqualTo(UPDATED_DELETE_ACCESS);
    }

    @Test
    @Transactional
    void putNonExistingRoleAndPermissions() throws Exception {
        int databaseSizeBeforeUpdate = roleAndPermissionsRepository.findAll().size();
        roleAndPermissions.setId(count.incrementAndGet());

        // Create the RoleAndPermissions
        RoleAndPermissionsDTO roleAndPermissionsDTO = roleAndPermissionsMapper.toDto(roleAndPermissions);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRoleAndPermissionsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, roleAndPermissionsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(roleAndPermissionsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRoleAndPermissions() throws Exception {
        int databaseSizeBeforeUpdate = roleAndPermissionsRepository.findAll().size();
        roleAndPermissions.setId(count.incrementAndGet());

        // Create the RoleAndPermissions
        RoleAndPermissionsDTO roleAndPermissionsDTO = roleAndPermissionsMapper.toDto(roleAndPermissions);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRoleAndPermissionsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(roleAndPermissionsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRoleAndPermissions() throws Exception {
        int databaseSizeBeforeUpdate = roleAndPermissionsRepository.findAll().size();
        roleAndPermissions.setId(count.incrementAndGet());

        // Create the RoleAndPermissions
        RoleAndPermissionsDTO roleAndPermissionsDTO = roleAndPermissionsMapper.toDto(roleAndPermissions);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRoleAndPermissionsMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(roleAndPermissionsDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRoleAndPermissionsWithPatch() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        int databaseSizeBeforeUpdate = roleAndPermissionsRepository.findAll().size();

        // Update the roleAndPermissions using partial update
        RoleAndPermissions partialUpdatedRoleAndPermissions = new RoleAndPermissions();
        partialUpdatedRoleAndPermissions.setId(roleAndPermissions.getId());

        partialUpdatedRoleAndPermissions.readAccess(UPDATED_READ_ACCESS).editAccess(UPDATED_EDIT_ACCESS);

        restRoleAndPermissionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRoleAndPermissions.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRoleAndPermissions))
            )
            .andExpect(status().isOk());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeUpdate);
        RoleAndPermissions testRoleAndPermissions = roleAndPermissionsList.get(roleAndPermissionsList.size() - 1);
        assertThat(testRoleAndPermissions.getReadAccess()).isEqualTo(UPDATED_READ_ACCESS);
        assertThat(testRoleAndPermissions.getCreateAccess()).isEqualTo(DEFAULT_CREATE_ACCESS);
        assertThat(testRoleAndPermissions.getEditAccess()).isEqualTo(UPDATED_EDIT_ACCESS);
        assertThat(testRoleAndPermissions.getDeleteAccess()).isEqualTo(DEFAULT_DELETE_ACCESS);
    }

    @Test
    @Transactional
    void fullUpdateRoleAndPermissionsWithPatch() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        int databaseSizeBeforeUpdate = roleAndPermissionsRepository.findAll().size();

        // Update the roleAndPermissions using partial update
        RoleAndPermissions partialUpdatedRoleAndPermissions = new RoleAndPermissions();
        partialUpdatedRoleAndPermissions.setId(roleAndPermissions.getId());

        partialUpdatedRoleAndPermissions
            .readAccess(UPDATED_READ_ACCESS)
            .createAccess(UPDATED_CREATE_ACCESS)
            .editAccess(UPDATED_EDIT_ACCESS)
            .deleteAccess(UPDATED_DELETE_ACCESS);

        restRoleAndPermissionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRoleAndPermissions.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRoleAndPermissions))
            )
            .andExpect(status().isOk());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeUpdate);
        RoleAndPermissions testRoleAndPermissions = roleAndPermissionsList.get(roleAndPermissionsList.size() - 1);
        assertThat(testRoleAndPermissions.getReadAccess()).isEqualTo(UPDATED_READ_ACCESS);
        assertThat(testRoleAndPermissions.getCreateAccess()).isEqualTo(UPDATED_CREATE_ACCESS);
        assertThat(testRoleAndPermissions.getEditAccess()).isEqualTo(UPDATED_EDIT_ACCESS);
        assertThat(testRoleAndPermissions.getDeleteAccess()).isEqualTo(UPDATED_DELETE_ACCESS);
    }

    @Test
    @Transactional
    void patchNonExistingRoleAndPermissions() throws Exception {
        int databaseSizeBeforeUpdate = roleAndPermissionsRepository.findAll().size();
        roleAndPermissions.setId(count.incrementAndGet());

        // Create the RoleAndPermissions
        RoleAndPermissionsDTO roleAndPermissionsDTO = roleAndPermissionsMapper.toDto(roleAndPermissions);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRoleAndPermissionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, roleAndPermissionsDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(roleAndPermissionsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRoleAndPermissions() throws Exception {
        int databaseSizeBeforeUpdate = roleAndPermissionsRepository.findAll().size();
        roleAndPermissions.setId(count.incrementAndGet());

        // Create the RoleAndPermissions
        RoleAndPermissionsDTO roleAndPermissionsDTO = roleAndPermissionsMapper.toDto(roleAndPermissions);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRoleAndPermissionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(roleAndPermissionsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRoleAndPermissions() throws Exception {
        int databaseSizeBeforeUpdate = roleAndPermissionsRepository.findAll().size();
        roleAndPermissions.setId(count.incrementAndGet());

        // Create the RoleAndPermissions
        RoleAndPermissionsDTO roleAndPermissionsDTO = roleAndPermissionsMapper.toDto(roleAndPermissions);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRoleAndPermissionsMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(roleAndPermissionsDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RoleAndPermissions in the database
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRoleAndPermissions() throws Exception {
        // Initialize the database
        roleAndPermissionsRepository.saveAndFlush(roleAndPermissions);

        int databaseSizeBeforeDelete = roleAndPermissionsRepository.findAll().size();

        // Delete the roleAndPermissions
        restRoleAndPermissionsMockMvc
            .perform(delete(ENTITY_API_URL_ID, roleAndPermissions.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RoleAndPermissions> roleAndPermissionsList = roleAndPermissionsRepository.findAll();
        assertThat(roleAndPermissionsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
