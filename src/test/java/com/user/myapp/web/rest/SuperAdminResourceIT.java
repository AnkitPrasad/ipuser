package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.SuperAdmin;
import com.user.myapp.repository.SuperAdminRepository;
import com.user.myapp.service.criteria.SuperAdminCriteria;
import com.user.myapp.service.dto.SuperAdminDTO;
import com.user.myapp.service.mapper.SuperAdminMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SuperAdminResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SuperAdminResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/super-admins";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SuperAdminRepository superAdminRepository;

    @Autowired
    private SuperAdminMapper superAdminMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSuperAdminMockMvc;

    private SuperAdmin superAdmin;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SuperAdmin createEntity(EntityManager em) {
        SuperAdmin superAdmin = new SuperAdmin().name(DEFAULT_NAME).email(DEFAULT_EMAIL);
        return superAdmin;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SuperAdmin createUpdatedEntity(EntityManager em) {
        SuperAdmin superAdmin = new SuperAdmin().name(UPDATED_NAME).email(UPDATED_EMAIL);
        return superAdmin;
    }

    @BeforeEach
    public void initTest() {
        superAdmin = createEntity(em);
    }

    @Test
    @Transactional
    void createSuperAdmin() throws Exception {
        int databaseSizeBeforeCreate = superAdminRepository.findAll().size();
        // Create the SuperAdmin
        SuperAdminDTO superAdminDTO = superAdminMapper.toDto(superAdmin);
        restSuperAdminMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(superAdminDTO)))
            .andExpect(status().isCreated());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeCreate + 1);
        SuperAdmin testSuperAdmin = superAdminList.get(superAdminList.size() - 1);
        assertThat(testSuperAdmin.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSuperAdmin.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    void createSuperAdminWithExistingId() throws Exception {
        // Create the SuperAdmin with an existing ID
        superAdmin.setId(1L);
        SuperAdminDTO superAdminDTO = superAdminMapper.toDto(superAdmin);

        int databaseSizeBeforeCreate = superAdminRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSuperAdminMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(superAdminDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSuperAdmins() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList
        restSuperAdminMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(superAdmin.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));
    }

    @Test
    @Transactional
    void getSuperAdmin() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get the superAdmin
        restSuperAdminMockMvc
            .perform(get(ENTITY_API_URL_ID, superAdmin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(superAdmin.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL));
    }

    @Test
    @Transactional
    void getSuperAdminsByIdFiltering() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        Long id = superAdmin.getId();

        defaultSuperAdminShouldBeFound("id.equals=" + id);
        defaultSuperAdminShouldNotBeFound("id.notEquals=" + id);

        defaultSuperAdminShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSuperAdminShouldNotBeFound("id.greaterThan=" + id);

        defaultSuperAdminShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSuperAdminShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllSuperAdminsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where name equals to DEFAULT_NAME
        defaultSuperAdminShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the superAdminList where name equals to UPDATED_NAME
        defaultSuperAdminShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSuperAdminsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where name in DEFAULT_NAME or UPDATED_NAME
        defaultSuperAdminShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the superAdminList where name equals to UPDATED_NAME
        defaultSuperAdminShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSuperAdminsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where name is not null
        defaultSuperAdminShouldBeFound("name.specified=true");

        // Get all the superAdminList where name is null
        defaultSuperAdminShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllSuperAdminsByNameContainsSomething() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where name contains DEFAULT_NAME
        defaultSuperAdminShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the superAdminList where name contains UPDATED_NAME
        defaultSuperAdminShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSuperAdminsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where name does not contain DEFAULT_NAME
        defaultSuperAdminShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the superAdminList where name does not contain UPDATED_NAME
        defaultSuperAdminShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSuperAdminsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where email equals to DEFAULT_EMAIL
        defaultSuperAdminShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the superAdminList where email equals to UPDATED_EMAIL
        defaultSuperAdminShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllSuperAdminsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultSuperAdminShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the superAdminList where email equals to UPDATED_EMAIL
        defaultSuperAdminShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllSuperAdminsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where email is not null
        defaultSuperAdminShouldBeFound("email.specified=true");

        // Get all the superAdminList where email is null
        defaultSuperAdminShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    void getAllSuperAdminsByEmailContainsSomething() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where email contains DEFAULT_EMAIL
        defaultSuperAdminShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the superAdminList where email contains UPDATED_EMAIL
        defaultSuperAdminShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllSuperAdminsByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        // Get all the superAdminList where email does not contain DEFAULT_EMAIL
        defaultSuperAdminShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the superAdminList where email does not contain UPDATED_EMAIL
        defaultSuperAdminShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllSuperAdminsByInPreferenceUserIsEqualToSomething() throws Exception {
        InPreferenceUser inPreferenceUser;
        if (TestUtil.findAll(em, InPreferenceUser.class).isEmpty()) {
            superAdminRepository.saveAndFlush(superAdmin);
            inPreferenceUser = InPreferenceUserResourceIT.createEntity(em);
        } else {
            inPreferenceUser = TestUtil.findAll(em, InPreferenceUser.class).get(0);
        }
        em.persist(inPreferenceUser);
        em.flush();
        superAdmin.setInPreferenceUser(inPreferenceUser);
        superAdminRepository.saveAndFlush(superAdmin);
        Long inPreferenceUserId = inPreferenceUser.getId();

        // Get all the superAdminList where inPreferenceUser equals to inPreferenceUserId
        defaultSuperAdminShouldBeFound("inPreferenceUserId.equals=" + inPreferenceUserId);

        // Get all the superAdminList where inPreferenceUser equals to (inPreferenceUserId + 1)
        defaultSuperAdminShouldNotBeFound("inPreferenceUserId.equals=" + (inPreferenceUserId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSuperAdminShouldBeFound(String filter) throws Exception {
        restSuperAdminMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(superAdmin.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));

        // Check, that the count call also returns 1
        restSuperAdminMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSuperAdminShouldNotBeFound(String filter) throws Exception {
        restSuperAdminMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSuperAdminMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingSuperAdmin() throws Exception {
        // Get the superAdmin
        restSuperAdminMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSuperAdmin() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        int databaseSizeBeforeUpdate = superAdminRepository.findAll().size();

        // Update the superAdmin
        SuperAdmin updatedSuperAdmin = superAdminRepository.findById(superAdmin.getId()).get();
        // Disconnect from session so that the updates on updatedSuperAdmin are not directly saved in db
        em.detach(updatedSuperAdmin);
        updatedSuperAdmin.name(UPDATED_NAME).email(UPDATED_EMAIL);
        SuperAdminDTO superAdminDTO = superAdminMapper.toDto(updatedSuperAdmin);

        restSuperAdminMockMvc
            .perform(
                put(ENTITY_API_URL_ID, superAdminDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(superAdminDTO))
            )
            .andExpect(status().isOk());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeUpdate);
        SuperAdmin testSuperAdmin = superAdminList.get(superAdminList.size() - 1);
        assertThat(testSuperAdmin.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSuperAdmin.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void putNonExistingSuperAdmin() throws Exception {
        int databaseSizeBeforeUpdate = superAdminRepository.findAll().size();
        superAdmin.setId(count.incrementAndGet());

        // Create the SuperAdmin
        SuperAdminDTO superAdminDTO = superAdminMapper.toDto(superAdmin);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSuperAdminMockMvc
            .perform(
                put(ENTITY_API_URL_ID, superAdminDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(superAdminDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSuperAdmin() throws Exception {
        int databaseSizeBeforeUpdate = superAdminRepository.findAll().size();
        superAdmin.setId(count.incrementAndGet());

        // Create the SuperAdmin
        SuperAdminDTO superAdminDTO = superAdminMapper.toDto(superAdmin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSuperAdminMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(superAdminDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSuperAdmin() throws Exception {
        int databaseSizeBeforeUpdate = superAdminRepository.findAll().size();
        superAdmin.setId(count.incrementAndGet());

        // Create the SuperAdmin
        SuperAdminDTO superAdminDTO = superAdminMapper.toDto(superAdmin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSuperAdminMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(superAdminDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSuperAdminWithPatch() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        int databaseSizeBeforeUpdate = superAdminRepository.findAll().size();

        // Update the superAdmin using partial update
        SuperAdmin partialUpdatedSuperAdmin = new SuperAdmin();
        partialUpdatedSuperAdmin.setId(superAdmin.getId());

        partialUpdatedSuperAdmin.email(UPDATED_EMAIL);

        restSuperAdminMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSuperAdmin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSuperAdmin))
            )
            .andExpect(status().isOk());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeUpdate);
        SuperAdmin testSuperAdmin = superAdminList.get(superAdminList.size() - 1);
        assertThat(testSuperAdmin.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSuperAdmin.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void fullUpdateSuperAdminWithPatch() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        int databaseSizeBeforeUpdate = superAdminRepository.findAll().size();

        // Update the superAdmin using partial update
        SuperAdmin partialUpdatedSuperAdmin = new SuperAdmin();
        partialUpdatedSuperAdmin.setId(superAdmin.getId());

        partialUpdatedSuperAdmin.name(UPDATED_NAME).email(UPDATED_EMAIL);

        restSuperAdminMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSuperAdmin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSuperAdmin))
            )
            .andExpect(status().isOk());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeUpdate);
        SuperAdmin testSuperAdmin = superAdminList.get(superAdminList.size() - 1);
        assertThat(testSuperAdmin.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSuperAdmin.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void patchNonExistingSuperAdmin() throws Exception {
        int databaseSizeBeforeUpdate = superAdminRepository.findAll().size();
        superAdmin.setId(count.incrementAndGet());

        // Create the SuperAdmin
        SuperAdminDTO superAdminDTO = superAdminMapper.toDto(superAdmin);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSuperAdminMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, superAdminDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(superAdminDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSuperAdmin() throws Exception {
        int databaseSizeBeforeUpdate = superAdminRepository.findAll().size();
        superAdmin.setId(count.incrementAndGet());

        // Create the SuperAdmin
        SuperAdminDTO superAdminDTO = superAdminMapper.toDto(superAdmin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSuperAdminMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(superAdminDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSuperAdmin() throws Exception {
        int databaseSizeBeforeUpdate = superAdminRepository.findAll().size();
        superAdmin.setId(count.incrementAndGet());

        // Create the SuperAdmin
        SuperAdminDTO superAdminDTO = superAdminMapper.toDto(superAdmin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSuperAdminMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(superAdminDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the SuperAdmin in the database
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSuperAdmin() throws Exception {
        // Initialize the database
        superAdminRepository.saveAndFlush(superAdmin);

        int databaseSizeBeforeDelete = superAdminRepository.findAll().size();

        // Delete the superAdmin
        restSuperAdminMockMvc
            .perform(delete(ENTITY_API_URL_ID, superAdmin.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SuperAdmin> superAdminList = superAdminRepository.findAll();
        assertThat(superAdminList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
