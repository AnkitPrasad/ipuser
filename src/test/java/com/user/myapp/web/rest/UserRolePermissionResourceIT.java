package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.PermissionMaster;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.domain.UserRolePermission;
import com.user.myapp.repository.UserRolePermissionRepository;
import com.user.myapp.service.criteria.UserRolePermissionCriteria;
import com.user.myapp.service.dto.UserRolePermissionDTO;
import com.user.myapp.service.mapper.UserRolePermissionMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UserRolePermissionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UserRolePermissionResourceIT {

    private static final String ENTITY_API_URL = "/api/user-role-permissions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UserRolePermissionRepository userRolePermissionRepository;

    @Autowired
    private UserRolePermissionMapper userRolePermissionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserRolePermissionMockMvc;

    private UserRolePermission userRolePermission;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserRolePermission createEntity(EntityManager em) {
        UserRolePermission userRolePermission = new UserRolePermission();
        return userRolePermission;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserRolePermission createUpdatedEntity(EntityManager em) {
        UserRolePermission userRolePermission = new UserRolePermission();
        return userRolePermission;
    }

    @BeforeEach
    public void initTest() {
        userRolePermission = createEntity(em);
    }

    @Test
    @Transactional
    void createUserRolePermission() throws Exception {
        int databaseSizeBeforeCreate = userRolePermissionRepository.findAll().size();
        // Create the UserRolePermission
        UserRolePermissionDTO userRolePermissionDTO = userRolePermissionMapper.toDto(userRolePermission);
        restUserRolePermissionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userRolePermissionDTO))
            )
            .andExpect(status().isCreated());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeCreate + 1);
        UserRolePermission testUserRolePermission = userRolePermissionList.get(userRolePermissionList.size() - 1);
    }

    @Test
    @Transactional
    void createUserRolePermissionWithExistingId() throws Exception {
        // Create the UserRolePermission with an existing ID
        userRolePermission.setId(1L);
        UserRolePermissionDTO userRolePermissionDTO = userRolePermissionMapper.toDto(userRolePermission);

        int databaseSizeBeforeCreate = userRolePermissionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserRolePermissionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userRolePermissionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllUserRolePermissions() throws Exception {
        // Initialize the database
        userRolePermissionRepository.saveAndFlush(userRolePermission);

        // Get all the userRolePermissionList
        restUserRolePermissionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userRolePermission.getId().intValue())));
    }

    @Test
    @Transactional
    void getUserRolePermission() throws Exception {
        // Initialize the database
        userRolePermissionRepository.saveAndFlush(userRolePermission);

        // Get the userRolePermission
        restUserRolePermissionMockMvc
            .perform(get(ENTITY_API_URL_ID, userRolePermission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userRolePermission.getId().intValue()));
    }

    @Test
    @Transactional
    void getUserRolePermissionsByIdFiltering() throws Exception {
        // Initialize the database
        userRolePermissionRepository.saveAndFlush(userRolePermission);

        Long id = userRolePermission.getId();

        defaultUserRolePermissionShouldBeFound("id.equals=" + id);
        defaultUserRolePermissionShouldNotBeFound("id.notEquals=" + id);

        defaultUserRolePermissionShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUserRolePermissionShouldNotBeFound("id.greaterThan=" + id);

        defaultUserRolePermissionShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUserRolePermissionShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllUserRolePermissionsByInPreferenceUserIsEqualToSomething() throws Exception {
        InPreferenceUser inPreferenceUser;
        if (TestUtil.findAll(em, InPreferenceUser.class).isEmpty()) {
            userRolePermissionRepository.saveAndFlush(userRolePermission);
            inPreferenceUser = InPreferenceUserResourceIT.createEntity(em);
        } else {
            inPreferenceUser = TestUtil.findAll(em, InPreferenceUser.class).get(0);
        }
        em.persist(inPreferenceUser);
        em.flush();
        userRolePermission.setInPreferenceUser(inPreferenceUser);
        userRolePermissionRepository.saveAndFlush(userRolePermission);
        Long inPreferenceUserId = inPreferenceUser.getId();

        // Get all the userRolePermissionList where inPreferenceUser equals to inPreferenceUserId
        defaultUserRolePermissionShouldBeFound("inPreferenceUserId.equals=" + inPreferenceUserId);

        // Get all the userRolePermissionList where inPreferenceUser equals to (inPreferenceUserId + 1)
        defaultUserRolePermissionShouldNotBeFound("inPreferenceUserId.equals=" + (inPreferenceUserId + 1));
    }

    @Test
    @Transactional
    void getAllUserRolePermissionsByRoleMasterIsEqualToSomething() throws Exception {
        RoleMaster roleMaster;
        if (TestUtil.findAll(em, RoleMaster.class).isEmpty()) {
            userRolePermissionRepository.saveAndFlush(userRolePermission);
            roleMaster = RoleMasterResourceIT.createEntity(em);
        } else {
            roleMaster = TestUtil.findAll(em, RoleMaster.class).get(0);
        }
        em.persist(roleMaster);
        em.flush();
        userRolePermission.setRoleMaster(roleMaster);
        userRolePermissionRepository.saveAndFlush(userRolePermission);
        Long roleMasterId = roleMaster.getId();

        // Get all the userRolePermissionList where roleMaster equals to roleMasterId
        defaultUserRolePermissionShouldBeFound("roleMasterId.equals=" + roleMasterId);

        // Get all the userRolePermissionList where roleMaster equals to (roleMasterId + 1)
        defaultUserRolePermissionShouldNotBeFound("roleMasterId.equals=" + (roleMasterId + 1));
    }

    @Test
    @Transactional
    void getAllUserRolePermissionsByPermissionMasterIsEqualToSomething() throws Exception {
        PermissionMaster permissionMaster;
        if (TestUtil.findAll(em, PermissionMaster.class).isEmpty()) {
            userRolePermissionRepository.saveAndFlush(userRolePermission);
            permissionMaster = PermissionMasterResourceIT.createEntity(em);
        } else {
            permissionMaster = TestUtil.findAll(em, PermissionMaster.class).get(0);
        }
        em.persist(permissionMaster);
        em.flush();
        userRolePermission.setPermissionMaster(permissionMaster);
        userRolePermissionRepository.saveAndFlush(userRolePermission);
        Long permissionMasterId = permissionMaster.getId();

        // Get all the userRolePermissionList where permissionMaster equals to permissionMasterId
        defaultUserRolePermissionShouldBeFound("permissionMasterId.equals=" + permissionMasterId);

        // Get all the userRolePermissionList where permissionMaster equals to (permissionMasterId + 1)
        defaultUserRolePermissionShouldNotBeFound("permissionMasterId.equals=" + (permissionMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserRolePermissionShouldBeFound(String filter) throws Exception {
        restUserRolePermissionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userRolePermission.getId().intValue())));

        // Check, that the count call also returns 1
        restUserRolePermissionMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserRolePermissionShouldNotBeFound(String filter) throws Exception {
        restUserRolePermissionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserRolePermissionMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingUserRolePermission() throws Exception {
        // Get the userRolePermission
        restUserRolePermissionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingUserRolePermission() throws Exception {
        // Initialize the database
        userRolePermissionRepository.saveAndFlush(userRolePermission);

        int databaseSizeBeforeUpdate = userRolePermissionRepository.findAll().size();

        // Update the userRolePermission
        UserRolePermission updatedUserRolePermission = userRolePermissionRepository.findById(userRolePermission.getId()).get();
        // Disconnect from session so that the updates on updatedUserRolePermission are not directly saved in db
        em.detach(updatedUserRolePermission);
        UserRolePermissionDTO userRolePermissionDTO = userRolePermissionMapper.toDto(updatedUserRolePermission);

        restUserRolePermissionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userRolePermissionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userRolePermissionDTO))
            )
            .andExpect(status().isOk());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeUpdate);
        UserRolePermission testUserRolePermission = userRolePermissionList.get(userRolePermissionList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingUserRolePermission() throws Exception {
        int databaseSizeBeforeUpdate = userRolePermissionRepository.findAll().size();
        userRolePermission.setId(count.incrementAndGet());

        // Create the UserRolePermission
        UserRolePermissionDTO userRolePermissionDTO = userRolePermissionMapper.toDto(userRolePermission);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserRolePermissionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userRolePermissionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userRolePermissionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUserRolePermission() throws Exception {
        int databaseSizeBeforeUpdate = userRolePermissionRepository.findAll().size();
        userRolePermission.setId(count.incrementAndGet());

        // Create the UserRolePermission
        UserRolePermissionDTO userRolePermissionDTO = userRolePermissionMapper.toDto(userRolePermission);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserRolePermissionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userRolePermissionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUserRolePermission() throws Exception {
        int databaseSizeBeforeUpdate = userRolePermissionRepository.findAll().size();
        userRolePermission.setId(count.incrementAndGet());

        // Create the UserRolePermission
        UserRolePermissionDTO userRolePermissionDTO = userRolePermissionMapper.toDto(userRolePermission);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserRolePermissionMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userRolePermissionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUserRolePermissionWithPatch() throws Exception {
        // Initialize the database
        userRolePermissionRepository.saveAndFlush(userRolePermission);

        int databaseSizeBeforeUpdate = userRolePermissionRepository.findAll().size();

        // Update the userRolePermission using partial update
        UserRolePermission partialUpdatedUserRolePermission = new UserRolePermission();
        partialUpdatedUserRolePermission.setId(userRolePermission.getId());

        restUserRolePermissionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserRolePermission.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserRolePermission))
            )
            .andExpect(status().isOk());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeUpdate);
        UserRolePermission testUserRolePermission = userRolePermissionList.get(userRolePermissionList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateUserRolePermissionWithPatch() throws Exception {
        // Initialize the database
        userRolePermissionRepository.saveAndFlush(userRolePermission);

        int databaseSizeBeforeUpdate = userRolePermissionRepository.findAll().size();

        // Update the userRolePermission using partial update
        UserRolePermission partialUpdatedUserRolePermission = new UserRolePermission();
        partialUpdatedUserRolePermission.setId(userRolePermission.getId());

        restUserRolePermissionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserRolePermission.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserRolePermission))
            )
            .andExpect(status().isOk());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeUpdate);
        UserRolePermission testUserRolePermission = userRolePermissionList.get(userRolePermissionList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingUserRolePermission() throws Exception {
        int databaseSizeBeforeUpdate = userRolePermissionRepository.findAll().size();
        userRolePermission.setId(count.incrementAndGet());

        // Create the UserRolePermission
        UserRolePermissionDTO userRolePermissionDTO = userRolePermissionMapper.toDto(userRolePermission);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserRolePermissionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, userRolePermissionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userRolePermissionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUserRolePermission() throws Exception {
        int databaseSizeBeforeUpdate = userRolePermissionRepository.findAll().size();
        userRolePermission.setId(count.incrementAndGet());

        // Create the UserRolePermission
        UserRolePermissionDTO userRolePermissionDTO = userRolePermissionMapper.toDto(userRolePermission);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserRolePermissionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userRolePermissionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUserRolePermission() throws Exception {
        int databaseSizeBeforeUpdate = userRolePermissionRepository.findAll().size();
        userRolePermission.setId(count.incrementAndGet());

        // Create the UserRolePermission
        UserRolePermissionDTO userRolePermissionDTO = userRolePermissionMapper.toDto(userRolePermission);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserRolePermissionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userRolePermissionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserRolePermission in the database
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUserRolePermission() throws Exception {
        // Initialize the database
        userRolePermissionRepository.saveAndFlush(userRolePermission);

        int databaseSizeBeforeDelete = userRolePermissionRepository.findAll().size();

        // Delete the userRolePermission
        restUserRolePermissionMockMvc
            .perform(delete(ENTITY_API_URL_ID, userRolePermission.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserRolePermission> userRolePermissionList = userRolePermissionRepository.findAll();
        assertThat(userRolePermissionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
