package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.Candidate;
import com.user.myapp.domain.Client;
import com.user.myapp.domain.ClientSetting;
import com.user.myapp.domain.enumeration.ClientType;
import com.user.myapp.repository.ClientRepository;
import com.user.myapp.service.criteria.ClientCriteria;
import com.user.myapp.service.dto.ClientDTO;
import com.user.myapp.service.mapper.ClientMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ClientResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ClientResourceIT {

    private static final String DEFAULT_LEGAL_ENTITY = "AAAAAAAAAA";
    private static final String UPDATED_LEGAL_ENTITY = "BBBBBBBBBB";

    private static final String DEFAULT_INDUSTRY = "AAAAAAAAAA";
    private static final String UPDATED_INDUSTRY = "BBBBBBBBBB";

    private static final String DEFAULT_BANK_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_BANK_DETAILS = "BBBBBBBBBB";

    private static final ClientType DEFAULT_CLIENT_TYPE = ClientType.Hiring;
    private static final ClientType UPDATED_CLIENT_TYPE = ClientType.Talent;

    private static final String DEFAULT_TNA_NO = "AAAAAAAAAA";
    private static final String UPDATED_TNA_NO = "BBBBBBBBBB";

    private static final String DEFAULT_GST_NO = "AAAAAAAAAA";
    private static final String UPDATED_GST_NO = "BBBBBBBBBB";

    private static final String DEFAULT_PAN = "AAAAAAAAAA";
    private static final String UPDATED_PAN = "BBBBBBBBBB";

    private static final String DEFAULT_UPLOAD_CLIENT_LOGO = "AAAAAAAAAA";
    private static final String UPDATED_UPLOAD_CLIENT_LOGO = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_CREATED_BY = 1L;
    private static final Long UPDATED_CREATED_BY = 2L;
    private static final Long SMALLER_CREATED_BY = 1L - 1L;

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_UPDATED_BY = 1L;
    private static final Long UPDATED_UPDATED_BY = 2L;
    private static final Long SMALLER_UPDATED_BY = 1L - 1L;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/clients";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientMapper clientMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restClientMockMvc;

    private Client client;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Client createEntity(EntityManager em) {
        Client client = new Client()
            .legalEntity(DEFAULT_LEGAL_ENTITY)
            .industry(DEFAULT_INDUSTRY)
            .bankDetails(DEFAULT_BANK_DETAILS)
            .clientType(DEFAULT_CLIENT_TYPE)
            .tnaNo(DEFAULT_TNA_NO)
            .gstNo(DEFAULT_GST_NO)
            .pan(DEFAULT_PAN)
            .uploadClientLogo(DEFAULT_UPLOAD_CLIENT_LOGO)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .status(DEFAULT_STATUS);
        return client;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Client createUpdatedEntity(EntityManager em) {
        Client client = new Client()
            .legalEntity(UPDATED_LEGAL_ENTITY)
            .industry(UPDATED_INDUSTRY)
            .bankDetails(UPDATED_BANK_DETAILS)
            .clientType(UPDATED_CLIENT_TYPE)
            .tnaNo(UPDATED_TNA_NO)
            .gstNo(UPDATED_GST_NO)
            .pan(UPDATED_PAN)
            .uploadClientLogo(UPDATED_UPLOAD_CLIENT_LOGO)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);
        return client;
    }

    @BeforeEach
    public void initTest() {
        client = createEntity(em);
    }

    @Test
    @Transactional
    void createClient() throws Exception {
        int databaseSizeBeforeCreate = clientRepository.findAll().size();
        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);
        restClientMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isCreated());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeCreate + 1);
        Client testClient = clientList.get(clientList.size() - 1);
        assertThat(testClient.getLegalEntity()).isEqualTo(DEFAULT_LEGAL_ENTITY);
        assertThat(testClient.getIndustry()).isEqualTo(DEFAULT_INDUSTRY);
        assertThat(testClient.getBankDetails()).isEqualTo(DEFAULT_BANK_DETAILS);
        assertThat(testClient.getClientType()).isEqualTo(DEFAULT_CLIENT_TYPE);
        assertThat(testClient.getTnaNo()).isEqualTo(DEFAULT_TNA_NO);
        assertThat(testClient.getGstNo()).isEqualTo(DEFAULT_GST_NO);
        assertThat(testClient.getPan()).isEqualTo(DEFAULT_PAN);
        assertThat(testClient.getUploadClientLogo()).isEqualTo(DEFAULT_UPLOAD_CLIENT_LOGO);
        assertThat(testClient.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testClient.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testClient.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testClient.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testClient.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createClientWithExistingId() throws Exception {
        // Create the Client with an existing ID
        client.setId(1L);
        ClientDTO clientDTO = clientMapper.toDto(client);

        int databaseSizeBeforeCreate = clientRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllClients() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList
        restClientMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(client.getId().intValue())))
            .andExpect(jsonPath("$.[*].legalEntity").value(hasItem(DEFAULT_LEGAL_ENTITY)))
            .andExpect(jsonPath("$.[*].industry").value(hasItem(DEFAULT_INDUSTRY)))
            .andExpect(jsonPath("$.[*].bankDetails").value(hasItem(DEFAULT_BANK_DETAILS)))
            .andExpect(jsonPath("$.[*].clientType").value(hasItem(DEFAULT_CLIENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].tnaNo").value(hasItem(DEFAULT_TNA_NO)))
            .andExpect(jsonPath("$.[*].gstNo").value(hasItem(DEFAULT_GST_NO)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].uploadClientLogo").value(hasItem(DEFAULT_UPLOAD_CLIENT_LOGO)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    @Transactional
    void getClient() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get the client
        restClientMockMvc
            .perform(get(ENTITY_API_URL_ID, client.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(client.getId().intValue()))
            .andExpect(jsonPath("$.legalEntity").value(DEFAULT_LEGAL_ENTITY))
            .andExpect(jsonPath("$.industry").value(DEFAULT_INDUSTRY))
            .andExpect(jsonPath("$.bankDetails").value(DEFAULT_BANK_DETAILS))
            .andExpect(jsonPath("$.clientType").value(DEFAULT_CLIENT_TYPE.toString()))
            .andExpect(jsonPath("$.tnaNo").value(DEFAULT_TNA_NO))
            .andExpect(jsonPath("$.gstNo").value(DEFAULT_GST_NO))
            .andExpect(jsonPath("$.pan").value(DEFAULT_PAN))
            .andExpect(jsonPath("$.uploadClientLogo").value(DEFAULT_UPLOAD_CLIENT_LOGO))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.intValue()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    void getClientsByIdFiltering() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        Long id = client.getId();

        defaultClientShouldBeFound("id.equals=" + id);
        defaultClientShouldNotBeFound("id.notEquals=" + id);

        defaultClientShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultClientShouldNotBeFound("id.greaterThan=" + id);

        defaultClientShouldBeFound("id.lessThanOrEqual=" + id);
        defaultClientShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllClientsByLegalEntityIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where legalEntity equals to DEFAULT_LEGAL_ENTITY
        defaultClientShouldBeFound("legalEntity.equals=" + DEFAULT_LEGAL_ENTITY);

        // Get all the clientList where legalEntity equals to UPDATED_LEGAL_ENTITY
        defaultClientShouldNotBeFound("legalEntity.equals=" + UPDATED_LEGAL_ENTITY);
    }

    @Test
    @Transactional
    void getAllClientsByLegalEntityIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where legalEntity in DEFAULT_LEGAL_ENTITY or UPDATED_LEGAL_ENTITY
        defaultClientShouldBeFound("legalEntity.in=" + DEFAULT_LEGAL_ENTITY + "," + UPDATED_LEGAL_ENTITY);

        // Get all the clientList where legalEntity equals to UPDATED_LEGAL_ENTITY
        defaultClientShouldNotBeFound("legalEntity.in=" + UPDATED_LEGAL_ENTITY);
    }

    @Test
    @Transactional
    void getAllClientsByLegalEntityIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where legalEntity is not null
        defaultClientShouldBeFound("legalEntity.specified=true");

        // Get all the clientList where legalEntity is null
        defaultClientShouldNotBeFound("legalEntity.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByLegalEntityContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where legalEntity contains DEFAULT_LEGAL_ENTITY
        defaultClientShouldBeFound("legalEntity.contains=" + DEFAULT_LEGAL_ENTITY);

        // Get all the clientList where legalEntity contains UPDATED_LEGAL_ENTITY
        defaultClientShouldNotBeFound("legalEntity.contains=" + UPDATED_LEGAL_ENTITY);
    }

    @Test
    @Transactional
    void getAllClientsByLegalEntityNotContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where legalEntity does not contain DEFAULT_LEGAL_ENTITY
        defaultClientShouldNotBeFound("legalEntity.doesNotContain=" + DEFAULT_LEGAL_ENTITY);

        // Get all the clientList where legalEntity does not contain UPDATED_LEGAL_ENTITY
        defaultClientShouldBeFound("legalEntity.doesNotContain=" + UPDATED_LEGAL_ENTITY);
    }

    @Test
    @Transactional
    void getAllClientsByIndustryIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where industry equals to DEFAULT_INDUSTRY
        defaultClientShouldBeFound("industry.equals=" + DEFAULT_INDUSTRY);

        // Get all the clientList where industry equals to UPDATED_INDUSTRY
        defaultClientShouldNotBeFound("industry.equals=" + UPDATED_INDUSTRY);
    }

    @Test
    @Transactional
    void getAllClientsByIndustryIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where industry in DEFAULT_INDUSTRY or UPDATED_INDUSTRY
        defaultClientShouldBeFound("industry.in=" + DEFAULT_INDUSTRY + "," + UPDATED_INDUSTRY);

        // Get all the clientList where industry equals to UPDATED_INDUSTRY
        defaultClientShouldNotBeFound("industry.in=" + UPDATED_INDUSTRY);
    }

    @Test
    @Transactional
    void getAllClientsByIndustryIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where industry is not null
        defaultClientShouldBeFound("industry.specified=true");

        // Get all the clientList where industry is null
        defaultClientShouldNotBeFound("industry.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByIndustryContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where industry contains DEFAULT_INDUSTRY
        defaultClientShouldBeFound("industry.contains=" + DEFAULT_INDUSTRY);

        // Get all the clientList where industry contains UPDATED_INDUSTRY
        defaultClientShouldNotBeFound("industry.contains=" + UPDATED_INDUSTRY);
    }

    @Test
    @Transactional
    void getAllClientsByIndustryNotContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where industry does not contain DEFAULT_INDUSTRY
        defaultClientShouldNotBeFound("industry.doesNotContain=" + DEFAULT_INDUSTRY);

        // Get all the clientList where industry does not contain UPDATED_INDUSTRY
        defaultClientShouldBeFound("industry.doesNotContain=" + UPDATED_INDUSTRY);
    }

    @Test
    @Transactional
    void getAllClientsByBankDetailsIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where bankDetails equals to DEFAULT_BANK_DETAILS
        defaultClientShouldBeFound("bankDetails.equals=" + DEFAULT_BANK_DETAILS);

        // Get all the clientList where bankDetails equals to UPDATED_BANK_DETAILS
        defaultClientShouldNotBeFound("bankDetails.equals=" + UPDATED_BANK_DETAILS);
    }

    @Test
    @Transactional
    void getAllClientsByBankDetailsIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where bankDetails in DEFAULT_BANK_DETAILS or UPDATED_BANK_DETAILS
        defaultClientShouldBeFound("bankDetails.in=" + DEFAULT_BANK_DETAILS + "," + UPDATED_BANK_DETAILS);

        // Get all the clientList where bankDetails equals to UPDATED_BANK_DETAILS
        defaultClientShouldNotBeFound("bankDetails.in=" + UPDATED_BANK_DETAILS);
    }

    @Test
    @Transactional
    void getAllClientsByBankDetailsIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where bankDetails is not null
        defaultClientShouldBeFound("bankDetails.specified=true");

        // Get all the clientList where bankDetails is null
        defaultClientShouldNotBeFound("bankDetails.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByBankDetailsContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where bankDetails contains DEFAULT_BANK_DETAILS
        defaultClientShouldBeFound("bankDetails.contains=" + DEFAULT_BANK_DETAILS);

        // Get all the clientList where bankDetails contains UPDATED_BANK_DETAILS
        defaultClientShouldNotBeFound("bankDetails.contains=" + UPDATED_BANK_DETAILS);
    }

    @Test
    @Transactional
    void getAllClientsByBankDetailsNotContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where bankDetails does not contain DEFAULT_BANK_DETAILS
        defaultClientShouldNotBeFound("bankDetails.doesNotContain=" + DEFAULT_BANK_DETAILS);

        // Get all the clientList where bankDetails does not contain UPDATED_BANK_DETAILS
        defaultClientShouldBeFound("bankDetails.doesNotContain=" + UPDATED_BANK_DETAILS);
    }

    @Test
    @Transactional
    void getAllClientsByClientTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientType equals to DEFAULT_CLIENT_TYPE
        defaultClientShouldBeFound("clientType.equals=" + DEFAULT_CLIENT_TYPE);

        // Get all the clientList where clientType equals to UPDATED_CLIENT_TYPE
        defaultClientShouldNotBeFound("clientType.equals=" + UPDATED_CLIENT_TYPE);
    }

    @Test
    @Transactional
    void getAllClientsByClientTypeIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientType in DEFAULT_CLIENT_TYPE or UPDATED_CLIENT_TYPE
        defaultClientShouldBeFound("clientType.in=" + DEFAULT_CLIENT_TYPE + "," + UPDATED_CLIENT_TYPE);

        // Get all the clientList where clientType equals to UPDATED_CLIENT_TYPE
        defaultClientShouldNotBeFound("clientType.in=" + UPDATED_CLIENT_TYPE);
    }

    @Test
    @Transactional
    void getAllClientsByClientTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where clientType is not null
        defaultClientShouldBeFound("clientType.specified=true");

        // Get all the clientList where clientType is null
        defaultClientShouldNotBeFound("clientType.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByTnaNoIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where tnaNo equals to DEFAULT_TNA_NO
        defaultClientShouldBeFound("tnaNo.equals=" + DEFAULT_TNA_NO);

        // Get all the clientList where tnaNo equals to UPDATED_TNA_NO
        defaultClientShouldNotBeFound("tnaNo.equals=" + UPDATED_TNA_NO);
    }

    @Test
    @Transactional
    void getAllClientsByTnaNoIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where tnaNo in DEFAULT_TNA_NO or UPDATED_TNA_NO
        defaultClientShouldBeFound("tnaNo.in=" + DEFAULT_TNA_NO + "," + UPDATED_TNA_NO);

        // Get all the clientList where tnaNo equals to UPDATED_TNA_NO
        defaultClientShouldNotBeFound("tnaNo.in=" + UPDATED_TNA_NO);
    }

    @Test
    @Transactional
    void getAllClientsByTnaNoIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where tnaNo is not null
        defaultClientShouldBeFound("tnaNo.specified=true");

        // Get all the clientList where tnaNo is null
        defaultClientShouldNotBeFound("tnaNo.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByTnaNoContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where tnaNo contains DEFAULT_TNA_NO
        defaultClientShouldBeFound("tnaNo.contains=" + DEFAULT_TNA_NO);

        // Get all the clientList where tnaNo contains UPDATED_TNA_NO
        defaultClientShouldNotBeFound("tnaNo.contains=" + UPDATED_TNA_NO);
    }

    @Test
    @Transactional
    void getAllClientsByTnaNoNotContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where tnaNo does not contain DEFAULT_TNA_NO
        defaultClientShouldNotBeFound("tnaNo.doesNotContain=" + DEFAULT_TNA_NO);

        // Get all the clientList where tnaNo does not contain UPDATED_TNA_NO
        defaultClientShouldBeFound("tnaNo.doesNotContain=" + UPDATED_TNA_NO);
    }

    @Test
    @Transactional
    void getAllClientsByGstNoIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where gstNo equals to DEFAULT_GST_NO
        defaultClientShouldBeFound("gstNo.equals=" + DEFAULT_GST_NO);

        // Get all the clientList where gstNo equals to UPDATED_GST_NO
        defaultClientShouldNotBeFound("gstNo.equals=" + UPDATED_GST_NO);
    }

    @Test
    @Transactional
    void getAllClientsByGstNoIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where gstNo in DEFAULT_GST_NO or UPDATED_GST_NO
        defaultClientShouldBeFound("gstNo.in=" + DEFAULT_GST_NO + "," + UPDATED_GST_NO);

        // Get all the clientList where gstNo equals to UPDATED_GST_NO
        defaultClientShouldNotBeFound("gstNo.in=" + UPDATED_GST_NO);
    }

    @Test
    @Transactional
    void getAllClientsByGstNoIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where gstNo is not null
        defaultClientShouldBeFound("gstNo.specified=true");

        // Get all the clientList where gstNo is null
        defaultClientShouldNotBeFound("gstNo.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByGstNoContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where gstNo contains DEFAULT_GST_NO
        defaultClientShouldBeFound("gstNo.contains=" + DEFAULT_GST_NO);

        // Get all the clientList where gstNo contains UPDATED_GST_NO
        defaultClientShouldNotBeFound("gstNo.contains=" + UPDATED_GST_NO);
    }

    @Test
    @Transactional
    void getAllClientsByGstNoNotContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where gstNo does not contain DEFAULT_GST_NO
        defaultClientShouldNotBeFound("gstNo.doesNotContain=" + DEFAULT_GST_NO);

        // Get all the clientList where gstNo does not contain UPDATED_GST_NO
        defaultClientShouldBeFound("gstNo.doesNotContain=" + UPDATED_GST_NO);
    }

    @Test
    @Transactional
    void getAllClientsByPanIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where pan equals to DEFAULT_PAN
        defaultClientShouldBeFound("pan.equals=" + DEFAULT_PAN);

        // Get all the clientList where pan equals to UPDATED_PAN
        defaultClientShouldNotBeFound("pan.equals=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    void getAllClientsByPanIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where pan in DEFAULT_PAN or UPDATED_PAN
        defaultClientShouldBeFound("pan.in=" + DEFAULT_PAN + "," + UPDATED_PAN);

        // Get all the clientList where pan equals to UPDATED_PAN
        defaultClientShouldNotBeFound("pan.in=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    void getAllClientsByPanIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where pan is not null
        defaultClientShouldBeFound("pan.specified=true");

        // Get all the clientList where pan is null
        defaultClientShouldNotBeFound("pan.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByPanContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where pan contains DEFAULT_PAN
        defaultClientShouldBeFound("pan.contains=" + DEFAULT_PAN);

        // Get all the clientList where pan contains UPDATED_PAN
        defaultClientShouldNotBeFound("pan.contains=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    void getAllClientsByPanNotContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where pan does not contain DEFAULT_PAN
        defaultClientShouldNotBeFound("pan.doesNotContain=" + DEFAULT_PAN);

        // Get all the clientList where pan does not contain UPDATED_PAN
        defaultClientShouldBeFound("pan.doesNotContain=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    void getAllClientsByUploadClientLogoIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where uploadClientLogo equals to DEFAULT_UPLOAD_CLIENT_LOGO
        defaultClientShouldBeFound("uploadClientLogo.equals=" + DEFAULT_UPLOAD_CLIENT_LOGO);

        // Get all the clientList where uploadClientLogo equals to UPDATED_UPLOAD_CLIENT_LOGO
        defaultClientShouldNotBeFound("uploadClientLogo.equals=" + UPDATED_UPLOAD_CLIENT_LOGO);
    }

    @Test
    @Transactional
    void getAllClientsByUploadClientLogoIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where uploadClientLogo in DEFAULT_UPLOAD_CLIENT_LOGO or UPDATED_UPLOAD_CLIENT_LOGO
        defaultClientShouldBeFound("uploadClientLogo.in=" + DEFAULT_UPLOAD_CLIENT_LOGO + "," + UPDATED_UPLOAD_CLIENT_LOGO);

        // Get all the clientList where uploadClientLogo equals to UPDATED_UPLOAD_CLIENT_LOGO
        defaultClientShouldNotBeFound("uploadClientLogo.in=" + UPDATED_UPLOAD_CLIENT_LOGO);
    }

    @Test
    @Transactional
    void getAllClientsByUploadClientLogoIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where uploadClientLogo is not null
        defaultClientShouldBeFound("uploadClientLogo.specified=true");

        // Get all the clientList where uploadClientLogo is null
        defaultClientShouldNotBeFound("uploadClientLogo.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByUploadClientLogoContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where uploadClientLogo contains DEFAULT_UPLOAD_CLIENT_LOGO
        defaultClientShouldBeFound("uploadClientLogo.contains=" + DEFAULT_UPLOAD_CLIENT_LOGO);

        // Get all the clientList where uploadClientLogo contains UPDATED_UPLOAD_CLIENT_LOGO
        defaultClientShouldNotBeFound("uploadClientLogo.contains=" + UPDATED_UPLOAD_CLIENT_LOGO);
    }

    @Test
    @Transactional
    void getAllClientsByUploadClientLogoNotContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where uploadClientLogo does not contain DEFAULT_UPLOAD_CLIENT_LOGO
        defaultClientShouldNotBeFound("uploadClientLogo.doesNotContain=" + DEFAULT_UPLOAD_CLIENT_LOGO);

        // Get all the clientList where uploadClientLogo does not contain UPDATED_UPLOAD_CLIENT_LOGO
        defaultClientShouldBeFound("uploadClientLogo.doesNotContain=" + UPDATED_UPLOAD_CLIENT_LOGO);
    }

    @Test
    @Transactional
    void getAllClientsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdDate equals to DEFAULT_CREATED_DATE
        defaultClientShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the clientList where createdDate equals to UPDATED_CREATED_DATE
        defaultClientShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllClientsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultClientShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the clientList where createdDate equals to UPDATED_CREATED_DATE
        defaultClientShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllClientsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdDate is not null
        defaultClientShouldBeFound("createdDate.specified=true");

        // Get all the clientList where createdDate is null
        defaultClientShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy equals to DEFAULT_CREATED_BY
        defaultClientShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the clientList where createdBy equals to UPDATED_CREATED_BY
        defaultClientShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultClientShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the clientList where createdBy equals to UPDATED_CREATED_BY
        defaultClientShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy is not null
        defaultClientShouldBeFound("createdBy.specified=true");

        // Get all the clientList where createdBy is null
        defaultClientShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByCreatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy is greater than or equal to DEFAULT_CREATED_BY
        defaultClientShouldBeFound("createdBy.greaterThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the clientList where createdBy is greater than or equal to UPDATED_CREATED_BY
        defaultClientShouldNotBeFound("createdBy.greaterThanOrEqual=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByCreatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy is less than or equal to DEFAULT_CREATED_BY
        defaultClientShouldBeFound("createdBy.lessThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the clientList where createdBy is less than or equal to SMALLER_CREATED_BY
        defaultClientShouldNotBeFound("createdBy.lessThanOrEqual=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByCreatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy is less than DEFAULT_CREATED_BY
        defaultClientShouldNotBeFound("createdBy.lessThan=" + DEFAULT_CREATED_BY);

        // Get all the clientList where createdBy is less than UPDATED_CREATED_BY
        defaultClientShouldBeFound("createdBy.lessThan=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByCreatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where createdBy is greater than DEFAULT_CREATED_BY
        defaultClientShouldNotBeFound("createdBy.greaterThan=" + DEFAULT_CREATED_BY);

        // Get all the clientList where createdBy is greater than SMALLER_CREATED_BY
        defaultClientShouldBeFound("createdBy.greaterThan=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultClientShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the clientList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultClientShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultClientShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the clientList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultClientShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedDate is not null
        defaultClientShouldBeFound("updatedDate.specified=true");

        // Get all the clientList where updatedDate is null
        defaultClientShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultClientShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the clientList where updatedBy equals to UPDATED_UPDATED_BY
        defaultClientShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultClientShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the clientList where updatedBy equals to UPDATED_UPDATED_BY
        defaultClientShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy is not null
        defaultClientShouldBeFound("updatedBy.specified=true");

        // Get all the clientList where updatedBy is null
        defaultClientShouldNotBeFound("updatedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy is greater than or equal to DEFAULT_UPDATED_BY
        defaultClientShouldBeFound("updatedBy.greaterThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the clientList where updatedBy is greater than or equal to UPDATED_UPDATED_BY
        defaultClientShouldNotBeFound("updatedBy.greaterThanOrEqual=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy is less than or equal to DEFAULT_UPDATED_BY
        defaultClientShouldBeFound("updatedBy.lessThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the clientList where updatedBy is less than or equal to SMALLER_UPDATED_BY
        defaultClientShouldNotBeFound("updatedBy.lessThanOrEqual=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy is less than DEFAULT_UPDATED_BY
        defaultClientShouldNotBeFound("updatedBy.lessThan=" + DEFAULT_UPDATED_BY);

        // Get all the clientList where updatedBy is less than UPDATED_UPDATED_BY
        defaultClientShouldBeFound("updatedBy.lessThan=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByUpdatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where updatedBy is greater than DEFAULT_UPDATED_BY
        defaultClientShouldNotBeFound("updatedBy.greaterThan=" + DEFAULT_UPDATED_BY);

        // Get all the clientList where updatedBy is greater than SMALLER_UPDATED_BY
        defaultClientShouldBeFound("updatedBy.greaterThan=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllClientsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where status equals to DEFAULT_STATUS
        defaultClientShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the clientList where status equals to UPDATED_STATUS
        defaultClientShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllClientsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultClientShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the clientList where status equals to UPDATED_STATUS
        defaultClientShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllClientsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where status is not null
        defaultClientShouldBeFound("status.specified=true");

        // Get all the clientList where status is null
        defaultClientShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    void getAllClientsByStatusContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where status contains DEFAULT_STATUS
        defaultClientShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the clientList where status contains UPDATED_STATUS
        defaultClientShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllClientsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList where status does not contain DEFAULT_STATUS
        defaultClientShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the clientList where status does not contain UPDATED_STATUS
        defaultClientShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllClientsByClientSettingIsEqualToSomething() throws Exception {
        ClientSetting clientSetting;
        if (TestUtil.findAll(em, ClientSetting.class).isEmpty()) {
            clientRepository.saveAndFlush(client);
            clientSetting = ClientSettingResourceIT.createEntity(em);
        } else {
            clientSetting = TestUtil.findAll(em, ClientSetting.class).get(0);
        }
        em.persist(clientSetting);
        em.flush();
        client.setClientSetting(clientSetting);
        clientRepository.saveAndFlush(client);
        Long clientSettingId = clientSetting.getId();

        // Get all the clientList where clientSetting equals to clientSettingId
        defaultClientShouldBeFound("clientSettingId.equals=" + clientSettingId);

        // Get all the clientList where clientSetting equals to (clientSettingId + 1)
        defaultClientShouldNotBeFound("clientSettingId.equals=" + (clientSettingId + 1));
    }

    @Test
    @Transactional
    void getAllClientsByCandidateIsEqualToSomething() throws Exception {
        Candidate candidate;
        if (TestUtil.findAll(em, Candidate.class).isEmpty()) {
            clientRepository.saveAndFlush(client);
            candidate = CandidateResourceIT.createEntity(em);
        } else {
            candidate = TestUtil.findAll(em, Candidate.class).get(0);
        }
        em.persist(candidate);
        em.flush();
        client.addCandidate(candidate);
        clientRepository.saveAndFlush(client);
        Long candidateId = candidate.getId();

        // Get all the clientList where candidate equals to candidateId
        defaultClientShouldBeFound("candidateId.equals=" + candidateId);

        // Get all the clientList where candidate equals to (candidateId + 1)
        defaultClientShouldNotBeFound("candidateId.equals=" + (candidateId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultClientShouldBeFound(String filter) throws Exception {
        restClientMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(client.getId().intValue())))
            .andExpect(jsonPath("$.[*].legalEntity").value(hasItem(DEFAULT_LEGAL_ENTITY)))
            .andExpect(jsonPath("$.[*].industry").value(hasItem(DEFAULT_INDUSTRY)))
            .andExpect(jsonPath("$.[*].bankDetails").value(hasItem(DEFAULT_BANK_DETAILS)))
            .andExpect(jsonPath("$.[*].clientType").value(hasItem(DEFAULT_CLIENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].tnaNo").value(hasItem(DEFAULT_TNA_NO)))
            .andExpect(jsonPath("$.[*].gstNo").value(hasItem(DEFAULT_GST_NO)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].uploadClientLogo").value(hasItem(DEFAULT_UPLOAD_CLIENT_LOGO)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));

        // Check, that the count call also returns 1
        restClientMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultClientShouldNotBeFound(String filter) throws Exception {
        restClientMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restClientMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingClient() throws Exception {
        // Get the client
        restClientMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingClient() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        int databaseSizeBeforeUpdate = clientRepository.findAll().size();

        // Update the client
        Client updatedClient = clientRepository.findById(client.getId()).get();
        // Disconnect from session so that the updates on updatedClient are not directly saved in db
        em.detach(updatedClient);
        updatedClient
            .legalEntity(UPDATED_LEGAL_ENTITY)
            .industry(UPDATED_INDUSTRY)
            .bankDetails(UPDATED_BANK_DETAILS)
            .clientType(UPDATED_CLIENT_TYPE)
            .tnaNo(UPDATED_TNA_NO)
            .gstNo(UPDATED_GST_NO)
            .pan(UPDATED_PAN)
            .uploadClientLogo(UPDATED_UPLOAD_CLIENT_LOGO)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);
        ClientDTO clientDTO = clientMapper.toDto(updatedClient);

        restClientMockMvc
            .perform(
                put(ENTITY_API_URL_ID, clientDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clientDTO))
            )
            .andExpect(status().isOk());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
        Client testClient = clientList.get(clientList.size() - 1);
        assertThat(testClient.getLegalEntity()).isEqualTo(UPDATED_LEGAL_ENTITY);
        assertThat(testClient.getIndustry()).isEqualTo(UPDATED_INDUSTRY);
        assertThat(testClient.getBankDetails()).isEqualTo(UPDATED_BANK_DETAILS);
        assertThat(testClient.getClientType()).isEqualTo(UPDATED_CLIENT_TYPE);
        assertThat(testClient.getTnaNo()).isEqualTo(UPDATED_TNA_NO);
        assertThat(testClient.getGstNo()).isEqualTo(UPDATED_GST_NO);
        assertThat(testClient.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testClient.getUploadClientLogo()).isEqualTo(UPDATED_UPLOAD_CLIENT_LOGO);
        assertThat(testClient.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testClient.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testClient.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testClient.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testClient.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingClient() throws Exception {
        int databaseSizeBeforeUpdate = clientRepository.findAll().size();
        client.setId(count.incrementAndGet());

        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientMockMvc
            .perform(
                put(ENTITY_API_URL_ID, clientDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clientDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchClient() throws Exception {
        int databaseSizeBeforeUpdate = clientRepository.findAll().size();
        client.setId(count.incrementAndGet());

        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clientDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamClient() throws Exception {
        int databaseSizeBeforeUpdate = clientRepository.findAll().size();
        client.setId(count.incrementAndGet());

        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateClientWithPatch() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        int databaseSizeBeforeUpdate = clientRepository.findAll().size();

        // Update the client using partial update
        Client partialUpdatedClient = new Client();
        partialUpdatedClient.setId(client.getId());

        partialUpdatedClient
            .legalEntity(UPDATED_LEGAL_ENTITY)
            .industry(UPDATED_INDUSTRY)
            .bankDetails(UPDATED_BANK_DETAILS)
            .pan(UPDATED_PAN)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);

        restClientMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClient.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClient))
            )
            .andExpect(status().isOk());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
        Client testClient = clientList.get(clientList.size() - 1);
        assertThat(testClient.getLegalEntity()).isEqualTo(UPDATED_LEGAL_ENTITY);
        assertThat(testClient.getIndustry()).isEqualTo(UPDATED_INDUSTRY);
        assertThat(testClient.getBankDetails()).isEqualTo(UPDATED_BANK_DETAILS);
        assertThat(testClient.getClientType()).isEqualTo(DEFAULT_CLIENT_TYPE);
        assertThat(testClient.getTnaNo()).isEqualTo(DEFAULT_TNA_NO);
        assertThat(testClient.getGstNo()).isEqualTo(DEFAULT_GST_NO);
        assertThat(testClient.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testClient.getUploadClientLogo()).isEqualTo(DEFAULT_UPLOAD_CLIENT_LOGO);
        assertThat(testClient.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testClient.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testClient.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testClient.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testClient.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateClientWithPatch() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        int databaseSizeBeforeUpdate = clientRepository.findAll().size();

        // Update the client using partial update
        Client partialUpdatedClient = new Client();
        partialUpdatedClient.setId(client.getId());

        partialUpdatedClient
            .legalEntity(UPDATED_LEGAL_ENTITY)
            .industry(UPDATED_INDUSTRY)
            .bankDetails(UPDATED_BANK_DETAILS)
            .clientType(UPDATED_CLIENT_TYPE)
            .tnaNo(UPDATED_TNA_NO)
            .gstNo(UPDATED_GST_NO)
            .pan(UPDATED_PAN)
            .uploadClientLogo(UPDATED_UPLOAD_CLIENT_LOGO)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);

        restClientMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClient.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClient))
            )
            .andExpect(status().isOk());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
        Client testClient = clientList.get(clientList.size() - 1);
        assertThat(testClient.getLegalEntity()).isEqualTo(UPDATED_LEGAL_ENTITY);
        assertThat(testClient.getIndustry()).isEqualTo(UPDATED_INDUSTRY);
        assertThat(testClient.getBankDetails()).isEqualTo(UPDATED_BANK_DETAILS);
        assertThat(testClient.getClientType()).isEqualTo(UPDATED_CLIENT_TYPE);
        assertThat(testClient.getTnaNo()).isEqualTo(UPDATED_TNA_NO);
        assertThat(testClient.getGstNo()).isEqualTo(UPDATED_GST_NO);
        assertThat(testClient.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testClient.getUploadClientLogo()).isEqualTo(UPDATED_UPLOAD_CLIENT_LOGO);
        assertThat(testClient.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testClient.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testClient.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testClient.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testClient.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingClient() throws Exception {
        int databaseSizeBeforeUpdate = clientRepository.findAll().size();
        client.setId(count.incrementAndGet());

        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, clientDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(clientDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchClient() throws Exception {
        int databaseSizeBeforeUpdate = clientRepository.findAll().size();
        client.setId(count.incrementAndGet());

        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(clientDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamClient() throws Exception {
        int databaseSizeBeforeUpdate = clientRepository.findAll().size();
        client.setId(count.incrementAndGet());

        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(clientDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteClient() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        int databaseSizeBeforeDelete = clientRepository.findAll().size();

        // Delete the client
        restClientMockMvc
            .perform(delete(ENTITY_API_URL_ID, client.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
