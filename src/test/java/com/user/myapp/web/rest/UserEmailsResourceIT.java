package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.UserEmails;
import com.user.myapp.repository.UserEmailsRepository;
import com.user.myapp.service.criteria.UserEmailsCriteria;
import com.user.myapp.service.dto.UserEmailsDTO;
import com.user.myapp.service.mapper.UserEmailsMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UserEmailsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UserEmailsResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final Long DEFAULT_LOGIN_ID = 1L;
    private static final Long UPDATED_LOGIN_ID = 2L;
    private static final Long SMALLER_LOGIN_ID = 1L - 1L;

    private static final String ENTITY_API_URL = "/api/user-emails";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UserEmailsRepository userEmailsRepository;

    @Autowired
    private UserEmailsMapper userEmailsMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserEmailsMockMvc;

    private UserEmails userEmails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserEmails createEntity(EntityManager em) {
        UserEmails userEmails = new UserEmails().email(DEFAULT_EMAIL).loginId(DEFAULT_LOGIN_ID);
        return userEmails;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserEmails createUpdatedEntity(EntityManager em) {
        UserEmails userEmails = new UserEmails().email(UPDATED_EMAIL).loginId(UPDATED_LOGIN_ID);
        return userEmails;
    }

    @BeforeEach
    public void initTest() {
        userEmails = createEntity(em);
    }

    @Test
    @Transactional
    void createUserEmails() throws Exception {
        int databaseSizeBeforeCreate = userEmailsRepository.findAll().size();
        // Create the UserEmails
        UserEmailsDTO userEmailsDTO = userEmailsMapper.toDto(userEmails);
        restUserEmailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userEmailsDTO)))
            .andExpect(status().isCreated());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeCreate + 1);
        UserEmails testUserEmails = userEmailsList.get(userEmailsList.size() - 1);
        assertThat(testUserEmails.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testUserEmails.getLoginId()).isEqualTo(DEFAULT_LOGIN_ID);
    }

    @Test
    @Transactional
    void createUserEmailsWithExistingId() throws Exception {
        // Create the UserEmails with an existing ID
        userEmails.setId(1L);
        UserEmailsDTO userEmailsDTO = userEmailsMapper.toDto(userEmails);

        int databaseSizeBeforeCreate = userEmailsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserEmailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userEmailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllUserEmails() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList
        restUserEmailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userEmails.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID.intValue())));
    }

    @Test
    @Transactional
    void getUserEmails() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get the userEmails
        restUserEmailsMockMvc
            .perform(get(ENTITY_API_URL_ID, userEmails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userEmails.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.loginId").value(DEFAULT_LOGIN_ID.intValue()));
    }

    @Test
    @Transactional
    void getUserEmailsByIdFiltering() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        Long id = userEmails.getId();

        defaultUserEmailsShouldBeFound("id.equals=" + id);
        defaultUserEmailsShouldNotBeFound("id.notEquals=" + id);

        defaultUserEmailsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUserEmailsShouldNotBeFound("id.greaterThan=" + id);

        defaultUserEmailsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUserEmailsShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllUserEmailsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where email equals to DEFAULT_EMAIL
        defaultUserEmailsShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the userEmailsList where email equals to UPDATED_EMAIL
        defaultUserEmailsShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllUserEmailsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultUserEmailsShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the userEmailsList where email equals to UPDATED_EMAIL
        defaultUserEmailsShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllUserEmailsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where email is not null
        defaultUserEmailsShouldBeFound("email.specified=true");

        // Get all the userEmailsList where email is null
        defaultUserEmailsShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    void getAllUserEmailsByEmailContainsSomething() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where email contains DEFAULT_EMAIL
        defaultUserEmailsShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the userEmailsList where email contains UPDATED_EMAIL
        defaultUserEmailsShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllUserEmailsByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where email does not contain DEFAULT_EMAIL
        defaultUserEmailsShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the userEmailsList where email does not contain UPDATED_EMAIL
        defaultUserEmailsShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllUserEmailsByLoginIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where loginId equals to DEFAULT_LOGIN_ID
        defaultUserEmailsShouldBeFound("loginId.equals=" + DEFAULT_LOGIN_ID);

        // Get all the userEmailsList where loginId equals to UPDATED_LOGIN_ID
        defaultUserEmailsShouldNotBeFound("loginId.equals=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllUserEmailsByLoginIdIsInShouldWork() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where loginId in DEFAULT_LOGIN_ID or UPDATED_LOGIN_ID
        defaultUserEmailsShouldBeFound("loginId.in=" + DEFAULT_LOGIN_ID + "," + UPDATED_LOGIN_ID);

        // Get all the userEmailsList where loginId equals to UPDATED_LOGIN_ID
        defaultUserEmailsShouldNotBeFound("loginId.in=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllUserEmailsByLoginIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where loginId is not null
        defaultUserEmailsShouldBeFound("loginId.specified=true");

        // Get all the userEmailsList where loginId is null
        defaultUserEmailsShouldNotBeFound("loginId.specified=false");
    }

    @Test
    @Transactional
    void getAllUserEmailsByLoginIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where loginId is greater than or equal to DEFAULT_LOGIN_ID
        defaultUserEmailsShouldBeFound("loginId.greaterThanOrEqual=" + DEFAULT_LOGIN_ID);

        // Get all the userEmailsList where loginId is greater than or equal to UPDATED_LOGIN_ID
        defaultUserEmailsShouldNotBeFound("loginId.greaterThanOrEqual=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllUserEmailsByLoginIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where loginId is less than or equal to DEFAULT_LOGIN_ID
        defaultUserEmailsShouldBeFound("loginId.lessThanOrEqual=" + DEFAULT_LOGIN_ID);

        // Get all the userEmailsList where loginId is less than or equal to SMALLER_LOGIN_ID
        defaultUserEmailsShouldNotBeFound("loginId.lessThanOrEqual=" + SMALLER_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllUserEmailsByLoginIdIsLessThanSomething() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where loginId is less than DEFAULT_LOGIN_ID
        defaultUserEmailsShouldNotBeFound("loginId.lessThan=" + DEFAULT_LOGIN_ID);

        // Get all the userEmailsList where loginId is less than UPDATED_LOGIN_ID
        defaultUserEmailsShouldBeFound("loginId.lessThan=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllUserEmailsByLoginIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        // Get all the userEmailsList where loginId is greater than DEFAULT_LOGIN_ID
        defaultUserEmailsShouldNotBeFound("loginId.greaterThan=" + DEFAULT_LOGIN_ID);

        // Get all the userEmailsList where loginId is greater than SMALLER_LOGIN_ID
        defaultUserEmailsShouldBeFound("loginId.greaterThan=" + SMALLER_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllUserEmailsByInPreferenceUserIsEqualToSomething() throws Exception {
        InPreferenceUser inPreferenceUser;
        if (TestUtil.findAll(em, InPreferenceUser.class).isEmpty()) {
            userEmailsRepository.saveAndFlush(userEmails);
            inPreferenceUser = InPreferenceUserResourceIT.createEntity(em);
        } else {
            inPreferenceUser = TestUtil.findAll(em, InPreferenceUser.class).get(0);
        }
        em.persist(inPreferenceUser);
        em.flush();
        userEmails.setInPreferenceUser(inPreferenceUser);
        userEmailsRepository.saveAndFlush(userEmails);
        Long inPreferenceUserId = inPreferenceUser.getId();

        // Get all the userEmailsList where inPreferenceUser equals to inPreferenceUserId
        defaultUserEmailsShouldBeFound("inPreferenceUserId.equals=" + inPreferenceUserId);

        // Get all the userEmailsList where inPreferenceUser equals to (inPreferenceUserId + 1)
        defaultUserEmailsShouldNotBeFound("inPreferenceUserId.equals=" + (inPreferenceUserId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserEmailsShouldBeFound(String filter) throws Exception {
        restUserEmailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userEmails.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID.intValue())));

        // Check, that the count call also returns 1
        restUserEmailsMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserEmailsShouldNotBeFound(String filter) throws Exception {
        restUserEmailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserEmailsMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingUserEmails() throws Exception {
        // Get the userEmails
        restUserEmailsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingUserEmails() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        int databaseSizeBeforeUpdate = userEmailsRepository.findAll().size();

        // Update the userEmails
        UserEmails updatedUserEmails = userEmailsRepository.findById(userEmails.getId()).get();
        // Disconnect from session so that the updates on updatedUserEmails are not directly saved in db
        em.detach(updatedUserEmails);
        updatedUserEmails.email(UPDATED_EMAIL).loginId(UPDATED_LOGIN_ID);
        UserEmailsDTO userEmailsDTO = userEmailsMapper.toDto(updatedUserEmails);

        restUserEmailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userEmailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userEmailsDTO))
            )
            .andExpect(status().isOk());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeUpdate);
        UserEmails testUserEmails = userEmailsList.get(userEmailsList.size() - 1);
        assertThat(testUserEmails.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserEmails.getLoginId()).isEqualTo(UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void putNonExistingUserEmails() throws Exception {
        int databaseSizeBeforeUpdate = userEmailsRepository.findAll().size();
        userEmails.setId(count.incrementAndGet());

        // Create the UserEmails
        UserEmailsDTO userEmailsDTO = userEmailsMapper.toDto(userEmails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserEmailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userEmailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userEmailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUserEmails() throws Exception {
        int databaseSizeBeforeUpdate = userEmailsRepository.findAll().size();
        userEmails.setId(count.incrementAndGet());

        // Create the UserEmails
        UserEmailsDTO userEmailsDTO = userEmailsMapper.toDto(userEmails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserEmailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userEmailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUserEmails() throws Exception {
        int databaseSizeBeforeUpdate = userEmailsRepository.findAll().size();
        userEmails.setId(count.incrementAndGet());

        // Create the UserEmails
        UserEmailsDTO userEmailsDTO = userEmailsMapper.toDto(userEmails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserEmailsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userEmailsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUserEmailsWithPatch() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        int databaseSizeBeforeUpdate = userEmailsRepository.findAll().size();

        // Update the userEmails using partial update
        UserEmails partialUpdatedUserEmails = new UserEmails();
        partialUpdatedUserEmails.setId(userEmails.getId());

        partialUpdatedUserEmails.email(UPDATED_EMAIL).loginId(UPDATED_LOGIN_ID);

        restUserEmailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserEmails.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserEmails))
            )
            .andExpect(status().isOk());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeUpdate);
        UserEmails testUserEmails = userEmailsList.get(userEmailsList.size() - 1);
        assertThat(testUserEmails.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserEmails.getLoginId()).isEqualTo(UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void fullUpdateUserEmailsWithPatch() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        int databaseSizeBeforeUpdate = userEmailsRepository.findAll().size();

        // Update the userEmails using partial update
        UserEmails partialUpdatedUserEmails = new UserEmails();
        partialUpdatedUserEmails.setId(userEmails.getId());

        partialUpdatedUserEmails.email(UPDATED_EMAIL).loginId(UPDATED_LOGIN_ID);

        restUserEmailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserEmails.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserEmails))
            )
            .andExpect(status().isOk());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeUpdate);
        UserEmails testUserEmails = userEmailsList.get(userEmailsList.size() - 1);
        assertThat(testUserEmails.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserEmails.getLoginId()).isEqualTo(UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void patchNonExistingUserEmails() throws Exception {
        int databaseSizeBeforeUpdate = userEmailsRepository.findAll().size();
        userEmails.setId(count.incrementAndGet());

        // Create the UserEmails
        UserEmailsDTO userEmailsDTO = userEmailsMapper.toDto(userEmails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserEmailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, userEmailsDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userEmailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUserEmails() throws Exception {
        int databaseSizeBeforeUpdate = userEmailsRepository.findAll().size();
        userEmails.setId(count.incrementAndGet());

        // Create the UserEmails
        UserEmailsDTO userEmailsDTO = userEmailsMapper.toDto(userEmails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserEmailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userEmailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUserEmails() throws Exception {
        int databaseSizeBeforeUpdate = userEmailsRepository.findAll().size();
        userEmails.setId(count.incrementAndGet());

        // Create the UserEmails
        UserEmailsDTO userEmailsDTO = userEmailsMapper.toDto(userEmails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserEmailsMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(userEmailsDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserEmails in the database
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUserEmails() throws Exception {
        // Initialize the database
        userEmailsRepository.saveAndFlush(userEmails);

        int databaseSizeBeforeDelete = userEmailsRepository.findAll().size();

        // Delete the userEmails
        restUserEmailsMockMvc
            .perform(delete(ENTITY_API_URL_ID, userEmails.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserEmails> userEmailsList = userEmailsRepository.findAll();
        assertThat(userEmailsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
