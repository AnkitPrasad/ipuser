package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.ClientSetting;
import com.user.myapp.repository.ClientSettingRepository;
import com.user.myapp.service.criteria.ClientSettingCriteria;
import com.user.myapp.service.dto.ClientSettingDTO;
import com.user.myapp.service.mapper.ClientSettingMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ClientSettingResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ClientSettingResourceIT {

    private static final Integer DEFAULT_CANDIDATE_SELF_ASSESSMENT = 1;
    private static final Integer UPDATED_CANDIDATE_SELF_ASSESSMENT = 2;
    private static final Integer SMALLER_CANDIDATE_SELF_ASSESSMENT = 1 - 1;

    private static final Integer DEFAULT_AUTO_RENINDER_ASSESSMENT = 1;
    private static final Integer UPDATED_AUTO_RENINDER_ASSESSMENT = 2;
    private static final Integer SMALLER_AUTO_RENINDER_ASSESSMENT = 1 - 1;

    private static final Boolean DEFAULT_ALLOW_CANDIDATE_PAID_VERSION = false;
    private static final Boolean UPDATED_ALLOW_CANDIDATE_PAID_VERSION = true;

    private static final Boolean DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION = false;
    private static final Boolean UPDATED_ALLOW_EMPLOYESS_PAID_VERSION = true;

    private static final Integer DEFAULT_AUTO_ARCHIEVE_REQUISITIONS = 1;
    private static final Integer UPDATED_AUTO_ARCHIEVE_REQUISITIONS = 2;
    private static final Integer SMALLER_AUTO_ARCHIEVE_REQUISITIONS = 1 - 1;

    private static final String DEFAULT_COMPANY_THEME_DARK_COLOUR = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_THEME_DARK_COLOUR = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_THEME_LIGHT_COLOUR = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_THEME_LIGHT_COLOUR = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_URL = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_URL = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_MENU = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_MENU = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/client-settings";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ClientSettingRepository clientSettingRepository;

    @Autowired
    private ClientSettingMapper clientSettingMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restClientSettingMockMvc;

    private ClientSetting clientSetting;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientSetting createEntity(EntityManager em) {
        ClientSetting clientSetting = new ClientSetting()
            .candidateSelfAssessment(DEFAULT_CANDIDATE_SELF_ASSESSMENT)
            .autoReninderAssessment(DEFAULT_AUTO_RENINDER_ASSESSMENT)
            .allowCandidatePaidVersion(DEFAULT_ALLOW_CANDIDATE_PAID_VERSION)
            .allowEmployessPaidVersion(DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION)
            .autoArchieveRequisitions(DEFAULT_AUTO_ARCHIEVE_REQUISITIONS)
            .companyThemeDarkColour(DEFAULT_COMPANY_THEME_DARK_COLOUR)
            .companyThemeLightColour(DEFAULT_COMPANY_THEME_LIGHT_COLOUR)
            .companyUrl(DEFAULT_COMPANY_URL)
            .companyMenu(DEFAULT_COMPANY_MENU);
        return clientSetting;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientSetting createUpdatedEntity(EntityManager em) {
        ClientSetting clientSetting = new ClientSetting()
            .candidateSelfAssessment(UPDATED_CANDIDATE_SELF_ASSESSMENT)
            .autoReninderAssessment(UPDATED_AUTO_RENINDER_ASSESSMENT)
            .allowCandidatePaidVersion(UPDATED_ALLOW_CANDIDATE_PAID_VERSION)
            .allowEmployessPaidVersion(UPDATED_ALLOW_EMPLOYESS_PAID_VERSION)
            .autoArchieveRequisitions(UPDATED_AUTO_ARCHIEVE_REQUISITIONS)
            .companyThemeDarkColour(UPDATED_COMPANY_THEME_DARK_COLOUR)
            .companyThemeLightColour(UPDATED_COMPANY_THEME_LIGHT_COLOUR)
            .companyUrl(UPDATED_COMPANY_URL)
            .companyMenu(UPDATED_COMPANY_MENU);
        return clientSetting;
    }

    @BeforeEach
    public void initTest() {
        clientSetting = createEntity(em);
    }

    @Test
    @Transactional
    void createClientSetting() throws Exception {
        int databaseSizeBeforeCreate = clientSettingRepository.findAll().size();
        // Create the ClientSetting
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);
        restClientSettingMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientSettingDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeCreate + 1);
        ClientSetting testClientSetting = clientSettingList.get(clientSettingList.size() - 1);
        assertThat(testClientSetting.getCandidateSelfAssessment()).isEqualTo(DEFAULT_CANDIDATE_SELF_ASSESSMENT);
        assertThat(testClientSetting.getAutoReninderAssessment()).isEqualTo(DEFAULT_AUTO_RENINDER_ASSESSMENT);
        assertThat(testClientSetting.getAllowCandidatePaidVersion()).isEqualTo(DEFAULT_ALLOW_CANDIDATE_PAID_VERSION);
        assertThat(testClientSetting.getAllowEmployessPaidVersion()).isEqualTo(DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION);
        assertThat(testClientSetting.getAutoArchieveRequisitions()).isEqualTo(DEFAULT_AUTO_ARCHIEVE_REQUISITIONS);
        assertThat(testClientSetting.getCompanyThemeDarkColour()).isEqualTo(DEFAULT_COMPANY_THEME_DARK_COLOUR);
        assertThat(testClientSetting.getCompanyThemeLightColour()).isEqualTo(DEFAULT_COMPANY_THEME_LIGHT_COLOUR);
        assertThat(testClientSetting.getCompanyUrl()).isEqualTo(DEFAULT_COMPANY_URL);
        assertThat(testClientSetting.getCompanyMenu()).isEqualTo(DEFAULT_COMPANY_MENU);
    }

    @Test
    @Transactional
    void createClientSettingWithExistingId() throws Exception {
        // Create the ClientSetting with an existing ID
        clientSetting.setId(1L);
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        int databaseSizeBeforeCreate = clientSettingRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientSettingMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientSettingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllClientSettings() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList
        restClientSettingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientSetting.getId().intValue())))
            .andExpect(jsonPath("$.[*].candidateSelfAssessment").value(hasItem(DEFAULT_CANDIDATE_SELF_ASSESSMENT)))
            .andExpect(jsonPath("$.[*].autoReninderAssessment").value(hasItem(DEFAULT_AUTO_RENINDER_ASSESSMENT)))
            .andExpect(jsonPath("$.[*].allowCandidatePaidVersion").value(hasItem(DEFAULT_ALLOW_CANDIDATE_PAID_VERSION.booleanValue())))
            .andExpect(jsonPath("$.[*].allowEmployessPaidVersion").value(hasItem(DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION.booleanValue())))
            .andExpect(jsonPath("$.[*].autoArchieveRequisitions").value(hasItem(DEFAULT_AUTO_ARCHIEVE_REQUISITIONS)))
            .andExpect(jsonPath("$.[*].companyThemeDarkColour").value(hasItem(DEFAULT_COMPANY_THEME_DARK_COLOUR)))
            .andExpect(jsonPath("$.[*].companyThemeLightColour").value(hasItem(DEFAULT_COMPANY_THEME_LIGHT_COLOUR)))
            .andExpect(jsonPath("$.[*].companyUrl").value(hasItem(DEFAULT_COMPANY_URL)))
            .andExpect(jsonPath("$.[*].companyMenu").value(hasItem(DEFAULT_COMPANY_MENU)));
    }

    @Test
    @Transactional
    void getClientSetting() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get the clientSetting
        restClientSettingMockMvc
            .perform(get(ENTITY_API_URL_ID, clientSetting.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(clientSetting.getId().intValue()))
            .andExpect(jsonPath("$.candidateSelfAssessment").value(DEFAULT_CANDIDATE_SELF_ASSESSMENT))
            .andExpect(jsonPath("$.autoReninderAssessment").value(DEFAULT_AUTO_RENINDER_ASSESSMENT))
            .andExpect(jsonPath("$.allowCandidatePaidVersion").value(DEFAULT_ALLOW_CANDIDATE_PAID_VERSION.booleanValue()))
            .andExpect(jsonPath("$.allowEmployessPaidVersion").value(DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION.booleanValue()))
            .andExpect(jsonPath("$.autoArchieveRequisitions").value(DEFAULT_AUTO_ARCHIEVE_REQUISITIONS))
            .andExpect(jsonPath("$.companyThemeDarkColour").value(DEFAULT_COMPANY_THEME_DARK_COLOUR))
            .andExpect(jsonPath("$.companyThemeLightColour").value(DEFAULT_COMPANY_THEME_LIGHT_COLOUR))
            .andExpect(jsonPath("$.companyUrl").value(DEFAULT_COMPANY_URL))
            .andExpect(jsonPath("$.companyMenu").value(DEFAULT_COMPANY_MENU));
    }

    @Test
    @Transactional
    void getClientSettingsByIdFiltering() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        Long id = clientSetting.getId();

        defaultClientSettingShouldBeFound("id.equals=" + id);
        defaultClientSettingShouldNotBeFound("id.notEquals=" + id);

        defaultClientSettingShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultClientSettingShouldNotBeFound("id.greaterThan=" + id);

        defaultClientSettingShouldBeFound("id.lessThanOrEqual=" + id);
        defaultClientSettingShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCandidateSelfAssessmentIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where candidateSelfAssessment equals to DEFAULT_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldBeFound("candidateSelfAssessment.equals=" + DEFAULT_CANDIDATE_SELF_ASSESSMENT);

        // Get all the clientSettingList where candidateSelfAssessment equals to UPDATED_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldNotBeFound("candidateSelfAssessment.equals=" + UPDATED_CANDIDATE_SELF_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCandidateSelfAssessmentIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where candidateSelfAssessment in DEFAULT_CANDIDATE_SELF_ASSESSMENT or UPDATED_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldBeFound(
            "candidateSelfAssessment.in=" + DEFAULT_CANDIDATE_SELF_ASSESSMENT + "," + UPDATED_CANDIDATE_SELF_ASSESSMENT
        );

        // Get all the clientSettingList where candidateSelfAssessment equals to UPDATED_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldNotBeFound("candidateSelfAssessment.in=" + UPDATED_CANDIDATE_SELF_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCandidateSelfAssessmentIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where candidateSelfAssessment is not null
        defaultClientSettingShouldBeFound("candidateSelfAssessment.specified=true");

        // Get all the clientSettingList where candidateSelfAssessment is null
        defaultClientSettingShouldNotBeFound("candidateSelfAssessment.specified=false");
    }

    @Test
    @Transactional
    void getAllClientSettingsByCandidateSelfAssessmentIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where candidateSelfAssessment is greater than or equal to DEFAULT_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldBeFound("candidateSelfAssessment.greaterThanOrEqual=" + DEFAULT_CANDIDATE_SELF_ASSESSMENT);

        // Get all the clientSettingList where candidateSelfAssessment is greater than or equal to UPDATED_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldNotBeFound("candidateSelfAssessment.greaterThanOrEqual=" + UPDATED_CANDIDATE_SELF_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCandidateSelfAssessmentIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where candidateSelfAssessment is less than or equal to DEFAULT_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldBeFound("candidateSelfAssessment.lessThanOrEqual=" + DEFAULT_CANDIDATE_SELF_ASSESSMENT);

        // Get all the clientSettingList where candidateSelfAssessment is less than or equal to SMALLER_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldNotBeFound("candidateSelfAssessment.lessThanOrEqual=" + SMALLER_CANDIDATE_SELF_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCandidateSelfAssessmentIsLessThanSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where candidateSelfAssessment is less than DEFAULT_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldNotBeFound("candidateSelfAssessment.lessThan=" + DEFAULT_CANDIDATE_SELF_ASSESSMENT);

        // Get all the clientSettingList where candidateSelfAssessment is less than UPDATED_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldBeFound("candidateSelfAssessment.lessThan=" + UPDATED_CANDIDATE_SELF_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCandidateSelfAssessmentIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where candidateSelfAssessment is greater than DEFAULT_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldNotBeFound("candidateSelfAssessment.greaterThan=" + DEFAULT_CANDIDATE_SELF_ASSESSMENT);

        // Get all the clientSettingList where candidateSelfAssessment is greater than SMALLER_CANDIDATE_SELF_ASSESSMENT
        defaultClientSettingShouldBeFound("candidateSelfAssessment.greaterThan=" + SMALLER_CANDIDATE_SELF_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoReninderAssessmentIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoReninderAssessment equals to DEFAULT_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldBeFound("autoReninderAssessment.equals=" + DEFAULT_AUTO_RENINDER_ASSESSMENT);

        // Get all the clientSettingList where autoReninderAssessment equals to UPDATED_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldNotBeFound("autoReninderAssessment.equals=" + UPDATED_AUTO_RENINDER_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoReninderAssessmentIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoReninderAssessment in DEFAULT_AUTO_RENINDER_ASSESSMENT or UPDATED_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldBeFound(
            "autoReninderAssessment.in=" + DEFAULT_AUTO_RENINDER_ASSESSMENT + "," + UPDATED_AUTO_RENINDER_ASSESSMENT
        );

        // Get all the clientSettingList where autoReninderAssessment equals to UPDATED_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldNotBeFound("autoReninderAssessment.in=" + UPDATED_AUTO_RENINDER_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoReninderAssessmentIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoReninderAssessment is not null
        defaultClientSettingShouldBeFound("autoReninderAssessment.specified=true");

        // Get all the clientSettingList where autoReninderAssessment is null
        defaultClientSettingShouldNotBeFound("autoReninderAssessment.specified=false");
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoReninderAssessmentIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoReninderAssessment is greater than or equal to DEFAULT_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldBeFound("autoReninderAssessment.greaterThanOrEqual=" + DEFAULT_AUTO_RENINDER_ASSESSMENT);

        // Get all the clientSettingList where autoReninderAssessment is greater than or equal to UPDATED_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldNotBeFound("autoReninderAssessment.greaterThanOrEqual=" + UPDATED_AUTO_RENINDER_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoReninderAssessmentIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoReninderAssessment is less than or equal to DEFAULT_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldBeFound("autoReninderAssessment.lessThanOrEqual=" + DEFAULT_AUTO_RENINDER_ASSESSMENT);

        // Get all the clientSettingList where autoReninderAssessment is less than or equal to SMALLER_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldNotBeFound("autoReninderAssessment.lessThanOrEqual=" + SMALLER_AUTO_RENINDER_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoReninderAssessmentIsLessThanSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoReninderAssessment is less than DEFAULT_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldNotBeFound("autoReninderAssessment.lessThan=" + DEFAULT_AUTO_RENINDER_ASSESSMENT);

        // Get all the clientSettingList where autoReninderAssessment is less than UPDATED_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldBeFound("autoReninderAssessment.lessThan=" + UPDATED_AUTO_RENINDER_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoReninderAssessmentIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoReninderAssessment is greater than DEFAULT_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldNotBeFound("autoReninderAssessment.greaterThan=" + DEFAULT_AUTO_RENINDER_ASSESSMENT);

        // Get all the clientSettingList where autoReninderAssessment is greater than SMALLER_AUTO_RENINDER_ASSESSMENT
        defaultClientSettingShouldBeFound("autoReninderAssessment.greaterThan=" + SMALLER_AUTO_RENINDER_ASSESSMENT);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAllowCandidatePaidVersionIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where allowCandidatePaidVersion equals to DEFAULT_ALLOW_CANDIDATE_PAID_VERSION
        defaultClientSettingShouldBeFound("allowCandidatePaidVersion.equals=" + DEFAULT_ALLOW_CANDIDATE_PAID_VERSION);

        // Get all the clientSettingList where allowCandidatePaidVersion equals to UPDATED_ALLOW_CANDIDATE_PAID_VERSION
        defaultClientSettingShouldNotBeFound("allowCandidatePaidVersion.equals=" + UPDATED_ALLOW_CANDIDATE_PAID_VERSION);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAllowCandidatePaidVersionIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where allowCandidatePaidVersion in DEFAULT_ALLOW_CANDIDATE_PAID_VERSION or UPDATED_ALLOW_CANDIDATE_PAID_VERSION
        defaultClientSettingShouldBeFound(
            "allowCandidatePaidVersion.in=" + DEFAULT_ALLOW_CANDIDATE_PAID_VERSION + "," + UPDATED_ALLOW_CANDIDATE_PAID_VERSION
        );

        // Get all the clientSettingList where allowCandidatePaidVersion equals to UPDATED_ALLOW_CANDIDATE_PAID_VERSION
        defaultClientSettingShouldNotBeFound("allowCandidatePaidVersion.in=" + UPDATED_ALLOW_CANDIDATE_PAID_VERSION);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAllowCandidatePaidVersionIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where allowCandidatePaidVersion is not null
        defaultClientSettingShouldBeFound("allowCandidatePaidVersion.specified=true");

        // Get all the clientSettingList where allowCandidatePaidVersion is null
        defaultClientSettingShouldNotBeFound("allowCandidatePaidVersion.specified=false");
    }

    @Test
    @Transactional
    void getAllClientSettingsByAllowEmployessPaidVersionIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where allowEmployessPaidVersion equals to DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION
        defaultClientSettingShouldBeFound("allowEmployessPaidVersion.equals=" + DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION);

        // Get all the clientSettingList where allowEmployessPaidVersion equals to UPDATED_ALLOW_EMPLOYESS_PAID_VERSION
        defaultClientSettingShouldNotBeFound("allowEmployessPaidVersion.equals=" + UPDATED_ALLOW_EMPLOYESS_PAID_VERSION);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAllowEmployessPaidVersionIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where allowEmployessPaidVersion in DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION or UPDATED_ALLOW_EMPLOYESS_PAID_VERSION
        defaultClientSettingShouldBeFound(
            "allowEmployessPaidVersion.in=" + DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION + "," + UPDATED_ALLOW_EMPLOYESS_PAID_VERSION
        );

        // Get all the clientSettingList where allowEmployessPaidVersion equals to UPDATED_ALLOW_EMPLOYESS_PAID_VERSION
        defaultClientSettingShouldNotBeFound("allowEmployessPaidVersion.in=" + UPDATED_ALLOW_EMPLOYESS_PAID_VERSION);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAllowEmployessPaidVersionIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where allowEmployessPaidVersion is not null
        defaultClientSettingShouldBeFound("allowEmployessPaidVersion.specified=true");

        // Get all the clientSettingList where allowEmployessPaidVersion is null
        defaultClientSettingShouldNotBeFound("allowEmployessPaidVersion.specified=false");
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoArchieveRequisitionsIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoArchieveRequisitions equals to DEFAULT_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldBeFound("autoArchieveRequisitions.equals=" + DEFAULT_AUTO_ARCHIEVE_REQUISITIONS);

        // Get all the clientSettingList where autoArchieveRequisitions equals to UPDATED_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldNotBeFound("autoArchieveRequisitions.equals=" + UPDATED_AUTO_ARCHIEVE_REQUISITIONS);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoArchieveRequisitionsIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoArchieveRequisitions in DEFAULT_AUTO_ARCHIEVE_REQUISITIONS or UPDATED_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldBeFound(
            "autoArchieveRequisitions.in=" + DEFAULT_AUTO_ARCHIEVE_REQUISITIONS + "," + UPDATED_AUTO_ARCHIEVE_REQUISITIONS
        );

        // Get all the clientSettingList where autoArchieveRequisitions equals to UPDATED_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldNotBeFound("autoArchieveRequisitions.in=" + UPDATED_AUTO_ARCHIEVE_REQUISITIONS);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoArchieveRequisitionsIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoArchieveRequisitions is not null
        defaultClientSettingShouldBeFound("autoArchieveRequisitions.specified=true");

        // Get all the clientSettingList where autoArchieveRequisitions is null
        defaultClientSettingShouldNotBeFound("autoArchieveRequisitions.specified=false");
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoArchieveRequisitionsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoArchieveRequisitions is greater than or equal to DEFAULT_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldBeFound("autoArchieveRequisitions.greaterThanOrEqual=" + DEFAULT_AUTO_ARCHIEVE_REQUISITIONS);

        // Get all the clientSettingList where autoArchieveRequisitions is greater than or equal to UPDATED_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldNotBeFound("autoArchieveRequisitions.greaterThanOrEqual=" + UPDATED_AUTO_ARCHIEVE_REQUISITIONS);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoArchieveRequisitionsIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoArchieveRequisitions is less than or equal to DEFAULT_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldBeFound("autoArchieveRequisitions.lessThanOrEqual=" + DEFAULT_AUTO_ARCHIEVE_REQUISITIONS);

        // Get all the clientSettingList where autoArchieveRequisitions is less than or equal to SMALLER_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldNotBeFound("autoArchieveRequisitions.lessThanOrEqual=" + SMALLER_AUTO_ARCHIEVE_REQUISITIONS);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoArchieveRequisitionsIsLessThanSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoArchieveRequisitions is less than DEFAULT_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldNotBeFound("autoArchieveRequisitions.lessThan=" + DEFAULT_AUTO_ARCHIEVE_REQUISITIONS);

        // Get all the clientSettingList where autoArchieveRequisitions is less than UPDATED_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldBeFound("autoArchieveRequisitions.lessThan=" + UPDATED_AUTO_ARCHIEVE_REQUISITIONS);
    }

    @Test
    @Transactional
    void getAllClientSettingsByAutoArchieveRequisitionsIsGreaterThanSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where autoArchieveRequisitions is greater than DEFAULT_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldNotBeFound("autoArchieveRequisitions.greaterThan=" + DEFAULT_AUTO_ARCHIEVE_REQUISITIONS);

        // Get all the clientSettingList where autoArchieveRequisitions is greater than SMALLER_AUTO_ARCHIEVE_REQUISITIONS
        defaultClientSettingShouldBeFound("autoArchieveRequisitions.greaterThan=" + SMALLER_AUTO_ARCHIEVE_REQUISITIONS);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeDarkColourIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeDarkColour equals to DEFAULT_COMPANY_THEME_DARK_COLOUR
        defaultClientSettingShouldBeFound("companyThemeDarkColour.equals=" + DEFAULT_COMPANY_THEME_DARK_COLOUR);

        // Get all the clientSettingList where companyThemeDarkColour equals to UPDATED_COMPANY_THEME_DARK_COLOUR
        defaultClientSettingShouldNotBeFound("companyThemeDarkColour.equals=" + UPDATED_COMPANY_THEME_DARK_COLOUR);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeDarkColourIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeDarkColour in DEFAULT_COMPANY_THEME_DARK_COLOUR or UPDATED_COMPANY_THEME_DARK_COLOUR
        defaultClientSettingShouldBeFound(
            "companyThemeDarkColour.in=" + DEFAULT_COMPANY_THEME_DARK_COLOUR + "," + UPDATED_COMPANY_THEME_DARK_COLOUR
        );

        // Get all the clientSettingList where companyThemeDarkColour equals to UPDATED_COMPANY_THEME_DARK_COLOUR
        defaultClientSettingShouldNotBeFound("companyThemeDarkColour.in=" + UPDATED_COMPANY_THEME_DARK_COLOUR);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeDarkColourIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeDarkColour is not null
        defaultClientSettingShouldBeFound("companyThemeDarkColour.specified=true");

        // Get all the clientSettingList where companyThemeDarkColour is null
        defaultClientSettingShouldNotBeFound("companyThemeDarkColour.specified=false");
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeDarkColourContainsSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeDarkColour contains DEFAULT_COMPANY_THEME_DARK_COLOUR
        defaultClientSettingShouldBeFound("companyThemeDarkColour.contains=" + DEFAULT_COMPANY_THEME_DARK_COLOUR);

        // Get all the clientSettingList where companyThemeDarkColour contains UPDATED_COMPANY_THEME_DARK_COLOUR
        defaultClientSettingShouldNotBeFound("companyThemeDarkColour.contains=" + UPDATED_COMPANY_THEME_DARK_COLOUR);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeDarkColourNotContainsSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeDarkColour does not contain DEFAULT_COMPANY_THEME_DARK_COLOUR
        defaultClientSettingShouldNotBeFound("companyThemeDarkColour.doesNotContain=" + DEFAULT_COMPANY_THEME_DARK_COLOUR);

        // Get all the clientSettingList where companyThemeDarkColour does not contain UPDATED_COMPANY_THEME_DARK_COLOUR
        defaultClientSettingShouldBeFound("companyThemeDarkColour.doesNotContain=" + UPDATED_COMPANY_THEME_DARK_COLOUR);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeLightColourIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeLightColour equals to DEFAULT_COMPANY_THEME_LIGHT_COLOUR
        defaultClientSettingShouldBeFound("companyThemeLightColour.equals=" + DEFAULT_COMPANY_THEME_LIGHT_COLOUR);

        // Get all the clientSettingList where companyThemeLightColour equals to UPDATED_COMPANY_THEME_LIGHT_COLOUR
        defaultClientSettingShouldNotBeFound("companyThemeLightColour.equals=" + UPDATED_COMPANY_THEME_LIGHT_COLOUR);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeLightColourIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeLightColour in DEFAULT_COMPANY_THEME_LIGHT_COLOUR or UPDATED_COMPANY_THEME_LIGHT_COLOUR
        defaultClientSettingShouldBeFound(
            "companyThemeLightColour.in=" + DEFAULT_COMPANY_THEME_LIGHT_COLOUR + "," + UPDATED_COMPANY_THEME_LIGHT_COLOUR
        );

        // Get all the clientSettingList where companyThemeLightColour equals to UPDATED_COMPANY_THEME_LIGHT_COLOUR
        defaultClientSettingShouldNotBeFound("companyThemeLightColour.in=" + UPDATED_COMPANY_THEME_LIGHT_COLOUR);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeLightColourIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeLightColour is not null
        defaultClientSettingShouldBeFound("companyThemeLightColour.specified=true");

        // Get all the clientSettingList where companyThemeLightColour is null
        defaultClientSettingShouldNotBeFound("companyThemeLightColour.specified=false");
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeLightColourContainsSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeLightColour contains DEFAULT_COMPANY_THEME_LIGHT_COLOUR
        defaultClientSettingShouldBeFound("companyThemeLightColour.contains=" + DEFAULT_COMPANY_THEME_LIGHT_COLOUR);

        // Get all the clientSettingList where companyThemeLightColour contains UPDATED_COMPANY_THEME_LIGHT_COLOUR
        defaultClientSettingShouldNotBeFound("companyThemeLightColour.contains=" + UPDATED_COMPANY_THEME_LIGHT_COLOUR);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyThemeLightColourNotContainsSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyThemeLightColour does not contain DEFAULT_COMPANY_THEME_LIGHT_COLOUR
        defaultClientSettingShouldNotBeFound("companyThemeLightColour.doesNotContain=" + DEFAULT_COMPANY_THEME_LIGHT_COLOUR);

        // Get all the clientSettingList where companyThemeLightColour does not contain UPDATED_COMPANY_THEME_LIGHT_COLOUR
        defaultClientSettingShouldBeFound("companyThemeLightColour.doesNotContain=" + UPDATED_COMPANY_THEME_LIGHT_COLOUR);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyUrl equals to DEFAULT_COMPANY_URL
        defaultClientSettingShouldBeFound("companyUrl.equals=" + DEFAULT_COMPANY_URL);

        // Get all the clientSettingList where companyUrl equals to UPDATED_COMPANY_URL
        defaultClientSettingShouldNotBeFound("companyUrl.equals=" + UPDATED_COMPANY_URL);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyUrlIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyUrl in DEFAULT_COMPANY_URL or UPDATED_COMPANY_URL
        defaultClientSettingShouldBeFound("companyUrl.in=" + DEFAULT_COMPANY_URL + "," + UPDATED_COMPANY_URL);

        // Get all the clientSettingList where companyUrl equals to UPDATED_COMPANY_URL
        defaultClientSettingShouldNotBeFound("companyUrl.in=" + UPDATED_COMPANY_URL);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyUrl is not null
        defaultClientSettingShouldBeFound("companyUrl.specified=true");

        // Get all the clientSettingList where companyUrl is null
        defaultClientSettingShouldNotBeFound("companyUrl.specified=false");
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyUrlContainsSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyUrl contains DEFAULT_COMPANY_URL
        defaultClientSettingShouldBeFound("companyUrl.contains=" + DEFAULT_COMPANY_URL);

        // Get all the clientSettingList where companyUrl contains UPDATED_COMPANY_URL
        defaultClientSettingShouldNotBeFound("companyUrl.contains=" + UPDATED_COMPANY_URL);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyUrlNotContainsSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyUrl does not contain DEFAULT_COMPANY_URL
        defaultClientSettingShouldNotBeFound("companyUrl.doesNotContain=" + DEFAULT_COMPANY_URL);

        // Get all the clientSettingList where companyUrl does not contain UPDATED_COMPANY_URL
        defaultClientSettingShouldBeFound("companyUrl.doesNotContain=" + UPDATED_COMPANY_URL);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyMenuIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyMenu equals to DEFAULT_COMPANY_MENU
        defaultClientSettingShouldBeFound("companyMenu.equals=" + DEFAULT_COMPANY_MENU);

        // Get all the clientSettingList where companyMenu equals to UPDATED_COMPANY_MENU
        defaultClientSettingShouldNotBeFound("companyMenu.equals=" + UPDATED_COMPANY_MENU);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyMenuIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyMenu in DEFAULT_COMPANY_MENU or UPDATED_COMPANY_MENU
        defaultClientSettingShouldBeFound("companyMenu.in=" + DEFAULT_COMPANY_MENU + "," + UPDATED_COMPANY_MENU);

        // Get all the clientSettingList where companyMenu equals to UPDATED_COMPANY_MENU
        defaultClientSettingShouldNotBeFound("companyMenu.in=" + UPDATED_COMPANY_MENU);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyMenuIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyMenu is not null
        defaultClientSettingShouldBeFound("companyMenu.specified=true");

        // Get all the clientSettingList where companyMenu is null
        defaultClientSettingShouldNotBeFound("companyMenu.specified=false");
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyMenuContainsSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyMenu contains DEFAULT_COMPANY_MENU
        defaultClientSettingShouldBeFound("companyMenu.contains=" + DEFAULT_COMPANY_MENU);

        // Get all the clientSettingList where companyMenu contains UPDATED_COMPANY_MENU
        defaultClientSettingShouldNotBeFound("companyMenu.contains=" + UPDATED_COMPANY_MENU);
    }

    @Test
    @Transactional
    void getAllClientSettingsByCompanyMenuNotContainsSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where companyMenu does not contain DEFAULT_COMPANY_MENU
        defaultClientSettingShouldNotBeFound("companyMenu.doesNotContain=" + DEFAULT_COMPANY_MENU);

        // Get all the clientSettingList where companyMenu does not contain UPDATED_COMPANY_MENU
        defaultClientSettingShouldBeFound("companyMenu.doesNotContain=" + UPDATED_COMPANY_MENU);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultClientSettingShouldBeFound(String filter) throws Exception {
        restClientSettingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientSetting.getId().intValue())))
            .andExpect(jsonPath("$.[*].candidateSelfAssessment").value(hasItem(DEFAULT_CANDIDATE_SELF_ASSESSMENT)))
            .andExpect(jsonPath("$.[*].autoReninderAssessment").value(hasItem(DEFAULT_AUTO_RENINDER_ASSESSMENT)))
            .andExpect(jsonPath("$.[*].allowCandidatePaidVersion").value(hasItem(DEFAULT_ALLOW_CANDIDATE_PAID_VERSION.booleanValue())))
            .andExpect(jsonPath("$.[*].allowEmployessPaidVersion").value(hasItem(DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION.booleanValue())))
            .andExpect(jsonPath("$.[*].autoArchieveRequisitions").value(hasItem(DEFAULT_AUTO_ARCHIEVE_REQUISITIONS)))
            .andExpect(jsonPath("$.[*].companyThemeDarkColour").value(hasItem(DEFAULT_COMPANY_THEME_DARK_COLOUR)))
            .andExpect(jsonPath("$.[*].companyThemeLightColour").value(hasItem(DEFAULT_COMPANY_THEME_LIGHT_COLOUR)))
            .andExpect(jsonPath("$.[*].companyUrl").value(hasItem(DEFAULT_COMPANY_URL)))
            .andExpect(jsonPath("$.[*].companyMenu").value(hasItem(DEFAULT_COMPANY_MENU)));

        // Check, that the count call also returns 1
        restClientSettingMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultClientSettingShouldNotBeFound(String filter) throws Exception {
        restClientSettingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restClientSettingMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingClientSetting() throws Exception {
        // Get the clientSetting
        restClientSettingMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingClientSetting() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();

        // Update the clientSetting
        ClientSetting updatedClientSetting = clientSettingRepository.findById(clientSetting.getId()).get();
        // Disconnect from session so that the updates on updatedClientSetting are not directly saved in db
        em.detach(updatedClientSetting);
        updatedClientSetting
            .candidateSelfAssessment(UPDATED_CANDIDATE_SELF_ASSESSMENT)
            .autoReninderAssessment(UPDATED_AUTO_RENINDER_ASSESSMENT)
            .allowCandidatePaidVersion(UPDATED_ALLOW_CANDIDATE_PAID_VERSION)
            .allowEmployessPaidVersion(UPDATED_ALLOW_EMPLOYESS_PAID_VERSION)
            .autoArchieveRequisitions(UPDATED_AUTO_ARCHIEVE_REQUISITIONS)
            .companyThemeDarkColour(UPDATED_COMPANY_THEME_DARK_COLOUR)
            .companyThemeLightColour(UPDATED_COMPANY_THEME_LIGHT_COLOUR)
            .companyUrl(UPDATED_COMPANY_URL)
            .companyMenu(UPDATED_COMPANY_MENU);
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(updatedClientSetting);

        restClientSettingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, clientSettingDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO))
            )
            .andExpect(status().isOk());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
        ClientSetting testClientSetting = clientSettingList.get(clientSettingList.size() - 1);
        assertThat(testClientSetting.getCandidateSelfAssessment()).isEqualTo(UPDATED_CANDIDATE_SELF_ASSESSMENT);
        assertThat(testClientSetting.getAutoReninderAssessment()).isEqualTo(UPDATED_AUTO_RENINDER_ASSESSMENT);
        assertThat(testClientSetting.getAllowCandidatePaidVersion()).isEqualTo(UPDATED_ALLOW_CANDIDATE_PAID_VERSION);
        assertThat(testClientSetting.getAllowEmployessPaidVersion()).isEqualTo(UPDATED_ALLOW_EMPLOYESS_PAID_VERSION);
        assertThat(testClientSetting.getAutoArchieveRequisitions()).isEqualTo(UPDATED_AUTO_ARCHIEVE_REQUISITIONS);
        assertThat(testClientSetting.getCompanyThemeDarkColour()).isEqualTo(UPDATED_COMPANY_THEME_DARK_COLOUR);
        assertThat(testClientSetting.getCompanyThemeLightColour()).isEqualTo(UPDATED_COMPANY_THEME_LIGHT_COLOUR);
        assertThat(testClientSetting.getCompanyUrl()).isEqualTo(UPDATED_COMPANY_URL);
        assertThat(testClientSetting.getCompanyMenu()).isEqualTo(UPDATED_COMPANY_MENU);
    }

    @Test
    @Transactional
    void putNonExistingClientSetting() throws Exception {
        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();
        clientSetting.setId(count.incrementAndGet());

        // Create the ClientSetting
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientSettingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, clientSettingDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchClientSetting() throws Exception {
        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();
        clientSetting.setId(count.incrementAndGet());

        // Create the ClientSetting
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientSettingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamClientSetting() throws Exception {
        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();
        clientSetting.setId(count.incrementAndGet());

        // Create the ClientSetting
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientSettingMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientSettingDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateClientSettingWithPatch() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();

        // Update the clientSetting using partial update
        ClientSetting partialUpdatedClientSetting = new ClientSetting();
        partialUpdatedClientSetting.setId(clientSetting.getId());

        partialUpdatedClientSetting
            .autoReninderAssessment(UPDATED_AUTO_RENINDER_ASSESSMENT)
            .autoArchieveRequisitions(UPDATED_AUTO_ARCHIEVE_REQUISITIONS);

        restClientSettingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClientSetting.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClientSetting))
            )
            .andExpect(status().isOk());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
        ClientSetting testClientSetting = clientSettingList.get(clientSettingList.size() - 1);
        assertThat(testClientSetting.getCandidateSelfAssessment()).isEqualTo(DEFAULT_CANDIDATE_SELF_ASSESSMENT);
        assertThat(testClientSetting.getAutoReninderAssessment()).isEqualTo(UPDATED_AUTO_RENINDER_ASSESSMENT);
        assertThat(testClientSetting.getAllowCandidatePaidVersion()).isEqualTo(DEFAULT_ALLOW_CANDIDATE_PAID_VERSION);
        assertThat(testClientSetting.getAllowEmployessPaidVersion()).isEqualTo(DEFAULT_ALLOW_EMPLOYESS_PAID_VERSION);
        assertThat(testClientSetting.getAutoArchieveRequisitions()).isEqualTo(UPDATED_AUTO_ARCHIEVE_REQUISITIONS);
        assertThat(testClientSetting.getCompanyThemeDarkColour()).isEqualTo(DEFAULT_COMPANY_THEME_DARK_COLOUR);
        assertThat(testClientSetting.getCompanyThemeLightColour()).isEqualTo(DEFAULT_COMPANY_THEME_LIGHT_COLOUR);
        assertThat(testClientSetting.getCompanyUrl()).isEqualTo(DEFAULT_COMPANY_URL);
        assertThat(testClientSetting.getCompanyMenu()).isEqualTo(DEFAULT_COMPANY_MENU);
    }

    @Test
    @Transactional
    void fullUpdateClientSettingWithPatch() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();

        // Update the clientSetting using partial update
        ClientSetting partialUpdatedClientSetting = new ClientSetting();
        partialUpdatedClientSetting.setId(clientSetting.getId());

        partialUpdatedClientSetting
            .candidateSelfAssessment(UPDATED_CANDIDATE_SELF_ASSESSMENT)
            .autoReninderAssessment(UPDATED_AUTO_RENINDER_ASSESSMENT)
            .allowCandidatePaidVersion(UPDATED_ALLOW_CANDIDATE_PAID_VERSION)
            .allowEmployessPaidVersion(UPDATED_ALLOW_EMPLOYESS_PAID_VERSION)
            .autoArchieveRequisitions(UPDATED_AUTO_ARCHIEVE_REQUISITIONS)
            .companyThemeDarkColour(UPDATED_COMPANY_THEME_DARK_COLOUR)
            .companyThemeLightColour(UPDATED_COMPANY_THEME_LIGHT_COLOUR)
            .companyUrl(UPDATED_COMPANY_URL)
            .companyMenu(UPDATED_COMPANY_MENU);

        restClientSettingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClientSetting.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClientSetting))
            )
            .andExpect(status().isOk());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
        ClientSetting testClientSetting = clientSettingList.get(clientSettingList.size() - 1);
        assertThat(testClientSetting.getCandidateSelfAssessment()).isEqualTo(UPDATED_CANDIDATE_SELF_ASSESSMENT);
        assertThat(testClientSetting.getAutoReninderAssessment()).isEqualTo(UPDATED_AUTO_RENINDER_ASSESSMENT);
        assertThat(testClientSetting.getAllowCandidatePaidVersion()).isEqualTo(UPDATED_ALLOW_CANDIDATE_PAID_VERSION);
        assertThat(testClientSetting.getAllowEmployessPaidVersion()).isEqualTo(UPDATED_ALLOW_EMPLOYESS_PAID_VERSION);
        assertThat(testClientSetting.getAutoArchieveRequisitions()).isEqualTo(UPDATED_AUTO_ARCHIEVE_REQUISITIONS);
        assertThat(testClientSetting.getCompanyThemeDarkColour()).isEqualTo(UPDATED_COMPANY_THEME_DARK_COLOUR);
        assertThat(testClientSetting.getCompanyThemeLightColour()).isEqualTo(UPDATED_COMPANY_THEME_LIGHT_COLOUR);
        assertThat(testClientSetting.getCompanyUrl()).isEqualTo(UPDATED_COMPANY_URL);
        assertThat(testClientSetting.getCompanyMenu()).isEqualTo(UPDATED_COMPANY_MENU);
    }

    @Test
    @Transactional
    void patchNonExistingClientSetting() throws Exception {
        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();
        clientSetting.setId(count.incrementAndGet());

        // Create the ClientSetting
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientSettingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, clientSettingDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchClientSetting() throws Exception {
        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();
        clientSetting.setId(count.incrementAndGet());

        // Create the ClientSetting
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientSettingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamClientSetting() throws Exception {
        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();
        clientSetting.setId(count.incrementAndGet());

        // Create the ClientSetting
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientSettingMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteClientSetting() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        int databaseSizeBeforeDelete = clientSettingRepository.findAll().size();

        // Delete the clientSetting
        restClientSettingMockMvc
            .perform(delete(ENTITY_API_URL_ID, clientSetting.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
