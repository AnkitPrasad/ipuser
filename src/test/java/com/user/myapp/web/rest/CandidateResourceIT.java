package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.Candidate;
import com.user.myapp.domain.CandidateDetails;
import com.user.myapp.domain.CandidateProfile;
import com.user.myapp.domain.Client;
import com.user.myapp.repository.CandidateRepository;
import com.user.myapp.service.criteria.CandidateCriteria;
import com.user.myapp.service.dto.CandidateDTO;
import com.user.myapp.service.mapper.CandidateMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CandidateResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CandidateResourceIT {

    private static final Boolean DEFAULT_IS_INTERNAL_CANDIDATE = false;
    private static final Boolean UPDATED_IS_INTERNAL_CANDIDATE = true;

    private static final String DEFAULT_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_CREATED_BY = 1L;
    private static final Long UPDATED_CREATED_BY = 2L;
    private static final Long SMALLER_CREATED_BY = 1L - 1L;

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_UPDATED_BY = 1L;
    private static final Long UPDATED_UPDATED_BY = 2L;
    private static final Long SMALLER_UPDATED_BY = 1L - 1L;

    private static final String DEFAULT_ASSESSMENT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_ASSESSMENT_STATUS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final Boolean DEFAULT_ARCHIVE_STATUS = false;
    private static final Boolean UPDATED_ARCHIVE_STATUS = true;

    private static final String ENTITY_API_URL = "/api/candidates";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private CandidateMapper candidateMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCandidateMockMvc;

    private Candidate candidate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Candidate createEntity(EntityManager em) {
        Candidate candidate = new Candidate()
            .isInternalCandidate(DEFAULT_IS_INTERNAL_CANDIDATE)
            .company(DEFAULT_COMPANY)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .assessmentStatus(DEFAULT_ASSESSMENT_STATUS)
            .status(DEFAULT_STATUS)
            .archiveStatus(DEFAULT_ARCHIVE_STATUS);
        return candidate;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Candidate createUpdatedEntity(EntityManager em) {
        Candidate candidate = new Candidate()
            .isInternalCandidate(UPDATED_IS_INTERNAL_CANDIDATE)
            .company(UPDATED_COMPANY)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .assessmentStatus(UPDATED_ASSESSMENT_STATUS)
            .status(UPDATED_STATUS)
            .archiveStatus(UPDATED_ARCHIVE_STATUS);
        return candidate;
    }

    @BeforeEach
    public void initTest() {
        candidate = createEntity(em);
    }

    @Test
    @Transactional
    void createCandidate() throws Exception {
        int databaseSizeBeforeCreate = candidateRepository.findAll().size();
        // Create the Candidate
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);
        restCandidateMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isCreated());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeCreate + 1);
        Candidate testCandidate = candidateList.get(candidateList.size() - 1);
        assertThat(testCandidate.getIsInternalCandidate()).isEqualTo(DEFAULT_IS_INTERNAL_CANDIDATE);
        assertThat(testCandidate.getCompany()).isEqualTo(DEFAULT_COMPANY);
        assertThat(testCandidate.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCandidate.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCandidate.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testCandidate.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testCandidate.getAssessmentStatus()).isEqualTo(DEFAULT_ASSESSMENT_STATUS);
        assertThat(testCandidate.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCandidate.getArchiveStatus()).isEqualTo(DEFAULT_ARCHIVE_STATUS);
    }

    @Test
    @Transactional
    void createCandidateWithExistingId() throws Exception {
        // Create the Candidate with an existing ID
        candidate.setId(1L);
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        int databaseSizeBeforeCreate = candidateRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidateMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCandidates() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList
        restCandidateMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidate.getId().intValue())))
            .andExpect(jsonPath("$.[*].isInternalCandidate").value(hasItem(DEFAULT_IS_INTERNAL_CANDIDATE.booleanValue())))
            .andExpect(jsonPath("$.[*].company").value(hasItem(DEFAULT_COMPANY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].assessmentStatus").value(hasItem(DEFAULT_ASSESSMENT_STATUS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].archiveStatus").value(hasItem(DEFAULT_ARCHIVE_STATUS.booleanValue())));
    }

    @Test
    @Transactional
    void getCandidate() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get the candidate
        restCandidateMockMvc
            .perform(get(ENTITY_API_URL_ID, candidate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(candidate.getId().intValue()))
            .andExpect(jsonPath("$.isInternalCandidate").value(DEFAULT_IS_INTERNAL_CANDIDATE.booleanValue()))
            .andExpect(jsonPath("$.company").value(DEFAULT_COMPANY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.intValue()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.intValue()))
            .andExpect(jsonPath("$.assessmentStatus").value(DEFAULT_ASSESSMENT_STATUS))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.archiveStatus").value(DEFAULT_ARCHIVE_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    void getCandidatesByIdFiltering() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        Long id = candidate.getId();

        defaultCandidateShouldBeFound("id.equals=" + id);
        defaultCandidateShouldNotBeFound("id.notEquals=" + id);

        defaultCandidateShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCandidateShouldNotBeFound("id.greaterThan=" + id);

        defaultCandidateShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCandidateShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCandidatesByIsInternalCandidateIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where isInternalCandidate equals to DEFAULT_IS_INTERNAL_CANDIDATE
        defaultCandidateShouldBeFound("isInternalCandidate.equals=" + DEFAULT_IS_INTERNAL_CANDIDATE);

        // Get all the candidateList where isInternalCandidate equals to UPDATED_IS_INTERNAL_CANDIDATE
        defaultCandidateShouldNotBeFound("isInternalCandidate.equals=" + UPDATED_IS_INTERNAL_CANDIDATE);
    }

    @Test
    @Transactional
    void getAllCandidatesByIsInternalCandidateIsInShouldWork() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where isInternalCandidate in DEFAULT_IS_INTERNAL_CANDIDATE or UPDATED_IS_INTERNAL_CANDIDATE
        defaultCandidateShouldBeFound("isInternalCandidate.in=" + DEFAULT_IS_INTERNAL_CANDIDATE + "," + UPDATED_IS_INTERNAL_CANDIDATE);

        // Get all the candidateList where isInternalCandidate equals to UPDATED_IS_INTERNAL_CANDIDATE
        defaultCandidateShouldNotBeFound("isInternalCandidate.in=" + UPDATED_IS_INTERNAL_CANDIDATE);
    }

    @Test
    @Transactional
    void getAllCandidatesByIsInternalCandidateIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where isInternalCandidate is not null
        defaultCandidateShouldBeFound("isInternalCandidate.specified=true");

        // Get all the candidateList where isInternalCandidate is null
        defaultCandidateShouldNotBeFound("isInternalCandidate.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidatesByCompanyIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where company equals to DEFAULT_COMPANY
        defaultCandidateShouldBeFound("company.equals=" + DEFAULT_COMPANY);

        // Get all the candidateList where company equals to UPDATED_COMPANY
        defaultCandidateShouldNotBeFound("company.equals=" + UPDATED_COMPANY);
    }

    @Test
    @Transactional
    void getAllCandidatesByCompanyIsInShouldWork() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where company in DEFAULT_COMPANY or UPDATED_COMPANY
        defaultCandidateShouldBeFound("company.in=" + DEFAULT_COMPANY + "," + UPDATED_COMPANY);

        // Get all the candidateList where company equals to UPDATED_COMPANY
        defaultCandidateShouldNotBeFound("company.in=" + UPDATED_COMPANY);
    }

    @Test
    @Transactional
    void getAllCandidatesByCompanyIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where company is not null
        defaultCandidateShouldBeFound("company.specified=true");

        // Get all the candidateList where company is null
        defaultCandidateShouldNotBeFound("company.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidatesByCompanyContainsSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where company contains DEFAULT_COMPANY
        defaultCandidateShouldBeFound("company.contains=" + DEFAULT_COMPANY);

        // Get all the candidateList where company contains UPDATED_COMPANY
        defaultCandidateShouldNotBeFound("company.contains=" + UPDATED_COMPANY);
    }

    @Test
    @Transactional
    void getAllCandidatesByCompanyNotContainsSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where company does not contain DEFAULT_COMPANY
        defaultCandidateShouldNotBeFound("company.doesNotContain=" + DEFAULT_COMPANY);

        // Get all the candidateList where company does not contain UPDATED_COMPANY
        defaultCandidateShouldBeFound("company.doesNotContain=" + UPDATED_COMPANY);
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCandidateShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the candidateList where createdDate equals to UPDATED_CREATED_DATE
        defaultCandidateShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCandidateShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the candidateList where createdDate equals to UPDATED_CREATED_DATE
        defaultCandidateShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdDate is not null
        defaultCandidateShouldBeFound("createdDate.specified=true");

        // Get all the candidateList where createdDate is null
        defaultCandidateShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdBy equals to DEFAULT_CREATED_BY
        defaultCandidateShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the candidateList where createdBy equals to UPDATED_CREATED_BY
        defaultCandidateShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCandidateShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the candidateList where createdBy equals to UPDATED_CREATED_BY
        defaultCandidateShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdBy is not null
        defaultCandidateShouldBeFound("createdBy.specified=true");

        // Get all the candidateList where createdBy is null
        defaultCandidateShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdBy is greater than or equal to DEFAULT_CREATED_BY
        defaultCandidateShouldBeFound("createdBy.greaterThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the candidateList where createdBy is greater than or equal to UPDATED_CREATED_BY
        defaultCandidateShouldNotBeFound("createdBy.greaterThanOrEqual=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdBy is less than or equal to DEFAULT_CREATED_BY
        defaultCandidateShouldBeFound("createdBy.lessThanOrEqual=" + DEFAULT_CREATED_BY);

        // Get all the candidateList where createdBy is less than or equal to SMALLER_CREATED_BY
        defaultCandidateShouldNotBeFound("createdBy.lessThanOrEqual=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdBy is less than DEFAULT_CREATED_BY
        defaultCandidateShouldNotBeFound("createdBy.lessThan=" + DEFAULT_CREATED_BY);

        // Get all the candidateList where createdBy is less than UPDATED_CREATED_BY
        defaultCandidateShouldBeFound("createdBy.lessThan=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByCreatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where createdBy is greater than DEFAULT_CREATED_BY
        defaultCandidateShouldNotBeFound("createdBy.greaterThan=" + DEFAULT_CREATED_BY);

        // Get all the candidateList where createdBy is greater than SMALLER_CREATED_BY
        defaultCandidateShouldBeFound("createdBy.greaterThan=" + SMALLER_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultCandidateShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the candidateList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultCandidateShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultCandidateShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the candidateList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultCandidateShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedDate is not null
        defaultCandidateShouldBeFound("updatedDate.specified=true");

        // Get all the candidateList where updatedDate is null
        defaultCandidateShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultCandidateShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the candidateList where updatedBy equals to UPDATED_UPDATED_BY
        defaultCandidateShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultCandidateShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the candidateList where updatedBy equals to UPDATED_UPDATED_BY
        defaultCandidateShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedBy is not null
        defaultCandidateShouldBeFound("updatedBy.specified=true");

        // Get all the candidateList where updatedBy is null
        defaultCandidateShouldNotBeFound("updatedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedByIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedBy is greater than or equal to DEFAULT_UPDATED_BY
        defaultCandidateShouldBeFound("updatedBy.greaterThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the candidateList where updatedBy is greater than or equal to UPDATED_UPDATED_BY
        defaultCandidateShouldNotBeFound("updatedBy.greaterThanOrEqual=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedByIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedBy is less than or equal to DEFAULT_UPDATED_BY
        defaultCandidateShouldBeFound("updatedBy.lessThanOrEqual=" + DEFAULT_UPDATED_BY);

        // Get all the candidateList where updatedBy is less than or equal to SMALLER_UPDATED_BY
        defaultCandidateShouldNotBeFound("updatedBy.lessThanOrEqual=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedByIsLessThanSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedBy is less than DEFAULT_UPDATED_BY
        defaultCandidateShouldNotBeFound("updatedBy.lessThan=" + DEFAULT_UPDATED_BY);

        // Get all the candidateList where updatedBy is less than UPDATED_UPDATED_BY
        defaultCandidateShouldBeFound("updatedBy.lessThan=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByUpdatedByIsGreaterThanSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where updatedBy is greater than DEFAULT_UPDATED_BY
        defaultCandidateShouldNotBeFound("updatedBy.greaterThan=" + DEFAULT_UPDATED_BY);

        // Get all the candidateList where updatedBy is greater than SMALLER_UPDATED_BY
        defaultCandidateShouldBeFound("updatedBy.greaterThan=" + SMALLER_UPDATED_BY);
    }

    @Test
    @Transactional
    void getAllCandidatesByAssessmentStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where assessmentStatus equals to DEFAULT_ASSESSMENT_STATUS
        defaultCandidateShouldBeFound("assessmentStatus.equals=" + DEFAULT_ASSESSMENT_STATUS);

        // Get all the candidateList where assessmentStatus equals to UPDATED_ASSESSMENT_STATUS
        defaultCandidateShouldNotBeFound("assessmentStatus.equals=" + UPDATED_ASSESSMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllCandidatesByAssessmentStatusIsInShouldWork() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where assessmentStatus in DEFAULT_ASSESSMENT_STATUS or UPDATED_ASSESSMENT_STATUS
        defaultCandidateShouldBeFound("assessmentStatus.in=" + DEFAULT_ASSESSMENT_STATUS + "," + UPDATED_ASSESSMENT_STATUS);

        // Get all the candidateList where assessmentStatus equals to UPDATED_ASSESSMENT_STATUS
        defaultCandidateShouldNotBeFound("assessmentStatus.in=" + UPDATED_ASSESSMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllCandidatesByAssessmentStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where assessmentStatus is not null
        defaultCandidateShouldBeFound("assessmentStatus.specified=true");

        // Get all the candidateList where assessmentStatus is null
        defaultCandidateShouldNotBeFound("assessmentStatus.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidatesByAssessmentStatusContainsSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where assessmentStatus contains DEFAULT_ASSESSMENT_STATUS
        defaultCandidateShouldBeFound("assessmentStatus.contains=" + DEFAULT_ASSESSMENT_STATUS);

        // Get all the candidateList where assessmentStatus contains UPDATED_ASSESSMENT_STATUS
        defaultCandidateShouldNotBeFound("assessmentStatus.contains=" + UPDATED_ASSESSMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllCandidatesByAssessmentStatusNotContainsSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where assessmentStatus does not contain DEFAULT_ASSESSMENT_STATUS
        defaultCandidateShouldNotBeFound("assessmentStatus.doesNotContain=" + DEFAULT_ASSESSMENT_STATUS);

        // Get all the candidateList where assessmentStatus does not contain UPDATED_ASSESSMENT_STATUS
        defaultCandidateShouldBeFound("assessmentStatus.doesNotContain=" + UPDATED_ASSESSMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllCandidatesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where status equals to DEFAULT_STATUS
        defaultCandidateShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the candidateList where status equals to UPDATED_STATUS
        defaultCandidateShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCandidatesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultCandidateShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the candidateList where status equals to UPDATED_STATUS
        defaultCandidateShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCandidatesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where status is not null
        defaultCandidateShouldBeFound("status.specified=true");

        // Get all the candidateList where status is null
        defaultCandidateShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidatesByArchiveStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where archiveStatus equals to DEFAULT_ARCHIVE_STATUS
        defaultCandidateShouldBeFound("archiveStatus.equals=" + DEFAULT_ARCHIVE_STATUS);

        // Get all the candidateList where archiveStatus equals to UPDATED_ARCHIVE_STATUS
        defaultCandidateShouldNotBeFound("archiveStatus.equals=" + UPDATED_ARCHIVE_STATUS);
    }

    @Test
    @Transactional
    void getAllCandidatesByArchiveStatusIsInShouldWork() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where archiveStatus in DEFAULT_ARCHIVE_STATUS or UPDATED_ARCHIVE_STATUS
        defaultCandidateShouldBeFound("archiveStatus.in=" + DEFAULT_ARCHIVE_STATUS + "," + UPDATED_ARCHIVE_STATUS);

        // Get all the candidateList where archiveStatus equals to UPDATED_ARCHIVE_STATUS
        defaultCandidateShouldNotBeFound("archiveStatus.in=" + UPDATED_ARCHIVE_STATUS);
    }

    @Test
    @Transactional
    void getAllCandidatesByArchiveStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList where archiveStatus is not null
        defaultCandidateShouldBeFound("archiveStatus.specified=true");

        // Get all the candidateList where archiveStatus is null
        defaultCandidateShouldNotBeFound("archiveStatus.specified=false");
    }

    @Test
    @Transactional
    void getAllCandidatesByCandidateDetailsIsEqualToSomething() throws Exception {
        CandidateDetails candidateDetails;
        if (TestUtil.findAll(em, CandidateDetails.class).isEmpty()) {
            candidateRepository.saveAndFlush(candidate);
            candidateDetails = CandidateDetailsResourceIT.createEntity(em);
        } else {
            candidateDetails = TestUtil.findAll(em, CandidateDetails.class).get(0);
        }
        em.persist(candidateDetails);
        em.flush();
        candidate.setCandidateDetails(candidateDetails);
        candidateRepository.saveAndFlush(candidate);
        Long candidateDetailsId = candidateDetails.getId();

        // Get all the candidateList where candidateDetails equals to candidateDetailsId
        defaultCandidateShouldBeFound("candidateDetailsId.equals=" + candidateDetailsId);

        // Get all the candidateList where candidateDetails equals to (candidateDetailsId + 1)
        defaultCandidateShouldNotBeFound("candidateDetailsId.equals=" + (candidateDetailsId + 1));
    }

    @Test
    @Transactional
    void getAllCandidatesByCandidateProfileIsEqualToSomething() throws Exception {
        CandidateProfile candidateProfile;
        if (TestUtil.findAll(em, CandidateProfile.class).isEmpty()) {
            candidateRepository.saveAndFlush(candidate);
            candidateProfile = CandidateProfileResourceIT.createEntity(em);
        } else {
            candidateProfile = TestUtil.findAll(em, CandidateProfile.class).get(0);
        }
        em.persist(candidateProfile);
        em.flush();
        candidate.addCandidateProfile(candidateProfile);
        candidateRepository.saveAndFlush(candidate);
        Long candidateProfileId = candidateProfile.getId();

        // Get all the candidateList where candidateProfile equals to candidateProfileId
        defaultCandidateShouldBeFound("candidateProfileId.equals=" + candidateProfileId);

        // Get all the candidateList where candidateProfile equals to (candidateProfileId + 1)
        defaultCandidateShouldNotBeFound("candidateProfileId.equals=" + (candidateProfileId + 1));
    }

    @Test
    @Transactional
    void getAllCandidatesByClientIsEqualToSomething() throws Exception {
        Client client;
        if (TestUtil.findAll(em, Client.class).isEmpty()) {
            candidateRepository.saveAndFlush(candidate);
            client = ClientResourceIT.createEntity(em);
        } else {
            client = TestUtil.findAll(em, Client.class).get(0);
        }
        em.persist(client);
        em.flush();
        candidate.setClient(client);
        candidateRepository.saveAndFlush(candidate);
        Long clientId = client.getId();

        // Get all the candidateList where client equals to clientId
        defaultCandidateShouldBeFound("clientId.equals=" + clientId);

        // Get all the candidateList where client equals to (clientId + 1)
        defaultCandidateShouldNotBeFound("clientId.equals=" + (clientId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCandidateShouldBeFound(String filter) throws Exception {
        restCandidateMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidate.getId().intValue())))
            .andExpect(jsonPath("$.[*].isInternalCandidate").value(hasItem(DEFAULT_IS_INTERNAL_CANDIDATE.booleanValue())))
            .andExpect(jsonPath("$.[*].company").value(hasItem(DEFAULT_COMPANY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.intValue())))
            .andExpect(jsonPath("$.[*].assessmentStatus").value(hasItem(DEFAULT_ASSESSMENT_STATUS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].archiveStatus").value(hasItem(DEFAULT_ARCHIVE_STATUS.booleanValue())));

        // Check, that the count call also returns 1
        restCandidateMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCandidateShouldNotBeFound(String filter) throws Exception {
        restCandidateMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCandidateMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCandidate() throws Exception {
        // Get the candidate
        restCandidateMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCandidate() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();

        // Update the candidate
        Candidate updatedCandidate = candidateRepository.findById(candidate.getId()).get();
        // Disconnect from session so that the updates on updatedCandidate are not directly saved in db
        em.detach(updatedCandidate);
        updatedCandidate
            .isInternalCandidate(UPDATED_IS_INTERNAL_CANDIDATE)
            .company(UPDATED_COMPANY)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .assessmentStatus(UPDATED_ASSESSMENT_STATUS)
            .status(UPDATED_STATUS)
            .archiveStatus(UPDATED_ARCHIVE_STATUS);
        CandidateDTO candidateDTO = candidateMapper.toDto(updatedCandidate);

        restCandidateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, candidateDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(candidateDTO))
            )
            .andExpect(status().isOk());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
        Candidate testCandidate = candidateList.get(candidateList.size() - 1);
        assertThat(testCandidate.getIsInternalCandidate()).isEqualTo(UPDATED_IS_INTERNAL_CANDIDATE);
        assertThat(testCandidate.getCompany()).isEqualTo(UPDATED_COMPANY);
        assertThat(testCandidate.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCandidate.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCandidate.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testCandidate.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testCandidate.getAssessmentStatus()).isEqualTo(UPDATED_ASSESSMENT_STATUS);
        assertThat(testCandidate.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCandidate.getArchiveStatus()).isEqualTo(UPDATED_ARCHIVE_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingCandidate() throws Exception {
        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();
        candidate.setId(count.incrementAndGet());

        // Create the Candidate
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, candidateDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(candidateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCandidate() throws Exception {
        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();
        candidate.setId(count.incrementAndGet());

        // Create the Candidate
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(candidateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCandidate() throws Exception {
        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();
        candidate.setId(count.incrementAndGet());

        // Create the Candidate
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCandidateWithPatch() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();

        // Update the candidate using partial update
        Candidate partialUpdatedCandidate = new Candidate();
        partialUpdatedCandidate.setId(candidate.getId());

        partialUpdatedCandidate
            .isInternalCandidate(UPDATED_IS_INTERNAL_CANDIDATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS);

        restCandidateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCandidate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCandidate))
            )
            .andExpect(status().isOk());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
        Candidate testCandidate = candidateList.get(candidateList.size() - 1);
        assertThat(testCandidate.getIsInternalCandidate()).isEqualTo(UPDATED_IS_INTERNAL_CANDIDATE);
        assertThat(testCandidate.getCompany()).isEqualTo(DEFAULT_COMPANY);
        assertThat(testCandidate.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCandidate.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCandidate.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testCandidate.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testCandidate.getAssessmentStatus()).isEqualTo(DEFAULT_ASSESSMENT_STATUS);
        assertThat(testCandidate.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCandidate.getArchiveStatus()).isEqualTo(DEFAULT_ARCHIVE_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateCandidateWithPatch() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();

        // Update the candidate using partial update
        Candidate partialUpdatedCandidate = new Candidate();
        partialUpdatedCandidate.setId(candidate.getId());

        partialUpdatedCandidate
            .isInternalCandidate(UPDATED_IS_INTERNAL_CANDIDATE)
            .company(UPDATED_COMPANY)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .assessmentStatus(UPDATED_ASSESSMENT_STATUS)
            .status(UPDATED_STATUS)
            .archiveStatus(UPDATED_ARCHIVE_STATUS);

        restCandidateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCandidate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCandidate))
            )
            .andExpect(status().isOk());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
        Candidate testCandidate = candidateList.get(candidateList.size() - 1);
        assertThat(testCandidate.getIsInternalCandidate()).isEqualTo(UPDATED_IS_INTERNAL_CANDIDATE);
        assertThat(testCandidate.getCompany()).isEqualTo(UPDATED_COMPANY);
        assertThat(testCandidate.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCandidate.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCandidate.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testCandidate.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testCandidate.getAssessmentStatus()).isEqualTo(UPDATED_ASSESSMENT_STATUS);
        assertThat(testCandidate.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCandidate.getArchiveStatus()).isEqualTo(UPDATED_ARCHIVE_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingCandidate() throws Exception {
        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();
        candidate.setId(count.incrementAndGet());

        // Create the Candidate
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, candidateDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(candidateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCandidate() throws Exception {
        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();
        candidate.setId(count.incrementAndGet());

        // Create the Candidate
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(candidateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCandidate() throws Exception {
        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();
        candidate.setId(count.incrementAndGet());

        // Create the Candidate
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCandidateMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(candidateDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCandidate() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        int databaseSizeBeforeDelete = candidateRepository.findAll().size();

        // Delete the candidate
        restCandidateMockMvc
            .perform(delete(ENTITY_API_URL_ID, candidate.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
