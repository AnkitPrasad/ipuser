package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.LoginCount;
import com.user.myapp.repository.LoginCountRepository;
import com.user.myapp.service.criteria.LoginCountCriteria;
import com.user.myapp.service.dto.LoginCountDTO;
import com.user.myapp.service.mapper.LoginCountMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LoginCountResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LoginCountResourceIT {

    private static final Instant DEFAULT_LOGINTIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LOGINTIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_LOGIN_ID = 1L;
    private static final Long UPDATED_LOGIN_ID = 2L;
    private static final Long SMALLER_LOGIN_ID = 1L - 1L;

    private static final String ENTITY_API_URL = "/api/login-counts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LoginCountRepository loginCountRepository;

    @Autowired
    private LoginCountMapper loginCountMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLoginCountMockMvc;

    private LoginCount loginCount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoginCount createEntity(EntityManager em) {
        LoginCount loginCount = new LoginCount().logintime(DEFAULT_LOGINTIME).loginId(DEFAULT_LOGIN_ID);
        return loginCount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoginCount createUpdatedEntity(EntityManager em) {
        LoginCount loginCount = new LoginCount().logintime(UPDATED_LOGINTIME).loginId(UPDATED_LOGIN_ID);
        return loginCount;
    }

    @BeforeEach
    public void initTest() {
        loginCount = createEntity(em);
    }

    @Test
    @Transactional
    void createLoginCount() throws Exception {
        int databaseSizeBeforeCreate = loginCountRepository.findAll().size();
        // Create the LoginCount
        LoginCountDTO loginCountDTO = loginCountMapper.toDto(loginCount);
        restLoginCountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loginCountDTO)))
            .andExpect(status().isCreated());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeCreate + 1);
        LoginCount testLoginCount = loginCountList.get(loginCountList.size() - 1);
        assertThat(testLoginCount.getLogintime()).isEqualTo(DEFAULT_LOGINTIME);
        assertThat(testLoginCount.getLoginId()).isEqualTo(DEFAULT_LOGIN_ID);
    }

    @Test
    @Transactional
    void createLoginCountWithExistingId() throws Exception {
        // Create the LoginCount with an existing ID
        loginCount.setId(1L);
        LoginCountDTO loginCountDTO = loginCountMapper.toDto(loginCount);

        int databaseSizeBeforeCreate = loginCountRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoginCountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loginCountDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllLoginCounts() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList
        restLoginCountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loginCount.getId().intValue())))
            .andExpect(jsonPath("$.[*].logintime").value(hasItem(DEFAULT_LOGINTIME.toString())))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID.intValue())));
    }

    @Test
    @Transactional
    void getLoginCount() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get the loginCount
        restLoginCountMockMvc
            .perform(get(ENTITY_API_URL_ID, loginCount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loginCount.getId().intValue()))
            .andExpect(jsonPath("$.logintime").value(DEFAULT_LOGINTIME.toString()))
            .andExpect(jsonPath("$.loginId").value(DEFAULT_LOGIN_ID.intValue()));
    }

    @Test
    @Transactional
    void getLoginCountsByIdFiltering() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        Long id = loginCount.getId();

        defaultLoginCountShouldBeFound("id.equals=" + id);
        defaultLoginCountShouldNotBeFound("id.notEquals=" + id);

        defaultLoginCountShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLoginCountShouldNotBeFound("id.greaterThan=" + id);

        defaultLoginCountShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLoginCountShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllLoginCountsByLogintimeIsEqualToSomething() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where logintime equals to DEFAULT_LOGINTIME
        defaultLoginCountShouldBeFound("logintime.equals=" + DEFAULT_LOGINTIME);

        // Get all the loginCountList where logintime equals to UPDATED_LOGINTIME
        defaultLoginCountShouldNotBeFound("logintime.equals=" + UPDATED_LOGINTIME);
    }

    @Test
    @Transactional
    void getAllLoginCountsByLogintimeIsInShouldWork() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where logintime in DEFAULT_LOGINTIME or UPDATED_LOGINTIME
        defaultLoginCountShouldBeFound("logintime.in=" + DEFAULT_LOGINTIME + "," + UPDATED_LOGINTIME);

        // Get all the loginCountList where logintime equals to UPDATED_LOGINTIME
        defaultLoginCountShouldNotBeFound("logintime.in=" + UPDATED_LOGINTIME);
    }

    @Test
    @Transactional
    void getAllLoginCountsByLogintimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where logintime is not null
        defaultLoginCountShouldBeFound("logintime.specified=true");

        // Get all the loginCountList where logintime is null
        defaultLoginCountShouldNotBeFound("logintime.specified=false");
    }

    @Test
    @Transactional
    void getAllLoginCountsByLoginIdIsEqualToSomething() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where loginId equals to DEFAULT_LOGIN_ID
        defaultLoginCountShouldBeFound("loginId.equals=" + DEFAULT_LOGIN_ID);

        // Get all the loginCountList where loginId equals to UPDATED_LOGIN_ID
        defaultLoginCountShouldNotBeFound("loginId.equals=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllLoginCountsByLoginIdIsInShouldWork() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where loginId in DEFAULT_LOGIN_ID or UPDATED_LOGIN_ID
        defaultLoginCountShouldBeFound("loginId.in=" + DEFAULT_LOGIN_ID + "," + UPDATED_LOGIN_ID);

        // Get all the loginCountList where loginId equals to UPDATED_LOGIN_ID
        defaultLoginCountShouldNotBeFound("loginId.in=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllLoginCountsByLoginIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where loginId is not null
        defaultLoginCountShouldBeFound("loginId.specified=true");

        // Get all the loginCountList where loginId is null
        defaultLoginCountShouldNotBeFound("loginId.specified=false");
    }

    @Test
    @Transactional
    void getAllLoginCountsByLoginIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where loginId is greater than or equal to DEFAULT_LOGIN_ID
        defaultLoginCountShouldBeFound("loginId.greaterThanOrEqual=" + DEFAULT_LOGIN_ID);

        // Get all the loginCountList where loginId is greater than or equal to UPDATED_LOGIN_ID
        defaultLoginCountShouldNotBeFound("loginId.greaterThanOrEqual=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllLoginCountsByLoginIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where loginId is less than or equal to DEFAULT_LOGIN_ID
        defaultLoginCountShouldBeFound("loginId.lessThanOrEqual=" + DEFAULT_LOGIN_ID);

        // Get all the loginCountList where loginId is less than or equal to SMALLER_LOGIN_ID
        defaultLoginCountShouldNotBeFound("loginId.lessThanOrEqual=" + SMALLER_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllLoginCountsByLoginIdIsLessThanSomething() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where loginId is less than DEFAULT_LOGIN_ID
        defaultLoginCountShouldNotBeFound("loginId.lessThan=" + DEFAULT_LOGIN_ID);

        // Get all the loginCountList where loginId is less than UPDATED_LOGIN_ID
        defaultLoginCountShouldBeFound("loginId.lessThan=" + UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllLoginCountsByLoginIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        // Get all the loginCountList where loginId is greater than DEFAULT_LOGIN_ID
        defaultLoginCountShouldNotBeFound("loginId.greaterThan=" + DEFAULT_LOGIN_ID);

        // Get all the loginCountList where loginId is greater than SMALLER_LOGIN_ID
        defaultLoginCountShouldBeFound("loginId.greaterThan=" + SMALLER_LOGIN_ID);
    }

    @Test
    @Transactional
    void getAllLoginCountsByInPreferenceUserIsEqualToSomething() throws Exception {
        InPreferenceUser inPreferenceUser;
        if (TestUtil.findAll(em, InPreferenceUser.class).isEmpty()) {
            loginCountRepository.saveAndFlush(loginCount);
            inPreferenceUser = InPreferenceUserResourceIT.createEntity(em);
        } else {
            inPreferenceUser = TestUtil.findAll(em, InPreferenceUser.class).get(0);
        }
        em.persist(inPreferenceUser);
        em.flush();
        loginCount.setInPreferenceUser(inPreferenceUser);
        loginCountRepository.saveAndFlush(loginCount);
        Long inPreferenceUserId = inPreferenceUser.getId();

        // Get all the loginCountList where inPreferenceUser equals to inPreferenceUserId
        defaultLoginCountShouldBeFound("inPreferenceUserId.equals=" + inPreferenceUserId);

        // Get all the loginCountList where inPreferenceUser equals to (inPreferenceUserId + 1)
        defaultLoginCountShouldNotBeFound("inPreferenceUserId.equals=" + (inPreferenceUserId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLoginCountShouldBeFound(String filter) throws Exception {
        restLoginCountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loginCount.getId().intValue())))
            .andExpect(jsonPath("$.[*].logintime").value(hasItem(DEFAULT_LOGINTIME.toString())))
            .andExpect(jsonPath("$.[*].loginId").value(hasItem(DEFAULT_LOGIN_ID.intValue())));

        // Check, that the count call also returns 1
        restLoginCountMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLoginCountShouldNotBeFound(String filter) throws Exception {
        restLoginCountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLoginCountMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingLoginCount() throws Exception {
        // Get the loginCount
        restLoginCountMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingLoginCount() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        int databaseSizeBeforeUpdate = loginCountRepository.findAll().size();

        // Update the loginCount
        LoginCount updatedLoginCount = loginCountRepository.findById(loginCount.getId()).get();
        // Disconnect from session so that the updates on updatedLoginCount are not directly saved in db
        em.detach(updatedLoginCount);
        updatedLoginCount.logintime(UPDATED_LOGINTIME).loginId(UPDATED_LOGIN_ID);
        LoginCountDTO loginCountDTO = loginCountMapper.toDto(updatedLoginCount);

        restLoginCountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loginCountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loginCountDTO))
            )
            .andExpect(status().isOk());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeUpdate);
        LoginCount testLoginCount = loginCountList.get(loginCountList.size() - 1);
        assertThat(testLoginCount.getLogintime()).isEqualTo(UPDATED_LOGINTIME);
        assertThat(testLoginCount.getLoginId()).isEqualTo(UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void putNonExistingLoginCount() throws Exception {
        int databaseSizeBeforeUpdate = loginCountRepository.findAll().size();
        loginCount.setId(count.incrementAndGet());

        // Create the LoginCount
        LoginCountDTO loginCountDTO = loginCountMapper.toDto(loginCount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoginCountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loginCountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loginCountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLoginCount() throws Exception {
        int databaseSizeBeforeUpdate = loginCountRepository.findAll().size();
        loginCount.setId(count.incrementAndGet());

        // Create the LoginCount
        LoginCountDTO loginCountDTO = loginCountMapper.toDto(loginCount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoginCountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loginCountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLoginCount() throws Exception {
        int databaseSizeBeforeUpdate = loginCountRepository.findAll().size();
        loginCount.setId(count.incrementAndGet());

        // Create the LoginCount
        LoginCountDTO loginCountDTO = loginCountMapper.toDto(loginCount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoginCountMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loginCountDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLoginCountWithPatch() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        int databaseSizeBeforeUpdate = loginCountRepository.findAll().size();

        // Update the loginCount using partial update
        LoginCount partialUpdatedLoginCount = new LoginCount();
        partialUpdatedLoginCount.setId(loginCount.getId());

        partialUpdatedLoginCount.logintime(UPDATED_LOGINTIME);

        restLoginCountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoginCount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoginCount))
            )
            .andExpect(status().isOk());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeUpdate);
        LoginCount testLoginCount = loginCountList.get(loginCountList.size() - 1);
        assertThat(testLoginCount.getLogintime()).isEqualTo(UPDATED_LOGINTIME);
        assertThat(testLoginCount.getLoginId()).isEqualTo(DEFAULT_LOGIN_ID);
    }

    @Test
    @Transactional
    void fullUpdateLoginCountWithPatch() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        int databaseSizeBeforeUpdate = loginCountRepository.findAll().size();

        // Update the loginCount using partial update
        LoginCount partialUpdatedLoginCount = new LoginCount();
        partialUpdatedLoginCount.setId(loginCount.getId());

        partialUpdatedLoginCount.logintime(UPDATED_LOGINTIME).loginId(UPDATED_LOGIN_ID);

        restLoginCountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoginCount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoginCount))
            )
            .andExpect(status().isOk());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeUpdate);
        LoginCount testLoginCount = loginCountList.get(loginCountList.size() - 1);
        assertThat(testLoginCount.getLogintime()).isEqualTo(UPDATED_LOGINTIME);
        assertThat(testLoginCount.getLoginId()).isEqualTo(UPDATED_LOGIN_ID);
    }

    @Test
    @Transactional
    void patchNonExistingLoginCount() throws Exception {
        int databaseSizeBeforeUpdate = loginCountRepository.findAll().size();
        loginCount.setId(count.incrementAndGet());

        // Create the LoginCount
        LoginCountDTO loginCountDTO = loginCountMapper.toDto(loginCount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoginCountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, loginCountDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loginCountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLoginCount() throws Exception {
        int databaseSizeBeforeUpdate = loginCountRepository.findAll().size();
        loginCount.setId(count.incrementAndGet());

        // Create the LoginCount
        LoginCountDTO loginCountDTO = loginCountMapper.toDto(loginCount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoginCountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loginCountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLoginCount() throws Exception {
        int databaseSizeBeforeUpdate = loginCountRepository.findAll().size();
        loginCount.setId(count.incrementAndGet());

        // Create the LoginCount
        LoginCountDTO loginCountDTO = loginCountMapper.toDto(loginCount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoginCountMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(loginCountDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the LoginCount in the database
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLoginCount() throws Exception {
        // Initialize the database
        loginCountRepository.saveAndFlush(loginCount);

        int databaseSizeBeforeDelete = loginCountRepository.findAll().size();

        // Delete the loginCount
        restLoginCountMockMvc
            .perform(delete(ENTITY_API_URL_ID, loginCount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LoginCount> loginCountList = loginCountRepository.findAll();
        assertThat(loginCountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
