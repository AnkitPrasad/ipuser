package com.user.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.user.myapp.IntegrationTest;
import com.user.myapp.domain.PermissionsOnAttributeMaster;
import com.user.myapp.domain.UserPermissionOnAttribute;
import com.user.myapp.repository.PermissionsOnAttributeMasterRepository;
import com.user.myapp.service.criteria.PermissionsOnAttributeMasterCriteria;
import com.user.myapp.service.dto.PermissionsOnAttributeMasterDTO;
import com.user.myapp.service.mapper.PermissionsOnAttributeMasterMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PermissionsOnAttributeMasterResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PermissionsOnAttributeMasterResourceIT {

    private static final String DEFAULT_ATTRIBUTE_KEY = "AAAAAAAAAA";
    private static final String UPDATED_ATTRIBUTE_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_ATTRIBUTE_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_ATTRIBUTE_TITLE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/permissions-on-attribute-masters";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PermissionsOnAttributeMasterRepository permissionsOnAttributeMasterRepository;

    @Autowired
    private PermissionsOnAttributeMasterMapper permissionsOnAttributeMasterMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPermissionsOnAttributeMasterMockMvc;

    private PermissionsOnAttributeMaster permissionsOnAttributeMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PermissionsOnAttributeMaster createEntity(EntityManager em) {
        PermissionsOnAttributeMaster permissionsOnAttributeMaster = new PermissionsOnAttributeMaster()
            .attributeKey(DEFAULT_ATTRIBUTE_KEY)
            .attributeTitle(DEFAULT_ATTRIBUTE_TITLE);
        return permissionsOnAttributeMaster;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PermissionsOnAttributeMaster createUpdatedEntity(EntityManager em) {
        PermissionsOnAttributeMaster permissionsOnAttributeMaster = new PermissionsOnAttributeMaster()
            .attributeKey(UPDATED_ATTRIBUTE_KEY)
            .attributeTitle(UPDATED_ATTRIBUTE_TITLE);
        return permissionsOnAttributeMaster;
    }

    @BeforeEach
    public void initTest() {
        permissionsOnAttributeMaster = createEntity(em);
    }

    @Test
    @Transactional
    void createPermissionsOnAttributeMaster() throws Exception {
        int databaseSizeBeforeCreate = permissionsOnAttributeMasterRepository.findAll().size();
        // Create the PermissionsOnAttributeMaster
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterMapper.toDto(
            permissionsOnAttributeMaster
        );
        restPermissionsOnAttributeMasterMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(permissionsOnAttributeMasterDTO))
            )
            .andExpect(status().isCreated());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeCreate + 1);
        PermissionsOnAttributeMaster testPermissionsOnAttributeMaster = permissionsOnAttributeMasterList.get(
            permissionsOnAttributeMasterList.size() - 1
        );
        assertThat(testPermissionsOnAttributeMaster.getAttributeKey()).isEqualTo(DEFAULT_ATTRIBUTE_KEY);
        assertThat(testPermissionsOnAttributeMaster.getAttributeTitle()).isEqualTo(DEFAULT_ATTRIBUTE_TITLE);
    }

    @Test
    @Transactional
    void createPermissionsOnAttributeMasterWithExistingId() throws Exception {
        // Create the PermissionsOnAttributeMaster with an existing ID
        permissionsOnAttributeMaster.setId(1L);
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterMapper.toDto(
            permissionsOnAttributeMaster
        );

        int databaseSizeBeforeCreate = permissionsOnAttributeMasterRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPermissionsOnAttributeMasterMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(permissionsOnAttributeMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMasters() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList
        restPermissionsOnAttributeMasterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(permissionsOnAttributeMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].attributeKey").value(hasItem(DEFAULT_ATTRIBUTE_KEY)))
            .andExpect(jsonPath("$.[*].attributeTitle").value(hasItem(DEFAULT_ATTRIBUTE_TITLE)));
    }

    @Test
    @Transactional
    void getPermissionsOnAttributeMaster() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get the permissionsOnAttributeMaster
        restPermissionsOnAttributeMasterMockMvc
            .perform(get(ENTITY_API_URL_ID, permissionsOnAttributeMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(permissionsOnAttributeMaster.getId().intValue()))
            .andExpect(jsonPath("$.attributeKey").value(DEFAULT_ATTRIBUTE_KEY))
            .andExpect(jsonPath("$.attributeTitle").value(DEFAULT_ATTRIBUTE_TITLE));
    }

    @Test
    @Transactional
    void getPermissionsOnAttributeMastersByIdFiltering() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        Long id = permissionsOnAttributeMaster.getId();

        defaultPermissionsOnAttributeMasterShouldBeFound("id.equals=" + id);
        defaultPermissionsOnAttributeMasterShouldNotBeFound("id.notEquals=" + id);

        defaultPermissionsOnAttributeMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPermissionsOnAttributeMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultPermissionsOnAttributeMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPermissionsOnAttributeMasterShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeKeyIsEqualToSomething() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeKey equals to DEFAULT_ATTRIBUTE_KEY
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeKey.equals=" + DEFAULT_ATTRIBUTE_KEY);

        // Get all the permissionsOnAttributeMasterList where attributeKey equals to UPDATED_ATTRIBUTE_KEY
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeKey.equals=" + UPDATED_ATTRIBUTE_KEY);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeKeyIsInShouldWork() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeKey in DEFAULT_ATTRIBUTE_KEY or UPDATED_ATTRIBUTE_KEY
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeKey.in=" + DEFAULT_ATTRIBUTE_KEY + "," + UPDATED_ATTRIBUTE_KEY);

        // Get all the permissionsOnAttributeMasterList where attributeKey equals to UPDATED_ATTRIBUTE_KEY
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeKey.in=" + UPDATED_ATTRIBUTE_KEY);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeKeyIsNullOrNotNull() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeKey is not null
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeKey.specified=true");

        // Get all the permissionsOnAttributeMasterList where attributeKey is null
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeKey.specified=false");
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeKeyContainsSomething() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeKey contains DEFAULT_ATTRIBUTE_KEY
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeKey.contains=" + DEFAULT_ATTRIBUTE_KEY);

        // Get all the permissionsOnAttributeMasterList where attributeKey contains UPDATED_ATTRIBUTE_KEY
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeKey.contains=" + UPDATED_ATTRIBUTE_KEY);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeKeyNotContainsSomething() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeKey does not contain DEFAULT_ATTRIBUTE_KEY
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeKey.doesNotContain=" + DEFAULT_ATTRIBUTE_KEY);

        // Get all the permissionsOnAttributeMasterList where attributeKey does not contain UPDATED_ATTRIBUTE_KEY
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeKey.doesNotContain=" + UPDATED_ATTRIBUTE_KEY);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeTitle equals to DEFAULT_ATTRIBUTE_TITLE
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeTitle.equals=" + DEFAULT_ATTRIBUTE_TITLE);

        // Get all the permissionsOnAttributeMasterList where attributeTitle equals to UPDATED_ATTRIBUTE_TITLE
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeTitle.equals=" + UPDATED_ATTRIBUTE_TITLE);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeTitleIsInShouldWork() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeTitle in DEFAULT_ATTRIBUTE_TITLE or UPDATED_ATTRIBUTE_TITLE
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeTitle.in=" + DEFAULT_ATTRIBUTE_TITLE + "," + UPDATED_ATTRIBUTE_TITLE);

        // Get all the permissionsOnAttributeMasterList where attributeTitle equals to UPDATED_ATTRIBUTE_TITLE
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeTitle.in=" + UPDATED_ATTRIBUTE_TITLE);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeTitle is not null
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeTitle.specified=true");

        // Get all the permissionsOnAttributeMasterList where attributeTitle is null
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeTitle.specified=false");
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeTitleContainsSomething() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeTitle contains DEFAULT_ATTRIBUTE_TITLE
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeTitle.contains=" + DEFAULT_ATTRIBUTE_TITLE);

        // Get all the permissionsOnAttributeMasterList where attributeTitle contains UPDATED_ATTRIBUTE_TITLE
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeTitle.contains=" + UPDATED_ATTRIBUTE_TITLE);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByAttributeTitleNotContainsSomething() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        // Get all the permissionsOnAttributeMasterList where attributeTitle does not contain DEFAULT_ATTRIBUTE_TITLE
        defaultPermissionsOnAttributeMasterShouldNotBeFound("attributeTitle.doesNotContain=" + DEFAULT_ATTRIBUTE_TITLE);

        // Get all the permissionsOnAttributeMasterList where attributeTitle does not contain UPDATED_ATTRIBUTE_TITLE
        defaultPermissionsOnAttributeMasterShouldBeFound("attributeTitle.doesNotContain=" + UPDATED_ATTRIBUTE_TITLE);
    }

    @Test
    @Transactional
    void getAllPermissionsOnAttributeMastersByUserPermissionOnAttributeIsEqualToSomething() throws Exception {
        UserPermissionOnAttribute userPermissionOnAttribute;
        if (TestUtil.findAll(em, UserPermissionOnAttribute.class).isEmpty()) {
            permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);
            userPermissionOnAttribute = UserPermissionOnAttributeResourceIT.createEntity(em);
        } else {
            userPermissionOnAttribute = TestUtil.findAll(em, UserPermissionOnAttribute.class).get(0);
        }
        em.persist(userPermissionOnAttribute);
        em.flush();
        permissionsOnAttributeMaster.addUserPermissionOnAttribute(userPermissionOnAttribute);
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);
        Long userPermissionOnAttributeId = userPermissionOnAttribute.getId();

        // Get all the permissionsOnAttributeMasterList where userPermissionOnAttribute equals to userPermissionOnAttributeId
        defaultPermissionsOnAttributeMasterShouldBeFound("userPermissionOnAttributeId.equals=" + userPermissionOnAttributeId);

        // Get all the permissionsOnAttributeMasterList where userPermissionOnAttribute equals to (userPermissionOnAttributeId + 1)
        defaultPermissionsOnAttributeMasterShouldNotBeFound("userPermissionOnAttributeId.equals=" + (userPermissionOnAttributeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPermissionsOnAttributeMasterShouldBeFound(String filter) throws Exception {
        restPermissionsOnAttributeMasterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(permissionsOnAttributeMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].attributeKey").value(hasItem(DEFAULT_ATTRIBUTE_KEY)))
            .andExpect(jsonPath("$.[*].attributeTitle").value(hasItem(DEFAULT_ATTRIBUTE_TITLE)));

        // Check, that the count call also returns 1
        restPermissionsOnAttributeMasterMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPermissionsOnAttributeMasterShouldNotBeFound(String filter) throws Exception {
        restPermissionsOnAttributeMasterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPermissionsOnAttributeMasterMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPermissionsOnAttributeMaster() throws Exception {
        // Get the permissionsOnAttributeMaster
        restPermissionsOnAttributeMasterMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPermissionsOnAttributeMaster() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        int databaseSizeBeforeUpdate = permissionsOnAttributeMasterRepository.findAll().size();

        // Update the permissionsOnAttributeMaster
        PermissionsOnAttributeMaster updatedPermissionsOnAttributeMaster = permissionsOnAttributeMasterRepository
            .findById(permissionsOnAttributeMaster.getId())
            .get();
        // Disconnect from session so that the updates on updatedPermissionsOnAttributeMaster are not directly saved in db
        em.detach(updatedPermissionsOnAttributeMaster);
        updatedPermissionsOnAttributeMaster.attributeKey(UPDATED_ATTRIBUTE_KEY).attributeTitle(UPDATED_ATTRIBUTE_TITLE);
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterMapper.toDto(
            updatedPermissionsOnAttributeMaster
        );

        restPermissionsOnAttributeMasterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, permissionsOnAttributeMasterDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(permissionsOnAttributeMasterDTO))
            )
            .andExpect(status().isOk());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeUpdate);
        PermissionsOnAttributeMaster testPermissionsOnAttributeMaster = permissionsOnAttributeMasterList.get(
            permissionsOnAttributeMasterList.size() - 1
        );
        assertThat(testPermissionsOnAttributeMaster.getAttributeKey()).isEqualTo(UPDATED_ATTRIBUTE_KEY);
        assertThat(testPermissionsOnAttributeMaster.getAttributeTitle()).isEqualTo(UPDATED_ATTRIBUTE_TITLE);
    }

    @Test
    @Transactional
    void putNonExistingPermissionsOnAttributeMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionsOnAttributeMasterRepository.findAll().size();
        permissionsOnAttributeMaster.setId(count.incrementAndGet());

        // Create the PermissionsOnAttributeMaster
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterMapper.toDto(
            permissionsOnAttributeMaster
        );

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPermissionsOnAttributeMasterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, permissionsOnAttributeMasterDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(permissionsOnAttributeMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPermissionsOnAttributeMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionsOnAttributeMasterRepository.findAll().size();
        permissionsOnAttributeMaster.setId(count.incrementAndGet());

        // Create the PermissionsOnAttributeMaster
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterMapper.toDto(
            permissionsOnAttributeMaster
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPermissionsOnAttributeMasterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(permissionsOnAttributeMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPermissionsOnAttributeMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionsOnAttributeMasterRepository.findAll().size();
        permissionsOnAttributeMaster.setId(count.incrementAndGet());

        // Create the PermissionsOnAttributeMaster
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterMapper.toDto(
            permissionsOnAttributeMaster
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPermissionsOnAttributeMasterMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(permissionsOnAttributeMasterDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePermissionsOnAttributeMasterWithPatch() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        int databaseSizeBeforeUpdate = permissionsOnAttributeMasterRepository.findAll().size();

        // Update the permissionsOnAttributeMaster using partial update
        PermissionsOnAttributeMaster partialUpdatedPermissionsOnAttributeMaster = new PermissionsOnAttributeMaster();
        partialUpdatedPermissionsOnAttributeMaster.setId(permissionsOnAttributeMaster.getId());

        restPermissionsOnAttributeMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPermissionsOnAttributeMaster.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPermissionsOnAttributeMaster))
            )
            .andExpect(status().isOk());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeUpdate);
        PermissionsOnAttributeMaster testPermissionsOnAttributeMaster = permissionsOnAttributeMasterList.get(
            permissionsOnAttributeMasterList.size() - 1
        );
        assertThat(testPermissionsOnAttributeMaster.getAttributeKey()).isEqualTo(DEFAULT_ATTRIBUTE_KEY);
        assertThat(testPermissionsOnAttributeMaster.getAttributeTitle()).isEqualTo(DEFAULT_ATTRIBUTE_TITLE);
    }

    @Test
    @Transactional
    void fullUpdatePermissionsOnAttributeMasterWithPatch() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        int databaseSizeBeforeUpdate = permissionsOnAttributeMasterRepository.findAll().size();

        // Update the permissionsOnAttributeMaster using partial update
        PermissionsOnAttributeMaster partialUpdatedPermissionsOnAttributeMaster = new PermissionsOnAttributeMaster();
        partialUpdatedPermissionsOnAttributeMaster.setId(permissionsOnAttributeMaster.getId());

        partialUpdatedPermissionsOnAttributeMaster.attributeKey(UPDATED_ATTRIBUTE_KEY).attributeTitle(UPDATED_ATTRIBUTE_TITLE);

        restPermissionsOnAttributeMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPermissionsOnAttributeMaster.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPermissionsOnAttributeMaster))
            )
            .andExpect(status().isOk());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeUpdate);
        PermissionsOnAttributeMaster testPermissionsOnAttributeMaster = permissionsOnAttributeMasterList.get(
            permissionsOnAttributeMasterList.size() - 1
        );
        assertThat(testPermissionsOnAttributeMaster.getAttributeKey()).isEqualTo(UPDATED_ATTRIBUTE_KEY);
        assertThat(testPermissionsOnAttributeMaster.getAttributeTitle()).isEqualTo(UPDATED_ATTRIBUTE_TITLE);
    }

    @Test
    @Transactional
    void patchNonExistingPermissionsOnAttributeMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionsOnAttributeMasterRepository.findAll().size();
        permissionsOnAttributeMaster.setId(count.incrementAndGet());

        // Create the PermissionsOnAttributeMaster
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterMapper.toDto(
            permissionsOnAttributeMaster
        );

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPermissionsOnAttributeMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, permissionsOnAttributeMasterDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(permissionsOnAttributeMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPermissionsOnAttributeMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionsOnAttributeMasterRepository.findAll().size();
        permissionsOnAttributeMaster.setId(count.incrementAndGet());

        // Create the PermissionsOnAttributeMaster
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterMapper.toDto(
            permissionsOnAttributeMaster
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPermissionsOnAttributeMasterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(permissionsOnAttributeMasterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPermissionsOnAttributeMaster() throws Exception {
        int databaseSizeBeforeUpdate = permissionsOnAttributeMasterRepository.findAll().size();
        permissionsOnAttributeMaster.setId(count.incrementAndGet());

        // Create the PermissionsOnAttributeMaster
        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = permissionsOnAttributeMasterMapper.toDto(
            permissionsOnAttributeMaster
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPermissionsOnAttributeMasterMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(permissionsOnAttributeMasterDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PermissionsOnAttributeMaster in the database
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePermissionsOnAttributeMaster() throws Exception {
        // Initialize the database
        permissionsOnAttributeMasterRepository.saveAndFlush(permissionsOnAttributeMaster);

        int databaseSizeBeforeDelete = permissionsOnAttributeMasterRepository.findAll().size();

        // Delete the permissionsOnAttributeMaster
        restPermissionsOnAttributeMasterMockMvc
            .perform(delete(ENTITY_API_URL_ID, permissionsOnAttributeMaster.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PermissionsOnAttributeMaster> permissionsOnAttributeMasterList = permissionsOnAttributeMasterRepository.findAll();
        assertThat(permissionsOnAttributeMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
