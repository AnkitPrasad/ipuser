package com.user.myapp.service.mapper;

import com.user.myapp.domain.CandidateDetails;
import com.user.myapp.service.dto.CandidateDetailsDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class CandidateDetailsMapperImpl implements CandidateDetailsMapper {

    @Override
    public CandidateDetails toEntity(CandidateDetailsDTO dto) {
        if ( dto == null ) {
            return null;
        }

        CandidateDetails candidateDetails = new CandidateDetails();

        candidateDetails.setId( dto.getId() );
        candidateDetails.setCandidateCountry( dto.getCandidateCountry() );
        candidateDetails.setBusinessUnits( dto.getBusinessUnits() );
        candidateDetails.setLegalEntity( dto.getLegalEntity() );
        candidateDetails.setCandidateSubFunction( dto.getCandidateSubFunction() );
        candidateDetails.setDesignation( dto.getDesignation() );
        candidateDetails.setJobTitle( dto.getJobTitle() );
        candidateDetails.setLocation( dto.getLocation() );
        candidateDetails.setSubBusinessUnits( dto.getSubBusinessUnits() );
        candidateDetails.setCandidateFunction( dto.getCandidateFunction() );
        candidateDetails.setWorkLevel( dto.getWorkLevel() );
        candidateDetails.setCandidateGrade( dto.getCandidateGrade() );

        return candidateDetails;
    }

    @Override
    public CandidateDetailsDTO toDto(CandidateDetails entity) {
        if ( entity == null ) {
            return null;
        }

        CandidateDetailsDTO candidateDetailsDTO = new CandidateDetailsDTO();

        candidateDetailsDTO.setId( entity.getId() );
        candidateDetailsDTO.setCandidateCountry( entity.getCandidateCountry() );
        candidateDetailsDTO.setBusinessUnits( entity.getBusinessUnits() );
        candidateDetailsDTO.setLegalEntity( entity.getLegalEntity() );
        candidateDetailsDTO.setCandidateSubFunction( entity.getCandidateSubFunction() );
        candidateDetailsDTO.setDesignation( entity.getDesignation() );
        candidateDetailsDTO.setJobTitle( entity.getJobTitle() );
        candidateDetailsDTO.setLocation( entity.getLocation() );
        candidateDetailsDTO.setSubBusinessUnits( entity.getSubBusinessUnits() );
        candidateDetailsDTO.setCandidateFunction( entity.getCandidateFunction() );
        candidateDetailsDTO.setWorkLevel( entity.getWorkLevel() );
        candidateDetailsDTO.setCandidateGrade( entity.getCandidateGrade() );

        return candidateDetailsDTO;
    }

    @Override
    public List<CandidateDetails> toEntity(List<CandidateDetailsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<CandidateDetails> list = new ArrayList<CandidateDetails>( dtoList.size() );
        for ( CandidateDetailsDTO candidateDetailsDTO : dtoList ) {
            list.add( toEntity( candidateDetailsDTO ) );
        }

        return list;
    }

    @Override
    public List<CandidateDetailsDTO> toDto(List<CandidateDetails> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CandidateDetailsDTO> list = new ArrayList<CandidateDetailsDTO>( entityList.size() );
        for ( CandidateDetails candidateDetails : entityList ) {
            list.add( toDto( candidateDetails ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(CandidateDetails entity, CandidateDetailsDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getCandidateCountry() != null ) {
            entity.setCandidateCountry( dto.getCandidateCountry() );
        }
        if ( dto.getBusinessUnits() != null ) {
            entity.setBusinessUnits( dto.getBusinessUnits() );
        }
        if ( dto.getLegalEntity() != null ) {
            entity.setLegalEntity( dto.getLegalEntity() );
        }
        if ( dto.getCandidateSubFunction() != null ) {
            entity.setCandidateSubFunction( dto.getCandidateSubFunction() );
        }
        if ( dto.getDesignation() != null ) {
            entity.setDesignation( dto.getDesignation() );
        }
        if ( dto.getJobTitle() != null ) {
            entity.setJobTitle( dto.getJobTitle() );
        }
        if ( dto.getLocation() != null ) {
            entity.setLocation( dto.getLocation() );
        }
        if ( dto.getSubBusinessUnits() != null ) {
            entity.setSubBusinessUnits( dto.getSubBusinessUnits() );
        }
        if ( dto.getCandidateFunction() != null ) {
            entity.setCandidateFunction( dto.getCandidateFunction() );
        }
        if ( dto.getWorkLevel() != null ) {
            entity.setWorkLevel( dto.getWorkLevel() );
        }
        if ( dto.getCandidateGrade() != null ) {
            entity.setCandidateGrade( dto.getCandidateGrade() );
        }
    }
}
