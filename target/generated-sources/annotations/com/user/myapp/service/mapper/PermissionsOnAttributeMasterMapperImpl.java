package com.user.myapp.service.mapper;

import com.user.myapp.domain.PermissionsOnAttributeMaster;
import com.user.myapp.service.dto.PermissionsOnAttributeMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class PermissionsOnAttributeMasterMapperImpl implements PermissionsOnAttributeMasterMapper {

    @Override
    public PermissionsOnAttributeMaster toEntity(PermissionsOnAttributeMasterDTO dto) {
        if ( dto == null ) {
            return null;
        }

        PermissionsOnAttributeMaster permissionsOnAttributeMaster = new PermissionsOnAttributeMaster();

        permissionsOnAttributeMaster.setId( dto.getId() );
        permissionsOnAttributeMaster.setAttributeKey( dto.getAttributeKey() );
        permissionsOnAttributeMaster.setAttributeTitle( dto.getAttributeTitle() );

        return permissionsOnAttributeMaster;
    }

    @Override
    public PermissionsOnAttributeMasterDTO toDto(PermissionsOnAttributeMaster entity) {
        if ( entity == null ) {
            return null;
        }

        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = new PermissionsOnAttributeMasterDTO();

        permissionsOnAttributeMasterDTO.setId( entity.getId() );
        permissionsOnAttributeMasterDTO.setAttributeKey( entity.getAttributeKey() );
        permissionsOnAttributeMasterDTO.setAttributeTitle( entity.getAttributeTitle() );

        return permissionsOnAttributeMasterDTO;
    }

    @Override
    public List<PermissionsOnAttributeMaster> toEntity(List<PermissionsOnAttributeMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<PermissionsOnAttributeMaster> list = new ArrayList<PermissionsOnAttributeMaster>( dtoList.size() );
        for ( PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO : dtoList ) {
            list.add( toEntity( permissionsOnAttributeMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<PermissionsOnAttributeMasterDTO> toDto(List<PermissionsOnAttributeMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<PermissionsOnAttributeMasterDTO> list = new ArrayList<PermissionsOnAttributeMasterDTO>( entityList.size() );
        for ( PermissionsOnAttributeMaster permissionsOnAttributeMaster : entityList ) {
            list.add( toDto( permissionsOnAttributeMaster ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(PermissionsOnAttributeMaster entity, PermissionsOnAttributeMasterDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getAttributeKey() != null ) {
            entity.setAttributeKey( dto.getAttributeKey() );
        }
        if ( dto.getAttributeTitle() != null ) {
            entity.setAttributeTitle( dto.getAttributeTitle() );
        }
    }
}
