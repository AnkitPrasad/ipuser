package com.user.myapp.service.mapper;

import com.user.myapp.domain.Client;
import com.user.myapp.domain.ClientSetting;
import com.user.myapp.service.dto.ClientDTO;
import com.user.myapp.service.dto.ClientSettingDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class ClientMapperImpl implements ClientMapper {

    @Override
    public Client toEntity(ClientDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Client client = new Client();

        client.setId( dto.getId() );
        client.setLegalEntity( dto.getLegalEntity() );
        client.setIndustry( dto.getIndustry() );
        client.setBankDetails( dto.getBankDetails() );
        client.setClientType( dto.getClientType() );
        client.setTnaNo( dto.getTnaNo() );
        client.setGstNo( dto.getGstNo() );
        client.setPan( dto.getPan() );
        client.setUploadClientLogo( dto.getUploadClientLogo() );
        client.setCreatedDate( dto.getCreatedDate() );
        client.setCreatedBy( dto.getCreatedBy() );
        client.setUpdatedDate( dto.getUpdatedDate() );
        client.setUpdatedBy( dto.getUpdatedBy() );
        client.setStatus( dto.getStatus() );
        client.clientSetting( clientSettingDTOToClientSetting( dto.getClientSetting() ) );

        return client;
    }

    @Override
    public List<Client> toEntity(List<ClientDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Client> list = new ArrayList<Client>( dtoList.size() );
        for ( ClientDTO clientDTO : dtoList ) {
            list.add( toEntity( clientDTO ) );
        }

        return list;
    }

    @Override
    public List<ClientDTO> toDto(List<Client> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ClientDTO> list = new ArrayList<ClientDTO>( entityList.size() );
        for ( Client client : entityList ) {
            list.add( toDto( client ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Client entity, ClientDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getLegalEntity() != null ) {
            entity.setLegalEntity( dto.getLegalEntity() );
        }
        if ( dto.getIndustry() != null ) {
            entity.setIndustry( dto.getIndustry() );
        }
        if ( dto.getBankDetails() != null ) {
            entity.setBankDetails( dto.getBankDetails() );
        }
        if ( dto.getClientType() != null ) {
            entity.setClientType( dto.getClientType() );
        }
        if ( dto.getTnaNo() != null ) {
            entity.setTnaNo( dto.getTnaNo() );
        }
        if ( dto.getGstNo() != null ) {
            entity.setGstNo( dto.getGstNo() );
        }
        if ( dto.getPan() != null ) {
            entity.setPan( dto.getPan() );
        }
        if ( dto.getUploadClientLogo() != null ) {
            entity.setUploadClientLogo( dto.getUploadClientLogo() );
        }
        if ( dto.getCreatedDate() != null ) {
            entity.setCreatedDate( dto.getCreatedDate() );
        }
        if ( dto.getCreatedBy() != null ) {
            entity.setCreatedBy( dto.getCreatedBy() );
        }
        if ( dto.getUpdatedDate() != null ) {
            entity.setUpdatedDate( dto.getUpdatedDate() );
        }
        if ( dto.getUpdatedBy() != null ) {
            entity.setUpdatedBy( dto.getUpdatedBy() );
        }
        if ( dto.getStatus() != null ) {
            entity.setStatus( dto.getStatus() );
        }
        if ( dto.getClientSetting() != null ) {
            if ( entity.getClientSetting() == null ) {
                entity.clientSetting( new ClientSetting() );
            }
            clientSettingDTOToClientSetting1( dto.getClientSetting(), entity.getClientSetting() );
        }
    }

    @Override
    public ClientDTO toDto(Client s) {
        if ( s == null ) {
            return null;
        }

        ClientDTO clientDTO = new ClientDTO();

        clientDTO.setClientSetting( toDtoClientSettingId( s.getClientSetting() ) );
        clientDTO.setId( s.getId() );
        clientDTO.setLegalEntity( s.getLegalEntity() );
        clientDTO.setIndustry( s.getIndustry() );
        clientDTO.setBankDetails( s.getBankDetails() );
        clientDTO.setClientType( s.getClientType() );
        clientDTO.setTnaNo( s.getTnaNo() );
        clientDTO.setGstNo( s.getGstNo() );
        clientDTO.setPan( s.getPan() );
        clientDTO.setUploadClientLogo( s.getUploadClientLogo() );
        clientDTO.setCreatedDate( s.getCreatedDate() );
        clientDTO.setCreatedBy( s.getCreatedBy() );
        clientDTO.setUpdatedDate( s.getUpdatedDate() );
        clientDTO.setUpdatedBy( s.getUpdatedBy() );
        clientDTO.setStatus( s.getStatus() );

        return clientDTO;
    }

    @Override
    public ClientSettingDTO toDtoClientSettingId(ClientSetting clientSetting) {
        if ( clientSetting == null ) {
            return null;
        }

        ClientSettingDTO clientSettingDTO = new ClientSettingDTO();

        clientSettingDTO.setId( clientSetting.getId() );

        return clientSettingDTO;
    }

    protected ClientSetting clientSettingDTOToClientSetting(ClientSettingDTO clientSettingDTO) {
        if ( clientSettingDTO == null ) {
            return null;
        }

        ClientSetting clientSetting = new ClientSetting();

        clientSetting.setId( clientSettingDTO.getId() );
        clientSetting.setCandidateSelfAssessment( clientSettingDTO.getCandidateSelfAssessment() );
        clientSetting.setAutoReninderAssessment( clientSettingDTO.getAutoReninderAssessment() );
        clientSetting.setAllowCandidatePaidVersion( clientSettingDTO.getAllowCandidatePaidVersion() );
        clientSetting.setAllowEmployessPaidVersion( clientSettingDTO.getAllowEmployessPaidVersion() );
        clientSetting.setAutoArchieveRequisitions( clientSettingDTO.getAutoArchieveRequisitions() );
        clientSetting.setCompanyThemeDarkColour( clientSettingDTO.getCompanyThemeDarkColour() );
        clientSetting.setCompanyThemeLightColour( clientSettingDTO.getCompanyThemeLightColour() );
        clientSetting.setCompanyUrl( clientSettingDTO.getCompanyUrl() );
        clientSetting.setCompanyMenu( clientSettingDTO.getCompanyMenu() );

        return clientSetting;
    }

    protected void clientSettingDTOToClientSetting1(ClientSettingDTO clientSettingDTO, ClientSetting mappingTarget) {
        if ( clientSettingDTO == null ) {
            return;
        }

        if ( clientSettingDTO.getId() != null ) {
            mappingTarget.setId( clientSettingDTO.getId() );
        }
        if ( clientSettingDTO.getCandidateSelfAssessment() != null ) {
            mappingTarget.setCandidateSelfAssessment( clientSettingDTO.getCandidateSelfAssessment() );
        }
        if ( clientSettingDTO.getAutoReninderAssessment() != null ) {
            mappingTarget.setAutoReninderAssessment( clientSettingDTO.getAutoReninderAssessment() );
        }
        if ( clientSettingDTO.getAllowCandidatePaidVersion() != null ) {
            mappingTarget.setAllowCandidatePaidVersion( clientSettingDTO.getAllowCandidatePaidVersion() );
        }
        if ( clientSettingDTO.getAllowEmployessPaidVersion() != null ) {
            mappingTarget.setAllowEmployessPaidVersion( clientSettingDTO.getAllowEmployessPaidVersion() );
        }
        if ( clientSettingDTO.getAutoArchieveRequisitions() != null ) {
            mappingTarget.setAutoArchieveRequisitions( clientSettingDTO.getAutoArchieveRequisitions() );
        }
        if ( clientSettingDTO.getCompanyThemeDarkColour() != null ) {
            mappingTarget.setCompanyThemeDarkColour( clientSettingDTO.getCompanyThemeDarkColour() );
        }
        if ( clientSettingDTO.getCompanyThemeLightColour() != null ) {
            mappingTarget.setCompanyThemeLightColour( clientSettingDTO.getCompanyThemeLightColour() );
        }
        if ( clientSettingDTO.getCompanyUrl() != null ) {
            mappingTarget.setCompanyUrl( clientSettingDTO.getCompanyUrl() );
        }
        if ( clientSettingDTO.getCompanyMenu() != null ) {
            mappingTarget.setCompanyMenu( clientSettingDTO.getCompanyMenu() );
        }
    }
}
