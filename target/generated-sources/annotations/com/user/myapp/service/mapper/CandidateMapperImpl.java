package com.user.myapp.service.mapper;

import com.user.myapp.domain.Candidate;
import com.user.myapp.domain.CandidateDetails;
import com.user.myapp.domain.Client;
import com.user.myapp.domain.ClientSetting;
import com.user.myapp.service.dto.CandidateDTO;
import com.user.myapp.service.dto.CandidateDetailsDTO;
import com.user.myapp.service.dto.ClientDTO;
import com.user.myapp.service.dto.ClientSettingDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class CandidateMapperImpl implements CandidateMapper {

    @Override
    public Candidate toEntity(CandidateDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Candidate candidate = new Candidate();

        candidate.setId( dto.getId() );
        candidate.setIsInternalCandidate( dto.getIsInternalCandidate() );
        candidate.setCompany( dto.getCompany() );
        candidate.setCreatedDate( dto.getCreatedDate() );
        candidate.setCreatedBy( dto.getCreatedBy() );
        candidate.setUpdatedDate( dto.getUpdatedDate() );
        candidate.setUpdatedBy( dto.getUpdatedBy() );
        candidate.setAssessmentStatus( dto.getAssessmentStatus() );
        candidate.setStatus( dto.getStatus() );
        candidate.setArchiveStatus( dto.getArchiveStatus() );
        candidate.candidateDetails( candidateDetailsDTOToCandidateDetails( dto.getCandidateDetails() ) );
        candidate.client( clientDTOToClient( dto.getClient() ) );

        return candidate;
    }

    @Override
    public List<Candidate> toEntity(List<CandidateDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Candidate> list = new ArrayList<Candidate>( dtoList.size() );
        for ( CandidateDTO candidateDTO : dtoList ) {
            list.add( toEntity( candidateDTO ) );
        }

        return list;
    }

    @Override
    public List<CandidateDTO> toDto(List<Candidate> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CandidateDTO> list = new ArrayList<CandidateDTO>( entityList.size() );
        for ( Candidate candidate : entityList ) {
            list.add( toDto( candidate ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Candidate entity, CandidateDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getIsInternalCandidate() != null ) {
            entity.setIsInternalCandidate( dto.getIsInternalCandidate() );
        }
        if ( dto.getCompany() != null ) {
            entity.setCompany( dto.getCompany() );
        }
        if ( dto.getCreatedDate() != null ) {
            entity.setCreatedDate( dto.getCreatedDate() );
        }
        if ( dto.getCreatedBy() != null ) {
            entity.setCreatedBy( dto.getCreatedBy() );
        }
        if ( dto.getUpdatedDate() != null ) {
            entity.setUpdatedDate( dto.getUpdatedDate() );
        }
        if ( dto.getUpdatedBy() != null ) {
            entity.setUpdatedBy( dto.getUpdatedBy() );
        }
        if ( dto.getAssessmentStatus() != null ) {
            entity.setAssessmentStatus( dto.getAssessmentStatus() );
        }
        if ( dto.getStatus() != null ) {
            entity.setStatus( dto.getStatus() );
        }
        if ( dto.getArchiveStatus() != null ) {
            entity.setArchiveStatus( dto.getArchiveStatus() );
        }
        if ( dto.getCandidateDetails() != null ) {
            if ( entity.getCandidateDetails() == null ) {
                entity.candidateDetails( new CandidateDetails() );
            }
            candidateDetailsDTOToCandidateDetails1( dto.getCandidateDetails(), entity.getCandidateDetails() );
        }
        if ( dto.getClient() != null ) {
            if ( entity.getClient() == null ) {
                entity.client( new Client() );
            }
            clientDTOToClient1( dto.getClient(), entity.getClient() );
        }
    }

    @Override
    public CandidateDTO toDto(Candidate s) {
        if ( s == null ) {
            return null;
        }

        CandidateDTO candidateDTO = new CandidateDTO();

        candidateDTO.setCandidateDetails( toDtoCandidateDetailsId( s.getCandidateDetails() ) );
        candidateDTO.setClient( toDtoClientId( s.getClient() ) );
        candidateDTO.setId( s.getId() );
        candidateDTO.setIsInternalCandidate( s.getIsInternalCandidate() );
        candidateDTO.setCompany( s.getCompany() );
        candidateDTO.setCreatedDate( s.getCreatedDate() );
        candidateDTO.setCreatedBy( s.getCreatedBy() );
        candidateDTO.setUpdatedDate( s.getUpdatedDate() );
        candidateDTO.setUpdatedBy( s.getUpdatedBy() );
        candidateDTO.setAssessmentStatus( s.getAssessmentStatus() );
        candidateDTO.setStatus( s.getStatus() );
        candidateDTO.setArchiveStatus( s.getArchiveStatus() );

        return candidateDTO;
    }

    @Override
    public CandidateDetailsDTO toDtoCandidateDetailsId(CandidateDetails candidateDetails) {
        if ( candidateDetails == null ) {
            return null;
        }

        CandidateDetailsDTO candidateDetailsDTO = new CandidateDetailsDTO();

        candidateDetailsDTO.setId( candidateDetails.getId() );

        return candidateDetailsDTO;
    }

    @Override
    public ClientDTO toDtoClientId(Client client) {
        if ( client == null ) {
            return null;
        }

        ClientDTO clientDTO = new ClientDTO();

        clientDTO.setId( client.getId() );

        return clientDTO;
    }

    protected CandidateDetails candidateDetailsDTOToCandidateDetails(CandidateDetailsDTO candidateDetailsDTO) {
        if ( candidateDetailsDTO == null ) {
            return null;
        }

        CandidateDetails candidateDetails = new CandidateDetails();

        candidateDetails.setId( candidateDetailsDTO.getId() );
        candidateDetails.setCandidateCountry( candidateDetailsDTO.getCandidateCountry() );
        candidateDetails.setBusinessUnits( candidateDetailsDTO.getBusinessUnits() );
        candidateDetails.setLegalEntity( candidateDetailsDTO.getLegalEntity() );
        candidateDetails.setCandidateSubFunction( candidateDetailsDTO.getCandidateSubFunction() );
        candidateDetails.setDesignation( candidateDetailsDTO.getDesignation() );
        candidateDetails.setJobTitle( candidateDetailsDTO.getJobTitle() );
        candidateDetails.setLocation( candidateDetailsDTO.getLocation() );
        candidateDetails.setSubBusinessUnits( candidateDetailsDTO.getSubBusinessUnits() );
        candidateDetails.setCandidateFunction( candidateDetailsDTO.getCandidateFunction() );
        candidateDetails.setWorkLevel( candidateDetailsDTO.getWorkLevel() );
        candidateDetails.setCandidateGrade( candidateDetailsDTO.getCandidateGrade() );

        return candidateDetails;
    }

    protected ClientSetting clientSettingDTOToClientSetting(ClientSettingDTO clientSettingDTO) {
        if ( clientSettingDTO == null ) {
            return null;
        }

        ClientSetting clientSetting = new ClientSetting();

        clientSetting.setId( clientSettingDTO.getId() );
        clientSetting.setCandidateSelfAssessment( clientSettingDTO.getCandidateSelfAssessment() );
        clientSetting.setAutoReninderAssessment( clientSettingDTO.getAutoReninderAssessment() );
        clientSetting.setAllowCandidatePaidVersion( clientSettingDTO.getAllowCandidatePaidVersion() );
        clientSetting.setAllowEmployessPaidVersion( clientSettingDTO.getAllowEmployessPaidVersion() );
        clientSetting.setAutoArchieveRequisitions( clientSettingDTO.getAutoArchieveRequisitions() );
        clientSetting.setCompanyThemeDarkColour( clientSettingDTO.getCompanyThemeDarkColour() );
        clientSetting.setCompanyThemeLightColour( clientSettingDTO.getCompanyThemeLightColour() );
        clientSetting.setCompanyUrl( clientSettingDTO.getCompanyUrl() );
        clientSetting.setCompanyMenu( clientSettingDTO.getCompanyMenu() );

        return clientSetting;
    }

    protected Client clientDTOToClient(ClientDTO clientDTO) {
        if ( clientDTO == null ) {
            return null;
        }

        Client client = new Client();

        client.setId( clientDTO.getId() );
        client.setLegalEntity( clientDTO.getLegalEntity() );
        client.setIndustry( clientDTO.getIndustry() );
        client.setBankDetails( clientDTO.getBankDetails() );
        client.setClientType( clientDTO.getClientType() );
        client.setTnaNo( clientDTO.getTnaNo() );
        client.setGstNo( clientDTO.getGstNo() );
        client.setPan( clientDTO.getPan() );
        client.setUploadClientLogo( clientDTO.getUploadClientLogo() );
        client.setCreatedDate( clientDTO.getCreatedDate() );
        client.setCreatedBy( clientDTO.getCreatedBy() );
        client.setUpdatedDate( clientDTO.getUpdatedDate() );
        client.setUpdatedBy( clientDTO.getUpdatedBy() );
        client.setStatus( clientDTO.getStatus() );
        client.clientSetting( clientSettingDTOToClientSetting( clientDTO.getClientSetting() ) );

        return client;
    }

    protected void candidateDetailsDTOToCandidateDetails1(CandidateDetailsDTO candidateDetailsDTO, CandidateDetails mappingTarget) {
        if ( candidateDetailsDTO == null ) {
            return;
        }

        if ( candidateDetailsDTO.getId() != null ) {
            mappingTarget.setId( candidateDetailsDTO.getId() );
        }
        if ( candidateDetailsDTO.getCandidateCountry() != null ) {
            mappingTarget.setCandidateCountry( candidateDetailsDTO.getCandidateCountry() );
        }
        if ( candidateDetailsDTO.getBusinessUnits() != null ) {
            mappingTarget.setBusinessUnits( candidateDetailsDTO.getBusinessUnits() );
        }
        if ( candidateDetailsDTO.getLegalEntity() != null ) {
            mappingTarget.setLegalEntity( candidateDetailsDTO.getLegalEntity() );
        }
        if ( candidateDetailsDTO.getCandidateSubFunction() != null ) {
            mappingTarget.setCandidateSubFunction( candidateDetailsDTO.getCandidateSubFunction() );
        }
        if ( candidateDetailsDTO.getDesignation() != null ) {
            mappingTarget.setDesignation( candidateDetailsDTO.getDesignation() );
        }
        if ( candidateDetailsDTO.getJobTitle() != null ) {
            mappingTarget.setJobTitle( candidateDetailsDTO.getJobTitle() );
        }
        if ( candidateDetailsDTO.getLocation() != null ) {
            mappingTarget.setLocation( candidateDetailsDTO.getLocation() );
        }
        if ( candidateDetailsDTO.getSubBusinessUnits() != null ) {
            mappingTarget.setSubBusinessUnits( candidateDetailsDTO.getSubBusinessUnits() );
        }
        if ( candidateDetailsDTO.getCandidateFunction() != null ) {
            mappingTarget.setCandidateFunction( candidateDetailsDTO.getCandidateFunction() );
        }
        if ( candidateDetailsDTO.getWorkLevel() != null ) {
            mappingTarget.setWorkLevel( candidateDetailsDTO.getWorkLevel() );
        }
        if ( candidateDetailsDTO.getCandidateGrade() != null ) {
            mappingTarget.setCandidateGrade( candidateDetailsDTO.getCandidateGrade() );
        }
    }

    protected void clientSettingDTOToClientSetting1(ClientSettingDTO clientSettingDTO, ClientSetting mappingTarget) {
        if ( clientSettingDTO == null ) {
            return;
        }

        if ( clientSettingDTO.getId() != null ) {
            mappingTarget.setId( clientSettingDTO.getId() );
        }
        if ( clientSettingDTO.getCandidateSelfAssessment() != null ) {
            mappingTarget.setCandidateSelfAssessment( clientSettingDTO.getCandidateSelfAssessment() );
        }
        if ( clientSettingDTO.getAutoReninderAssessment() != null ) {
            mappingTarget.setAutoReninderAssessment( clientSettingDTO.getAutoReninderAssessment() );
        }
        if ( clientSettingDTO.getAllowCandidatePaidVersion() != null ) {
            mappingTarget.setAllowCandidatePaidVersion( clientSettingDTO.getAllowCandidatePaidVersion() );
        }
        if ( clientSettingDTO.getAllowEmployessPaidVersion() != null ) {
            mappingTarget.setAllowEmployessPaidVersion( clientSettingDTO.getAllowEmployessPaidVersion() );
        }
        if ( clientSettingDTO.getAutoArchieveRequisitions() != null ) {
            mappingTarget.setAutoArchieveRequisitions( clientSettingDTO.getAutoArchieveRequisitions() );
        }
        if ( clientSettingDTO.getCompanyThemeDarkColour() != null ) {
            mappingTarget.setCompanyThemeDarkColour( clientSettingDTO.getCompanyThemeDarkColour() );
        }
        if ( clientSettingDTO.getCompanyThemeLightColour() != null ) {
            mappingTarget.setCompanyThemeLightColour( clientSettingDTO.getCompanyThemeLightColour() );
        }
        if ( clientSettingDTO.getCompanyUrl() != null ) {
            mappingTarget.setCompanyUrl( clientSettingDTO.getCompanyUrl() );
        }
        if ( clientSettingDTO.getCompanyMenu() != null ) {
            mappingTarget.setCompanyMenu( clientSettingDTO.getCompanyMenu() );
        }
    }

    protected void clientDTOToClient1(ClientDTO clientDTO, Client mappingTarget) {
        if ( clientDTO == null ) {
            return;
        }

        if ( clientDTO.getId() != null ) {
            mappingTarget.setId( clientDTO.getId() );
        }
        if ( clientDTO.getLegalEntity() != null ) {
            mappingTarget.setLegalEntity( clientDTO.getLegalEntity() );
        }
        if ( clientDTO.getIndustry() != null ) {
            mappingTarget.setIndustry( clientDTO.getIndustry() );
        }
        if ( clientDTO.getBankDetails() != null ) {
            mappingTarget.setBankDetails( clientDTO.getBankDetails() );
        }
        if ( clientDTO.getClientType() != null ) {
            mappingTarget.setClientType( clientDTO.getClientType() );
        }
        if ( clientDTO.getTnaNo() != null ) {
            mappingTarget.setTnaNo( clientDTO.getTnaNo() );
        }
        if ( clientDTO.getGstNo() != null ) {
            mappingTarget.setGstNo( clientDTO.getGstNo() );
        }
        if ( clientDTO.getPan() != null ) {
            mappingTarget.setPan( clientDTO.getPan() );
        }
        if ( clientDTO.getUploadClientLogo() != null ) {
            mappingTarget.setUploadClientLogo( clientDTO.getUploadClientLogo() );
        }
        if ( clientDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( clientDTO.getCreatedDate() );
        }
        if ( clientDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( clientDTO.getCreatedBy() );
        }
        if ( clientDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( clientDTO.getUpdatedDate() );
        }
        if ( clientDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( clientDTO.getUpdatedBy() );
        }
        if ( clientDTO.getStatus() != null ) {
            mappingTarget.setStatus( clientDTO.getStatus() );
        }
        if ( clientDTO.getClientSetting() != null ) {
            if ( mappingTarget.getClientSetting() == null ) {
                mappingTarget.clientSetting( new ClientSetting() );
            }
            clientSettingDTOToClientSetting1( clientDTO.getClientSetting(), mappingTarget.getClientSetting() );
        }
    }
}
