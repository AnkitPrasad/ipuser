package com.user.myapp.service.mapper;

import com.user.myapp.domain.ClientSetting;
import com.user.myapp.service.dto.ClientSettingDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:35:59+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class ClientSettingMapperImpl implements ClientSettingMapper {

    @Override
    public ClientSetting toEntity(ClientSettingDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ClientSetting clientSetting = new ClientSetting();

        clientSetting.setId( dto.getId() );
        clientSetting.setCandidateSelfAssessment( dto.getCandidateSelfAssessment() );
        clientSetting.setAutoReninderAssessment( dto.getAutoReninderAssessment() );
        clientSetting.setAllowCandidatePaidVersion( dto.getAllowCandidatePaidVersion() );
        clientSetting.setAllowEmployessPaidVersion( dto.getAllowEmployessPaidVersion() );
        clientSetting.setAutoArchieveRequisitions( dto.getAutoArchieveRequisitions() );
        clientSetting.setCompanyThemeDarkColour( dto.getCompanyThemeDarkColour() );
        clientSetting.setCompanyThemeLightColour( dto.getCompanyThemeLightColour() );
        clientSetting.setCompanyUrl( dto.getCompanyUrl() );
        clientSetting.setCompanyMenu( dto.getCompanyMenu() );

        return clientSetting;
    }

    @Override
    public ClientSettingDTO toDto(ClientSetting entity) {
        if ( entity == null ) {
            return null;
        }

        ClientSettingDTO clientSettingDTO = new ClientSettingDTO();

        clientSettingDTO.setId( entity.getId() );
        clientSettingDTO.setCandidateSelfAssessment( entity.getCandidateSelfAssessment() );
        clientSettingDTO.setAutoReninderAssessment( entity.getAutoReninderAssessment() );
        clientSettingDTO.setAllowCandidatePaidVersion( entity.getAllowCandidatePaidVersion() );
        clientSettingDTO.setAllowEmployessPaidVersion( entity.getAllowEmployessPaidVersion() );
        clientSettingDTO.setAutoArchieveRequisitions( entity.getAutoArchieveRequisitions() );
        clientSettingDTO.setCompanyThemeDarkColour( entity.getCompanyThemeDarkColour() );
        clientSettingDTO.setCompanyThemeLightColour( entity.getCompanyThemeLightColour() );
        clientSettingDTO.setCompanyUrl( entity.getCompanyUrl() );
        clientSettingDTO.setCompanyMenu( entity.getCompanyMenu() );

        return clientSettingDTO;
    }

    @Override
    public List<ClientSetting> toEntity(List<ClientSettingDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ClientSetting> list = new ArrayList<ClientSetting>( dtoList.size() );
        for ( ClientSettingDTO clientSettingDTO : dtoList ) {
            list.add( toEntity( clientSettingDTO ) );
        }

        return list;
    }

    @Override
    public List<ClientSettingDTO> toDto(List<ClientSetting> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ClientSettingDTO> list = new ArrayList<ClientSettingDTO>( entityList.size() );
        for ( ClientSetting clientSetting : entityList ) {
            list.add( toDto( clientSetting ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(ClientSetting entity, ClientSettingDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getCandidateSelfAssessment() != null ) {
            entity.setCandidateSelfAssessment( dto.getCandidateSelfAssessment() );
        }
        if ( dto.getAutoReninderAssessment() != null ) {
            entity.setAutoReninderAssessment( dto.getAutoReninderAssessment() );
        }
        if ( dto.getAllowCandidatePaidVersion() != null ) {
            entity.setAllowCandidatePaidVersion( dto.getAllowCandidatePaidVersion() );
        }
        if ( dto.getAllowEmployessPaidVersion() != null ) {
            entity.setAllowEmployessPaidVersion( dto.getAllowEmployessPaidVersion() );
        }
        if ( dto.getAutoArchieveRequisitions() != null ) {
            entity.setAutoArchieveRequisitions( dto.getAutoArchieveRequisitions() );
        }
        if ( dto.getCompanyThemeDarkColour() != null ) {
            entity.setCompanyThemeDarkColour( dto.getCompanyThemeDarkColour() );
        }
        if ( dto.getCompanyThemeLightColour() != null ) {
            entity.setCompanyThemeLightColour( dto.getCompanyThemeLightColour() );
        }
        if ( dto.getCompanyUrl() != null ) {
            entity.setCompanyUrl( dto.getCompanyUrl() );
        }
        if ( dto.getCompanyMenu() != null ) {
            entity.setCompanyMenu( dto.getCompanyMenu() );
        }
    }
}
