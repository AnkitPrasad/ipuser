package com.user.myapp.service.mapper;

import com.user.myapp.domain.Partner;
import com.user.myapp.service.dto.PartnerDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:35:59+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class PartnerMapperImpl implements PartnerMapper {

    @Override
    public Partner toEntity(PartnerDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Partner partner = new Partner();

        partner.setId( dto.getId() );
        partner.setName( dto.getName() );
        partner.setDomain( dto.getDomain() );
        partner.setPartnerCommission( dto.getPartnerCommission() );
        partner.setPartnerType( dto.getPartnerType() );
        partner.setValueM( dto.getValueM() );
        partner.setValueC( dto.getValueC() );
        partner.setCreatedDate( dto.getCreatedDate() );
        partner.setCreatedBy( dto.getCreatedBy() );
        partner.setUpdatedDate( dto.getUpdatedDate() );
        partner.setUpdatedBy( dto.getUpdatedBy() );
        partner.setStatus( dto.getStatus() );

        return partner;
    }

    @Override
    public PartnerDTO toDto(Partner entity) {
        if ( entity == null ) {
            return null;
        }

        PartnerDTO partnerDTO = new PartnerDTO();

        partnerDTO.setId( entity.getId() );
        partnerDTO.setName( entity.getName() );
        partnerDTO.setDomain( entity.getDomain() );
        partnerDTO.setPartnerCommission( entity.getPartnerCommission() );
        partnerDTO.setPartnerType( entity.getPartnerType() );
        partnerDTO.setValueM( entity.getValueM() );
        partnerDTO.setValueC( entity.getValueC() );
        partnerDTO.setCreatedDate( entity.getCreatedDate() );
        partnerDTO.setCreatedBy( entity.getCreatedBy() );
        partnerDTO.setUpdatedDate( entity.getUpdatedDate() );
        partnerDTO.setUpdatedBy( entity.getUpdatedBy() );
        partnerDTO.setStatus( entity.getStatus() );

        return partnerDTO;
    }

    @Override
    public List<Partner> toEntity(List<PartnerDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Partner> list = new ArrayList<Partner>( dtoList.size() );
        for ( PartnerDTO partnerDTO : dtoList ) {
            list.add( toEntity( partnerDTO ) );
        }

        return list;
    }

    @Override
    public List<PartnerDTO> toDto(List<Partner> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<PartnerDTO> list = new ArrayList<PartnerDTO>( entityList.size() );
        for ( Partner partner : entityList ) {
            list.add( toDto( partner ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Partner entity, PartnerDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getName() != null ) {
            entity.setName( dto.getName() );
        }
        if ( dto.getDomain() != null ) {
            entity.setDomain( dto.getDomain() );
        }
        if ( dto.getPartnerCommission() != null ) {
            entity.setPartnerCommission( dto.getPartnerCommission() );
        }
        if ( dto.getPartnerType() != null ) {
            entity.setPartnerType( dto.getPartnerType() );
        }
        if ( dto.getValueM() != null ) {
            entity.setValueM( dto.getValueM() );
        }
        if ( dto.getValueC() != null ) {
            entity.setValueC( dto.getValueC() );
        }
        if ( dto.getCreatedDate() != null ) {
            entity.setCreatedDate( dto.getCreatedDate() );
        }
        if ( dto.getCreatedBy() != null ) {
            entity.setCreatedBy( dto.getCreatedBy() );
        }
        if ( dto.getUpdatedDate() != null ) {
            entity.setUpdatedDate( dto.getUpdatedDate() );
        }
        if ( dto.getUpdatedBy() != null ) {
            entity.setUpdatedBy( dto.getUpdatedBy() );
        }
        if ( dto.getStatus() != null ) {
            entity.setStatus( dto.getStatus() );
        }
    }
}
