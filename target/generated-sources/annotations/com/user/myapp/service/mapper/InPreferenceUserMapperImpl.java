package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class InPreferenceUserMapperImpl implements InPreferenceUserMapper {

    @Override
    public InPreferenceUser toEntity(InPreferenceUserDTO dto) {
        if ( dto == null ) {
            return null;
        }

        InPreferenceUser inPreferenceUser = new InPreferenceUser();

        inPreferenceUser.setId( dto.getId() );
        inPreferenceUser.setFirstName( dto.getFirstName() );
        inPreferenceUser.setLastName( dto.getLastName() );
        inPreferenceUser.setMobileNo( dto.getMobileNo() );
        inPreferenceUser.setProfileUrl( dto.getProfileUrl() );
        inPreferenceUser.setUserType( dto.getUserType() );
        inPreferenceUser.setEmailId( dto.getEmailId() );
        inPreferenceUser.setLoginId( dto.getLoginId() );
        inPreferenceUser.setSecret( dto.getSecret() );
        inPreferenceUser.setCreatedDate( dto.getCreatedDate() );
        inPreferenceUser.setCreatedBy( dto.getCreatedBy() );
        inPreferenceUser.setUpdatedDate( dto.getUpdatedDate() );
        inPreferenceUser.setUpdatedBy( dto.getUpdatedBy() );
        inPreferenceUser.setStatus( dto.getStatus() );

        return inPreferenceUser;
    }

    @Override
    public InPreferenceUserDTO toDto(InPreferenceUser entity) {
        if ( entity == null ) {
            return null;
        }

        InPreferenceUserDTO inPreferenceUserDTO = new InPreferenceUserDTO();

        inPreferenceUserDTO.setId( entity.getId() );
        inPreferenceUserDTO.setFirstName( entity.getFirstName() );
        inPreferenceUserDTO.setLastName( entity.getLastName() );
        inPreferenceUserDTO.setMobileNo( entity.getMobileNo() );
        inPreferenceUserDTO.setProfileUrl( entity.getProfileUrl() );
        inPreferenceUserDTO.setUserType( entity.getUserType() );
        inPreferenceUserDTO.setEmailId( entity.getEmailId() );
        inPreferenceUserDTO.setLoginId( entity.getLoginId() );
        inPreferenceUserDTO.setSecret( entity.getSecret() );
        inPreferenceUserDTO.setCreatedDate( entity.getCreatedDate() );
        inPreferenceUserDTO.setCreatedBy( entity.getCreatedBy() );
        inPreferenceUserDTO.setUpdatedDate( entity.getUpdatedDate() );
        inPreferenceUserDTO.setUpdatedBy( entity.getUpdatedBy() );
        inPreferenceUserDTO.setStatus( entity.getStatus() );

        return inPreferenceUserDTO;
    }

    @Override
    public List<InPreferenceUser> toEntity(List<InPreferenceUserDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<InPreferenceUser> list = new ArrayList<InPreferenceUser>( dtoList.size() );
        for ( InPreferenceUserDTO inPreferenceUserDTO : dtoList ) {
            list.add( toEntity( inPreferenceUserDTO ) );
        }

        return list;
    }

    @Override
    public List<InPreferenceUserDTO> toDto(List<InPreferenceUser> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<InPreferenceUserDTO> list = new ArrayList<InPreferenceUserDTO>( entityList.size() );
        for ( InPreferenceUser inPreferenceUser : entityList ) {
            list.add( toDto( inPreferenceUser ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(InPreferenceUser entity, InPreferenceUserDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getFirstName() != null ) {
            entity.setFirstName( dto.getFirstName() );
        }
        if ( dto.getLastName() != null ) {
            entity.setLastName( dto.getLastName() );
        }
        if ( dto.getMobileNo() != null ) {
            entity.setMobileNo( dto.getMobileNo() );
        }
        if ( dto.getProfileUrl() != null ) {
            entity.setProfileUrl( dto.getProfileUrl() );
        }
        if ( dto.getUserType() != null ) {
            entity.setUserType( dto.getUserType() );
        }
        if ( dto.getEmailId() != null ) {
            entity.setEmailId( dto.getEmailId() );
        }
        if ( dto.getLoginId() != null ) {
            entity.setLoginId( dto.getLoginId() );
        }
        if ( dto.getSecret() != null ) {
            entity.setSecret( dto.getSecret() );
        }
        if ( dto.getCreatedDate() != null ) {
            entity.setCreatedDate( dto.getCreatedDate() );
        }
        if ( dto.getCreatedBy() != null ) {
            entity.setCreatedBy( dto.getCreatedBy() );
        }
        if ( dto.getUpdatedDate() != null ) {
            entity.setUpdatedDate( dto.getUpdatedDate() );
        }
        if ( dto.getUpdatedBy() != null ) {
            entity.setUpdatedBy( dto.getUpdatedBy() );
        }
        if ( dto.getStatus() != null ) {
            entity.setStatus( dto.getStatus() );
        }
    }
}
