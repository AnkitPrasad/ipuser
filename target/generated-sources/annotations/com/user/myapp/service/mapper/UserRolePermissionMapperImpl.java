package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.PermissionMaster;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.domain.UserRolePermission;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.PermissionMasterDTO;
import com.user.myapp.service.dto.RoleMasterDTO;
import com.user.myapp.service.dto.UserRolePermissionDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class UserRolePermissionMapperImpl implements UserRolePermissionMapper {

    @Override
    public UserRolePermission toEntity(UserRolePermissionDTO dto) {
        if ( dto == null ) {
            return null;
        }

        UserRolePermission userRolePermission = new UserRolePermission();

        userRolePermission.setId( dto.getId() );
        userRolePermission.inPreferenceUser( inPreferenceUserDTOToInPreferenceUser( dto.getInPreferenceUser() ) );
        userRolePermission.roleMaster( roleMasterDTOToRoleMaster( dto.getRoleMaster() ) );
        userRolePermission.permissionMaster( permissionMasterDTOToPermissionMaster( dto.getPermissionMaster() ) );

        return userRolePermission;
    }

    @Override
    public List<UserRolePermission> toEntity(List<UserRolePermissionDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<UserRolePermission> list = new ArrayList<UserRolePermission>( dtoList.size() );
        for ( UserRolePermissionDTO userRolePermissionDTO : dtoList ) {
            list.add( toEntity( userRolePermissionDTO ) );
        }

        return list;
    }

    @Override
    public List<UserRolePermissionDTO> toDto(List<UserRolePermission> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserRolePermissionDTO> list = new ArrayList<UserRolePermissionDTO>( entityList.size() );
        for ( UserRolePermission userRolePermission : entityList ) {
            list.add( toDto( userRolePermission ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(UserRolePermission entity, UserRolePermissionDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getInPreferenceUser() != null ) {
            if ( entity.getInPreferenceUser() == null ) {
                entity.inPreferenceUser( new InPreferenceUser() );
            }
            inPreferenceUserDTOToInPreferenceUser1( dto.getInPreferenceUser(), entity.getInPreferenceUser() );
        }
        if ( dto.getRoleMaster() != null ) {
            if ( entity.getRoleMaster() == null ) {
                entity.roleMaster( new RoleMaster() );
            }
            roleMasterDTOToRoleMaster1( dto.getRoleMaster(), entity.getRoleMaster() );
        }
        if ( dto.getPermissionMaster() != null ) {
            if ( entity.getPermissionMaster() == null ) {
                entity.permissionMaster( new PermissionMaster() );
            }
            permissionMasterDTOToPermissionMaster1( dto.getPermissionMaster(), entity.getPermissionMaster() );
        }
    }

    @Override
    public UserRolePermissionDTO toDto(UserRolePermission s) {
        if ( s == null ) {
            return null;
        }

        UserRolePermissionDTO userRolePermissionDTO = new UserRolePermissionDTO();

        userRolePermissionDTO.setInPreferenceUser( toDtoInPreferenceUserId( s.getInPreferenceUser() ) );
        userRolePermissionDTO.setRoleMaster( toDtoRoleMasterId( s.getRoleMaster() ) );
        userRolePermissionDTO.setPermissionMaster( toDtoPermissionMasterId( s.getPermissionMaster() ) );
        userRolePermissionDTO.setId( s.getId() );

        return userRolePermissionDTO;
    }

    @Override
    public InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser) {
        if ( inPreferenceUser == null ) {
            return null;
        }

        InPreferenceUserDTO inPreferenceUserDTO = new InPreferenceUserDTO();

        inPreferenceUserDTO.setId( inPreferenceUser.getId() );

        return inPreferenceUserDTO;
    }

    @Override
    public RoleMasterDTO toDtoRoleMasterId(RoleMaster roleMaster) {
        if ( roleMaster == null ) {
            return null;
        }

        RoleMasterDTO roleMasterDTO = new RoleMasterDTO();

        roleMasterDTO.setId( roleMaster.getId() );

        return roleMasterDTO;
    }

    @Override
    public PermissionMasterDTO toDtoPermissionMasterId(PermissionMaster permissionMaster) {
        if ( permissionMaster == null ) {
            return null;
        }

        PermissionMasterDTO permissionMasterDTO = new PermissionMasterDTO();

        permissionMasterDTO.setId( permissionMaster.getId() );

        return permissionMasterDTO;
    }

    protected InPreferenceUser inPreferenceUserDTOToInPreferenceUser(InPreferenceUserDTO inPreferenceUserDTO) {
        if ( inPreferenceUserDTO == null ) {
            return null;
        }

        InPreferenceUser inPreferenceUser = new InPreferenceUser();

        inPreferenceUser.setId( inPreferenceUserDTO.getId() );
        inPreferenceUser.setFirstName( inPreferenceUserDTO.getFirstName() );
        inPreferenceUser.setLastName( inPreferenceUserDTO.getLastName() );
        inPreferenceUser.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        inPreferenceUser.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        inPreferenceUser.setUserType( inPreferenceUserDTO.getUserType() );
        inPreferenceUser.setEmailId( inPreferenceUserDTO.getEmailId() );
        inPreferenceUser.setLoginId( inPreferenceUserDTO.getLoginId() );
        inPreferenceUser.setSecret( inPreferenceUserDTO.getSecret() );
        inPreferenceUser.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        inPreferenceUser.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        inPreferenceUser.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        inPreferenceUser.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        inPreferenceUser.setStatus( inPreferenceUserDTO.getStatus() );

        return inPreferenceUser;
    }

    protected RoleMaster roleMasterDTOToRoleMaster(RoleMasterDTO roleMasterDTO) {
        if ( roleMasterDTO == null ) {
            return null;
        }

        RoleMaster roleMaster = new RoleMaster();

        roleMaster.setId( roleMasterDTO.getId() );
        roleMaster.setRoleName( roleMasterDTO.getRoleName() );
        roleMaster.setDescription( roleMasterDTO.getDescription() );
        roleMaster.setCreatedDate( roleMasterDTO.getCreatedDate() );
        roleMaster.setCreatedBy( roleMasterDTO.getCreatedBy() );
        roleMaster.setUpdatedDate( roleMasterDTO.getUpdatedDate() );
        roleMaster.setUpdatedBy( roleMasterDTO.getUpdatedBy() );
        roleMaster.setStatus( roleMasterDTO.getStatus() );
        roleMaster.inPreferenceUser( inPreferenceUserDTOToInPreferenceUser( roleMasterDTO.getInPreferenceUser() ) );

        return roleMaster;
    }

    protected PermissionMaster permissionMasterDTOToPermissionMaster(PermissionMasterDTO permissionMasterDTO) {
        if ( permissionMasterDTO == null ) {
            return null;
        }

        PermissionMaster permissionMaster = new PermissionMaster();

        permissionMaster.setId( permissionMasterDTO.getId() );
        permissionMaster.setPageName( permissionMasterDTO.getPageName() );
        permissionMaster.setPageUrl( permissionMasterDTO.getPageUrl() );

        return permissionMaster;
    }

    protected void inPreferenceUserDTOToInPreferenceUser1(InPreferenceUserDTO inPreferenceUserDTO, InPreferenceUser mappingTarget) {
        if ( inPreferenceUserDTO == null ) {
            return;
        }

        if ( inPreferenceUserDTO.getId() != null ) {
            mappingTarget.setId( inPreferenceUserDTO.getId() );
        }
        if ( inPreferenceUserDTO.getFirstName() != null ) {
            mappingTarget.setFirstName( inPreferenceUserDTO.getFirstName() );
        }
        if ( inPreferenceUserDTO.getLastName() != null ) {
            mappingTarget.setLastName( inPreferenceUserDTO.getLastName() );
        }
        if ( inPreferenceUserDTO.getMobileNo() != null ) {
            mappingTarget.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        }
        if ( inPreferenceUserDTO.getProfileUrl() != null ) {
            mappingTarget.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        }
        if ( inPreferenceUserDTO.getUserType() != null ) {
            mappingTarget.setUserType( inPreferenceUserDTO.getUserType() );
        }
        if ( inPreferenceUserDTO.getEmailId() != null ) {
            mappingTarget.setEmailId( inPreferenceUserDTO.getEmailId() );
        }
        if ( inPreferenceUserDTO.getLoginId() != null ) {
            mappingTarget.setLoginId( inPreferenceUserDTO.getLoginId() );
        }
        if ( inPreferenceUserDTO.getSecret() != null ) {
            mappingTarget.setSecret( inPreferenceUserDTO.getSecret() );
        }
        if ( inPreferenceUserDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        }
        if ( inPreferenceUserDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        }
        if ( inPreferenceUserDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        }
        if ( inPreferenceUserDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        }
        if ( inPreferenceUserDTO.getStatus() != null ) {
            mappingTarget.setStatus( inPreferenceUserDTO.getStatus() );
        }
    }

    protected void roleMasterDTOToRoleMaster1(RoleMasterDTO roleMasterDTO, RoleMaster mappingTarget) {
        if ( roleMasterDTO == null ) {
            return;
        }

        if ( roleMasterDTO.getId() != null ) {
            mappingTarget.setId( roleMasterDTO.getId() );
        }
        if ( roleMasterDTO.getRoleName() != null ) {
            mappingTarget.setRoleName( roleMasterDTO.getRoleName() );
        }
        if ( roleMasterDTO.getDescription() != null ) {
            mappingTarget.setDescription( roleMasterDTO.getDescription() );
        }
        if ( roleMasterDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( roleMasterDTO.getCreatedDate() );
        }
        if ( roleMasterDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( roleMasterDTO.getCreatedBy() );
        }
        if ( roleMasterDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( roleMasterDTO.getUpdatedDate() );
        }
        if ( roleMasterDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( roleMasterDTO.getUpdatedBy() );
        }
        if ( roleMasterDTO.getStatus() != null ) {
            mappingTarget.setStatus( roleMasterDTO.getStatus() );
        }
        if ( roleMasterDTO.getInPreferenceUser() != null ) {
            if ( mappingTarget.getInPreferenceUser() == null ) {
                mappingTarget.inPreferenceUser( new InPreferenceUser() );
            }
            inPreferenceUserDTOToInPreferenceUser1( roleMasterDTO.getInPreferenceUser(), mappingTarget.getInPreferenceUser() );
        }
    }

    protected void permissionMasterDTOToPermissionMaster1(PermissionMasterDTO permissionMasterDTO, PermissionMaster mappingTarget) {
        if ( permissionMasterDTO == null ) {
            return;
        }

        if ( permissionMasterDTO.getId() != null ) {
            mappingTarget.setId( permissionMasterDTO.getId() );
        }
        if ( permissionMasterDTO.getPageName() != null ) {
            mappingTarget.setPageName( permissionMasterDTO.getPageName() );
        }
        if ( permissionMasterDTO.getPageUrl() != null ) {
            mappingTarget.setPageUrl( permissionMasterDTO.getPageUrl() );
        }
    }
}
