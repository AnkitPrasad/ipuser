package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.LoginCount;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.LoginCountDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class LoginCountMapperImpl implements LoginCountMapper {

    @Override
    public LoginCount toEntity(LoginCountDTO dto) {
        if ( dto == null ) {
            return null;
        }

        LoginCount loginCount = new LoginCount();

        loginCount.setId( dto.getId() );
        loginCount.setLogintime( dto.getLogintime() );
        loginCount.setLoginId( dto.getLoginId() );
        loginCount.inPreferenceUser( inPreferenceUserDTOToInPreferenceUser( dto.getInPreferenceUser() ) );

        return loginCount;
    }

    @Override
    public List<LoginCount> toEntity(List<LoginCountDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<LoginCount> list = new ArrayList<LoginCount>( dtoList.size() );
        for ( LoginCountDTO loginCountDTO : dtoList ) {
            list.add( toEntity( loginCountDTO ) );
        }

        return list;
    }

    @Override
    public List<LoginCountDTO> toDto(List<LoginCount> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<LoginCountDTO> list = new ArrayList<LoginCountDTO>( entityList.size() );
        for ( LoginCount loginCount : entityList ) {
            list.add( toDto( loginCount ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(LoginCount entity, LoginCountDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getLogintime() != null ) {
            entity.setLogintime( dto.getLogintime() );
        }
        if ( dto.getLoginId() != null ) {
            entity.setLoginId( dto.getLoginId() );
        }
        if ( dto.getInPreferenceUser() != null ) {
            if ( entity.getInPreferenceUser() == null ) {
                entity.inPreferenceUser( new InPreferenceUser() );
            }
            inPreferenceUserDTOToInPreferenceUser1( dto.getInPreferenceUser(), entity.getInPreferenceUser() );
        }
    }

    @Override
    public LoginCountDTO toDto(LoginCount s) {
        if ( s == null ) {
            return null;
        }

        LoginCountDTO loginCountDTO = new LoginCountDTO();

        loginCountDTO.setInPreferenceUser( toDtoInPreferenceUserId( s.getInPreferenceUser() ) );
        loginCountDTO.setId( s.getId() );
        loginCountDTO.setLogintime( s.getLogintime() );
        loginCountDTO.setLoginId( s.getLoginId() );

        return loginCountDTO;
    }

    @Override
    public InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser) {
        if ( inPreferenceUser == null ) {
            return null;
        }

        InPreferenceUserDTO inPreferenceUserDTO = new InPreferenceUserDTO();

        inPreferenceUserDTO.setId( inPreferenceUser.getId() );

        return inPreferenceUserDTO;
    }

    protected InPreferenceUser inPreferenceUserDTOToInPreferenceUser(InPreferenceUserDTO inPreferenceUserDTO) {
        if ( inPreferenceUserDTO == null ) {
            return null;
        }

        InPreferenceUser inPreferenceUser = new InPreferenceUser();

        inPreferenceUser.setId( inPreferenceUserDTO.getId() );
        inPreferenceUser.setFirstName( inPreferenceUserDTO.getFirstName() );
        inPreferenceUser.setLastName( inPreferenceUserDTO.getLastName() );
        inPreferenceUser.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        inPreferenceUser.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        inPreferenceUser.setUserType( inPreferenceUserDTO.getUserType() );
        inPreferenceUser.setEmailId( inPreferenceUserDTO.getEmailId() );
        inPreferenceUser.setLoginId( inPreferenceUserDTO.getLoginId() );
        inPreferenceUser.setSecret( inPreferenceUserDTO.getSecret() );
        inPreferenceUser.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        inPreferenceUser.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        inPreferenceUser.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        inPreferenceUser.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        inPreferenceUser.setStatus( inPreferenceUserDTO.getStatus() );

        return inPreferenceUser;
    }

    protected void inPreferenceUserDTOToInPreferenceUser1(InPreferenceUserDTO inPreferenceUserDTO, InPreferenceUser mappingTarget) {
        if ( inPreferenceUserDTO == null ) {
            return;
        }

        if ( inPreferenceUserDTO.getId() != null ) {
            mappingTarget.setId( inPreferenceUserDTO.getId() );
        }
        if ( inPreferenceUserDTO.getFirstName() != null ) {
            mappingTarget.setFirstName( inPreferenceUserDTO.getFirstName() );
        }
        if ( inPreferenceUserDTO.getLastName() != null ) {
            mappingTarget.setLastName( inPreferenceUserDTO.getLastName() );
        }
        if ( inPreferenceUserDTO.getMobileNo() != null ) {
            mappingTarget.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        }
        if ( inPreferenceUserDTO.getProfileUrl() != null ) {
            mappingTarget.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        }
        if ( inPreferenceUserDTO.getUserType() != null ) {
            mappingTarget.setUserType( inPreferenceUserDTO.getUserType() );
        }
        if ( inPreferenceUserDTO.getEmailId() != null ) {
            mappingTarget.setEmailId( inPreferenceUserDTO.getEmailId() );
        }
        if ( inPreferenceUserDTO.getLoginId() != null ) {
            mappingTarget.setLoginId( inPreferenceUserDTO.getLoginId() );
        }
        if ( inPreferenceUserDTO.getSecret() != null ) {
            mappingTarget.setSecret( inPreferenceUserDTO.getSecret() );
        }
        if ( inPreferenceUserDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        }
        if ( inPreferenceUserDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        }
        if ( inPreferenceUserDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        }
        if ( inPreferenceUserDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        }
        if ( inPreferenceUserDTO.getStatus() != null ) {
            mappingTarget.setStatus( inPreferenceUserDTO.getStatus() );
        }
    }
}
