package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.RoleMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class RoleMasterMapperImpl implements RoleMasterMapper {

    @Override
    public RoleMaster toEntity(RoleMasterDTO dto) {
        if ( dto == null ) {
            return null;
        }

        RoleMaster roleMaster = new RoleMaster();

        roleMaster.setId( dto.getId() );
        roleMaster.setRoleName( dto.getRoleName() );
        roleMaster.setDescription( dto.getDescription() );
        roleMaster.setCreatedDate( dto.getCreatedDate() );
        roleMaster.setCreatedBy( dto.getCreatedBy() );
        roleMaster.setUpdatedDate( dto.getUpdatedDate() );
        roleMaster.setUpdatedBy( dto.getUpdatedBy() );
        roleMaster.setStatus( dto.getStatus() );
        roleMaster.inPreferenceUser( inPreferenceUserDTOToInPreferenceUser( dto.getInPreferenceUser() ) );

        return roleMaster;
    }

    @Override
    public List<RoleMaster> toEntity(List<RoleMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<RoleMaster> list = new ArrayList<RoleMaster>( dtoList.size() );
        for ( RoleMasterDTO roleMasterDTO : dtoList ) {
            list.add( toEntity( roleMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<RoleMasterDTO> toDto(List<RoleMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<RoleMasterDTO> list = new ArrayList<RoleMasterDTO>( entityList.size() );
        for ( RoleMaster roleMaster : entityList ) {
            list.add( toDto( roleMaster ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(RoleMaster entity, RoleMasterDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getRoleName() != null ) {
            entity.setRoleName( dto.getRoleName() );
        }
        if ( dto.getDescription() != null ) {
            entity.setDescription( dto.getDescription() );
        }
        if ( dto.getCreatedDate() != null ) {
            entity.setCreatedDate( dto.getCreatedDate() );
        }
        if ( dto.getCreatedBy() != null ) {
            entity.setCreatedBy( dto.getCreatedBy() );
        }
        if ( dto.getUpdatedDate() != null ) {
            entity.setUpdatedDate( dto.getUpdatedDate() );
        }
        if ( dto.getUpdatedBy() != null ) {
            entity.setUpdatedBy( dto.getUpdatedBy() );
        }
        if ( dto.getStatus() != null ) {
            entity.setStatus( dto.getStatus() );
        }
        if ( dto.getInPreferenceUser() != null ) {
            if ( entity.getInPreferenceUser() == null ) {
                entity.inPreferenceUser( new InPreferenceUser() );
            }
            inPreferenceUserDTOToInPreferenceUser1( dto.getInPreferenceUser(), entity.getInPreferenceUser() );
        }
    }

    @Override
    public RoleMasterDTO toDto(RoleMaster s) {
        if ( s == null ) {
            return null;
        }

        RoleMasterDTO roleMasterDTO = new RoleMasterDTO();

        roleMasterDTO.setInPreferenceUser( toDtoInPreferenceUserId( s.getInPreferenceUser() ) );
        roleMasterDTO.setId( s.getId() );
        roleMasterDTO.setRoleName( s.getRoleName() );
        roleMasterDTO.setDescription( s.getDescription() );
        roleMasterDTO.setCreatedDate( s.getCreatedDate() );
        roleMasterDTO.setCreatedBy( s.getCreatedBy() );
        roleMasterDTO.setUpdatedDate( s.getUpdatedDate() );
        roleMasterDTO.setUpdatedBy( s.getUpdatedBy() );
        roleMasterDTO.setStatus( s.getStatus() );

        return roleMasterDTO;
    }

    @Override
    public InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser) {
        if ( inPreferenceUser == null ) {
            return null;
        }

        InPreferenceUserDTO inPreferenceUserDTO = new InPreferenceUserDTO();

        inPreferenceUserDTO.setId( inPreferenceUser.getId() );

        return inPreferenceUserDTO;
    }

    protected InPreferenceUser inPreferenceUserDTOToInPreferenceUser(InPreferenceUserDTO inPreferenceUserDTO) {
        if ( inPreferenceUserDTO == null ) {
            return null;
        }

        InPreferenceUser inPreferenceUser = new InPreferenceUser();

        inPreferenceUser.setId( inPreferenceUserDTO.getId() );
        inPreferenceUser.setFirstName( inPreferenceUserDTO.getFirstName() );
        inPreferenceUser.setLastName( inPreferenceUserDTO.getLastName() );
        inPreferenceUser.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        inPreferenceUser.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        inPreferenceUser.setUserType( inPreferenceUserDTO.getUserType() );
        inPreferenceUser.setEmailId( inPreferenceUserDTO.getEmailId() );
        inPreferenceUser.setLoginId( inPreferenceUserDTO.getLoginId() );
        inPreferenceUser.setSecret( inPreferenceUserDTO.getSecret() );
        inPreferenceUser.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        inPreferenceUser.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        inPreferenceUser.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        inPreferenceUser.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        inPreferenceUser.setStatus( inPreferenceUserDTO.getStatus() );

        return inPreferenceUser;
    }

    protected void inPreferenceUserDTOToInPreferenceUser1(InPreferenceUserDTO inPreferenceUserDTO, InPreferenceUser mappingTarget) {
        if ( inPreferenceUserDTO == null ) {
            return;
        }

        if ( inPreferenceUserDTO.getId() != null ) {
            mappingTarget.setId( inPreferenceUserDTO.getId() );
        }
        if ( inPreferenceUserDTO.getFirstName() != null ) {
            mappingTarget.setFirstName( inPreferenceUserDTO.getFirstName() );
        }
        if ( inPreferenceUserDTO.getLastName() != null ) {
            mappingTarget.setLastName( inPreferenceUserDTO.getLastName() );
        }
        if ( inPreferenceUserDTO.getMobileNo() != null ) {
            mappingTarget.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        }
        if ( inPreferenceUserDTO.getProfileUrl() != null ) {
            mappingTarget.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        }
        if ( inPreferenceUserDTO.getUserType() != null ) {
            mappingTarget.setUserType( inPreferenceUserDTO.getUserType() );
        }
        if ( inPreferenceUserDTO.getEmailId() != null ) {
            mappingTarget.setEmailId( inPreferenceUserDTO.getEmailId() );
        }
        if ( inPreferenceUserDTO.getLoginId() != null ) {
            mappingTarget.setLoginId( inPreferenceUserDTO.getLoginId() );
        }
        if ( inPreferenceUserDTO.getSecret() != null ) {
            mappingTarget.setSecret( inPreferenceUserDTO.getSecret() );
        }
        if ( inPreferenceUserDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        }
        if ( inPreferenceUserDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        }
        if ( inPreferenceUserDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        }
        if ( inPreferenceUserDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        }
        if ( inPreferenceUserDTO.getStatus() != null ) {
            mappingTarget.setStatus( inPreferenceUserDTO.getStatus() );
        }
    }
}
