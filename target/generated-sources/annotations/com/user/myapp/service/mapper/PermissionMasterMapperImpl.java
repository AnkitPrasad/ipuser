package com.user.myapp.service.mapper;

import com.user.myapp.domain.PermissionMaster;
import com.user.myapp.service.dto.PermissionMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class PermissionMasterMapperImpl implements PermissionMasterMapper {

    @Override
    public PermissionMaster toEntity(PermissionMasterDTO dto) {
        if ( dto == null ) {
            return null;
        }

        PermissionMaster permissionMaster = new PermissionMaster();

        permissionMaster.setId( dto.getId() );
        permissionMaster.setPageName( dto.getPageName() );
        permissionMaster.setPageUrl( dto.getPageUrl() );

        return permissionMaster;
    }

    @Override
    public PermissionMasterDTO toDto(PermissionMaster entity) {
        if ( entity == null ) {
            return null;
        }

        PermissionMasterDTO permissionMasterDTO = new PermissionMasterDTO();

        permissionMasterDTO.setId( entity.getId() );
        permissionMasterDTO.setPageName( entity.getPageName() );
        permissionMasterDTO.setPageUrl( entity.getPageUrl() );

        return permissionMasterDTO;
    }

    @Override
    public List<PermissionMaster> toEntity(List<PermissionMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<PermissionMaster> list = new ArrayList<PermissionMaster>( dtoList.size() );
        for ( PermissionMasterDTO permissionMasterDTO : dtoList ) {
            list.add( toEntity( permissionMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<PermissionMasterDTO> toDto(List<PermissionMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<PermissionMasterDTO> list = new ArrayList<PermissionMasterDTO>( entityList.size() );
        for ( PermissionMaster permissionMaster : entityList ) {
            list.add( toDto( permissionMaster ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(PermissionMaster entity, PermissionMasterDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getPageName() != null ) {
            entity.setPageName( dto.getPageName() );
        }
        if ( dto.getPageUrl() != null ) {
            entity.setPageUrl( dto.getPageUrl() );
        }
    }
}
