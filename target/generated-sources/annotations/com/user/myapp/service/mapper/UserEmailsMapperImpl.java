package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.UserEmails;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.UserEmailsDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class UserEmailsMapperImpl implements UserEmailsMapper {

    @Override
    public UserEmails toEntity(UserEmailsDTO dto) {
        if ( dto == null ) {
            return null;
        }

        UserEmails userEmails = new UserEmails();

        userEmails.setId( dto.getId() );
        userEmails.setEmail( dto.getEmail() );
        userEmails.setLoginId( dto.getLoginId() );
        userEmails.inPreferenceUser( inPreferenceUserDTOToInPreferenceUser( dto.getInPreferenceUser() ) );

        return userEmails;
    }

    @Override
    public List<UserEmails> toEntity(List<UserEmailsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<UserEmails> list = new ArrayList<UserEmails>( dtoList.size() );
        for ( UserEmailsDTO userEmailsDTO : dtoList ) {
            list.add( toEntity( userEmailsDTO ) );
        }

        return list;
    }

    @Override
    public List<UserEmailsDTO> toDto(List<UserEmails> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserEmailsDTO> list = new ArrayList<UserEmailsDTO>( entityList.size() );
        for ( UserEmails userEmails : entityList ) {
            list.add( toDto( userEmails ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(UserEmails entity, UserEmailsDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getEmail() != null ) {
            entity.setEmail( dto.getEmail() );
        }
        if ( dto.getLoginId() != null ) {
            entity.setLoginId( dto.getLoginId() );
        }
        if ( dto.getInPreferenceUser() != null ) {
            if ( entity.getInPreferenceUser() == null ) {
                entity.inPreferenceUser( new InPreferenceUser() );
            }
            inPreferenceUserDTOToInPreferenceUser1( dto.getInPreferenceUser(), entity.getInPreferenceUser() );
        }
    }

    @Override
    public UserEmailsDTO toDto(UserEmails s) {
        if ( s == null ) {
            return null;
        }

        UserEmailsDTO userEmailsDTO = new UserEmailsDTO();

        userEmailsDTO.setInPreferenceUser( toDtoInPreferenceUserId( s.getInPreferenceUser() ) );
        userEmailsDTO.setId( s.getId() );
        userEmailsDTO.setEmail( s.getEmail() );
        userEmailsDTO.setLoginId( s.getLoginId() );

        return userEmailsDTO;
    }

    @Override
    public InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser) {
        if ( inPreferenceUser == null ) {
            return null;
        }

        InPreferenceUserDTO inPreferenceUserDTO = new InPreferenceUserDTO();

        inPreferenceUserDTO.setId( inPreferenceUser.getId() );

        return inPreferenceUserDTO;
    }

    protected InPreferenceUser inPreferenceUserDTOToInPreferenceUser(InPreferenceUserDTO inPreferenceUserDTO) {
        if ( inPreferenceUserDTO == null ) {
            return null;
        }

        InPreferenceUser inPreferenceUser = new InPreferenceUser();

        inPreferenceUser.setId( inPreferenceUserDTO.getId() );
        inPreferenceUser.setFirstName( inPreferenceUserDTO.getFirstName() );
        inPreferenceUser.setLastName( inPreferenceUserDTO.getLastName() );
        inPreferenceUser.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        inPreferenceUser.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        inPreferenceUser.setUserType( inPreferenceUserDTO.getUserType() );
        inPreferenceUser.setEmailId( inPreferenceUserDTO.getEmailId() );
        inPreferenceUser.setLoginId( inPreferenceUserDTO.getLoginId() );
        inPreferenceUser.setSecret( inPreferenceUserDTO.getSecret() );
        inPreferenceUser.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        inPreferenceUser.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        inPreferenceUser.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        inPreferenceUser.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        inPreferenceUser.setStatus( inPreferenceUserDTO.getStatus() );

        return inPreferenceUser;
    }

    protected void inPreferenceUserDTOToInPreferenceUser1(InPreferenceUserDTO inPreferenceUserDTO, InPreferenceUser mappingTarget) {
        if ( inPreferenceUserDTO == null ) {
            return;
        }

        if ( inPreferenceUserDTO.getId() != null ) {
            mappingTarget.setId( inPreferenceUserDTO.getId() );
        }
        if ( inPreferenceUserDTO.getFirstName() != null ) {
            mappingTarget.setFirstName( inPreferenceUserDTO.getFirstName() );
        }
        if ( inPreferenceUserDTO.getLastName() != null ) {
            mappingTarget.setLastName( inPreferenceUserDTO.getLastName() );
        }
        if ( inPreferenceUserDTO.getMobileNo() != null ) {
            mappingTarget.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        }
        if ( inPreferenceUserDTO.getProfileUrl() != null ) {
            mappingTarget.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        }
        if ( inPreferenceUserDTO.getUserType() != null ) {
            mappingTarget.setUserType( inPreferenceUserDTO.getUserType() );
        }
        if ( inPreferenceUserDTO.getEmailId() != null ) {
            mappingTarget.setEmailId( inPreferenceUserDTO.getEmailId() );
        }
        if ( inPreferenceUserDTO.getLoginId() != null ) {
            mappingTarget.setLoginId( inPreferenceUserDTO.getLoginId() );
        }
        if ( inPreferenceUserDTO.getSecret() != null ) {
            mappingTarget.setSecret( inPreferenceUserDTO.getSecret() );
        }
        if ( inPreferenceUserDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        }
        if ( inPreferenceUserDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        }
        if ( inPreferenceUserDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        }
        if ( inPreferenceUserDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        }
        if ( inPreferenceUserDTO.getStatus() != null ) {
            mappingTarget.setStatus( inPreferenceUserDTO.getStatus() );
        }
    }
}
