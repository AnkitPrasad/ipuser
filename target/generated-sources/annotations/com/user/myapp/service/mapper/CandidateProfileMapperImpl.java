package com.user.myapp.service.mapper;

import com.user.myapp.domain.Candidate;
import com.user.myapp.domain.CandidateDetails;
import com.user.myapp.domain.CandidateProfile;
import com.user.myapp.domain.Client;
import com.user.myapp.domain.ClientSetting;
import com.user.myapp.service.dto.CandidateDTO;
import com.user.myapp.service.dto.CandidateDetailsDTO;
import com.user.myapp.service.dto.CandidateProfileDTO;
import com.user.myapp.service.dto.ClientDTO;
import com.user.myapp.service.dto.ClientSettingDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:35:59+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class CandidateProfileMapperImpl implements CandidateProfileMapper {

    @Override
    public CandidateProfile toEntity(CandidateProfileDTO dto) {
        if ( dto == null ) {
            return null;
        }

        CandidateProfile candidateProfile = new CandidateProfile();

        candidateProfile.setId( dto.getId() );
        candidateProfile.setAboutMe( dto.getAboutMe() );
        candidateProfile.setCareerSummary( dto.getCareerSummary() );
        candidateProfile.setEducation( dto.getEducation() );
        candidateProfile.setDob( dto.getDob() );
        candidateProfile.setCurrentEmployment( dto.getCurrentEmployment() );
        candidateProfile.setCertifications( dto.getCertifications() );
        candidateProfile.setExperience( dto.getExperience() );
        candidateProfile.candidate( candidateDTOToCandidate( dto.getCandidate() ) );

        return candidateProfile;
    }

    @Override
    public List<CandidateProfile> toEntity(List<CandidateProfileDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<CandidateProfile> list = new ArrayList<CandidateProfile>( dtoList.size() );
        for ( CandidateProfileDTO candidateProfileDTO : dtoList ) {
            list.add( toEntity( candidateProfileDTO ) );
        }

        return list;
    }

    @Override
    public List<CandidateProfileDTO> toDto(List<CandidateProfile> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CandidateProfileDTO> list = new ArrayList<CandidateProfileDTO>( entityList.size() );
        for ( CandidateProfile candidateProfile : entityList ) {
            list.add( toDto( candidateProfile ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(CandidateProfile entity, CandidateProfileDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getAboutMe() != null ) {
            entity.setAboutMe( dto.getAboutMe() );
        }
        if ( dto.getCareerSummary() != null ) {
            entity.setCareerSummary( dto.getCareerSummary() );
        }
        if ( dto.getEducation() != null ) {
            entity.setEducation( dto.getEducation() );
        }
        if ( dto.getDob() != null ) {
            entity.setDob( dto.getDob() );
        }
        if ( dto.getCurrentEmployment() != null ) {
            entity.setCurrentEmployment( dto.getCurrentEmployment() );
        }
        if ( dto.getCertifications() != null ) {
            entity.setCertifications( dto.getCertifications() );
        }
        if ( dto.getExperience() != null ) {
            entity.setExperience( dto.getExperience() );
        }
        if ( dto.getCandidate() != null ) {
            if ( entity.getCandidate() == null ) {
                entity.candidate( new Candidate() );
            }
            candidateDTOToCandidate1( dto.getCandidate(), entity.getCandidate() );
        }
    }

    @Override
    public CandidateProfileDTO toDto(CandidateProfile s) {
        if ( s == null ) {
            return null;
        }

        CandidateProfileDTO candidateProfileDTO = new CandidateProfileDTO();

        candidateProfileDTO.setCandidate( toDtoCandidateId( s.getCandidate() ) );
        candidateProfileDTO.setId( s.getId() );
        candidateProfileDTO.setAboutMe( s.getAboutMe() );
        candidateProfileDTO.setCareerSummary( s.getCareerSummary() );
        candidateProfileDTO.setEducation( s.getEducation() );
        candidateProfileDTO.setDob( s.getDob() );
        candidateProfileDTO.setCurrentEmployment( s.getCurrentEmployment() );
        candidateProfileDTO.setCertifications( s.getCertifications() );
        candidateProfileDTO.setExperience( s.getExperience() );

        return candidateProfileDTO;
    }

    @Override
    public CandidateDTO toDtoCandidateId(Candidate candidate) {
        if ( candidate == null ) {
            return null;
        }

        CandidateDTO candidateDTO = new CandidateDTO();

        candidateDTO.setId( candidate.getId() );

        return candidateDTO;
    }

    protected CandidateDetails candidateDetailsDTOToCandidateDetails(CandidateDetailsDTO candidateDetailsDTO) {
        if ( candidateDetailsDTO == null ) {
            return null;
        }

        CandidateDetails candidateDetails = new CandidateDetails();

        candidateDetails.setId( candidateDetailsDTO.getId() );
        candidateDetails.setCandidateCountry( candidateDetailsDTO.getCandidateCountry() );
        candidateDetails.setBusinessUnits( candidateDetailsDTO.getBusinessUnits() );
        candidateDetails.setLegalEntity( candidateDetailsDTO.getLegalEntity() );
        candidateDetails.setCandidateSubFunction( candidateDetailsDTO.getCandidateSubFunction() );
        candidateDetails.setDesignation( candidateDetailsDTO.getDesignation() );
        candidateDetails.setJobTitle( candidateDetailsDTO.getJobTitle() );
        candidateDetails.setLocation( candidateDetailsDTO.getLocation() );
        candidateDetails.setSubBusinessUnits( candidateDetailsDTO.getSubBusinessUnits() );
        candidateDetails.setCandidateFunction( candidateDetailsDTO.getCandidateFunction() );
        candidateDetails.setWorkLevel( candidateDetailsDTO.getWorkLevel() );
        candidateDetails.setCandidateGrade( candidateDetailsDTO.getCandidateGrade() );

        return candidateDetails;
    }

    protected ClientSetting clientSettingDTOToClientSetting(ClientSettingDTO clientSettingDTO) {
        if ( clientSettingDTO == null ) {
            return null;
        }

        ClientSetting clientSetting = new ClientSetting();

        clientSetting.setId( clientSettingDTO.getId() );
        clientSetting.setCandidateSelfAssessment( clientSettingDTO.getCandidateSelfAssessment() );
        clientSetting.setAutoReninderAssessment( clientSettingDTO.getAutoReninderAssessment() );
        clientSetting.setAllowCandidatePaidVersion( clientSettingDTO.getAllowCandidatePaidVersion() );
        clientSetting.setAllowEmployessPaidVersion( clientSettingDTO.getAllowEmployessPaidVersion() );
        clientSetting.setAutoArchieveRequisitions( clientSettingDTO.getAutoArchieveRequisitions() );
        clientSetting.setCompanyThemeDarkColour( clientSettingDTO.getCompanyThemeDarkColour() );
        clientSetting.setCompanyThemeLightColour( clientSettingDTO.getCompanyThemeLightColour() );
        clientSetting.setCompanyUrl( clientSettingDTO.getCompanyUrl() );
        clientSetting.setCompanyMenu( clientSettingDTO.getCompanyMenu() );

        return clientSetting;
    }

    protected Client clientDTOToClient(ClientDTO clientDTO) {
        if ( clientDTO == null ) {
            return null;
        }

        Client client = new Client();

        client.setId( clientDTO.getId() );
        client.setLegalEntity( clientDTO.getLegalEntity() );
        client.setIndustry( clientDTO.getIndustry() );
        client.setBankDetails( clientDTO.getBankDetails() );
        client.setClientType( clientDTO.getClientType() );
        client.setTnaNo( clientDTO.getTnaNo() );
        client.setGstNo( clientDTO.getGstNo() );
        client.setPan( clientDTO.getPan() );
        client.setUploadClientLogo( clientDTO.getUploadClientLogo() );
        client.setCreatedDate( clientDTO.getCreatedDate() );
        client.setCreatedBy( clientDTO.getCreatedBy() );
        client.setUpdatedDate( clientDTO.getUpdatedDate() );
        client.setUpdatedBy( clientDTO.getUpdatedBy() );
        client.setStatus( clientDTO.getStatus() );
        client.clientSetting( clientSettingDTOToClientSetting( clientDTO.getClientSetting() ) );

        return client;
    }

    protected Candidate candidateDTOToCandidate(CandidateDTO candidateDTO) {
        if ( candidateDTO == null ) {
            return null;
        }

        Candidate candidate = new Candidate();

        candidate.setId( candidateDTO.getId() );
        candidate.setIsInternalCandidate( candidateDTO.getIsInternalCandidate() );
        candidate.setCompany( candidateDTO.getCompany() );
        candidate.setCreatedDate( candidateDTO.getCreatedDate() );
        candidate.setCreatedBy( candidateDTO.getCreatedBy() );
        candidate.setUpdatedDate( candidateDTO.getUpdatedDate() );
        candidate.setUpdatedBy( candidateDTO.getUpdatedBy() );
        candidate.setAssessmentStatus( candidateDTO.getAssessmentStatus() );
        candidate.setStatus( candidateDTO.getStatus() );
        candidate.setArchiveStatus( candidateDTO.getArchiveStatus() );
        candidate.candidateDetails( candidateDetailsDTOToCandidateDetails( candidateDTO.getCandidateDetails() ) );
        candidate.client( clientDTOToClient( candidateDTO.getClient() ) );

        return candidate;
    }

    protected void candidateDetailsDTOToCandidateDetails1(CandidateDetailsDTO candidateDetailsDTO, CandidateDetails mappingTarget) {
        if ( candidateDetailsDTO == null ) {
            return;
        }

        if ( candidateDetailsDTO.getId() != null ) {
            mappingTarget.setId( candidateDetailsDTO.getId() );
        }
        if ( candidateDetailsDTO.getCandidateCountry() != null ) {
            mappingTarget.setCandidateCountry( candidateDetailsDTO.getCandidateCountry() );
        }
        if ( candidateDetailsDTO.getBusinessUnits() != null ) {
            mappingTarget.setBusinessUnits( candidateDetailsDTO.getBusinessUnits() );
        }
        if ( candidateDetailsDTO.getLegalEntity() != null ) {
            mappingTarget.setLegalEntity( candidateDetailsDTO.getLegalEntity() );
        }
        if ( candidateDetailsDTO.getCandidateSubFunction() != null ) {
            mappingTarget.setCandidateSubFunction( candidateDetailsDTO.getCandidateSubFunction() );
        }
        if ( candidateDetailsDTO.getDesignation() != null ) {
            mappingTarget.setDesignation( candidateDetailsDTO.getDesignation() );
        }
        if ( candidateDetailsDTO.getJobTitle() != null ) {
            mappingTarget.setJobTitle( candidateDetailsDTO.getJobTitle() );
        }
        if ( candidateDetailsDTO.getLocation() != null ) {
            mappingTarget.setLocation( candidateDetailsDTO.getLocation() );
        }
        if ( candidateDetailsDTO.getSubBusinessUnits() != null ) {
            mappingTarget.setSubBusinessUnits( candidateDetailsDTO.getSubBusinessUnits() );
        }
        if ( candidateDetailsDTO.getCandidateFunction() != null ) {
            mappingTarget.setCandidateFunction( candidateDetailsDTO.getCandidateFunction() );
        }
        if ( candidateDetailsDTO.getWorkLevel() != null ) {
            mappingTarget.setWorkLevel( candidateDetailsDTO.getWorkLevel() );
        }
        if ( candidateDetailsDTO.getCandidateGrade() != null ) {
            mappingTarget.setCandidateGrade( candidateDetailsDTO.getCandidateGrade() );
        }
    }

    protected void clientSettingDTOToClientSetting1(ClientSettingDTO clientSettingDTO, ClientSetting mappingTarget) {
        if ( clientSettingDTO == null ) {
            return;
        }

        if ( clientSettingDTO.getId() != null ) {
            mappingTarget.setId( clientSettingDTO.getId() );
        }
        if ( clientSettingDTO.getCandidateSelfAssessment() != null ) {
            mappingTarget.setCandidateSelfAssessment( clientSettingDTO.getCandidateSelfAssessment() );
        }
        if ( clientSettingDTO.getAutoReninderAssessment() != null ) {
            mappingTarget.setAutoReninderAssessment( clientSettingDTO.getAutoReninderAssessment() );
        }
        if ( clientSettingDTO.getAllowCandidatePaidVersion() != null ) {
            mappingTarget.setAllowCandidatePaidVersion( clientSettingDTO.getAllowCandidatePaidVersion() );
        }
        if ( clientSettingDTO.getAllowEmployessPaidVersion() != null ) {
            mappingTarget.setAllowEmployessPaidVersion( clientSettingDTO.getAllowEmployessPaidVersion() );
        }
        if ( clientSettingDTO.getAutoArchieveRequisitions() != null ) {
            mappingTarget.setAutoArchieveRequisitions( clientSettingDTO.getAutoArchieveRequisitions() );
        }
        if ( clientSettingDTO.getCompanyThemeDarkColour() != null ) {
            mappingTarget.setCompanyThemeDarkColour( clientSettingDTO.getCompanyThemeDarkColour() );
        }
        if ( clientSettingDTO.getCompanyThemeLightColour() != null ) {
            mappingTarget.setCompanyThemeLightColour( clientSettingDTO.getCompanyThemeLightColour() );
        }
        if ( clientSettingDTO.getCompanyUrl() != null ) {
            mappingTarget.setCompanyUrl( clientSettingDTO.getCompanyUrl() );
        }
        if ( clientSettingDTO.getCompanyMenu() != null ) {
            mappingTarget.setCompanyMenu( clientSettingDTO.getCompanyMenu() );
        }
    }

    protected void clientDTOToClient1(ClientDTO clientDTO, Client mappingTarget) {
        if ( clientDTO == null ) {
            return;
        }

        if ( clientDTO.getId() != null ) {
            mappingTarget.setId( clientDTO.getId() );
        }
        if ( clientDTO.getLegalEntity() != null ) {
            mappingTarget.setLegalEntity( clientDTO.getLegalEntity() );
        }
        if ( clientDTO.getIndustry() != null ) {
            mappingTarget.setIndustry( clientDTO.getIndustry() );
        }
        if ( clientDTO.getBankDetails() != null ) {
            mappingTarget.setBankDetails( clientDTO.getBankDetails() );
        }
        if ( clientDTO.getClientType() != null ) {
            mappingTarget.setClientType( clientDTO.getClientType() );
        }
        if ( clientDTO.getTnaNo() != null ) {
            mappingTarget.setTnaNo( clientDTO.getTnaNo() );
        }
        if ( clientDTO.getGstNo() != null ) {
            mappingTarget.setGstNo( clientDTO.getGstNo() );
        }
        if ( clientDTO.getPan() != null ) {
            mappingTarget.setPan( clientDTO.getPan() );
        }
        if ( clientDTO.getUploadClientLogo() != null ) {
            mappingTarget.setUploadClientLogo( clientDTO.getUploadClientLogo() );
        }
        if ( clientDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( clientDTO.getCreatedDate() );
        }
        if ( clientDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( clientDTO.getCreatedBy() );
        }
        if ( clientDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( clientDTO.getUpdatedDate() );
        }
        if ( clientDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( clientDTO.getUpdatedBy() );
        }
        if ( clientDTO.getStatus() != null ) {
            mappingTarget.setStatus( clientDTO.getStatus() );
        }
        if ( clientDTO.getClientSetting() != null ) {
            if ( mappingTarget.getClientSetting() == null ) {
                mappingTarget.clientSetting( new ClientSetting() );
            }
            clientSettingDTOToClientSetting1( clientDTO.getClientSetting(), mappingTarget.getClientSetting() );
        }
    }

    protected void candidateDTOToCandidate1(CandidateDTO candidateDTO, Candidate mappingTarget) {
        if ( candidateDTO == null ) {
            return;
        }

        if ( candidateDTO.getId() != null ) {
            mappingTarget.setId( candidateDTO.getId() );
        }
        if ( candidateDTO.getIsInternalCandidate() != null ) {
            mappingTarget.setIsInternalCandidate( candidateDTO.getIsInternalCandidate() );
        }
        if ( candidateDTO.getCompany() != null ) {
            mappingTarget.setCompany( candidateDTO.getCompany() );
        }
        if ( candidateDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( candidateDTO.getCreatedDate() );
        }
        if ( candidateDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( candidateDTO.getCreatedBy() );
        }
        if ( candidateDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( candidateDTO.getUpdatedDate() );
        }
        if ( candidateDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( candidateDTO.getUpdatedBy() );
        }
        if ( candidateDTO.getAssessmentStatus() != null ) {
            mappingTarget.setAssessmentStatus( candidateDTO.getAssessmentStatus() );
        }
        if ( candidateDTO.getStatus() != null ) {
            mappingTarget.setStatus( candidateDTO.getStatus() );
        }
        if ( candidateDTO.getArchiveStatus() != null ) {
            mappingTarget.setArchiveStatus( candidateDTO.getArchiveStatus() );
        }
        if ( candidateDTO.getCandidateDetails() != null ) {
            if ( mappingTarget.getCandidateDetails() == null ) {
                mappingTarget.candidateDetails( new CandidateDetails() );
            }
            candidateDetailsDTOToCandidateDetails1( candidateDTO.getCandidateDetails(), mappingTarget.getCandidateDetails() );
        }
        if ( candidateDTO.getClient() != null ) {
            if ( mappingTarget.getClient() == null ) {
                mappingTarget.client( new Client() );
            }
            clientDTOToClient1( candidateDTO.getClient(), mappingTarget.getClient() );
        }
    }
}
