package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.PermissionsOnAttributeMaster;
import com.user.myapp.domain.RoleMaster;
import com.user.myapp.domain.UserPermissionOnAttribute;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.PermissionsOnAttributeMasterDTO;
import com.user.myapp.service.dto.RoleMasterDTO;
import com.user.myapp.service.dto.UserPermissionOnAttributeDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class UserPermissionOnAttributeMapperImpl implements UserPermissionOnAttributeMapper {

    @Override
    public UserPermissionOnAttribute toEntity(UserPermissionOnAttributeDTO dto) {
        if ( dto == null ) {
            return null;
        }

        UserPermissionOnAttribute userPermissionOnAttribute = new UserPermissionOnAttribute();

        userPermissionOnAttribute.setId( dto.getId() );
        userPermissionOnAttribute.setAttributeValue( dto.getAttributeValue() );
        userPermissionOnAttribute.roleMaster( roleMasterDTOToRoleMaster( dto.getRoleMaster() ) );
        userPermissionOnAttribute.permissionsOnAttributeMaster( permissionsOnAttributeMasterDTOToPermissionsOnAttributeMaster( dto.getPermissionsOnAttributeMaster() ) );

        return userPermissionOnAttribute;
    }

    @Override
    public List<UserPermissionOnAttribute> toEntity(List<UserPermissionOnAttributeDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<UserPermissionOnAttribute> list = new ArrayList<UserPermissionOnAttribute>( dtoList.size() );
        for ( UserPermissionOnAttributeDTO userPermissionOnAttributeDTO : dtoList ) {
            list.add( toEntity( userPermissionOnAttributeDTO ) );
        }

        return list;
    }

    @Override
    public List<UserPermissionOnAttributeDTO> toDto(List<UserPermissionOnAttribute> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserPermissionOnAttributeDTO> list = new ArrayList<UserPermissionOnAttributeDTO>( entityList.size() );
        for ( UserPermissionOnAttribute userPermissionOnAttribute : entityList ) {
            list.add( toDto( userPermissionOnAttribute ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(UserPermissionOnAttribute entity, UserPermissionOnAttributeDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getAttributeValue() != null ) {
            entity.setAttributeValue( dto.getAttributeValue() );
        }
        if ( dto.getRoleMaster() != null ) {
            if ( entity.getRoleMaster() == null ) {
                entity.roleMaster( new RoleMaster() );
            }
            roleMasterDTOToRoleMaster1( dto.getRoleMaster(), entity.getRoleMaster() );
        }
        if ( dto.getPermissionsOnAttributeMaster() != null ) {
            if ( entity.getPermissionsOnAttributeMaster() == null ) {
                entity.permissionsOnAttributeMaster( new PermissionsOnAttributeMaster() );
            }
            permissionsOnAttributeMasterDTOToPermissionsOnAttributeMaster1( dto.getPermissionsOnAttributeMaster(), entity.getPermissionsOnAttributeMaster() );
        }
    }

    @Override
    public UserPermissionOnAttributeDTO toDto(UserPermissionOnAttribute s) {
        if ( s == null ) {
            return null;
        }

        UserPermissionOnAttributeDTO userPermissionOnAttributeDTO = new UserPermissionOnAttributeDTO();

        userPermissionOnAttributeDTO.setRoleMaster( toDtoRoleMasterId( s.getRoleMaster() ) );
        userPermissionOnAttributeDTO.setPermissionsOnAttributeMaster( toDtoPermissionsOnAttributeMasterId( s.getPermissionsOnAttributeMaster() ) );
        userPermissionOnAttributeDTO.setId( s.getId() );
        userPermissionOnAttributeDTO.setAttributeValue( s.getAttributeValue() );

        return userPermissionOnAttributeDTO;
    }

    @Override
    public RoleMasterDTO toDtoRoleMasterId(RoleMaster roleMaster) {
        if ( roleMaster == null ) {
            return null;
        }

        RoleMasterDTO roleMasterDTO = new RoleMasterDTO();

        roleMasterDTO.setId( roleMaster.getId() );

        return roleMasterDTO;
    }

    @Override
    public PermissionsOnAttributeMasterDTO toDtoPermissionsOnAttributeMasterId(PermissionsOnAttributeMaster permissionsOnAttributeMaster) {
        if ( permissionsOnAttributeMaster == null ) {
            return null;
        }

        PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO = new PermissionsOnAttributeMasterDTO();

        permissionsOnAttributeMasterDTO.setId( permissionsOnAttributeMaster.getId() );

        return permissionsOnAttributeMasterDTO;
    }

    protected InPreferenceUser inPreferenceUserDTOToInPreferenceUser(InPreferenceUserDTO inPreferenceUserDTO) {
        if ( inPreferenceUserDTO == null ) {
            return null;
        }

        InPreferenceUser inPreferenceUser = new InPreferenceUser();

        inPreferenceUser.setId( inPreferenceUserDTO.getId() );
        inPreferenceUser.setFirstName( inPreferenceUserDTO.getFirstName() );
        inPreferenceUser.setLastName( inPreferenceUserDTO.getLastName() );
        inPreferenceUser.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        inPreferenceUser.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        inPreferenceUser.setUserType( inPreferenceUserDTO.getUserType() );
        inPreferenceUser.setEmailId( inPreferenceUserDTO.getEmailId() );
        inPreferenceUser.setLoginId( inPreferenceUserDTO.getLoginId() );
        inPreferenceUser.setSecret( inPreferenceUserDTO.getSecret() );
        inPreferenceUser.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        inPreferenceUser.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        inPreferenceUser.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        inPreferenceUser.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        inPreferenceUser.setStatus( inPreferenceUserDTO.getStatus() );

        return inPreferenceUser;
    }

    protected RoleMaster roleMasterDTOToRoleMaster(RoleMasterDTO roleMasterDTO) {
        if ( roleMasterDTO == null ) {
            return null;
        }

        RoleMaster roleMaster = new RoleMaster();

        roleMaster.setId( roleMasterDTO.getId() );
        roleMaster.setRoleName( roleMasterDTO.getRoleName() );
        roleMaster.setDescription( roleMasterDTO.getDescription() );
        roleMaster.setCreatedDate( roleMasterDTO.getCreatedDate() );
        roleMaster.setCreatedBy( roleMasterDTO.getCreatedBy() );
        roleMaster.setUpdatedDate( roleMasterDTO.getUpdatedDate() );
        roleMaster.setUpdatedBy( roleMasterDTO.getUpdatedBy() );
        roleMaster.setStatus( roleMasterDTO.getStatus() );
        roleMaster.inPreferenceUser( inPreferenceUserDTOToInPreferenceUser( roleMasterDTO.getInPreferenceUser() ) );

        return roleMaster;
    }

    protected PermissionsOnAttributeMaster permissionsOnAttributeMasterDTOToPermissionsOnAttributeMaster(PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO) {
        if ( permissionsOnAttributeMasterDTO == null ) {
            return null;
        }

        PermissionsOnAttributeMaster permissionsOnAttributeMaster = new PermissionsOnAttributeMaster();

        permissionsOnAttributeMaster.setId( permissionsOnAttributeMasterDTO.getId() );
        permissionsOnAttributeMaster.setAttributeKey( permissionsOnAttributeMasterDTO.getAttributeKey() );
        permissionsOnAttributeMaster.setAttributeTitle( permissionsOnAttributeMasterDTO.getAttributeTitle() );

        return permissionsOnAttributeMaster;
    }

    protected void inPreferenceUserDTOToInPreferenceUser1(InPreferenceUserDTO inPreferenceUserDTO, InPreferenceUser mappingTarget) {
        if ( inPreferenceUserDTO == null ) {
            return;
        }

        if ( inPreferenceUserDTO.getId() != null ) {
            mappingTarget.setId( inPreferenceUserDTO.getId() );
        }
        if ( inPreferenceUserDTO.getFirstName() != null ) {
            mappingTarget.setFirstName( inPreferenceUserDTO.getFirstName() );
        }
        if ( inPreferenceUserDTO.getLastName() != null ) {
            mappingTarget.setLastName( inPreferenceUserDTO.getLastName() );
        }
        if ( inPreferenceUserDTO.getMobileNo() != null ) {
            mappingTarget.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        }
        if ( inPreferenceUserDTO.getProfileUrl() != null ) {
            mappingTarget.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        }
        if ( inPreferenceUserDTO.getUserType() != null ) {
            mappingTarget.setUserType( inPreferenceUserDTO.getUserType() );
        }
        if ( inPreferenceUserDTO.getEmailId() != null ) {
            mappingTarget.setEmailId( inPreferenceUserDTO.getEmailId() );
        }
        if ( inPreferenceUserDTO.getLoginId() != null ) {
            mappingTarget.setLoginId( inPreferenceUserDTO.getLoginId() );
        }
        if ( inPreferenceUserDTO.getSecret() != null ) {
            mappingTarget.setSecret( inPreferenceUserDTO.getSecret() );
        }
        if ( inPreferenceUserDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        }
        if ( inPreferenceUserDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        }
        if ( inPreferenceUserDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        }
        if ( inPreferenceUserDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        }
        if ( inPreferenceUserDTO.getStatus() != null ) {
            mappingTarget.setStatus( inPreferenceUserDTO.getStatus() );
        }
    }

    protected void roleMasterDTOToRoleMaster1(RoleMasterDTO roleMasterDTO, RoleMaster mappingTarget) {
        if ( roleMasterDTO == null ) {
            return;
        }

        if ( roleMasterDTO.getId() != null ) {
            mappingTarget.setId( roleMasterDTO.getId() );
        }
        if ( roleMasterDTO.getRoleName() != null ) {
            mappingTarget.setRoleName( roleMasterDTO.getRoleName() );
        }
        if ( roleMasterDTO.getDescription() != null ) {
            mappingTarget.setDescription( roleMasterDTO.getDescription() );
        }
        if ( roleMasterDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( roleMasterDTO.getCreatedDate() );
        }
        if ( roleMasterDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( roleMasterDTO.getCreatedBy() );
        }
        if ( roleMasterDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( roleMasterDTO.getUpdatedDate() );
        }
        if ( roleMasterDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( roleMasterDTO.getUpdatedBy() );
        }
        if ( roleMasterDTO.getStatus() != null ) {
            mappingTarget.setStatus( roleMasterDTO.getStatus() );
        }
        if ( roleMasterDTO.getInPreferenceUser() != null ) {
            if ( mappingTarget.getInPreferenceUser() == null ) {
                mappingTarget.inPreferenceUser( new InPreferenceUser() );
            }
            inPreferenceUserDTOToInPreferenceUser1( roleMasterDTO.getInPreferenceUser(), mappingTarget.getInPreferenceUser() );
        }
    }

    protected void permissionsOnAttributeMasterDTOToPermissionsOnAttributeMaster1(PermissionsOnAttributeMasterDTO permissionsOnAttributeMasterDTO, PermissionsOnAttributeMaster mappingTarget) {
        if ( permissionsOnAttributeMasterDTO == null ) {
            return;
        }

        if ( permissionsOnAttributeMasterDTO.getId() != null ) {
            mappingTarget.setId( permissionsOnAttributeMasterDTO.getId() );
        }
        if ( permissionsOnAttributeMasterDTO.getAttributeKey() != null ) {
            mappingTarget.setAttributeKey( permissionsOnAttributeMasterDTO.getAttributeKey() );
        }
        if ( permissionsOnAttributeMasterDTO.getAttributeTitle() != null ) {
            mappingTarget.setAttributeTitle( permissionsOnAttributeMasterDTO.getAttributeTitle() );
        }
    }
}
