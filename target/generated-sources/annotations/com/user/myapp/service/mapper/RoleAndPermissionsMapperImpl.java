package com.user.myapp.service.mapper;

import com.user.myapp.domain.RoleAndPermissions;
import com.user.myapp.service.dto.RoleAndPermissionsDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:36:00+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class RoleAndPermissionsMapperImpl implements RoleAndPermissionsMapper {

    @Override
    public RoleAndPermissions toEntity(RoleAndPermissionsDTO dto) {
        if ( dto == null ) {
            return null;
        }

        RoleAndPermissions roleAndPermissions = new RoleAndPermissions();

        roleAndPermissions.setId( dto.getId() );
        roleAndPermissions.setReadAccess( dto.getReadAccess() );
        roleAndPermissions.setCreateAccess( dto.getCreateAccess() );
        roleAndPermissions.setEditAccess( dto.getEditAccess() );
        roleAndPermissions.setDeleteAccess( dto.getDeleteAccess() );

        return roleAndPermissions;
    }

    @Override
    public RoleAndPermissionsDTO toDto(RoleAndPermissions entity) {
        if ( entity == null ) {
            return null;
        }

        RoleAndPermissionsDTO roleAndPermissionsDTO = new RoleAndPermissionsDTO();

        roleAndPermissionsDTO.setId( entity.getId() );
        roleAndPermissionsDTO.setReadAccess( entity.getReadAccess() );
        roleAndPermissionsDTO.setCreateAccess( entity.getCreateAccess() );
        roleAndPermissionsDTO.setEditAccess( entity.getEditAccess() );
        roleAndPermissionsDTO.setDeleteAccess( entity.getDeleteAccess() );

        return roleAndPermissionsDTO;
    }

    @Override
    public List<RoleAndPermissions> toEntity(List<RoleAndPermissionsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<RoleAndPermissions> list = new ArrayList<RoleAndPermissions>( dtoList.size() );
        for ( RoleAndPermissionsDTO roleAndPermissionsDTO : dtoList ) {
            list.add( toEntity( roleAndPermissionsDTO ) );
        }

        return list;
    }

    @Override
    public List<RoleAndPermissionsDTO> toDto(List<RoleAndPermissions> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<RoleAndPermissionsDTO> list = new ArrayList<RoleAndPermissionsDTO>( entityList.size() );
        for ( RoleAndPermissions roleAndPermissions : entityList ) {
            list.add( toDto( roleAndPermissions ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(RoleAndPermissions entity, RoleAndPermissionsDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getReadAccess() != null ) {
            entity.setReadAccess( dto.getReadAccess() );
        }
        if ( dto.getCreateAccess() != null ) {
            entity.setCreateAccess( dto.getCreateAccess() );
        }
        if ( dto.getEditAccess() != null ) {
            entity.setEditAccess( dto.getEditAccess() );
        }
        if ( dto.getDeleteAccess() != null ) {
            entity.setDeleteAccess( dto.getDeleteAccess() );
        }
    }
}
