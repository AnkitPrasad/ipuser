package com.user.myapp.service.mapper;

import com.user.myapp.domain.InPreferenceUser;
import com.user.myapp.domain.SuperAdmin;
import com.user.myapp.service.dto.InPreferenceUserDTO;
import com.user.myapp.service.dto.SuperAdminDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-03-16T16:35:59+0530",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.11 (Oracle Corporation)"
)
@Component
public class SuperAdminMapperImpl implements SuperAdminMapper {

    @Override
    public SuperAdmin toEntity(SuperAdminDTO dto) {
        if ( dto == null ) {
            return null;
        }

        SuperAdmin superAdmin = new SuperAdmin();

        superAdmin.setId( dto.getId() );
        superAdmin.setName( dto.getName() );
        superAdmin.setEmail( dto.getEmail() );
        superAdmin.inPreferenceUser( inPreferenceUserDTOToInPreferenceUser( dto.getInPreferenceUser() ) );

        return superAdmin;
    }

    @Override
    public List<SuperAdmin> toEntity(List<SuperAdminDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<SuperAdmin> list = new ArrayList<SuperAdmin>( dtoList.size() );
        for ( SuperAdminDTO superAdminDTO : dtoList ) {
            list.add( toEntity( superAdminDTO ) );
        }

        return list;
    }

    @Override
    public List<SuperAdminDTO> toDto(List<SuperAdmin> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<SuperAdminDTO> list = new ArrayList<SuperAdminDTO>( entityList.size() );
        for ( SuperAdmin superAdmin : entityList ) {
            list.add( toDto( superAdmin ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(SuperAdmin entity, SuperAdminDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getName() != null ) {
            entity.setName( dto.getName() );
        }
        if ( dto.getEmail() != null ) {
            entity.setEmail( dto.getEmail() );
        }
        if ( dto.getInPreferenceUser() != null ) {
            if ( entity.getInPreferenceUser() == null ) {
                entity.inPreferenceUser( new InPreferenceUser() );
            }
            inPreferenceUserDTOToInPreferenceUser1( dto.getInPreferenceUser(), entity.getInPreferenceUser() );
        }
    }

    @Override
    public SuperAdminDTO toDto(SuperAdmin s) {
        if ( s == null ) {
            return null;
        }

        SuperAdminDTO superAdminDTO = new SuperAdminDTO();

        superAdminDTO.setInPreferenceUser( toDtoInPreferenceUserId( s.getInPreferenceUser() ) );
        superAdminDTO.setId( s.getId() );
        superAdminDTO.setName( s.getName() );
        superAdminDTO.setEmail( s.getEmail() );

        return superAdminDTO;
    }

    @Override
    public InPreferenceUserDTO toDtoInPreferenceUserId(InPreferenceUser inPreferenceUser) {
        if ( inPreferenceUser == null ) {
            return null;
        }

        InPreferenceUserDTO inPreferenceUserDTO = new InPreferenceUserDTO();

        inPreferenceUserDTO.setId( inPreferenceUser.getId() );

        return inPreferenceUserDTO;
    }

    protected InPreferenceUser inPreferenceUserDTOToInPreferenceUser(InPreferenceUserDTO inPreferenceUserDTO) {
        if ( inPreferenceUserDTO == null ) {
            return null;
        }

        InPreferenceUser inPreferenceUser = new InPreferenceUser();

        inPreferenceUser.setId( inPreferenceUserDTO.getId() );
        inPreferenceUser.setFirstName( inPreferenceUserDTO.getFirstName() );
        inPreferenceUser.setLastName( inPreferenceUserDTO.getLastName() );
        inPreferenceUser.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        inPreferenceUser.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        inPreferenceUser.setUserType( inPreferenceUserDTO.getUserType() );
        inPreferenceUser.setEmailId( inPreferenceUserDTO.getEmailId() );
        inPreferenceUser.setLoginId( inPreferenceUserDTO.getLoginId() );
        inPreferenceUser.setSecret( inPreferenceUserDTO.getSecret() );
        inPreferenceUser.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        inPreferenceUser.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        inPreferenceUser.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        inPreferenceUser.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        inPreferenceUser.setStatus( inPreferenceUserDTO.getStatus() );

        return inPreferenceUser;
    }

    protected void inPreferenceUserDTOToInPreferenceUser1(InPreferenceUserDTO inPreferenceUserDTO, InPreferenceUser mappingTarget) {
        if ( inPreferenceUserDTO == null ) {
            return;
        }

        if ( inPreferenceUserDTO.getId() != null ) {
            mappingTarget.setId( inPreferenceUserDTO.getId() );
        }
        if ( inPreferenceUserDTO.getFirstName() != null ) {
            mappingTarget.setFirstName( inPreferenceUserDTO.getFirstName() );
        }
        if ( inPreferenceUserDTO.getLastName() != null ) {
            mappingTarget.setLastName( inPreferenceUserDTO.getLastName() );
        }
        if ( inPreferenceUserDTO.getMobileNo() != null ) {
            mappingTarget.setMobileNo( inPreferenceUserDTO.getMobileNo() );
        }
        if ( inPreferenceUserDTO.getProfileUrl() != null ) {
            mappingTarget.setProfileUrl( inPreferenceUserDTO.getProfileUrl() );
        }
        if ( inPreferenceUserDTO.getUserType() != null ) {
            mappingTarget.setUserType( inPreferenceUserDTO.getUserType() );
        }
        if ( inPreferenceUserDTO.getEmailId() != null ) {
            mappingTarget.setEmailId( inPreferenceUserDTO.getEmailId() );
        }
        if ( inPreferenceUserDTO.getLoginId() != null ) {
            mappingTarget.setLoginId( inPreferenceUserDTO.getLoginId() );
        }
        if ( inPreferenceUserDTO.getSecret() != null ) {
            mappingTarget.setSecret( inPreferenceUserDTO.getSecret() );
        }
        if ( inPreferenceUserDTO.getCreatedDate() != null ) {
            mappingTarget.setCreatedDate( inPreferenceUserDTO.getCreatedDate() );
        }
        if ( inPreferenceUserDTO.getCreatedBy() != null ) {
            mappingTarget.setCreatedBy( inPreferenceUserDTO.getCreatedBy() );
        }
        if ( inPreferenceUserDTO.getUpdatedDate() != null ) {
            mappingTarget.setUpdatedDate( inPreferenceUserDTO.getUpdatedDate() );
        }
        if ( inPreferenceUserDTO.getUpdatedBy() != null ) {
            mappingTarget.setUpdatedBy( inPreferenceUserDTO.getUpdatedBy() );
        }
        if ( inPreferenceUserDTO.getStatus() != null ) {
            mappingTarget.setStatus( inPreferenceUserDTO.getStatus() );
        }
    }
}
