package com.user.myapp.domain;

import java.time.Instant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RoleMaster.class)
public abstract class RoleMaster_ {

	public static volatile SetAttribute<RoleMaster, UserPermissionOnAttribute> userPermissionOnAttributes;
	public static volatile SingularAttribute<RoleMaster, Instant> createdDate;
	public static volatile SingularAttribute<RoleMaster, Long> updatedBy;
	public static volatile SingularAttribute<RoleMaster, Long> createdBy;
	public static volatile SingularAttribute<RoleMaster, String> roleName;
	public static volatile SingularAttribute<RoleMaster, String> description;
	public static volatile SingularAttribute<RoleMaster, Long> id;
	public static volatile SingularAttribute<RoleMaster, Instant> updatedDate;
	public static volatile SingularAttribute<RoleMaster, InPreferenceUser> inPreferenceUser;
	public static volatile SingularAttribute<RoleMaster, Boolean> status;
	public static volatile SetAttribute<RoleMaster, UserRolePermission> userRolePermissions;

	public static final String USER_PERMISSION_ON_ATTRIBUTES = "userPermissionOnAttributes";
	public static final String CREATED_DATE = "createdDate";
	public static final String UPDATED_BY = "updatedBy";
	public static final String CREATED_BY = "createdBy";
	public static final String ROLE_NAME = "roleName";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String UPDATED_DATE = "updatedDate";
	public static final String IN_PREFERENCE_USER = "inPreferenceUser";
	public static final String STATUS = "status";
	public static final String USER_ROLE_PERMISSIONS = "userRolePermissions";

}

