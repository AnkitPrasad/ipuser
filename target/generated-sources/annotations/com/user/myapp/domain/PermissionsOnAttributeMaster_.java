package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PermissionsOnAttributeMaster.class)
public abstract class PermissionsOnAttributeMaster_ {

	public static volatile SetAttribute<PermissionsOnAttributeMaster, UserPermissionOnAttribute> userPermissionOnAttributes;
	public static volatile SingularAttribute<PermissionsOnAttributeMaster, Long> id;
	public static volatile SingularAttribute<PermissionsOnAttributeMaster, String> attributeKey;
	public static volatile SingularAttribute<PermissionsOnAttributeMaster, String> attributeTitle;

	public static final String USER_PERMISSION_ON_ATTRIBUTES = "userPermissionOnAttributes";
	public static final String ID = "id";
	public static final String ATTRIBUTE_KEY = "attributeKey";
	public static final String ATTRIBUTE_TITLE = "attributeTitle";

}

