package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ClientSetting.class)
public abstract class ClientSetting_ {

	public static volatile SingularAttribute<ClientSetting, Integer> autoReninderAssessment;
	public static volatile SingularAttribute<ClientSetting, Integer> candidateSelfAssessment;
	public static volatile SingularAttribute<ClientSetting, String> companyThemeDarkColour;
	public static volatile SingularAttribute<ClientSetting, Boolean> allowEmployessPaidVersion;
	public static volatile SingularAttribute<ClientSetting, String> companyMenu;
	public static volatile SingularAttribute<ClientSetting, Boolean> allowCandidatePaidVersion;
	public static volatile SingularAttribute<ClientSetting, Integer> autoArchieveRequisitions;
	public static volatile SingularAttribute<ClientSetting, String> companyThemeLightColour;
	public static volatile SingularAttribute<ClientSetting, Long> id;
	public static volatile SingularAttribute<ClientSetting, String> companyUrl;

	public static final String AUTO_RENINDER_ASSESSMENT = "autoReninderAssessment";
	public static final String CANDIDATE_SELF_ASSESSMENT = "candidateSelfAssessment";
	public static final String COMPANY_THEME_DARK_COLOUR = "companyThemeDarkColour";
	public static final String ALLOW_EMPLOYESS_PAID_VERSION = "allowEmployessPaidVersion";
	public static final String COMPANY_MENU = "companyMenu";
	public static final String ALLOW_CANDIDATE_PAID_VERSION = "allowCandidatePaidVersion";
	public static final String AUTO_ARCHIEVE_REQUISITIONS = "autoArchieveRequisitions";
	public static final String COMPANY_THEME_LIGHT_COLOUR = "companyThemeLightColour";
	public static final String ID = "id";
	public static final String COMPANY_URL = "companyUrl";

}

