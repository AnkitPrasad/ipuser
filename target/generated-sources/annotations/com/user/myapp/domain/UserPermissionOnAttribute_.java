package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserPermissionOnAttribute.class)
public abstract class UserPermissionOnAttribute_ {

	public static volatile SingularAttribute<UserPermissionOnAttribute, String> attributeValue;
	public static volatile SingularAttribute<UserPermissionOnAttribute, Long> id;
	public static volatile SingularAttribute<UserPermissionOnAttribute, PermissionsOnAttributeMaster> permissionsOnAttributeMaster;
	public static volatile SingularAttribute<UserPermissionOnAttribute, RoleMaster> roleMaster;

	public static final String ATTRIBUTE_VALUE = "attributeValue";
	public static final String ID = "id";
	public static final String PERMISSIONS_ON_ATTRIBUTE_MASTER = "permissionsOnAttributeMaster";
	public static final String ROLE_MASTER = "roleMaster";

}

