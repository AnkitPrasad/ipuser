package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserEmails.class)
public abstract class UserEmails_ {

	public static volatile SingularAttribute<UserEmails, Long> loginId;
	public static volatile SingularAttribute<UserEmails, Long> id;
	public static volatile SingularAttribute<UserEmails, InPreferenceUser> inPreferenceUser;
	public static volatile SingularAttribute<UserEmails, String> email;

	public static final String LOGIN_ID = "loginId";
	public static final String ID = "id";
	public static final String IN_PREFERENCE_USER = "inPreferenceUser";
	public static final String EMAIL = "email";

}

