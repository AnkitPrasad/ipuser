package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PermissionMaster.class)
public abstract class PermissionMaster_ {

	public static volatile SingularAttribute<PermissionMaster, String> pageUrl;
	public static volatile SingularAttribute<PermissionMaster, Long> id;
	public static volatile SingularAttribute<PermissionMaster, String> pageName;
	public static volatile SetAttribute<PermissionMaster, UserRolePermission> userRolePermissions;

	public static final String PAGE_URL = "pageUrl";
	public static final String ID = "id";
	public static final String PAGE_NAME = "pageName";
	public static final String USER_ROLE_PERMISSIONS = "userRolePermissions";

}

