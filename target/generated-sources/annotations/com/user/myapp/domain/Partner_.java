package com.user.myapp.domain;

import com.user.myapp.domain.enumeration.Partnertype;
import java.time.Instant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Partner.class)
public abstract class Partner_ {

	public static volatile SingularAttribute<Partner, Instant> createdDate;
	public static volatile SingularAttribute<Partner, Long> updatedBy;
	public static volatile SingularAttribute<Partner, Long> createdBy;
	public static volatile SingularAttribute<Partner, String> domain;
	public static volatile SingularAttribute<Partner, Integer> valueC;
	public static volatile SingularAttribute<Partner, String> name;
	public static volatile SingularAttribute<Partner, Integer> valueM;
	public static volatile SingularAttribute<Partner, Long> id;
	public static volatile SingularAttribute<Partner, Double> partnerCommission;
	public static volatile SingularAttribute<Partner, Partnertype> partnerType;
	public static volatile SingularAttribute<Partner, Instant> updatedDate;
	public static volatile SingularAttribute<Partner, Boolean> status;

	public static final String CREATED_DATE = "createdDate";
	public static final String UPDATED_BY = "updatedBy";
	public static final String CREATED_BY = "createdBy";
	public static final String DOMAIN = "domain";
	public static final String VALUE_C = "valueC";
	public static final String NAME = "name";
	public static final String VALUE_M = "valueM";
	public static final String ID = "id";
	public static final String PARTNER_COMMISSION = "partnerCommission";
	public static final String PARTNER_TYPE = "partnerType";
	public static final String UPDATED_DATE = "updatedDate";
	public static final String STATUS = "status";

}

