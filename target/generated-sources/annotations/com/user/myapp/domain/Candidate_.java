package com.user.myapp.domain;

import java.time.Instant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Candidate.class)
public abstract class Candidate_ {

	public static volatile SingularAttribute<Candidate, Long> updatedBy;
	public static volatile SetAttribute<Candidate, CandidateProfile> candidateProfiles;
	public static volatile SingularAttribute<Candidate, CandidateDetails> candidateDetails;
	public static volatile SingularAttribute<Candidate, String> assessmentStatus;
	public static volatile SingularAttribute<Candidate, Instant> updatedDate;
	public static volatile SingularAttribute<Candidate, Instant> createdDate;
	public static volatile SingularAttribute<Candidate, Long> createdBy;
	public static volatile SingularAttribute<Candidate, Boolean> isInternalCandidate;
	public static volatile SingularAttribute<Candidate, Boolean> archiveStatus;
	public static volatile SingularAttribute<Candidate, Client> client;
	public static volatile SingularAttribute<Candidate, String> company;
	public static volatile SingularAttribute<Candidate, Long> id;
	public static volatile SingularAttribute<Candidate, Boolean> status;

	public static final String UPDATED_BY = "updatedBy";
	public static final String CANDIDATE_PROFILES = "candidateProfiles";
	public static final String CANDIDATE_DETAILS = "candidateDetails";
	public static final String ASSESSMENT_STATUS = "assessmentStatus";
	public static final String UPDATED_DATE = "updatedDate";
	public static final String CREATED_DATE = "createdDate";
	public static final String CREATED_BY = "createdBy";
	public static final String IS_INTERNAL_CANDIDATE = "isInternalCandidate";
	public static final String ARCHIVE_STATUS = "archiveStatus";
	public static final String CLIENT = "client";
	public static final String COMPANY = "company";
	public static final String ID = "id";
	public static final String STATUS = "status";

}

