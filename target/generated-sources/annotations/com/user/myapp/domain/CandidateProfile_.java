package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CandidateProfile.class)
public abstract class CandidateProfile_ {

	public static volatile SingularAttribute<CandidateProfile, String> currentEmployment;
	public static volatile SingularAttribute<CandidateProfile, Candidate> candidate;
	public static volatile SingularAttribute<CandidateProfile, String> education;
	public static volatile SingularAttribute<CandidateProfile, String> dob;
	public static volatile SingularAttribute<CandidateProfile, String> careerSummary;
	public static volatile SingularAttribute<CandidateProfile, Long> id;
	public static volatile SingularAttribute<CandidateProfile, String> certifications;
	public static volatile SingularAttribute<CandidateProfile, String> experience;
	public static volatile SingularAttribute<CandidateProfile, String> aboutMe;

	public static final String CURRENT_EMPLOYMENT = "currentEmployment";
	public static final String CANDIDATE = "candidate";
	public static final String EDUCATION = "education";
	public static final String DOB = "dob";
	public static final String CAREER_SUMMARY = "careerSummary";
	public static final String ID = "id";
	public static final String CERTIFICATIONS = "certifications";
	public static final String EXPERIENCE = "experience";
	public static final String ABOUT_ME = "aboutMe";

}

