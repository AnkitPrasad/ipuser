package com.user.myapp.domain;

import com.user.myapp.domain.enumeration.ClientType;
import java.time.Instant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Client.class)
public abstract class Client_ {

	public static volatile SingularAttribute<Client, Long> updatedBy;
	public static volatile SingularAttribute<Client, String> uploadClientLogo;
	public static volatile SingularAttribute<Client, String> gstNo;
	public static volatile SingularAttribute<Client, String> industry;
	public static volatile SingularAttribute<Client, Instant> updatedDate;
	public static volatile SingularAttribute<Client, ClientSetting> clientSetting;
	public static volatile SingularAttribute<Client, String> legalEntity;
	public static volatile SingularAttribute<Client, String> bankDetails;
	public static volatile SetAttribute<Client, Candidate> candidates;
	public static volatile SingularAttribute<Client, ClientType> clientType;
	public static volatile SingularAttribute<Client, Instant> createdDate;
	public static volatile SingularAttribute<Client, Long> createdBy;
	public static volatile SingularAttribute<Client, Long> id;
	public static volatile SingularAttribute<Client, String> pan;
	public static volatile SingularAttribute<Client, String> tnaNo;
	public static volatile SingularAttribute<Client, String> status;

	public static final String UPDATED_BY = "updatedBy";
	public static final String UPLOAD_CLIENT_LOGO = "uploadClientLogo";
	public static final String GST_NO = "gstNo";
	public static final String INDUSTRY = "industry";
	public static final String UPDATED_DATE = "updatedDate";
	public static final String CLIENT_SETTING = "clientSetting";
	public static final String LEGAL_ENTITY = "legalEntity";
	public static final String BANK_DETAILS = "bankDetails";
	public static final String CANDIDATES = "candidates";
	public static final String CLIENT_TYPE = "clientType";
	public static final String CREATED_DATE = "createdDate";
	public static final String CREATED_BY = "createdBy";
	public static final String ID = "id";
	public static final String PAN = "pan";
	public static final String TNA_NO = "tnaNo";
	public static final String STATUS = "status";

}

