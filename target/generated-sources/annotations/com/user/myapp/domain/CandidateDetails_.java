package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CandidateDetails.class)
public abstract class CandidateDetails_ {

	public static volatile SingularAttribute<CandidateDetails, String> candidateFunction;
	public static volatile SingularAttribute<CandidateDetails, String> businessUnits;
	public static volatile SingularAttribute<CandidateDetails, String> candidateCountry;
	public static volatile SingularAttribute<CandidateDetails, String> jobTitle;
	public static volatile SingularAttribute<CandidateDetails, String> candidateGrade;
	public static volatile SingularAttribute<CandidateDetails, String> location;
	public static volatile SingularAttribute<CandidateDetails, Long> id;
	public static volatile SingularAttribute<CandidateDetails, String> designation;
	public static volatile SingularAttribute<CandidateDetails, String> candidateSubFunction;
	public static volatile SingularAttribute<CandidateDetails, String> subBusinessUnits;
	public static volatile SingularAttribute<CandidateDetails, String> workLevel;
	public static volatile SingularAttribute<CandidateDetails, String> legalEntity;

	public static final String CANDIDATE_FUNCTION = "candidateFunction";
	public static final String BUSINESS_UNITS = "businessUnits";
	public static final String CANDIDATE_COUNTRY = "candidateCountry";
	public static final String JOB_TITLE = "jobTitle";
	public static final String CANDIDATE_GRADE = "candidateGrade";
	public static final String LOCATION = "location";
	public static final String ID = "id";
	public static final String DESIGNATION = "designation";
	public static final String CANDIDATE_SUB_FUNCTION = "candidateSubFunction";
	public static final String SUB_BUSINESS_UNITS = "subBusinessUnits";
	public static final String WORK_LEVEL = "workLevel";
	public static final String LEGAL_ENTITY = "legalEntity";

}

