package com.user.myapp.domain;

import com.user.myapp.domain.enumeration.Permission;
import java.time.Instant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InPreferenceUser.class)
public abstract class InPreferenceUser_ {

	public static volatile SingularAttribute<InPreferenceUser, String> profileUrl;
	public static volatile SingularAttribute<InPreferenceUser, String> lastName;
	public static volatile SetAttribute<InPreferenceUser, LoginCount> loginCounts;
	public static volatile SingularAttribute<InPreferenceUser, Long> loginId;
	public static volatile SingularAttribute<InPreferenceUser, Long> updatedBy;
	public static volatile SingularAttribute<InPreferenceUser, String> emailId;
	public static volatile SingularAttribute<InPreferenceUser, String> mobileNo;
	public static volatile SingularAttribute<InPreferenceUser, String> secret;
	public static volatile SingularAttribute<InPreferenceUser, Instant> updatedDate;
	public static volatile SetAttribute<InPreferenceUser, RoleMaster> roleMasters;
	public static volatile SetAttribute<InPreferenceUser, UserRolePermission> userRolePermissions;
	public static volatile SingularAttribute<InPreferenceUser, String> firstName;
	public static volatile SingularAttribute<InPreferenceUser, Instant> createdDate;
	public static volatile SingularAttribute<InPreferenceUser, Long> createdBy;
	public static volatile SetAttribute<InPreferenceUser, UserEmails> userEmails;
	public static volatile SingularAttribute<InPreferenceUser, Long> id;
	public static volatile SingularAttribute<InPreferenceUser, Permission> userType;
	public static volatile SingularAttribute<InPreferenceUser, Boolean> status;

	public static final String PROFILE_URL = "profileUrl";
	public static final String LAST_NAME = "lastName";
	public static final String LOGIN_COUNTS = "loginCounts";
	public static final String LOGIN_ID = "loginId";
	public static final String UPDATED_BY = "updatedBy";
	public static final String EMAIL_ID = "emailId";
	public static final String MOBILE_NO = "mobileNo";
	public static final String SECRET = "secret";
	public static final String UPDATED_DATE = "updatedDate";
	public static final String ROLE_MASTERS = "roleMasters";
	public static final String USER_ROLE_PERMISSIONS = "userRolePermissions";
	public static final String FIRST_NAME = "firstName";
	public static final String CREATED_DATE = "createdDate";
	public static final String CREATED_BY = "createdBy";
	public static final String USER_EMAILS = "userEmails";
	public static final String ID = "id";
	public static final String USER_TYPE = "userType";
	public static final String STATUS = "status";

}

