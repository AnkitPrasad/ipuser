package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserRolePermission.class)
public abstract class UserRolePermission_ {

	public static volatile SingularAttribute<UserRolePermission, Long> id;
	public static volatile SingularAttribute<UserRolePermission, PermissionMaster> permissionMaster;
	public static volatile SingularAttribute<UserRolePermission, InPreferenceUser> inPreferenceUser;
	public static volatile SingularAttribute<UserRolePermission, RoleMaster> roleMaster;

	public static final String ID = "id";
	public static final String PERMISSION_MASTER = "permissionMaster";
	public static final String IN_PREFERENCE_USER = "inPreferenceUser";
	public static final String ROLE_MASTER = "roleMaster";

}

