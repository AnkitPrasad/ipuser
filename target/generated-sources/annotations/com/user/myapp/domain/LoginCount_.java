package com.user.myapp.domain;

import java.time.Instant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LoginCount.class)
public abstract class LoginCount_ {

	public static volatile SingularAttribute<LoginCount, Instant> logintime;
	public static volatile SingularAttribute<LoginCount, Long> loginId;
	public static volatile SingularAttribute<LoginCount, Long> id;
	public static volatile SingularAttribute<LoginCount, InPreferenceUser> inPreferenceUser;

	public static final String LOGINTIME = "logintime";
	public static final String LOGIN_ID = "loginId";
	public static final String ID = "id";
	public static final String IN_PREFERENCE_USER = "inPreferenceUser";

}

