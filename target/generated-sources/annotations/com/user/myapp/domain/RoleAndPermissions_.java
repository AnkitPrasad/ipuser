package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RoleAndPermissions.class)
public abstract class RoleAndPermissions_ {

	public static volatile SingularAttribute<RoleAndPermissions, Boolean> editAccess;
	public static volatile SingularAttribute<RoleAndPermissions, Long> id;
	public static volatile SingularAttribute<RoleAndPermissions, Boolean> readAccess;
	public static volatile SingularAttribute<RoleAndPermissions, Boolean> deleteAccess;
	public static volatile SingularAttribute<RoleAndPermissions, Boolean> createAccess;

	public static final String EDIT_ACCESS = "editAccess";
	public static final String ID = "id";
	public static final String READ_ACCESS = "readAccess";
	public static final String DELETE_ACCESS = "deleteAccess";
	public static final String CREATE_ACCESS = "createAccess";

}

