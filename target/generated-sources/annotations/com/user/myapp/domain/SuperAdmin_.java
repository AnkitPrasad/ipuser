package com.user.myapp.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SuperAdmin.class)
public abstract class SuperAdmin_ {

	public static volatile SingularAttribute<SuperAdmin, String> name;
	public static volatile SingularAttribute<SuperAdmin, Long> id;
	public static volatile SingularAttribute<SuperAdmin, InPreferenceUser> inPreferenceUser;
	public static volatile SingularAttribute<SuperAdmin, String> email;

	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String IN_PREFERENCE_USER = "inPreferenceUser";
	public static final String EMAIL = "email";

}

